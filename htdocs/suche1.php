<?php
//Include Common Files @1-83C617B7
define("RelativePath", ".");
define("PathToCurrentPage", "/");
include(RelativePath . "/Common.php");
include(RelativePath . "/Template.php");
include(RelativePath . "/Sorter.php");
include(RelativePath . "/Navigator.php");
  
//End Include Common Files

class clsRecordkuenstlerSearch { //kuenstlerSearch Class @3-3F8C5B1C

//Variables @3-76058651

    // Public variables
    var $ComponentName;
    var $HTMLFormAction;
    var $PressedButton;
    var $Errors;
    var $ErrorBlock;
    var $FormSubmitted;
    var $FormEnctype;
    var $Visible;
    var $Recordset;

    var $CCSEvents = "";
    var $CCSEventResult;

    var $RelativePath = "";

    var $InsertAllowed = false;
    var $UpdateAllowed = false;
    var $DeleteAllowed = false;
    var $ReadAllowed   = false;
    var $EditMode      = false;
    var $ds;
    var $ValidatingControls;
    var $Controls;

    // Class variables
//End Variables

//Class_Initialize Event @3-A4253EEC
    function clsRecordkuenstlerSearch($RelativePath = "")
    {

        global $FileName;
        $this->Visible = true;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Record kuenstlerSearch/Error";
        $this->ReadAllowed = true;
        if($this->Visible)
        {
            $this->ComponentName = "kuenstlerSearch";
            $CCSForm = split(":", CCGetFromGet("ccsForm", ""), 2);
            if(sizeof($CCSForm) == 1)
                $CCSForm[1] = "";
            list($FormName, $FormMethod) = $CCSForm;
            $this->FormEnctype = "application/x-www-form-urlencoded";
            $this->FormSubmitted = ($FormName == $this->ComponentName);
            $Method = $this->FormSubmitted ? ccsPost : ccsGet;
            $this->s_Name = new clsControl(ccsTextBox, "s_Name", "s_Name", ccsText, "", CCGetRequestParam("s_Name", $Method));
            $this->s_Vorname = new clsControl(ccsTextBox, "s_Vorname", "s_Vorname", ccsText, "", CCGetRequestParam("s_Vorname", $Method));
            $this->s_Kategorie = new clsControl(ccsListBox, "s_Kategorie", "s_Kategorie", ccsText, "", CCGetRequestParam("s_Kategorie", $Method));
            $this->s_Kategorie->DSType = dsTable;
            list($this->s_Kategorie->BoundColumn, $this->s_Kategorie->TextColumn, $this->s_Kategorie->DBFormat) = array("Kategorie", "Kategorie", "");
            $this->s_Kategorie->ds = new clsDBConnection1();
            $this->s_Kategorie->ds->SQL = "SELECT *  " .
"FROM kategorie";
            $this->s_K2 = new clsControl(ccsListBox, "s_K2", "s_K2", ccsText, "", CCGetRequestParam("s_K2", $Method));
            $this->s_K2->DSType = dsTable;
            list($this->s_K2->BoundColumn, $this->s_K2->TextColumn, $this->s_K2->DBFormat) = array("Kategorie", "Kategorie", "");
            $this->s_K2->ds = new clsDBConnection1();
            $this->s_K2->ds->SQL = "SELECT *  " .
"FROM kategorie";
            $this->s_K3 = new clsControl(ccsListBox, "s_K3", "s_K3", ccsText, "", CCGetRequestParam("s_K3", $Method));
            $this->s_K3->DSType = dsTable;
            list($this->s_K3->BoundColumn, $this->s_K3->TextColumn, $this->s_K3->DBFormat) = array("Kategorie", "Kategorie", "");
            $this->s_K3->ds = new clsDBConnection1();
            $this->s_K3->ds->SQL = "SELECT *  " .
"FROM kategorie";
            $this->s_K4 = new clsControl(ccsListBox, "s_K4", "s_K4", ccsText, "", CCGetRequestParam("s_K4", $Method));
            $this->s_K4->DSType = dsTable;
            list($this->s_K4->BoundColumn, $this->s_K4->TextColumn, $this->s_K4->DBFormat) = array("Kategorie", "Kategorie", "");
            $this->s_K4->ds = new clsDBConnection1();
            $this->s_K4->ds->SQL = "SELECT *  " .
"FROM kategorie";
            $this->s_K5 = new clsControl(ccsListBox, "s_K5", "s_K5", ccsText, "", CCGetRequestParam("s_K5", $Method));
            $this->s_K5->DSType = dsTable;
            list($this->s_K5->BoundColumn, $this->s_K5->TextColumn, $this->s_K5->DBFormat) = array("Kategorie", "Kategorie", "");
            $this->s_K5->ds = new clsDBConnection1();
            $this->s_K5->ds->SQL = "SELECT *  " .
"FROM kategorie";
            $this->s_K6 = new clsControl(ccsListBox, "s_K6", "s_K6", ccsText, "", CCGetRequestParam("s_K6", $Method));
            $this->s_K6->DSType = dsTable;
            list($this->s_K6->BoundColumn, $this->s_K6->TextColumn, $this->s_K6->DBFormat) = array("Kategorie", "Kategorie", "");
            $this->s_K6->ds = new clsDBConnection1();
            $this->s_K6->ds->SQL = "SELECT *  " .
"FROM kategorie";
            $this->ClearParameters = new clsControl(ccsLink, "ClearParameters", "ClearParameters", ccsText, "", CCGetRequestParam("ClearParameters", $Method));
            $this->ClearParameters->Parameters = CCGetQueryString("QueryString", Array("s_Name", "s_Vorname", "s_Kategorie", "s_K2", "s_K3", "s_K4", "s_K5", "s_K6", "ccsForm"));
            $this->ClearParameters->Page = "suche1.php";
            $this->Button_DoSearch = new clsButton("Button_DoSearch");
        }
    }
//End Class_Initialize Event

//Validate Method @3-09C8DBF9
    function Validate()
    {
        $Validation = true;
        $Where = "";
        $Validation = ($this->s_Name->Validate() && $Validation);
        $Validation = ($this->s_Vorname->Validate() && $Validation);
        $Validation = ($this->s_Kategorie->Validate() && $Validation);
        $Validation = ($this->s_K2->Validate() && $Validation);
        $Validation = ($this->s_K3->Validate() && $Validation);
        $Validation = ($this->s_K4->Validate() && $Validation);
        $Validation = ($this->s_K5->Validate() && $Validation);
        $Validation = ($this->s_K6->Validate() && $Validation);
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "OnValidate");
        $Validation =  $Validation && ($this->s_Name->Errors->Count() == 0);
        $Validation =  $Validation && ($this->s_Vorname->Errors->Count() == 0);
        $Validation =  $Validation && ($this->s_Kategorie->Errors->Count() == 0);
        $Validation =  $Validation && ($this->s_K2->Errors->Count() == 0);
        $Validation =  $Validation && ($this->s_K3->Errors->Count() == 0);
        $Validation =  $Validation && ($this->s_K4->Errors->Count() == 0);
        $Validation =  $Validation && ($this->s_K5->Errors->Count() == 0);
        $Validation =  $Validation && ($this->s_K6->Errors->Count() == 0);
        return (($this->Errors->Count() == 0) && $Validation);
    }
//End Validate Method

//CheckErrors Method @3-B6D80DCD
    function CheckErrors()
    {
        $errors = false;
        $errors = ($errors || $this->s_Name->Errors->Count());
        $errors = ($errors || $this->s_Vorname->Errors->Count());
        $errors = ($errors || $this->s_Kategorie->Errors->Count());
        $errors = ($errors || $this->s_K2->Errors->Count());
        $errors = ($errors || $this->s_K3->Errors->Count());
        $errors = ($errors || $this->s_K4->Errors->Count());
        $errors = ($errors || $this->s_K5->Errors->Count());
        $errors = ($errors || $this->s_K6->Errors->Count());
        $errors = ($errors || $this->ClearParameters->Errors->Count());
        $errors = ($errors || $this->Errors->Count());
        return $errors;
    }
//End CheckErrors Method

//Operation Method @3-E4995ADB
    function Operation()
    {
        if(!$this->Visible)
            return;

        global $Redirect;
        global $FileName;

        if(!$this->FormSubmitted) {
            return;
        }

        if($this->FormSubmitted) {
            $this->PressedButton = "Button_DoSearch";
            if(strlen(CCGetParam("Button_DoSearch", ""))) {
                $this->PressedButton = "Button_DoSearch";
            }
        }
        $Redirect = "suche1.php";
        if($this->Validate()) {
            if($this->PressedButton == "Button_DoSearch") {
                if(!CCGetEvent($this->Button_DoSearch->CCSEvents, "OnClick")) {
                    $Redirect = "";
                } else {
                    $Redirect = "suche1.php" . "?" . CCMergeQueryStrings(CCGetQueryString("Form", Array("Button_DoSearch")));
                }
            }
        } else {
            $Redirect = "";
        }
    }
//End Operation Method

//Show Method @3-C53533A9
    function Show()
    {
        global $Tpl;
        global $FileName;
        $Error = "";

        if(!$this->Visible)
            return;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect");

        $this->s_Kategorie->Prepare();
        $this->s_K2->Prepare();
        $this->s_K3->Prepare();
        $this->s_K4->Prepare();
        $this->s_K5->Prepare();
        $this->s_K6->Prepare();

        $RecordBlock = "Record " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $RecordBlock;
        $this->EditMode = $this->EditMode && $this->ReadAllowed;
        if (!$this->FormSubmitted) {
        }

        if($this->FormSubmitted || $this->CheckErrors()) {
            $Error .= $this->s_Name->Errors->ToString();
            $Error .= $this->s_Vorname->Errors->ToString();
            $Error .= $this->s_Kategorie->Errors->ToString();
            $Error .= $this->s_K2->Errors->ToString();
            $Error .= $this->s_K3->Errors->ToString();
            $Error .= $this->s_K4->Errors->ToString();
            $Error .= $this->s_K5->Errors->ToString();
            $Error .= $this->s_K6->Errors->ToString();
            $Error .= $this->ClearParameters->Errors->ToString();
            $Error .= $this->Errors->ToString();
            $Tpl->SetVar("Error", $Error);
            $Tpl->Parse("Error", false);
        }
        $CCSForm = $this->EditMode ? $this->ComponentName . ":" . "Edit" : $this->ComponentName;
        $this->HTMLFormAction = $FileName . "?" . CCAddParam(CCGetQueryString("QueryString", ""), "ccsForm", $CCSForm);
        $Tpl->SetVar("Action", $this->HTMLFormAction);
        $Tpl->SetVar("HTMLFormName", $this->ComponentName);
        $Tpl->SetVar("HTMLFormEnctype", $this->FormEnctype);

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow");
        if(!$this->Visible) {
            $Tpl->block_path = $ParentPath;
            return;
        }

        $this->s_Name->Show();
        $this->s_Vorname->Show();
        $this->s_Kategorie->Show();
        $this->s_K2->Show();
        $this->s_K3->Show();
        $this->s_K4->Show();
        $this->s_K5->Show();
        $this->s_K6->Show();
        $this->ClearParameters->Show();
        $this->Button_DoSearch->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
    }
//End Show Method

} //End kuenstlerSearch Class @3-FCB6E20C

class clsGridkuenstler { //kuenstler class @2-83C783C9

//Variables @2-C4470DF4

    // Public variables
    var $ComponentName;
    var $Visible;
    var $Errors;
    var $ErrorBlock;
    var $ds; var $PageSize;
    var $SorterName = "";
    var $SorterDirection = "";
    var $PageNumber;

    var $CCSEvents = "";
    var $CCSEventResult;

    var $RelativePath = "";

    // Grid Controls
    var $StaticControls; var $RowControls;
    var $Sorter_Name;
    var $Sorter_Vorname;
    var $Navigator;
//End Variables

//Class_Initialize Event @2-D98504F2
    function clsGridkuenstler($RelativePath = "")
    {
        global $FileName;
        $this->ComponentName = "kuenstler";
        $this->Visible = True;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Grid kuenstler";
        $this->ds = new clskuenstlerDataSource();
        $this->PageSize = CCGetParam($this->ComponentName . "PageSize", "");
        if(!is_numeric($this->PageSize) || !strlen($this->PageSize))
            $this->PageSize = 10;
        else
            $this->PageSize = intval($this->PageSize);
        if ($this->PageSize > 100)
            $this->PageSize = 100;
        if($this->PageSize == 0)
            $this->Errors->addError("<p>Form: Grid " . $this->ComponentName . "<br>Error: (CCS06) Invalid page size.</p>");
        $this->PageNumber = intval(CCGetParam($this->ComponentName . "Page", 1));
        $this->SorterName = CCGetParam("kuenstlerOrder", "");
        $this->SorterDirection = CCGetParam("kuenstlerDir", "");

        $this->Name = new clsControl(ccsLink, "Name", "Name", ccsText, "", CCGetRequestParam("Name", ccsGet));
        $this->Vorname = new clsControl(ccsLink, "Vorname", "Vorname", ccsText, "", CCGetRequestParam("Vorname", ccsGet));
        $this->kuenstler_TotalRecords = new clsControl(ccsLabel, "kuenstler_TotalRecords", "kuenstler_TotalRecords", ccsText, "", CCGetRequestParam("kuenstler_TotalRecords", ccsGet));
        $this->Sorter_Name = new clsSorter($this->ComponentName, "Sorter_Name", $FileName);
        $this->Sorter_Vorname = new clsSorter($this->ComponentName, "Sorter_Vorname", $FileName);
        $this->Navigator = new clsNavigator($this->ComponentName, "Navigator", $FileName, 10, tpSimple);
    }
//End Class_Initialize Event

//Initialize Method @2-03626367
    function Initialize()
    {
        if(!$this->Visible) return;

        $this->ds->PageSize = $this->PageSize;
        $this->ds->AbsolutePage = $this->PageNumber;
        $this->ds->SetOrder($this->SorterName, $this->SorterDirection);
    }
//End Initialize Method

//Show Method @2-5B4F55FA
    function Show()
    {
        global $Tpl;
        if(!$this->Visible) return;

        $ShownRecords = 0;

        $this->ds->Parameters["expr29"] = 2;
        $this->ds->Parameters["urls_keyword"] = CCGetFromGet("s_keyword", "");

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect");


        $this->ds->Prepare();
        $this->ds->Open();

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow");
        if(!$this->Visible) return;

        $GridBlock = "Grid " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $GridBlock;


        $is_next_record = $this->ds->next_record();
        if($is_next_record && $ShownRecords < $this->PageSize)
        {
            do {
                    $this->ds->SetValues();
                $Tpl->block_path = $ParentPath . "/" . $GridBlock . "/Row";
                $this->Name->SetValue($this->ds->Name->GetValue());
                $this->Name->Parameters = CCGetQueryString("QueryString", Array("ccsForm"));
                $this->Name->Parameters = CCAddParam($this->Name->Parameters, "Kuenstler_ID", $this->ds->f("Kuenstler_ID"));
                $this->Name->Page = "kuenstler1.php";
                $this->Vorname->SetValue($this->ds->Vorname->GetValue());
                $this->Vorname->Parameters = CCGetQueryString("QueryString", Array("ccsForm"));
                $this->Vorname->Parameters = CCAddParam($this->Vorname->Parameters, "Kuenstler_ID", $this->ds->f("Kuenstler_ID"));
                $this->Vorname->Page = "kuenstler1.php";
                $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShowRow");
                $this->Name->Show();
                $this->Vorname->Show();
                $Tpl->block_path = $ParentPath . "/" . $GridBlock;
                $Tpl->parse("Row", true);
                $ShownRecords++;
                $is_next_record = $this->ds->next_record();
            } while ($is_next_record && $ShownRecords < $this->PageSize);
        }
        else // Show NoRecords block if no records are found
        {
            $Tpl->parse("NoRecords", false);
        }

        $errors = $this->GetErrors();
        if(strlen($errors))
        {
            $Tpl->replaceblock("", $errors);
            $Tpl->block_path = $ParentPath;
            return;
        }
        $this->Navigator->PageNumber = $this->ds->AbsolutePage;
        $this->Navigator->TotalPages = $this->ds->PageCount();
        $this->kuenstler_TotalRecords->Show();
        $this->Sorter_Name->Show();
        $this->Sorter_Vorname->Show();
        $this->Navigator->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
        $this->ds->close();
    }
//End Show Method

//GetErrors Method @2-976B23CC
    function GetErrors()
    {
        $errors = "";
        $errors .= $this->Name->Errors->ToString();
        $errors .= $this->Vorname->Errors->ToString();
        $errors .= $this->Errors->ToString();
        $errors .= $this->ds->Errors->ToString();
        return $errors;
    }
//End GetErrors Method

} //End kuenstler Class @2-FCB6E20C

class clskuenstlerDataSource extends clsDBConnection1 {  //kuenstlerDataSource Class @2-03C1C86E

//DataSource Variables @2-C6BC4A07
    var $CCSEvents = "";
    var $CCSEventResult;
    var $ErrorBlock;
    var $CmdExecution;

    var $CountSQL;
    var $wp;


    // Datasource fields
    var $Name;
    var $Vorname;
//End DataSource Variables

//DataSourceClass_Initialize Event @2-FC98DA0C
    function clskuenstlerDataSource()
    {
        $this->ErrorBlock = "Grid kuenstler";
        $this->Initialize();
        $this->Name = new clsField("Name", ccsText, "");
        $this->Vorname = new clsField("Vorname", ccsText, "");

    }
//End DataSourceClass_Initialize Event

//SetOrder Method @2-5D720E6A
    function SetOrder($SorterName, $SorterDirection)
    {
        $this->Order = "";
        $this->Order = CCGetOrder($this->Order, $SorterName, $SorterDirection, 
            array("Sorter_Name" => array("Name", ""), 
            "Sorter_Vorname" => array("Vorname", "")));
    }
//End SetOrder Method

//Prepare Method @2-67590212
    function Prepare()
    {
        $this->wp = new clsSQLParameters($this->ErrorBlock);
        $this->wp->AddParameter("1", "expr29", ccsText, "", "", $this->Parameters["expr29"], "", false);
        $this->wp->AddParameter("2", "urls_keyword", ccsText, "", "", $this->Parameters["urls_keyword"], "", false);
        $this->wp->AddParameter("3", "urls_keyword", ccsText, "", "", $this->Parameters["urls_keyword"], "", false);
        $this->wp->AddParameter("4", "urls_keyword", ccsText, "", "", $this->Parameters["urls_keyword"], "", false);
        $this->wp->AddParameter("5", "urls_keyword", ccsText, "", "", $this->Parameters["urls_keyword"], "", false);
        $this->wp->AddParameter("6", "urls_keyword", ccsText, "", "", $this->Parameters["urls_keyword"], "", false);
        $this->wp->AddParameter("7", "urls_keyword", ccsText, "", "", $this->Parameters["urls_keyword"], "", false);
        $this->wp->AddParameter("8", "urls_keyword", ccsText, "", "", $this->Parameters["urls_keyword"], "", false);
        $this->wp->AddParameter("9", "urls_keyword", ccsText, "", "", $this->Parameters["urls_keyword"], "", false);
        $this->wp->AddParameter("10", "urls_keyword", ccsText, "", "", $this->Parameters["urls_keyword"], "", false);
        $this->wp->AddParameter("11", "urls_keyword", ccsText, "", "", $this->Parameters["urls_keyword"], "", false);
        $this->wp->AddParameter("12", "urls_keyword", ccsText, "", "", $this->Parameters["urls_keyword"], "", false);
        $this->wp->Criterion[1] = $this->wp->Operation(opEqual, "`Group`", $this->wp->GetDBValue("1"), $this->ToSQL($this->wp->GetDBValue("1"), ccsText),false);
        $this->wp->Criterion[2] = $this->wp->Operation(opContains, "Name", $this->wp->GetDBValue("2"), $this->ToSQL($this->wp->GetDBValue("2"), ccsText),false);
        $this->wp->Criterion[3] = $this->wp->Operation(opContains, "Vorname", $this->wp->GetDBValue("3"), $this->ToSQL($this->wp->GetDBValue("3"), ccsText),false);
        $this->wp->Criterion[4] = $this->wp->Operation(opContains, "Kategorie", $this->wp->GetDBValue("4"), $this->ToSQL($this->wp->GetDBValue("4"), ccsText),false);
        $this->wp->Criterion[5] = $this->wp->Operation(opContains, "K2", $this->wp->GetDBValue("5"), $this->ToSQL($this->wp->GetDBValue("5"), ccsText),false);
        $this->wp->Criterion[6] = $this->wp->Operation(opContains, "K3", $this->wp->GetDBValue("6"), $this->ToSQL($this->wp->GetDBValue("6"), ccsText),false);
        $this->wp->Criterion[7] = $this->wp->Operation(opContains, "K4", $this->wp->GetDBValue("7"), $this->ToSQL($this->wp->GetDBValue("7"), ccsText),false);
        $this->wp->Criterion[8] = $this->wp->Operation(opContains, "K5", $this->wp->GetDBValue("8"), $this->ToSQL($this->wp->GetDBValue("8"), ccsText),false);
        $this->wp->Criterion[9] = $this->wp->Operation(opContains, "K6", $this->wp->GetDBValue("9"), $this->ToSQL($this->wp->GetDBValue("9"), ccsText),false);
        $this->wp->Criterion[10] = $this->wp->Operation(opContains, "K7", $this->wp->GetDBValue("10"), $this->ToSQL($this->wp->GetDBValue("10"), ccsText),false);
        $this->wp->Criterion[11] = $this->wp->Operation(opContains, "K8", $this->wp->GetDBValue("11"), $this->ToSQL($this->wp->GetDBValue("11"), ccsText),false);
        $this->wp->Criterion[12] = $this->wp->Operation(opContains, "K9", $this->wp->GetDBValue("12"), $this->ToSQL($this->wp->GetDBValue("12"), ccsText),false);
        $this->Where = $this->wp->opAND(
             false, 
             $this->wp->Criterion[1], $this->wp->opOR(
             true, $this->wp->opOR(
             false, $this->wp->opOR(
             false, $this->wp->opOR(
             false, $this->wp->opOR(
             false, $this->wp->opOR(
             false, $this->wp->opOR(
             false, $this->wp->opOR(
             false, $this->wp->opOR(
             false, $this->wp->opOR(
             false, 
             $this->wp->Criterion[2], 
             $this->wp->Criterion[3]), 
             $this->wp->Criterion[4]), 
             $this->wp->Criterion[5]), 
             $this->wp->Criterion[6]), 
             $this->wp->Criterion[7]), 
             $this->wp->Criterion[8]), 
             $this->wp->Criterion[9]), 
             $this->wp->Criterion[10]), 
             $this->wp->Criterion[11]), 
             $this->wp->Criterion[12]));
    }
//End Prepare Method

//Open Method @2-98ED8B0D
    function Open()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildSelect");
        $this->CountSQL = "SELECT COUNT(*)  " .
        "FROM kuenstler";
        $this->SQL = "SELECT *  " .
        "FROM kuenstler";
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteSelect");
        $this->RecordsCount = CCGetDBValue(CCBuildSQL($this->CountSQL, $this->Where, ""), $this);
        $this->query($this->OptimizeSQL(CCBuildSQL($this->SQL, $this->Where, $this->Order)));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteSelect");
    }
//End Open Method

//SetValues Method @2-609031A7
    function SetValues()
    {
        $this->Name->SetDBValue($this->f("Name"));
        $this->Vorname->SetDBValue($this->f("Vorname"));
    }
//End SetValues Method

} //End kuenstlerDataSource Class @2-FCB6E20C

class clsRecordkuenstlerSearch1 { //kuenstlerSearch1 Class @32-0B7D30A2

//Variables @32-76058651

    // Public variables
    var $ComponentName;
    var $HTMLFormAction;
    var $PressedButton;
    var $Errors;
    var $ErrorBlock;
    var $FormSubmitted;
    var $FormEnctype;
    var $Visible;
    var $Recordset;

    var $CCSEvents = "";
    var $CCSEventResult;

    var $RelativePath = "";

    var $InsertAllowed = false;
    var $UpdateAllowed = false;
    var $DeleteAllowed = false;
    var $ReadAllowed   = false;
    var $EditMode      = false;
    var $ds;
    var $ValidatingControls;
    var $Controls;

    // Class variables
//End Variables

//Class_Initialize Event @32-C016E4AC
    function clsRecordkuenstlerSearch1($RelativePath = "")
    {

        global $FileName;
        $this->Visible = true;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Record kuenstlerSearch1/Error";
        $this->ReadAllowed = true;
        if($this->Visible)
        {
            $this->ComponentName = "kuenstlerSearch1";
            $CCSForm = split(":", CCGetFromGet("ccsForm", ""), 2);
            if(sizeof($CCSForm) == 1)
                $CCSForm[1] = "";
            list($FormName, $FormMethod) = $CCSForm;
            $this->FormEnctype = "application/x-www-form-urlencoded";
            $this->FormSubmitted = ($FormName == $this->ComponentName);
            $Method = $this->FormSubmitted ? ccsPost : ccsGet;
            $this->s_keyword = new clsControl(ccsTextBox, "s_keyword", "s_keyword", ccsText, "", CCGetRequestParam("s_keyword", $Method));
            $this->ClearParameters = new clsControl(ccsLink, "ClearParameters", "ClearParameters", ccsText, "", CCGetRequestParam("ClearParameters", $Method));
            $this->ClearParameters->Parameters = CCGetQueryString("QueryString", Array("s_keyword", "ccsForm"));
            $this->ClearParameters->Page = "suche1.php";
            $this->Button_DoSearch = new clsButton("Button_DoSearch");
        }
    }
//End Class_Initialize Event

//Validate Method @32-C6CEFB72
    function Validate()
    {
        $Validation = true;
        $Where = "";
        $Validation = ($this->s_keyword->Validate() && $Validation);
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "OnValidate");
        $Validation =  $Validation && ($this->s_keyword->Errors->Count() == 0);
        return (($this->Errors->Count() == 0) && $Validation);
    }
//End Validate Method

//CheckErrors Method @32-B0E04A52
    function CheckErrors()
    {
        $errors = false;
        $errors = ($errors || $this->s_keyword->Errors->Count());
        $errors = ($errors || $this->ClearParameters->Errors->Count());
        $errors = ($errors || $this->Errors->Count());
        return $errors;
    }
//End CheckErrors Method

//Operation Method @32-BCA2F4E3
    function Operation()
    {
        if(!$this->Visible)
            return;

        global $Redirect;
        global $FileName;

        if(!$this->FormSubmitted) {
            return;
        }

        if($this->FormSubmitted) {
            $this->PressedButton = "Button_DoSearch";
            if(strlen(CCGetParam("Button_DoSearch", ""))) {
                $this->PressedButton = "Button_DoSearch";
            }
        }
        $Redirect = "kuenstler_OK.php";
        if($this->Validate()) {
            if($this->PressedButton == "Button_DoSearch") {
                if(!CCGetEvent($this->Button_DoSearch->CCSEvents, "OnClick")) {
                    $Redirect = "";
                } else {
                    $Redirect = "kuenstler_OK.php" . "?" . CCMergeQueryStrings(CCGetQueryString("Form", Array("Button_DoSearch")));
                }
            }
        } else {
            $Redirect = "";
        }
    }
//End Operation Method

//Show Method @32-E060C57D
    function Show()
    {
        global $Tpl;
        global $FileName;
        $Error = "";

        if(!$this->Visible)
            return;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect");


        $RecordBlock = "Record " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $RecordBlock;
        $this->EditMode = $this->EditMode && $this->ReadAllowed;
        if (!$this->FormSubmitted) {
        }

        if($this->FormSubmitted || $this->CheckErrors()) {
            $Error .= $this->s_keyword->Errors->ToString();
            $Error .= $this->ClearParameters->Errors->ToString();
            $Error .= $this->Errors->ToString();
            $Tpl->SetVar("Error", $Error);
            $Tpl->Parse("Error", false);
        }
        $CCSForm = $this->EditMode ? $this->ComponentName . ":" . "Edit" : $this->ComponentName;
        $this->HTMLFormAction = $FileName . "?" . CCAddParam(CCGetQueryString("QueryString", ""), "ccsForm", $CCSForm);
        $Tpl->SetVar("Action", $this->HTMLFormAction);
        $Tpl->SetVar("HTMLFormName", $this->ComponentName);
        $Tpl->SetVar("HTMLFormEnctype", $this->FormEnctype);

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow");
        if(!$this->Visible) {
            $Tpl->block_path = $ParentPath;
            return;
        }

        $this->s_keyword->Show();
        $this->ClearParameters->Show();
        $this->Button_DoSearch->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
    }
//End Show Method

} //End kuenstlerSearch1 Class @32-FCB6E20C





//Initialize Page @1-DDAFBE7B
// Variables
$FileName = "";
$Redirect = "";
$Tpl = "";
$TemplateFileName = "";
$BlockToParse = "main";
$ComponentName = "";

// Events;
$CCSEvents = "";
$CCSEventResult = "";

$FileName = "suche1.php";
$Redirect = "";
$TemplateFileName = "suche1.html";
$BlockToParse = "main";
$TemplateEncoding = "";
$FileEncoding = "";
$PathToRoot = "./";
//End Initialize Page

//Initialize Objects @1-0414C294
$DBConnection1 = new clsDBConnection1();

// Controls
$kuenstlerSearch = new clsRecordkuenstlerSearch();
$kuenstler = new clsGridkuenstler();
$kuenstlerSearch1 = new clsRecordkuenstlerSearch1();
$kuenstler->Initialize();

// Events
include("./suche1_events.php");
BindEvents();

$CCSEventResult = CCGetEvent($CCSEvents, "AfterInitialize");

$Charset = $Charset ? $Charset : $TemplateEncoding;
if ($Charset)
    header("Content-Type: text/html; charset=" . $Charset);
//End Initialize Objects

//Initialize HTML Template @1-E2A5B61F
$CCSEventResult = CCGetEvent($CCSEvents, "OnInitializeView");
$Tpl = new clsTemplate($FileEncoding, $TemplateEncoding);
$Tpl->LoadTemplate(PathToCurrentPage . $TemplateFileName, $BlockToParse, $TemplateEncoding);
$Tpl->block_path = "/$BlockToParse";
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeShow");
//End Initialize HTML Template

//Execute Components @1-32C0C78F
$kuenstlerSearch->Operation();
$kuenstlerSearch1->Operation();
//End Execute Components

//Go to destination page @1-1E8D3815
if($Redirect)
{
    $CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload");
    $DBConnection1->close();
    header("Location: " . $Redirect);
    unset($kuenstlerSearch);
    unset($kuenstler);
    unset($kuenstlerSearch1);
    unset($Tpl);
    exit;
}
//End Go to destination page

//Show Page @1-F9BAD5B3
$kuenstlerSearch->Show();
$kuenstler->Show();
$kuenstlerSearch1->Show();
$Tpl->block_path = "";
$Tpl->PParse("main", false);
//End Show Page

//Unload Page @1-411EEA98
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload");
$DBConnection1->close();
unset($kuenstlerSearch);
unset($kuenstler);
unset($kuenstlerSearch1);
unset($Tpl);
//End Unload Page


?>
