<?php
//Include Common Files @1-83C617B7
define("RelativePath", ".");
define("PathToCurrentPage", "/");
include(RelativePath . "/Common.php");
include(RelativePath . "/Template.php");
include(RelativePath . "/Sorter.php");
include(RelativePath . "/Navigator.php");
  
//End Include Common Files

class clsGridNewGrid1 { //NewGrid1 class @2-4958862A

//Variables @2-33F76C6B

    // Public variables
    var $ComponentName;
    var $Visible;
    var $Errors;
    var $ErrorBlock;
    var $ds; var $PageSize;
    var $SorterName = "";
    var $SorterDirection = "";
    var $PageNumber;

    var $CCSEvents = "";
    var $CCSEventResult;

    var $RelativePath = "";

    // Grid Controls
    var $StaticControls; var $RowControls;
//End Variables

//Class_Initialize Event @2-00D44AC9
    function clsGridNewGrid1($RelativePath = "")
    {
        global $FileName;
        $this->ComponentName = "NewGrid1";
        $this->Visible = True;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Grid NewGrid1";
        $this->ds = new clsNewGrid1DataSource();
        $this->PageSize = 20;
        if($this->PageSize == 0)
            $this->Errors->addError("<p>Form: Grid " . $this->ComponentName . "<br>Error: (CCS06) Invalid page size.</p>");
        $this->PageNumber = intval(CCGetParam($this->ComponentName . "Page", 1));

        $this->Datum = new clsControl(ccsLabel, "Datum", "Datum", ccsText, "", CCGetRequestParam("Datum", ccsGet));
        $this->Datum->HTML = true;
        $this->Titel = new clsControl(ccsLabel, "Titel", "Titel", ccsText, "", CCGetRequestParam("Titel", ccsGet));
        $this->Kurzbeschreibung = new clsControl(ccsLabel, "Kurzbeschreibung", "Kurzbeschreibung", ccsText, "", CCGetRequestParam("Kurzbeschreibung", ccsGet));
        $this->Kurzbeschreibung->HTML = true;
        $this->Beschreibung = new clsControl(ccsLabel, "Beschreibung", "Beschreibung", ccsText, "", CCGetRequestParam("Beschreibung", ccsGet));
        $this->Beschreibung->HTML = true;
        $this->Website = new clsControl(ccsLabel, "Website", "Website", ccsText, "", CCGetRequestParam("Website", ccsGet));
        $this->upload1 = new clsControl(ccsLabel, "upload1", "upload1", ccsText, "", CCGetRequestParam("upload1", ccsGet));
        $this->doc1 = new clsControl(ccsLabel, "doc1", "doc1", ccsText, "", CCGetRequestParam("doc1", ccsGet));
        $this->upload2 = new clsControl(ccsLabel, "upload2", "upload2", ccsText, "", CCGetRequestParam("upload2", ccsGet));
        $this->doc2 = new clsControl(ccsLabel, "doc2", "doc2", ccsText, "", CCGetRequestParam("doc2", ccsGet));
        $this->upload3 = new clsControl(ccsLabel, "upload3", "upload3", ccsText, "", CCGetRequestParam("upload3", ccsGet));
        $this->doc3 = new clsControl(ccsLabel, "doc3", "doc3", ccsText, "", CCGetRequestParam("doc3", ccsGet));
    }
//End Class_Initialize Event

//Initialize Method @2-03626367
    function Initialize()
    {
        if(!$this->Visible) return;

        $this->ds->PageSize = $this->PageSize;
        $this->ds->AbsolutePage = $this->PageNumber;
        $this->ds->SetOrder($this->SorterName, $this->SorterDirection);
    }
//End Initialize Method

//Show Method @2-7A630DA9
    function Show()
    {
        global $Tpl;
        if(!$this->Visible) return;

        $ShownRecords = 0;


        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect");


        $this->ds->Prepare();
        $this->ds->Open();

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow");
        if(!$this->Visible) return;

        $GridBlock = "Grid " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $GridBlock;


        $is_next_record = $this->ds->next_record();
        if($is_next_record && $ShownRecords < $this->PageSize)
        {
            do {
                    $this->ds->SetValues();
                $Tpl->block_path = $ParentPath . "/" . $GridBlock . "/Row";
                $this->Datum->SetValue($this->ds->Datum->GetValue());
                $this->Titel->SetValue($this->ds->Titel->GetValue());
                $this->Kurzbeschreibung->SetValue($this->ds->Kurzbeschreibung->GetValue());
                $this->Beschreibung->SetValue($this->ds->Beschreibung->GetValue());
                $this->Website->SetValue($this->ds->Website->GetValue());
                $this->upload1->SetValue($this->ds->upload1->GetValue());
                $this->doc1->SetValue($this->ds->doc1->GetValue());
                $this->upload2->SetValue($this->ds->upload2->GetValue());
                $this->doc2->SetValue($this->ds->doc2->GetValue());
                $this->upload3->SetValue($this->ds->upload3->GetValue());
                $this->doc3->SetValue($this->ds->doc3->GetValue());
                $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShowRow");
                $this->Datum->Show();
                $this->Titel->Show();
                $this->Kurzbeschreibung->Show();
                $this->Beschreibung->Show();
                $this->Website->Show();
                $this->upload1->Show();
                $this->doc1->Show();
                $this->upload2->Show();
                $this->doc2->Show();
                $this->upload3->Show();
                $this->doc3->Show();
                $Tpl->block_path = $ParentPath . "/" . $GridBlock;
                $Tpl->parse("Row", true);
                $ShownRecords++;
                $is_next_record = $this->ds->next_record();
            } while ($is_next_record && $ShownRecords < $this->PageSize);
        }
        else // Show NoRecords block if no records are found
        {
            $Tpl->parse("NoRecords", false);
        }

        $errors = $this->GetErrors();
        if(strlen($errors))
        {
            $Tpl->replaceblock("", $errors);
            $Tpl->block_path = $ParentPath;
            return;
        }
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
        $this->ds->close();
    }
//End Show Method

//GetErrors Method @2-BB3918BA
    function GetErrors()
    {
        $errors = "";
        $errors .= $this->Datum->Errors->ToString();
        $errors .= $this->Titel->Errors->ToString();
        $errors .= $this->Kurzbeschreibung->Errors->ToString();
        $errors .= $this->Beschreibung->Errors->ToString();
        $errors .= $this->Website->Errors->ToString();
        $errors .= $this->upload1->Errors->ToString();
        $errors .= $this->doc1->Errors->ToString();
        $errors .= $this->upload2->Errors->ToString();
        $errors .= $this->doc2->Errors->ToString();
        $errors .= $this->upload3->Errors->ToString();
        $errors .= $this->doc3->Errors->ToString();
        $errors .= $this->Errors->ToString();
        $errors .= $this->ds->Errors->ToString();
        return $errors;
    }
//End GetErrors Method

} //End NewGrid1 Class @2-FCB6E20C

class clsNewGrid1DataSource extends clsDBConnection1 {  //NewGrid1DataSource Class @2-57ECF20B

//DataSource Variables @2-E7A22D2C
    var $CCSEvents = "";
    var $CCSEventResult;
    var $ErrorBlock;
    var $CmdExecution;

    var $CountSQL;
    var $wp;


    // Datasource fields
    var $Datum;
    var $Titel;
    var $Kurzbeschreibung;
    var $Beschreibung;
    var $Website;
    var $upload1;
    var $doc1;
    var $upload2;
    var $doc2;
    var $upload3;
    var $doc3;
//End DataSource Variables

//DataSourceClass_Initialize Event @2-FBC3F88F
    function clsNewGrid1DataSource()
    {
        $this->ErrorBlock = "Grid NewGrid1";
        $this->Initialize();
        $this->Datum = new clsField("Datum", ccsText, "");
        $this->Titel = new clsField("Titel", ccsText, "");
        $this->Kurzbeschreibung = new clsField("Kurzbeschreibung", ccsText, "");
        $this->Beschreibung = new clsField("Beschreibung", ccsText, "");
        $this->Website = new clsField("Website", ccsText, "");
        $this->upload1 = new clsField("upload1", ccsText, "");
        $this->doc1 = new clsField("doc1", ccsText, "");
        $this->upload2 = new clsField("upload2", ccsText, "");
        $this->doc2 = new clsField("doc2", ccsText, "");
        $this->upload3 = new clsField("upload3", ccsText, "");
        $this->doc3 = new clsField("doc3", ccsText, "");

    }
//End DataSourceClass_Initialize Event

//SetOrder Method @2-4F4089A1
    function SetOrder($SorterName, $SorterDirection)
    {
        $this->Order = "Reihenfolge";
        $this->Order = CCGetOrder($this->Order, $SorterName, $SorterDirection, 
            "");
    }
//End SetOrder Method

//Prepare Method @2-DFF3DD87
    function Prepare()
    {
    }
//End Prepare Method

//Open Method @2-BC9907EC
    function Open()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildSelect");
        $this->CountSQL = "SELECT COUNT(*)  " .
        "FROM internes";
        $this->SQL = "SELECT *  " .
        "FROM internes";
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteSelect");
        $this->RecordsCount = CCGetDBValue(CCBuildSQL($this->CountSQL, $this->Where, ""), $this);
        $this->query($this->OptimizeSQL(CCBuildSQL($this->SQL, $this->Where, $this->Order)));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteSelect");
    }
//End Open Method

//SetValues Method @2-79E476CB
    function SetValues()
    {
        $this->Datum->SetDBValue($this->f("Datum"));
        $this->Titel->SetDBValue($this->f("Titel"));
        $this->Kurzbeschreibung->SetDBValue($this->f("Kurzbeschreibung"));
        $this->Beschreibung->SetDBValue($this->f("Beschreibung"));
        $this->Website->SetDBValue($this->f("Website"));
        $this->upload1->SetDBValue($this->f("upload1"));
        $this->doc1->SetDBValue($this->f("doc1"));
        $this->upload2->SetDBValue($this->f("upload2"));
        $this->doc2->SetDBValue($this->f("doc2"));
        $this->upload3->SetDBValue($this->f("upload3"));
        $this->doc3->SetDBValue($this->f("doc3"));
    }
//End SetValues Method

} //End NewGrid1DataSource Class @2-FCB6E20C





//Initialize Page @1-B2FA4BD7
// Variables
$FileName = "";
$Redirect = "";
$Tpl = "";
$TemplateFileName = "";
$BlockToParse = "main";
$ComponentName = "";

// Events;
$CCSEvents = "";
$CCSEventResult = "";

$FileName = "internes.php";
$Redirect = "";
$TemplateFileName = "internes.html";
$BlockToParse = "main";
$TemplateEncoding = "";
$FileEncoding = "";
$PathToRoot = "./";
//End Initialize Page

//Authenticate User @1-99E9115E
CCSecurityRedirect("0;1;3;4", "login_internes.php");
//End Authenticate User

//Initialize Objects @1-2200BAC9
$DBConnection1 = new clsDBConnection1();

// Controls
$NewGrid1 = new clsGridNewGrid1();
$NewGrid1->Initialize();

$CCSEventResult = CCGetEvent($CCSEvents, "AfterInitialize");

$Charset = $Charset ? $Charset : $TemplateEncoding;
if ($Charset)
    header("Content-Type: text/html; charset=" . $Charset);
//End Initialize Objects

//Initialize HTML Template @1-E2A5B61F
$CCSEventResult = CCGetEvent($CCSEvents, "OnInitializeView");
$Tpl = new clsTemplate($FileEncoding, $TemplateEncoding);
$Tpl->LoadTemplate(PathToCurrentPage . $TemplateFileName, $BlockToParse, $TemplateEncoding);
$Tpl->block_path = "/$BlockToParse";
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeShow");
//End Initialize HTML Template

//Go to destination page @1-1B29BAB2
if($Redirect)
{
    $CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload");
    $DBConnection1->close();
    header("Location: " . $Redirect);
    unset($NewGrid1);
    unset($Tpl);
    exit;
}
//End Go to destination page

//Show Page @1-A5FD5819
$NewGrid1->Show();
$Tpl->block_path = "";
$Tpl->PParse("main", false);
//End Show Page

//Unload Page @1-262D89C8
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload");
$DBConnection1->close();
unset($NewGrid1);
unset($Tpl);
//End Unload Page


?>
