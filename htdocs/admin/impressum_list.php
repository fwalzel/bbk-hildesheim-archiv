<?php
//Include Common Files @1-4EDE2568
define("RelativePath", "..");
define("PathToCurrentPage", "/admin/");
include(RelativePath . "/Common.php");
include(RelativePath . "/Template.php");
include(RelativePath . "/Sorter.php");
include(RelativePath . "/Navigator.php");
  
//End Include Common Files

//Include Page implementation @14-D18F3F45
include_once(RelativePath . "/admin/menu.php");
//End Include Page implementation

class clsGridimpressum { //impressum class @2-BAA6ADD9

//Variables @2-E6D70B86

    // Public variables
    var $ComponentName;
    var $Visible;
    var $Errors;
    var $ErrorBlock;
    var $ds; var $PageSize;
    var $SorterName = "";
    var $SorterDirection = "";
    var $PageNumber;

    var $CCSEvents = "";
    var $CCSEventResult;

    var $RelativePath = "";

    // Grid Controls
    var $StaticControls; var $RowControls;
    var $Sorter_impressum_ID;
    var $Sorter_Titel;
    var $Navigator;
//End Variables

//Class_Initialize Event @2-0346B296
    function clsGridimpressum($RelativePath = "")
    {
        global $FileName;
        $this->ComponentName = "impressum";
        $this->Visible = True;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Grid impressum";
        $this->ds = new clsimpressumDataSource();
        $this->PageSize = CCGetParam($this->ComponentName . "PageSize", "");
        if(!is_numeric($this->PageSize) || !strlen($this->PageSize))
            $this->PageSize = 20;
        else
            $this->PageSize = intval($this->PageSize);
        if ($this->PageSize > 100)
            $this->PageSize = 100;
        if($this->PageSize == 0)
            $this->Errors->addError("<p>Form: Grid " . $this->ComponentName . "<br>Error: (CCS06) Invalid page size.</p>");
        $this->PageNumber = intval(CCGetParam($this->ComponentName . "Page", 1));
        $this->SorterName = CCGetParam("impressumOrder", "");
        $this->SorterDirection = CCGetParam("impressumDir", "");

        $this->impressum_ID = new clsControl(ccsLink, "impressum_ID", "impressum_ID", ccsInteger, "", CCGetRequestParam("impressum_ID", ccsGet));
        $this->Titel = new clsControl(ccsLabel, "Titel", "Titel", ccsText, "", CCGetRequestParam("Titel", ccsGet));
        $this->Sorter_impressum_ID = new clsSorter($this->ComponentName, "Sorter_impressum_ID", $FileName);
        $this->Sorter_Titel = new clsSorter($this->ComponentName, "Sorter_Titel", $FileName);
        $this->impressum_Insert = new clsControl(ccsLink, "impressum_Insert", "impressum_Insert", ccsText, "", CCGetRequestParam("impressum_Insert", ccsGet));
        $this->impressum_Insert->Parameters = CCGetQueryString("QueryString", Array("impressum_ID", "ccsForm"));
        $this->impressum_Insert->Page = "impressum_maint.php";
        $this->Navigator = new clsNavigator($this->ComponentName, "Navigator", $FileName, 10, tpSimple);
    }
//End Class_Initialize Event

//Initialize Method @2-03626367
    function Initialize()
    {
        if(!$this->Visible) return;

        $this->ds->PageSize = $this->PageSize;
        $this->ds->AbsolutePage = $this->PageNumber;
        $this->ds->SetOrder($this->SorterName, $this->SorterDirection);
    }
//End Initialize Method

//Show Method @2-55CC989B
    function Show()
    {
        global $Tpl;
        if(!$this->Visible) return;

        $ShownRecords = 0;


        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect");


        $this->ds->Prepare();
        $this->ds->Open();

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow");
        if(!$this->Visible) return;

        $GridBlock = "Grid " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $GridBlock;


        $is_next_record = $this->ds->next_record();
        if($is_next_record && $ShownRecords < $this->PageSize)
        {
            do {
                    $this->ds->SetValues();
                $Tpl->block_path = $ParentPath . "/" . $GridBlock . "/Row";
                $this->impressum_ID->SetValue($this->ds->impressum_ID->GetValue());
                $this->impressum_ID->Parameters = CCGetQueryString("QueryString", Array("ccsForm"));
                $this->impressum_ID->Parameters = CCAddParam($this->impressum_ID->Parameters, "impressum_ID", $this->ds->f("impressum_ID"));
                $this->impressum_ID->Page = "impressum_maint.php";
                $this->Titel->SetValue($this->ds->Titel->GetValue());
                $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShowRow");
                $this->impressum_ID->Show();
                $this->Titel->Show();
                $Tpl->block_path = $ParentPath . "/" . $GridBlock;
                $Tpl->parse("Row", true);
                $ShownRecords++;
                $is_next_record = $this->ds->next_record();
            } while ($is_next_record && $ShownRecords < $this->PageSize);
        }
        else // Show NoRecords block if no records are found
        {
            $Tpl->parse("NoRecords", false);
        }

        $errors = $this->GetErrors();
        if(strlen($errors))
        {
            $Tpl->replaceblock("", $errors);
            $Tpl->block_path = $ParentPath;
            return;
        }
        $this->Navigator->PageNumber = $this->ds->AbsolutePage;
        $this->Navigator->TotalPages = $this->ds->PageCount();
        $this->Sorter_impressum_ID->Show();
        $this->Sorter_Titel->Show();
        $this->impressum_Insert->Show();
        $this->Navigator->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
        $this->ds->close();
    }
//End Show Method

//GetErrors Method @2-23AFB444
    function GetErrors()
    {
        $errors = "";
        $errors .= $this->impressum_ID->Errors->ToString();
        $errors .= $this->Titel->Errors->ToString();
        $errors .= $this->Errors->ToString();
        $errors .= $this->ds->Errors->ToString();
        return $errors;
    }
//End GetErrors Method

} //End impressum Class @2-FCB6E20C

class clsimpressumDataSource extends clsDBConnection1 {  //impressumDataSource Class @2-D9B24018

//DataSource Variables @2-F97ADFBA
    var $CCSEvents = "";
    var $CCSEventResult;
    var $ErrorBlock;
    var $CmdExecution;

    var $CountSQL;
    var $wp;


    // Datasource fields
    var $impressum_ID;
    var $Titel;
//End DataSource Variables

//DataSourceClass_Initialize Event @2-1A55BD94
    function clsimpressumDataSource()
    {
        $this->ErrorBlock = "Grid impressum";
        $this->Initialize();
        $this->impressum_ID = new clsField("impressum_ID", ccsInteger, "");
        $this->Titel = new clsField("Titel", ccsText, "");

    }
//End DataSourceClass_Initialize Event

//SetOrder Method @2-6AF98039
    function SetOrder($SorterName, $SorterDirection)
    {
        $this->Order = "";
        $this->Order = CCGetOrder($this->Order, $SorterName, $SorterDirection, 
            array("Sorter_impressum_ID" => array("impressum_ID", ""), 
            "Sorter_Titel" => array("Titel", "")));
    }
//End SetOrder Method

//Prepare Method @2-DFF3DD87
    function Prepare()
    {
    }
//End Prepare Method

//Open Method @2-2A446EF9
    function Open()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildSelect");
        $this->CountSQL = "SELECT COUNT(*)  " .
        "FROM impressum";
        $this->SQL = "SELECT impressum.impressum_ID, impressum.Titel  " .
        "FROM impressum";
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteSelect");
        $this->RecordsCount = CCGetDBValue(CCBuildSQL($this->CountSQL, $this->Where, ""), $this);
        $this->query($this->OptimizeSQL(CCBuildSQL($this->SQL, $this->Where, $this->Order)));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteSelect");
    }
//End Open Method

//SetValues Method @2-6C7D4D7F
    function SetValues()
    {
        $this->impressum_ID->SetDBValue(trim($this->f("impressum_ID")));
        $this->Titel->SetDBValue($this->f("Titel"));
    }
//End SetValues Method

} //End impressumDataSource Class @2-FCB6E20C

//Initialize Page @1-999AB8E6
// Variables
$FileName = "";
$Redirect = "";
$Tpl = "";
$TemplateFileName = "";
$BlockToParse = "main";
$ComponentName = "";

// Events;
$CCSEvents = "";
$CCSEventResult = "";

$FileName = "impressum_list.php";
$Redirect = "";
$TemplateFileName = "impressum_list.html";
$BlockToParse = "main";
$TemplateEncoding = "";
$FileEncoding = "";
$PathToRoot = "../";
//End Initialize Page

//Authenticate User @1-C7A40A44
CCSecurityRedirect("3;4", "../login.php");
//End Authenticate User

//Initialize Objects @1-B8D1A303
$DBConnection1 = new clsDBConnection1();

// Controls
$menu = new clsmenu("");
$menu->BindEvents();
$menu->Initialize();
$impressum = new clsGridimpressum();
$impressum->Initialize();

$CCSEventResult = CCGetEvent($CCSEvents, "AfterInitialize");

$Charset = $Charset ? $Charset : $TemplateEncoding;
if ($Charset)
    header("Content-Type: text/html; charset=" . $Charset);
//End Initialize Objects

//Initialize HTML Template @1-E2A5B61F
$CCSEventResult = CCGetEvent($CCSEvents, "OnInitializeView");
$Tpl = new clsTemplate($FileEncoding, $TemplateEncoding);
$Tpl->LoadTemplate(PathToCurrentPage . $TemplateFileName, $BlockToParse, $TemplateEncoding);
$Tpl->block_path = "/$BlockToParse";
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeShow");
//End Initialize HTML Template

//Execute Components @1-F2B7AC12
$menu->Operations();
//End Execute Components

//Go to destination page @1-99303A78
if($Redirect)
{
    $CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload");
    $DBConnection1->close();
    header("Location: " . $Redirect);
    $menu->Class_Terminate();
    unset($menu);
    unset($impressum);
    unset($Tpl);
    exit;
}
//End Go to destination page

//Show Page @1-2C6832E3
$menu->Show("menu");
$impressum->Show();
$Tpl->block_path = "";
$Tpl->PParse("main", false);
//End Show Page

//Unload Page @1-11054909
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload");
$DBConnection1->close();
$menu->Class_Terminate();
unset($menu);
unset($impressum);
unset($Tpl);
//End Unload Page


?>
