<?php
//Include Common Files @1-4EDE2568
define("RelativePath", "..");
define("PathToCurrentPage", "/admin/");
include(RelativePath . "/Common.php");
include(RelativePath . "/Template.php");
include(RelativePath . "/Sorter.php");
include(RelativePath . "/Navigator.php");
  
//End Include Common Files

//Include Page implementation @20-D18F3F45
include_once(RelativePath . "/admin/menu.php");
//End Include Page implementation

class clsGridlogin { //login class @2-C868A64F

//Variables @2-A9B8FBD2

    // Public variables
    var $ComponentName;
    var $Visible;
    var $Errors;
    var $ErrorBlock;
    var $ds; var $PageSize;
    var $SorterName = "";
    var $SorterDirection = "";
    var $PageNumber;

    var $CCSEvents = "";
    var $CCSEventResult;

    var $RelativePath = "";

    // Grid Controls
    var $StaticControls; var $RowControls;
    var $Sorter_Login_ID;
    var $Sorter_Login;
    var $Sorter_Passwort;
    var $Sorter_Group;
    var $Navigator;
//End Variables

//Class_Initialize Event @2-38308878
    function clsGridlogin($RelativePath = "")
    {
        global $FileName;
        $this->ComponentName = "login";
        $this->Visible = True;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Grid login";
        $this->ds = new clsloginDataSource();
        $this->PageSize = CCGetParam($this->ComponentName . "PageSize", "");
        if(!is_numeric($this->PageSize) || !strlen($this->PageSize))
            $this->PageSize = 20;
        else
            $this->PageSize = intval($this->PageSize);
        if ($this->PageSize > 100)
            $this->PageSize = 100;
        if($this->PageSize == 0)
            $this->Errors->addError("<p>Form: Grid " . $this->ComponentName . "<br>Error: (CCS06) Invalid page size.</p>");
        $this->PageNumber = intval(CCGetParam($this->ComponentName . "Page", 1));
        $this->SorterName = CCGetParam("loginOrder", "");
        $this->SorterDirection = CCGetParam("loginDir", "");

        $this->Login_ID = new clsControl(ccsLink, "Login_ID", "Login_ID", ccsInteger, "", CCGetRequestParam("Login_ID", ccsGet));
        $this->Login = new clsControl(ccsLabel, "Login", "Login", ccsText, "", CCGetRequestParam("Login", ccsGet));
        $this->Passwort = new clsControl(ccsLabel, "Passwort", "Passwort", ccsText, "", CCGetRequestParam("Passwort", ccsGet));
        $this->Group = new clsControl(ccsLabel, "Group", "Group", ccsInteger, "", CCGetRequestParam("Group", ccsGet));
        $this->Sorter_Login_ID = new clsSorter($this->ComponentName, "Sorter_Login_ID", $FileName);
        $this->Sorter_Login = new clsSorter($this->ComponentName, "Sorter_Login", $FileName);
        $this->Sorter_Passwort = new clsSorter($this->ComponentName, "Sorter_Passwort", $FileName);
        $this->Sorter_Group = new clsSorter($this->ComponentName, "Sorter_Group", $FileName);
        $this->login_Insert = new clsControl(ccsLink, "login_Insert", "login_Insert", ccsText, "", CCGetRequestParam("login_Insert", ccsGet));
        $this->login_Insert->Parameters = CCGetQueryString("QueryString", Array("Login_ID", "ccsForm"));
        $this->login_Insert->Page = "login_maint.php";
        $this->Navigator = new clsNavigator($this->ComponentName, "Navigator", $FileName, 10, tpSimple);
    }
//End Class_Initialize Event

//Initialize Method @2-03626367
    function Initialize()
    {
        if(!$this->Visible) return;

        $this->ds->PageSize = $this->PageSize;
        $this->ds->AbsolutePage = $this->PageNumber;
        $this->ds->SetOrder($this->SorterName, $this->SorterDirection);
    }
//End Initialize Method

//Show Method @2-E3C6D69F
    function Show()
    {
        global $Tpl;
        if(!$this->Visible) return;

        $ShownRecords = 0;


        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect");


        $this->ds->Prepare();
        $this->ds->Open();

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow");
        if(!$this->Visible) return;

        $GridBlock = "Grid " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $GridBlock;


        $is_next_record = $this->ds->next_record();
        if($is_next_record && $ShownRecords < $this->PageSize)
        {
            do {
                    $this->ds->SetValues();
                $Tpl->block_path = $ParentPath . "/" . $GridBlock . "/Row";
                $this->Login_ID->SetValue($this->ds->Login_ID->GetValue());
                $this->Login_ID->Parameters = CCGetQueryString("QueryString", Array("ccsForm"));
                $this->Login_ID->Parameters = CCAddParam($this->Login_ID->Parameters, "Login_ID", $this->ds->f("Login_ID"));
                $this->Login_ID->Page = "login_maint.php";
                $this->Login->SetValue($this->ds->Login->GetValue());
                $this->Passwort->SetValue($this->ds->Passwort->GetValue());
                $this->Group->SetValue($this->ds->Group->GetValue());
                $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShowRow");
                $this->Login_ID->Show();
                $this->Login->Show();
                $this->Passwort->Show();
                $this->Group->Show();
                $Tpl->block_path = $ParentPath . "/" . $GridBlock;
                $Tpl->parse("Row", true);
                $ShownRecords++;
                $is_next_record = $this->ds->next_record();
            } while ($is_next_record && $ShownRecords < $this->PageSize);
        }
        else // Show NoRecords block if no records are found
        {
            $Tpl->parse("NoRecords", false);
        }

        $errors = $this->GetErrors();
        if(strlen($errors))
        {
            $Tpl->replaceblock("", $errors);
            $Tpl->block_path = $ParentPath;
            return;
        }
        $this->Navigator->PageNumber = $this->ds->AbsolutePage;
        $this->Navigator->TotalPages = $this->ds->PageCount();
        $this->Sorter_Login_ID->Show();
        $this->Sorter_Login->Show();
        $this->Sorter_Passwort->Show();
        $this->Sorter_Group->Show();
        $this->login_Insert->Show();
        $this->Navigator->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
        $this->ds->close();
    }
//End Show Method

//GetErrors Method @2-8225C715
    function GetErrors()
    {
        $errors = "";
        $errors .= $this->Login_ID->Errors->ToString();
        $errors .= $this->Login->Errors->ToString();
        $errors .= $this->Passwort->Errors->ToString();
        $errors .= $this->Group->Errors->ToString();
        $errors .= $this->Errors->ToString();
        $errors .= $this->ds->Errors->ToString();
        return $errors;
    }
//End GetErrors Method

} //End login Class @2-FCB6E20C

class clsloginDataSource extends clsDBConnection1 {  //loginDataSource Class @2-0FA025BE

//DataSource Variables @2-2471A555
    var $CCSEvents = "";
    var $CCSEventResult;
    var $ErrorBlock;
    var $CmdExecution;

    var $CountSQL;
    var $wp;


    // Datasource fields
    var $Login_ID;
    var $Login;
    var $Passwort;
    var $Group;
//End DataSource Variables

//DataSourceClass_Initialize Event @2-A5D0C8D9
    function clsloginDataSource()
    {
        $this->ErrorBlock = "Grid login";
        $this->Initialize();
        $this->Login_ID = new clsField("Login_ID", ccsInteger, "");
        $this->Login = new clsField("Login", ccsText, "");
        $this->Passwort = new clsField("Passwort", ccsText, "");
        $this->Group = new clsField("Group", ccsInteger, "");

    }
//End DataSourceClass_Initialize Event

//SetOrder Method @2-93957FD1
    function SetOrder($SorterName, $SorterDirection)
    {
        $this->Order = "";
        $this->Order = CCGetOrder($this->Order, $SorterName, $SorterDirection, 
            array("Sorter_Login_ID" => array("Login_ID", ""), 
            "Sorter_Login" => array("Login", ""), 
            "Sorter_Passwort" => array("Passwort", ""), 
            "Sorter_Group" => array("Group", "")));
    }
//End SetOrder Method

//Prepare Method @2-DFF3DD87
    function Prepare()
    {
    }
//End Prepare Method

//Open Method @2-CD6365FD
    function Open()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildSelect");
        $this->CountSQL = "SELECT COUNT(*)  " .
        "FROM login";
        $this->SQL = "SELECT login.Login_ID, login.Login, login.Passwort, login.`Group`  " .
        "FROM login";
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteSelect");
        $this->RecordsCount = CCGetDBValue(CCBuildSQL($this->CountSQL, $this->Where, ""), $this);
        $this->query($this->OptimizeSQL(CCBuildSQL($this->SQL, $this->Where, $this->Order)));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteSelect");
    }
//End Open Method

//SetValues Method @2-BF36F0BA
    function SetValues()
    {
        $this->Login_ID->SetDBValue(trim($this->f("Login_ID")));
        $this->Login->SetDBValue($this->f("Login"));
        $this->Passwort->SetDBValue($this->f("Passwort"));
        $this->Group->SetDBValue(trim($this->f("Group")));
    }
//End SetValues Method

} //End loginDataSource Class @2-FCB6E20C

//Initialize Page @1-6EC6F51E
// Variables
$FileName = "";
$Redirect = "";
$Tpl = "";
$TemplateFileName = "";
$BlockToParse = "main";
$ComponentName = "";

// Events;
$CCSEvents = "";
$CCSEventResult = "";

$FileName = "login_list.php";
$Redirect = "";
$TemplateFileName = "login_list.html";
$BlockToParse = "main";
$TemplateEncoding = "";
$FileEncoding = "";
$PathToRoot = "../";
//End Initialize Page

//Authenticate User @1-C7A40A44
CCSecurityRedirect("3;4", "../login.php");
//End Authenticate User

//Initialize Objects @1-861BCD12
$DBConnection1 = new clsDBConnection1();

// Controls
$menu = new clsmenu("");
$menu->BindEvents();
$menu->Initialize();
$login = new clsGridlogin();
$login->Initialize();

$CCSEventResult = CCGetEvent($CCSEvents, "AfterInitialize");

$Charset = $Charset ? $Charset : $TemplateEncoding;
if ($Charset)
    header("Content-Type: text/html; charset=" . $Charset);
//End Initialize Objects

//Initialize HTML Template @1-E2A5B61F
$CCSEventResult = CCGetEvent($CCSEvents, "OnInitializeView");
$Tpl = new clsTemplate($FileEncoding, $TemplateEncoding);
$Tpl->LoadTemplate(PathToCurrentPage . $TemplateFileName, $BlockToParse, $TemplateEncoding);
$Tpl->block_path = "/$BlockToParse";
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeShow");
//End Initialize HTML Template

//Execute Components @1-F2B7AC12
$menu->Operations();
//End Execute Components

//Go to destination page @1-AC0BC674
if($Redirect)
{
    $CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload");
    $DBConnection1->close();
    header("Location: " . $Redirect);
    $menu->Class_Terminate();
    unset($menu);
    unset($login);
    unset($Tpl);
    exit;
}
//End Go to destination page

//Show Page @1-C2FE835D
$menu->Show("menu");
$login->Show();
$Tpl->block_path = "";
$Tpl->PParse("main", false);
//End Show Page

//Unload Page @1-A41C9D7B
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload");
$DBConnection1->close();
$menu->Class_Terminate();
unset($menu);
unset($login);
unset($Tpl);
//End Unload Page


?>
