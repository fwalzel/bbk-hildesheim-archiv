<?php
//Include Common Files @1-4EDE2568
define("RelativePath", "..");
define("PathToCurrentPage", "/admin/");
include(RelativePath . "/Common.php");
include(RelativePath . "/Template.php");
include(RelativePath . "/Sorter.php");
include(RelativePath . "/Navigator.php");
  
//End Include Common Files

//Include Page implementation @21-D18F3F45
include_once(RelativePath . "/admin/menu.php");
//End Include Page implementation

class clsRecordkuenstler { //kuenstler Class @2-3DFCF17F

//Variables @2-76058651

    // Public variables
    var $ComponentName;
    var $HTMLFormAction;
    var $PressedButton;
    var $Errors;
    var $ErrorBlock;
    var $FormSubmitted;
    var $FormEnctype;
    var $Visible;
    var $Recordset;

    var $CCSEvents = "";
    var $CCSEventResult;

    var $RelativePath = "";

    var $InsertAllowed = false;
    var $UpdateAllowed = false;
    var $DeleteAllowed = false;
    var $ReadAllowed   = false;
    var $EditMode      = false;
    var $ds;
    var $ValidatingControls;
    var $Controls;

    // Class variables
//End Variables

//Class_Initialize Event @2-BD28A756
    function clsRecordkuenstler($RelativePath = "")
    {

        global $FileName;
        $this->Visible = true;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Record kuenstler/Error";
        $this->ds = new clskuenstlerDataSource();
        $this->InsertAllowed = true;
        $this->UpdateAllowed = true;
        $this->DeleteAllowed = true;
        $this->ReadAllowed = true;
        if($this->Visible)
        {
            $this->ComponentName = "kuenstler";
            $CCSForm = split(":", CCGetFromGet("ccsForm", ""), 2);
            if(sizeof($CCSForm) == 1)
                $CCSForm[1] = "";
            list($FormName, $FormMethod) = $CCSForm;
            $this->EditMode = ($FormMethod == "Edit");
            $this->FormEnctype = "multipart/form-data";
            $this->FormSubmitted = ($FormName == $this->ComponentName);
            $Method = $this->FormSubmitted ? ccsPost : ccsGet;
            $this->Name = new clsControl(ccsTextBox, "Name", "Name", ccsText, "", CCGetRequestParam("Name", $Method));
            $this->Name->Required = true;
            $this->Vorname = new clsControl(ccsTextBox, "Vorname", "Vorname", ccsText, "", CCGetRequestParam("Vorname", $Method));
            $this->Adresse = new clsControl(ccsTextBox, "Adresse", "Adresse", ccsText, "", CCGetRequestParam("Adresse", $Method));
            $this->PLZ = new clsControl(ccsTextBox, "PLZ", "PLZ", ccsText, "", CCGetRequestParam("PLZ", $Method));
            $this->Stadt = new clsControl(ccsTextBox, "Stadt", "Stadt", ccsText, "", CCGetRequestParam("Stadt", $Method));
            $this->Telefon = new clsControl(ccsTextBox, "Telefon", "Telefon", ccsText, "", CCGetRequestParam("Telefon", $Method));
            $this->email = new clsControl(ccsTextBox, "email", "E Mail", ccsText, "", CCGetRequestParam("email", $Method));
            $this->Webseite = new clsControl(ccsTextBox, "Webseite", "Webseite", ccsText, "", CCGetRequestParam("Webseite", $Method));
            $this->Foto = new clsControl(ccsImage, "Foto", "Foto", ccsText, "", CCGetRequestParam("Foto", $Method));
            $this->FileUpload1 = new clsFileUpload("FileUpload1", "FileUpload1", "../TEMP/", "../fotos/", "*.jpg;*.jpeg;*.gif", "", 100000);
            $this->FileUpload1->Required = true;
            $this->Vita = new clsControl(ccsTextArea, "Vita", "Vita", ccsMemo, "", CCGetRequestParam("Vita", $Method));
            $this->Kategorie = new clsControl(ccsListBox, "Kategorie", "Kategorie", ccsText, "", CCGetRequestParam("Kategorie", $Method));
            $this->Kategorie->DSType = dsTable;
            list($this->Kategorie->BoundColumn, $this->Kategorie->TextColumn, $this->Kategorie->DBFormat) = array("Kategorie", "Kategorie", "");
            $this->Kategorie->ds = new clsDBConnection1();
            $this->Kategorie->ds->SQL = "SELECT *  " .
"FROM kategorie";
            $this->Kategorie->Required = true;
            $this->Kategorie2 = new clsControl(ccsListBox, "Kategorie2", "Kategorie", ccsText, "", CCGetRequestParam("Kategorie2", $Method));
            $this->Kategorie2->DSType = dsTable;
            list($this->Kategorie2->BoundColumn, $this->Kategorie2->TextColumn, $this->Kategorie2->DBFormat) = array("Kategorie", "Kategorie", "");
            $this->Kategorie2->ds = new clsDBConnection1();
            $this->Kategorie2->ds->SQL = "SELECT *  " .
"FROM kategorie";
            $this->Kategorie3 = new clsControl(ccsListBox, "Kategorie3", "Kategorie", ccsText, "", CCGetRequestParam("Kategorie3", $Method));
            $this->Kategorie3->DSType = dsTable;
            list($this->Kategorie3->BoundColumn, $this->Kategorie3->TextColumn, $this->Kategorie3->DBFormat) = array("Kategorie", "Kategorie", "");
            $this->Kategorie3->ds = new clsDBConnection1();
            $this->Kategorie3->ds->SQL = "SELECT *  " .
"FROM kategorie";
            $this->Kategorie4 = new clsControl(ccsListBox, "Kategorie4", "Kategorie", ccsText, "", CCGetRequestParam("Kategorie4", $Method));
            $this->Kategorie4->DSType = dsTable;
            list($this->Kategorie4->BoundColumn, $this->Kategorie4->TextColumn, $this->Kategorie4->DBFormat) = array("Kategorie", "Kategorie", "");
            $this->Kategorie4->ds = new clsDBConnection1();
            $this->Kategorie4->ds->SQL = "SELECT *  " .
"FROM kategorie";
            $this->Kategorie5 = new clsControl(ccsListBox, "Kategorie5", "Kategorie", ccsText, "", CCGetRequestParam("Kategorie5", $Method));
            $this->Kategorie5->DSType = dsTable;
            list($this->Kategorie5->BoundColumn, $this->Kategorie5->TextColumn, $this->Kategorie5->DBFormat) = array("Kategorie", "Kategorie", "");
            $this->Kategorie5->ds = new clsDBConnection1();
            $this->Kategorie5->ds->SQL = "SELECT *  " .
"FROM kategorie";
            $this->Kategorie6 = new clsControl(ccsListBox, "Kategorie6", "Kategorie", ccsText, "", CCGetRequestParam("Kategorie6", $Method));
            $this->Kategorie6->DSType = dsTable;
            list($this->Kategorie6->BoundColumn, $this->Kategorie6->TextColumn, $this->Kategorie6->DBFormat) = array("Kategorie", "Kategorie", "");
            $this->Kategorie6->ds = new clsDBConnection1();
            $this->Kategorie6->ds->SQL = "SELECT *  " .
"FROM kategorie";
            $this->Kategorie7 = new clsControl(ccsListBox, "Kategorie7", "Kategorie", ccsText, "", CCGetRequestParam("Kategorie7", $Method));
            $this->Kategorie7->DSType = dsTable;
            list($this->Kategorie7->BoundColumn, $this->Kategorie7->TextColumn, $this->Kategorie7->DBFormat) = array("Kategorie", "Kategorie", "");
            $this->Kategorie7->ds = new clsDBConnection1();
            $this->Kategorie7->ds->SQL = "SELECT *  " .
"FROM kategorie";
            $this->Kategorie8 = new clsControl(ccsListBox, "Kategorie8", "Kategorie", ccsText, "", CCGetRequestParam("Kategorie8", $Method));
            $this->Kategorie8->DSType = dsTable;
            list($this->Kategorie8->BoundColumn, $this->Kategorie8->TextColumn, $this->Kategorie8->DBFormat) = array("Kategorie", "Kategorie", "");
            $this->Kategorie8->ds = new clsDBConnection1();
            $this->Kategorie8->ds->SQL = "SELECT *  " .
"FROM kategorie";
            $this->Kategorie9 = new clsControl(ccsListBox, "Kategorie9", "Kategorie", ccsText, "", CCGetRequestParam("Kategorie9", $Method));
            $this->Kategorie9->DSType = dsTable;
            list($this->Kategorie9->BoundColumn, $this->Kategorie9->TextColumn, $this->Kategorie9->DBFormat) = array("Kategorie", "Kategorie", "");
            $this->Kategorie9->ds = new clsDBConnection1();
            $this->Kategorie9->ds->SQL = "SELECT *  " .
"FROM kategorie";
            $this->Austellungstermine = new clsControl(ccsTextBox, "Austellungstermine", "Austellungstermine", ccsText, "", CCGetRequestParam("Austellungstermine", $Method));
            $this->login = new clsControl(ccsTextBox, "login", "login", ccsText, "", CCGetRequestParam("login", $Method));
            $this->TextBox1 = new clsControl(ccsTextBox, "TextBox1", "TextBox1", ccsText, "", CCGetRequestParam("TextBox1", $Method));
            $this->Hidden1 = new clsControl(ccsHidden, "Hidden1", "Hidden1", ccsText, "", CCGetRequestParam("Hidden1", $Method));
            $this->Hidden1->Required = true;
            $this->Button_Insert = new clsButton("Button_Insert");
            $this->Button_Update = new clsButton("Button_Update");
            $this->Button_Delete = new clsButton("Button_Delete");
            if(!$this->FormSubmitted) {
                if(!is_array($this->Hidden1->Value) && !strlen($this->Hidden1->Value) && $this->Hidden1->Value !== false)
                $this->Hidden1->SetText(2);
            }
        }
    }
//End Class_Initialize Event

//Initialize Method @2-FA88DE75
    function Initialize()
    {

        if(!$this->Visible)
            return;

        $this->ds->Parameters["urlKuenstler_ID"] = CCGetFromGet("Kuenstler_ID", "");
    }
//End Initialize Method

//Validate Method @2-3AD6ACE2
    function Validate()
    {
        $Validation = true;
        $Where = "";
        if(strlen($this->email->GetText()) && !preg_match ("/^[\w\.-]{1,}\@([\da-zA-Z-]{1,}\.){1,}[\da-zA-Z-]+$/", $this->email->GetText())) {
            $this->email->Errors->addError("Die Eingabepr�fung f�r das Feld E Mail meldet einen Fehler.");
        }
        $Validation = ($this->Name->Validate() && $Validation);
        $Validation = ($this->Vorname->Validate() && $Validation);
        $Validation = ($this->Adresse->Validate() && $Validation);
        $Validation = ($this->PLZ->Validate() && $Validation);
        $Validation = ($this->Stadt->Validate() && $Validation);
        $Validation = ($this->Telefon->Validate() && $Validation);
        $Validation = ($this->email->Validate() && $Validation);
        $Validation = ($this->Webseite->Validate() && $Validation);
        $Validation = ($this->FileUpload1->Validate() && $Validation);
        $Validation = ($this->Vita->Validate() && $Validation);
        $Validation = ($this->Kategorie->Validate() && $Validation);
        $Validation = ($this->Kategorie2->Validate() && $Validation);
        $Validation = ($this->Kategorie3->Validate() && $Validation);
        $Validation = ($this->Kategorie4->Validate() && $Validation);
        $Validation = ($this->Kategorie5->Validate() && $Validation);
        $Validation = ($this->Kategorie6->Validate() && $Validation);
        $Validation = ($this->Kategorie7->Validate() && $Validation);
        $Validation = ($this->Kategorie8->Validate() && $Validation);
        $Validation = ($this->Kategorie9->Validate() && $Validation);
        $Validation = ($this->Austellungstermine->Validate() && $Validation);
        $Validation = ($this->login->Validate() && $Validation);
        $Validation = ($this->TextBox1->Validate() && $Validation);
        $Validation = ($this->Hidden1->Validate() && $Validation);
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "OnValidate");
        $Validation =  $Validation && ($this->Name->Errors->Count() == 0);
        $Validation =  $Validation && ($this->Vorname->Errors->Count() == 0);
        $Validation =  $Validation && ($this->Adresse->Errors->Count() == 0);
        $Validation =  $Validation && ($this->PLZ->Errors->Count() == 0);
        $Validation =  $Validation && ($this->Stadt->Errors->Count() == 0);
        $Validation =  $Validation && ($this->Telefon->Errors->Count() == 0);
        $Validation =  $Validation && ($this->email->Errors->Count() == 0);
        $Validation =  $Validation && ($this->Webseite->Errors->Count() == 0);
        $Validation =  $Validation && ($this->FileUpload1->Errors->Count() == 0);
        $Validation =  $Validation && ($this->Vita->Errors->Count() == 0);
        $Validation =  $Validation && ($this->Kategorie->Errors->Count() == 0);
        $Validation =  $Validation && ($this->Kategorie2->Errors->Count() == 0);
        $Validation =  $Validation && ($this->Kategorie3->Errors->Count() == 0);
        $Validation =  $Validation && ($this->Kategorie4->Errors->Count() == 0);
        $Validation =  $Validation && ($this->Kategorie5->Errors->Count() == 0);
        $Validation =  $Validation && ($this->Kategorie6->Errors->Count() == 0);
        $Validation =  $Validation && ($this->Kategorie7->Errors->Count() == 0);
        $Validation =  $Validation && ($this->Kategorie8->Errors->Count() == 0);
        $Validation =  $Validation && ($this->Kategorie9->Errors->Count() == 0);
        $Validation =  $Validation && ($this->Austellungstermine->Errors->Count() == 0);
        $Validation =  $Validation && ($this->login->Errors->Count() == 0);
        $Validation =  $Validation && ($this->TextBox1->Errors->Count() == 0);
        $Validation =  $Validation && ($this->Hidden1->Errors->Count() == 0);
        return (($this->Errors->Count() == 0) && $Validation);
    }
//End Validate Method

//CheckErrors Method @2-B2D09223
    function CheckErrors()
    {
        $errors = false;
        $errors = ($errors || $this->Name->Errors->Count());
        $errors = ($errors || $this->Vorname->Errors->Count());
        $errors = ($errors || $this->Adresse->Errors->Count());
        $errors = ($errors || $this->PLZ->Errors->Count());
        $errors = ($errors || $this->Stadt->Errors->Count());
        $errors = ($errors || $this->Telefon->Errors->Count());
        $errors = ($errors || $this->email->Errors->Count());
        $errors = ($errors || $this->Webseite->Errors->Count());
        $errors = ($errors || $this->Foto->Errors->Count());
        $errors = ($errors || $this->FileUpload1->Errors->Count());
        $errors = ($errors || $this->Vita->Errors->Count());
        $errors = ($errors || $this->Kategorie->Errors->Count());
        $errors = ($errors || $this->Kategorie2->Errors->Count());
        $errors = ($errors || $this->Kategorie3->Errors->Count());
        $errors = ($errors || $this->Kategorie4->Errors->Count());
        $errors = ($errors || $this->Kategorie5->Errors->Count());
        $errors = ($errors || $this->Kategorie6->Errors->Count());
        $errors = ($errors || $this->Kategorie7->Errors->Count());
        $errors = ($errors || $this->Kategorie8->Errors->Count());
        $errors = ($errors || $this->Kategorie9->Errors->Count());
        $errors = ($errors || $this->Austellungstermine->Errors->Count());
        $errors = ($errors || $this->login->Errors->Count());
        $errors = ($errors || $this->TextBox1->Errors->Count());
        $errors = ($errors || $this->Hidden1->Errors->Count());
        $errors = ($errors || $this->Errors->Count());
        $errors = ($errors || $this->ds->Errors->Count());
        return $errors;
    }
//End CheckErrors Method

//Operation Method @2-77A50290
    function Operation()
    {
        if(!$this->Visible)
            return;

        global $Redirect;
        global $FileName;

        $this->ds->Prepare();
        if(!$this->FormSubmitted) {
            $this->EditMode = $this->ds->AllParametersSet;
            return;
        }

        $this->FileUpload1->Upload();

        if($this->FormSubmitted) {
            $this->PressedButton = $this->EditMode ? "Button_Update" : "Button_Insert";
            if(strlen(CCGetParam("Button_Insert", ""))) {
                $this->PressedButton = "Button_Insert";
            } else if(strlen(CCGetParam("Button_Update", ""))) {
                $this->PressedButton = "Button_Update";
            } else if(strlen(CCGetParam("Button_Delete", ""))) {
                $this->PressedButton = "Button_Delete";
            }
        }
        $Redirect = "kuenstler_list.php" . "?" . CCGetQueryString("QueryString", Array("ccsForm"));
        if($this->PressedButton == "Button_Delete") {
            if(!CCGetEvent($this->Button_Delete->CCSEvents, "OnClick") || !$this->DeleteRow()) {
                $Redirect = "";
            }
        } else if($this->Validate()) {
            if($this->PressedButton == "Button_Insert") {
                if(!CCGetEvent($this->Button_Insert->CCSEvents, "OnClick") || !$this->InsertRow()) {
                    $Redirect = "";
                }
            } else if($this->PressedButton == "Button_Update") {
                if(!CCGetEvent($this->Button_Update->CCSEvents, "OnClick") || !$this->UpdateRow()) {
                    $Redirect = "";
                }
            }
        } else {
            $Redirect = "";
        }
    }
//End Operation Method

//InsertRow Method @2-A9064F43
    function InsertRow()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeInsert");
        if(!$this->InsertAllowed) return false;
        $this->ds->Name->SetValue($this->Name->GetValue());
        $this->ds->Vorname->SetValue($this->Vorname->GetValue());
        $this->ds->Adresse->SetValue($this->Adresse->GetValue());
        $this->ds->PLZ->SetValue($this->PLZ->GetValue());
        $this->ds->Stadt->SetValue($this->Stadt->GetValue());
        $this->ds->Telefon->SetValue($this->Telefon->GetValue());
        $this->ds->email->SetValue($this->email->GetValue());
        $this->ds->Webseite->SetValue($this->Webseite->GetValue());
        $this->ds->Foto->SetValue($this->Foto->GetValue());
        $this->ds->FileUpload1->SetValue($this->FileUpload1->GetValue());
        $this->ds->Vita->SetValue($this->Vita->GetValue());
        $this->ds->Kategorie->SetValue($this->Kategorie->GetValue());
        $this->ds->Kategorie2->SetValue($this->Kategorie2->GetValue());
        $this->ds->Kategorie3->SetValue($this->Kategorie3->GetValue());
        $this->ds->Kategorie4->SetValue($this->Kategorie4->GetValue());
        $this->ds->Kategorie5->SetValue($this->Kategorie5->GetValue());
        $this->ds->Kategorie6->SetValue($this->Kategorie6->GetValue());
        $this->ds->Kategorie7->SetValue($this->Kategorie7->GetValue());
        $this->ds->Kategorie8->SetValue($this->Kategorie8->GetValue());
        $this->ds->Kategorie9->SetValue($this->Kategorie9->GetValue());
        $this->ds->Austellungstermine->SetValue($this->Austellungstermine->GetValue());
        $this->ds->login->SetValue($this->login->GetValue());
        $this->ds->TextBox1->SetValue($this->TextBox1->GetValue());
        $this->ds->Hidden1->SetValue($this->Hidden1->GetValue());
        $this->ds->Insert();
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterInsert");
        if($this->ds->Errors->Count() == 0) {
            $this->FileUpload1->Move();
        }
        return (!$this->CheckErrors());
    }
//End InsertRow Method

//UpdateRow Method @2-32E978BA
    function UpdateRow()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeUpdate");
        if(!$this->UpdateAllowed) return false;
        $this->ds->Name->SetValue($this->Name->GetValue());
        $this->ds->Vorname->SetValue($this->Vorname->GetValue());
        $this->ds->Adresse->SetValue($this->Adresse->GetValue());
        $this->ds->PLZ->SetValue($this->PLZ->GetValue());
        $this->ds->Stadt->SetValue($this->Stadt->GetValue());
        $this->ds->Telefon->SetValue($this->Telefon->GetValue());
        $this->ds->email->SetValue($this->email->GetValue());
        $this->ds->Webseite->SetValue($this->Webseite->GetValue());
        $this->ds->Foto->SetValue($this->Foto->GetValue());
        $this->ds->FileUpload1->SetValue($this->FileUpload1->GetValue());
        $this->ds->Vita->SetValue($this->Vita->GetValue());
        $this->ds->Kategorie->SetValue($this->Kategorie->GetValue());
        $this->ds->Kategorie2->SetValue($this->Kategorie2->GetValue());
        $this->ds->Kategorie3->SetValue($this->Kategorie3->GetValue());
        $this->ds->Kategorie4->SetValue($this->Kategorie4->GetValue());
        $this->ds->Kategorie5->SetValue($this->Kategorie5->GetValue());
        $this->ds->Kategorie6->SetValue($this->Kategorie6->GetValue());
        $this->ds->Kategorie7->SetValue($this->Kategorie7->GetValue());
        $this->ds->Kategorie8->SetValue($this->Kategorie8->GetValue());
        $this->ds->Kategorie9->SetValue($this->Kategorie9->GetValue());
        $this->ds->Austellungstermine->SetValue($this->Austellungstermine->GetValue());
        $this->ds->login->SetValue($this->login->GetValue());
        $this->ds->TextBox1->SetValue($this->TextBox1->GetValue());
        $this->ds->Hidden1->SetValue($this->Hidden1->GetValue());
        $this->ds->Update();
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterUpdate");
        if($this->ds->Errors->Count() == 0) {
            $this->FileUpload1->Move();
        }
        return (!$this->CheckErrors());
    }
//End UpdateRow Method

//DeleteRow Method @2-DC81EAF8
    function DeleteRow()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeDelete");
        if(!$this->DeleteAllowed) return false;
        $this->ds->Delete();
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterDelete");
        if($this->ds->Errors->Count() == 0) {
            $this->FileUpload1->Delete();
        }
        return (!$this->CheckErrors());
    }
//End DeleteRow Method

//Show Method @2-B53FE138
    function Show()
    {
        global $Tpl;
        global $FileName;
        $Error = "";

        if(!$this->Visible)
            return;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect");

        $this->Kategorie->Prepare();
        $this->Kategorie2->Prepare();
        $this->Kategorie3->Prepare();
        $this->Kategorie4->Prepare();
        $this->Kategorie5->Prepare();
        $this->Kategorie6->Prepare();
        $this->Kategorie7->Prepare();
        $this->Kategorie8->Prepare();
        $this->Kategorie9->Prepare();

        $RecordBlock = "Record " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $RecordBlock;
        $this->EditMode = $this->EditMode && $this->ReadAllowed;
        if($this->EditMode) {
            if($this->ds->Errors->Count()){
                $this->Errors->AddErrors($this->ds->Errors);
                $this->ds->Errors->clear();
            }
            $this->ds->open();
            if($this->ds->Errors->Count() == 0 && $this->ds->next_record()) {
                $this->ds->SetValues();
                $this->Foto->SetValue($this->ds->Foto->GetValue());
                if(!$this->FormSubmitted){
                    $this->Name->SetValue($this->ds->Name->GetValue());
                    $this->Vorname->SetValue($this->ds->Vorname->GetValue());
                    $this->Adresse->SetValue($this->ds->Adresse->GetValue());
                    $this->PLZ->SetValue($this->ds->PLZ->GetValue());
                    $this->Stadt->SetValue($this->ds->Stadt->GetValue());
                    $this->Telefon->SetValue($this->ds->Telefon->GetValue());
                    $this->email->SetValue($this->ds->email->GetValue());
                    $this->Webseite->SetValue($this->ds->Webseite->GetValue());
                    $this->FileUpload1->SetValue($this->ds->FileUpload1->GetValue());
                    $this->Vita->SetValue($this->ds->Vita->GetValue());
                    $this->Kategorie->SetValue($this->ds->Kategorie->GetValue());
                    $this->Kategorie2->SetValue($this->ds->Kategorie2->GetValue());
                    $this->Kategorie3->SetValue($this->ds->Kategorie3->GetValue());
                    $this->Kategorie4->SetValue($this->ds->Kategorie4->GetValue());
                    $this->Kategorie5->SetValue($this->ds->Kategorie5->GetValue());
                    $this->Kategorie6->SetValue($this->ds->Kategorie6->GetValue());
                    $this->Kategorie7->SetValue($this->ds->Kategorie7->GetValue());
                    $this->Kategorie8->SetValue($this->ds->Kategorie8->GetValue());
                    $this->Kategorie9->SetValue($this->ds->Kategorie9->GetValue());
                    $this->Austellungstermine->SetValue($this->ds->Austellungstermine->GetValue());
                    $this->login->SetValue($this->ds->login->GetValue());
                    $this->TextBox1->SetValue($this->ds->TextBox1->GetValue());
                    $this->Hidden1->SetValue($this->ds->Hidden1->GetValue());
                }
            } else {
                $this->EditMode = false;
            }
        }

        if($this->FormSubmitted || $this->CheckErrors()) {
            $Error .= $this->Name->Errors->ToString();
            $Error .= $this->Vorname->Errors->ToString();
            $Error .= $this->Adresse->Errors->ToString();
            $Error .= $this->PLZ->Errors->ToString();
            $Error .= $this->Stadt->Errors->ToString();
            $Error .= $this->Telefon->Errors->ToString();
            $Error .= $this->email->Errors->ToString();
            $Error .= $this->Webseite->Errors->ToString();
            $Error .= $this->Foto->Errors->ToString();
            $Error .= $this->FileUpload1->Errors->ToString();
            $Error .= $this->Vita->Errors->ToString();
            $Error .= $this->Kategorie->Errors->ToString();
            $Error .= $this->Kategorie2->Errors->ToString();
            $Error .= $this->Kategorie3->Errors->ToString();
            $Error .= $this->Kategorie4->Errors->ToString();
            $Error .= $this->Kategorie5->Errors->ToString();
            $Error .= $this->Kategorie6->Errors->ToString();
            $Error .= $this->Kategorie7->Errors->ToString();
            $Error .= $this->Kategorie8->Errors->ToString();
            $Error .= $this->Kategorie9->Errors->ToString();
            $Error .= $this->Austellungstermine->Errors->ToString();
            $Error .= $this->login->Errors->ToString();
            $Error .= $this->TextBox1->Errors->ToString();
            $Error .= $this->Hidden1->Errors->ToString();
            $Error .= $this->Errors->ToString();
            $Error .= $this->ds->Errors->ToString();
            $Tpl->SetVar("Error", $Error);
            $Tpl->Parse("Error", false);
        }
        $CCSForm = $this->EditMode ? $this->ComponentName . ":" . "Edit" : $this->ComponentName;
        $this->HTMLFormAction = $FileName . "?" . CCAddParam(CCGetQueryString("QueryString", ""), "ccsForm", $CCSForm);
        $Tpl->SetVar("Action", $this->HTMLFormAction);
        $Tpl->SetVar("HTMLFormName", $this->ComponentName);
        $Tpl->SetVar("HTMLFormEnctype", $this->FormEnctype);
        $this->Button_Insert->Visible = !$this->EditMode && $this->InsertAllowed;
        $this->Button_Update->Visible = $this->EditMode && $this->UpdateAllowed;
        $this->Button_Delete->Visible = $this->EditMode && $this->DeleteAllowed;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow");
        if(!$this->Visible) {
            $Tpl->block_path = $ParentPath;
            return;
        }

        $this->Name->Show();
        $this->Vorname->Show();
        $this->Adresse->Show();
        $this->PLZ->Show();
        $this->Stadt->Show();
        $this->Telefon->Show();
        $this->email->Show();
        $this->Webseite->Show();
        $this->Foto->Show();
        $this->FileUpload1->Show();
        $this->Vita->Show();
        $this->Kategorie->Show();
        $this->Kategorie2->Show();
        $this->Kategorie3->Show();
        $this->Kategorie4->Show();
        $this->Kategorie5->Show();
        $this->Kategorie6->Show();
        $this->Kategorie7->Show();
        $this->Kategorie8->Show();
        $this->Kategorie9->Show();
        $this->Austellungstermine->Show();
        $this->login->Show();
        $this->TextBox1->Show();
        $this->Hidden1->Show();
        $this->Button_Insert->Show();
        $this->Button_Update->Show();
        $this->Button_Delete->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
        $this->ds->close();
    }
//End Show Method

} //End kuenstler Class @2-FCB6E20C

class clskuenstlerDataSource extends clsDBConnection1 {  //kuenstlerDataSource Class @2-03C1C86E

//DataSource Variables @2-F56DE28C
    var $CCSEvents = "";
    var $CCSEventResult;
    var $ErrorBlock;
    var $CmdExecution;

    var $InsertParameters;
    var $UpdateParameters;
    var $DeleteParameters;
    var $wp;
    var $AllParametersSet;


    // Datasource fields
    var $Name;
    var $Vorname;
    var $Adresse;
    var $PLZ;
    var $Stadt;
    var $Telefon;
    var $email;
    var $Webseite;
    var $Foto;
    var $FileUpload1;
    var $Vita;
    var $Kategorie;
    var $Kategorie2;
    var $Kategorie3;
    var $Kategorie4;
    var $Kategorie5;
    var $Kategorie6;
    var $Kategorie7;
    var $Kategorie8;
    var $Kategorie9;
    var $Austellungstermine;
    var $login;
    var $TextBox1;
    var $Hidden1;
//End DataSource Variables

//DataSourceClass_Initialize Event @2-2F9AC699
    function clskuenstlerDataSource()
    {
        $this->ErrorBlock = "Record kuenstler/Error";
        $this->Initialize();
        $this->Name = new clsField("Name", ccsText, "");
        $this->Vorname = new clsField("Vorname", ccsText, "");
        $this->Adresse = new clsField("Adresse", ccsText, "");
        $this->PLZ = new clsField("PLZ", ccsText, "");
        $this->Stadt = new clsField("Stadt", ccsText, "");
        $this->Telefon = new clsField("Telefon", ccsText, "");
        $this->email = new clsField("email", ccsText, "");
        $this->Webseite = new clsField("Webseite", ccsText, "");
        $this->Foto = new clsField("Foto", ccsText, "");
        $this->FileUpload1 = new clsField("FileUpload1", ccsText, "");
        $this->Vita = new clsField("Vita", ccsMemo, "");
        $this->Kategorie = new clsField("Kategorie", ccsText, "");
        $this->Kategorie2 = new clsField("Kategorie2", ccsText, "");
        $this->Kategorie3 = new clsField("Kategorie3", ccsText, "");
        $this->Kategorie4 = new clsField("Kategorie4", ccsText, "");
        $this->Kategorie5 = new clsField("Kategorie5", ccsText, "");
        $this->Kategorie6 = new clsField("Kategorie6", ccsText, "");
        $this->Kategorie7 = new clsField("Kategorie7", ccsText, "");
        $this->Kategorie8 = new clsField("Kategorie8", ccsText, "");
        $this->Kategorie9 = new clsField("Kategorie9", ccsText, "");
        $this->Austellungstermine = new clsField("Austellungstermine", ccsText, "");
        $this->login = new clsField("login", ccsText, "");
        $this->TextBox1 = new clsField("TextBox1", ccsText, "");
        $this->Hidden1 = new clsField("Hidden1", ccsText, "");

    }
//End DataSourceClass_Initialize Event

//Prepare Method @2-1F9C2320
    function Prepare()
    {
        $this->wp = new clsSQLParameters($this->ErrorBlock);
        $this->wp->AddParameter("1", "urlKuenstler_ID", ccsInteger, "", "", $this->Parameters["urlKuenstler_ID"], "", false);
        $this->AllParametersSet = $this->wp->AllParamsSet();
        $this->wp->Criterion[1] = $this->wp->Operation(opEqual, "Kuenstler_ID", $this->wp->GetDBValue("1"), $this->ToSQL($this->wp->GetDBValue("1"), ccsInteger),false);
        $this->Where = 
             $this->wp->Criterion[1];
    }
//End Prepare Method

//Open Method @2-EB7F5BBD
    function Open()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildSelect");
        $this->SQL = "SELECT *  " .
        "FROM kuenstler";
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteSelect");
        $this->PageSize = 1;
        $this->query($this->OptimizeSQL(CCBuildSQL($this->SQL, $this->Where, $this->Order)));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteSelect");
    }
//End Open Method

//SetValues Method @2-90AFF5B8
    function SetValues()
    {
        $this->Name->SetDBValue($this->f("Name"));
        $this->Vorname->SetDBValue($this->f("Vorname"));
        $this->Adresse->SetDBValue($this->f("Adresse"));
        $this->PLZ->SetDBValue($this->f("PLZ"));
        $this->Stadt->SetDBValue($this->f("Stadt"));
        $this->Telefon->SetDBValue($this->f("Telefon"));
        $this->email->SetDBValue($this->f("email"));
        $this->Webseite->SetDBValue($this->f("Webseite"));
        $this->Foto->SetDBValue($this->f("Foto"));
        $this->FileUpload1->SetDBValue($this->f("Foto"));
        $this->Vita->SetDBValue($this->f("Vita"));
        $this->Kategorie->SetDBValue($this->f("Kategorie"));
        $this->Kategorie2->SetDBValue($this->f("K2"));
        $this->Kategorie3->SetDBValue($this->f("K3"));
        $this->Kategorie4->SetDBValue($this->f("K4"));
        $this->Kategorie5->SetDBValue($this->f("K5"));
        $this->Kategorie6->SetDBValue($this->f("K6"));
        $this->Kategorie7->SetDBValue($this->f("K7"));
        $this->Kategorie8->SetDBValue($this->f("K8"));
        $this->Kategorie9->SetDBValue($this->f("K9"));
        $this->Austellungstermine->SetDBValue($this->f("Austellungstermine"));
        $this->login->SetDBValue($this->f("login"));
        $this->TextBox1->SetDBValue($this->f("passwort"));
        $this->Hidden1->SetDBValue($this->f("Group"));
    }
//End SetValues Method

//Insert Method @2-C8C7A741
    function Insert()
    {
        $this->CmdExecution = true;
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildInsert");
        $this->SQL = "INSERT INTO kuenstler ("
             . "Name, "
             . "Vorname, "
             . "Adresse, "
             . "PLZ, "
             . "Stadt, "
             . "Telefon, "
             . "email, "
             . "Webseite, "
             . "Foto, "
             . "Vita, "
             . "Kategorie, "
             . "K2, "
             . "K3, "
             . "K4, "
             . "K5, "
             . "K6, "
             . "K7, "
             . "K8, "
             . "K9, "
             . "Austellungstermine, "
             . "login, "
             . "passwort, "
             . "`Group`"
             . ") VALUES ("
             . $this->ToSQL($this->Name->GetDBValue(), $this->Name->DataType) . ", "
             . $this->ToSQL($this->Vorname->GetDBValue(), $this->Vorname->DataType) . ", "
             . $this->ToSQL($this->Adresse->GetDBValue(), $this->Adresse->DataType) . ", "
             . $this->ToSQL($this->PLZ->GetDBValue(), $this->PLZ->DataType) . ", "
             . $this->ToSQL($this->Stadt->GetDBValue(), $this->Stadt->DataType) . ", "
             . $this->ToSQL($this->Telefon->GetDBValue(), $this->Telefon->DataType) . ", "
             . $this->ToSQL($this->email->GetDBValue(), $this->email->DataType) . ", "
             . $this->ToSQL($this->Webseite->GetDBValue(), $this->Webseite->DataType) . ", "
             . $this->ToSQL($this->FileUpload1->GetDBValue(), $this->FileUpload1->DataType) . ", "
             . $this->ToSQL($this->Vita->GetDBValue(), $this->Vita->DataType) . ", "
             . $this->ToSQL($this->Kategorie->GetDBValue(), $this->Kategorie->DataType) . ", "
             . $this->ToSQL($this->Kategorie2->GetDBValue(), $this->Kategorie2->DataType) . ", "
             . $this->ToSQL($this->Kategorie3->GetDBValue(), $this->Kategorie3->DataType) . ", "
             . $this->ToSQL($this->Kategorie4->GetDBValue(), $this->Kategorie4->DataType) . ", "
             . $this->ToSQL($this->Kategorie5->GetDBValue(), $this->Kategorie5->DataType) . ", "
             . $this->ToSQL($this->Kategorie6->GetDBValue(), $this->Kategorie6->DataType) . ", "
             . $this->ToSQL($this->Kategorie7->GetDBValue(), $this->Kategorie7->DataType) . ", "
             . $this->ToSQL($this->Kategorie8->GetDBValue(), $this->Kategorie8->DataType) . ", "
             . $this->ToSQL($this->Kategorie9->GetDBValue(), $this->Kategorie9->DataType) . ", "
             . $this->ToSQL($this->Austellungstermine->GetDBValue(), $this->Austellungstermine->DataType) . ", "
             . $this->ToSQL($this->login->GetDBValue(), $this->login->DataType) . ", "
             . $this->ToSQL($this->TextBox1->GetDBValue(), $this->TextBox1->DataType) . ", "
             . $this->ToSQL($this->Hidden1->GetDBValue(), $this->Hidden1->DataType)
             . ")";
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteInsert");
        if($this->Errors->Count() == 0 && $this->CmdExecution) {
            $this->query($this->SQL);
            $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteInsert");
        }
        $this->close();
    }
//End Insert Method

//Update Method @2-F72E8305
    function Update()
    {
        $this->CmdExecution = true;
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildUpdate");
        $this->SQL = "UPDATE kuenstler SET "
             . "Name=" . $this->ToSQL($this->Name->GetDBValue(), $this->Name->DataType) . ", "
             . "Vorname=" . $this->ToSQL($this->Vorname->GetDBValue(), $this->Vorname->DataType) . ", "
             . "Adresse=" . $this->ToSQL($this->Adresse->GetDBValue(), $this->Adresse->DataType) . ", "
             . "PLZ=" . $this->ToSQL($this->PLZ->GetDBValue(), $this->PLZ->DataType) . ", "
             . "Stadt=" . $this->ToSQL($this->Stadt->GetDBValue(), $this->Stadt->DataType) . ", "
             . "Telefon=" . $this->ToSQL($this->Telefon->GetDBValue(), $this->Telefon->DataType) . ", "
             . "email=" . $this->ToSQL($this->email->GetDBValue(), $this->email->DataType) . ", "
             . "Webseite=" . $this->ToSQL($this->Webseite->GetDBValue(), $this->Webseite->DataType) . ", "
             . "Foto=" . $this->ToSQL($this->FileUpload1->GetDBValue(), $this->FileUpload1->DataType) . ", "
             . "Vita=" . $this->ToSQL($this->Vita->GetDBValue(), $this->Vita->DataType) . ", "
             . "Kategorie=" . $this->ToSQL($this->Kategorie->GetDBValue(), $this->Kategorie->DataType) . ", "
             . "K2=" . $this->ToSQL($this->Kategorie2->GetDBValue(), $this->Kategorie2->DataType) . ", "
             . "K3=" . $this->ToSQL($this->Kategorie3->GetDBValue(), $this->Kategorie3->DataType) . ", "
             . "K4=" . $this->ToSQL($this->Kategorie4->GetDBValue(), $this->Kategorie4->DataType) . ", "
             . "K5=" . $this->ToSQL($this->Kategorie5->GetDBValue(), $this->Kategorie5->DataType) . ", "
             . "K6=" . $this->ToSQL($this->Kategorie6->GetDBValue(), $this->Kategorie6->DataType) . ", "
             . "K7=" . $this->ToSQL($this->Kategorie7->GetDBValue(), $this->Kategorie7->DataType) . ", "
             . "K8=" . $this->ToSQL($this->Kategorie8->GetDBValue(), $this->Kategorie8->DataType) . ", "
             . "K9=" . $this->ToSQL($this->Kategorie9->GetDBValue(), $this->Kategorie9->DataType) . ", "
             . "Austellungstermine=" . $this->ToSQL($this->Austellungstermine->GetDBValue(), $this->Austellungstermine->DataType) . ", "
             . "login=" . $this->ToSQL($this->login->GetDBValue(), $this->login->DataType) . ", "
             . "passwort=" . $this->ToSQL($this->TextBox1->GetDBValue(), $this->TextBox1->DataType) . ", "
             . "`Group`=" . $this->ToSQL($this->Hidden1->GetDBValue(), $this->Hidden1->DataType);
        $this->SQL = CCBuildSQL($this->SQL, $this->Where, "");
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteUpdate");
        if($this->Errors->Count() == 0 && $this->CmdExecution) {
            $this->query($this->SQL);
            $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteUpdate");
        }
        $this->close();
    }
//End Update Method

//Delete Method @2-4E7F57D9
    function Delete()
    {
        $this->CmdExecution = true;
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildDelete");
        $this->SQL = "DELETE FROM kuenstler";
        $this->SQL = CCBuildSQL($this->SQL, $this->Where, "");
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteDelete");
        if($this->Errors->Count() == 0 && $this->CmdExecution) {
            $this->query($this->SQL);
            $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteDelete");
        }
        $this->close();
    }
//End Delete Method

} //End kuenstlerDataSource Class @2-FCB6E20C

//Initialize Page @1-C60BACB4
// Variables
$FileName = "";
$Redirect = "";
$Tpl = "";
$TemplateFileName = "";
$BlockToParse = "main";
$ComponentName = "";

// Events;
$CCSEvents = "";
$CCSEventResult = "";

$FileName = "kuenstler_maint.php";
$Redirect = "";
$TemplateFileName = "kuenstler_maint.html";
$BlockToParse = "main";
$TemplateEncoding = "";
$FileEncoding = "";
$PathToRoot = "../";
//End Initialize Page

//Authenticate User @1-C7A40A44
CCSecurityRedirect("3;4", "../login.php");
//End Authenticate User

//Initialize Objects @1-86A3BE23
$DBConnection1 = new clsDBConnection1();

// Controls
$menu = new clsmenu("");
$menu->BindEvents();
$menu->Initialize();
$kuenstler = new clsRecordkuenstler();
$kuenstler->Initialize();

$CCSEventResult = CCGetEvent($CCSEvents, "AfterInitialize");

$Charset = $Charset ? $Charset : $TemplateEncoding;
if ($Charset)
    header("Content-Type: text/html; charset=" . $Charset);
//End Initialize Objects

//Initialize HTML Template @1-E2A5B61F
$CCSEventResult = CCGetEvent($CCSEvents, "OnInitializeView");
$Tpl = new clsTemplate($FileEncoding, $TemplateEncoding);
$Tpl->LoadTemplate(PathToCurrentPage . $TemplateFileName, $BlockToParse, $TemplateEncoding);
$Tpl->block_path = "/$BlockToParse";
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeShow");
//End Initialize HTML Template

//Execute Components @1-8F246FB3
$menu->Operations();
$kuenstler->Operation();
//End Execute Components

//Go to destination page @1-16397CBB
if($Redirect)
{
    $CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload");
    $DBConnection1->close();
    header("Location: " . $Redirect);
    $menu->Class_Terminate();
    unset($menu);
    unset($kuenstler);
    unset($Tpl);
    exit;
}
//End Go to destination page

//Show Page @1-7BDB0507
$menu->Show("menu");
$kuenstler->Show();
$Tpl->block_path = "";
$Tpl->PParse("main", false);
//End Show Page

//Unload Page @1-51BE073F
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload");
$DBConnection1->close();
$menu->Class_Terminate();
unset($menu);
unset($kuenstler);
unset($Tpl);
//End Unload Page


?>
