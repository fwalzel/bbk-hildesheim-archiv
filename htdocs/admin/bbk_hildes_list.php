<?php
//Include Common Files @1-4EDE2568
define("RelativePath", "..");
define("PathToCurrentPage", "/admin/");
include(RelativePath . "/Common.php");
include(RelativePath . "/Template.php");
include(RelativePath . "/Sorter.php");
include(RelativePath . "/Navigator.php");
  
//End Include Common Files

//Include Page implementation @45-D18F3F45
include_once(RelativePath . "/admin/menu.php");
//End Include Page implementation

class clsRecordbbk_hildesheimSearch { //bbk_hildesheimSearch Class @2-528DDE60

//Variables @2-76058651

    // Public variables
    var $ComponentName;
    var $HTMLFormAction;
    var $PressedButton;
    var $Errors;
    var $ErrorBlock;
    var $FormSubmitted;
    var $FormEnctype;
    var $Visible;
    var $Recordset;

    var $CCSEvents = "";
    var $CCSEventResult;

    var $RelativePath = "";

    var $InsertAllowed = false;
    var $UpdateAllowed = false;
    var $DeleteAllowed = false;
    var $ReadAllowed   = false;
    var $EditMode      = false;
    var $ds;
    var $ValidatingControls;
    var $Controls;

    // Class variables
//End Variables

//Class_Initialize Event @2-BCC5D3E8
    function clsRecordbbk_hildesheimSearch($RelativePath = "")
    {

        global $FileName;
        $this->Visible = true;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Record bbk_hildesheimSearch/Error";
        $this->ReadAllowed = true;
        if($this->Visible)
        {
            $this->ComponentName = "bbk_hildesheimSearch";
            $CCSForm = split(":", CCGetFromGet("ccsForm", ""), 2);
            if(sizeof($CCSForm) == 1)
                $CCSForm[1] = "";
            list($FormName, $FormMethod) = $CCSForm;
            $this->FormEnctype = "application/x-www-form-urlencoded";
            $this->FormSubmitted = ($FormName == $this->ComponentName);
            $Method = $this->FormSubmitted ? ccsPost : ccsGet;
            $this->s_Name = new clsControl(ccsTextBox, "s_Name", "s_Name", ccsText, "", CCGetRequestParam("s_Name", $Method));
            $this->s_Vorname = new clsControl(ccsTextBox, "s_Vorname", "s_Vorname", ccsText, "", CCGetRequestParam("s_Vorname", $Method));
            $this->s_Titel = new clsControl(ccsTextBox, "s_Titel", "s_Titel", ccsText, "", CCGetRequestParam("s_Titel", $Method));
            $this->s_Email = new clsControl(ccsTextBox, "s_Email", "s_Email", ccsText, "", CCGetRequestParam("s_Email", $Method));
            $this->s_Telefon = new clsControl(ccsTextBox, "s_Telefon", "s_Telefon", ccsText, "", CCGetRequestParam("s_Telefon", $Method));
            $this->Button_DoSearch = new clsButton("Button_DoSearch");
        }
    }
//End Class_Initialize Event

//Validate Method @2-45DAA110
    function Validate()
    {
        $Validation = true;
        $Where = "";
        $Validation = ($this->s_Name->Validate() && $Validation);
        $Validation = ($this->s_Vorname->Validate() && $Validation);
        $Validation = ($this->s_Titel->Validate() && $Validation);
        $Validation = ($this->s_Email->Validate() && $Validation);
        $Validation = ($this->s_Telefon->Validate() && $Validation);
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "OnValidate");
        $Validation =  $Validation && ($this->s_Name->Errors->Count() == 0);
        $Validation =  $Validation && ($this->s_Vorname->Errors->Count() == 0);
        $Validation =  $Validation && ($this->s_Titel->Errors->Count() == 0);
        $Validation =  $Validation && ($this->s_Email->Errors->Count() == 0);
        $Validation =  $Validation && ($this->s_Telefon->Errors->Count() == 0);
        return (($this->Errors->Count() == 0) && $Validation);
    }
//End Validate Method

//CheckErrors Method @2-9400DFD8
    function CheckErrors()
    {
        $errors = false;
        $errors = ($errors || $this->s_Name->Errors->Count());
        $errors = ($errors || $this->s_Vorname->Errors->Count());
        $errors = ($errors || $this->s_Titel->Errors->Count());
        $errors = ($errors || $this->s_Email->Errors->Count());
        $errors = ($errors || $this->s_Telefon->Errors->Count());
        $errors = ($errors || $this->Errors->Count());
        return $errors;
    }
//End CheckErrors Method

//Operation Method @2-CD83D0BE
    function Operation()
    {
        if(!$this->Visible)
            return;

        global $Redirect;
        global $FileName;

        if(!$this->FormSubmitted) {
            return;
        }

        if($this->FormSubmitted) {
            $this->PressedButton = "Button_DoSearch";
            if(strlen(CCGetParam("Button_DoSearch", ""))) {
                $this->PressedButton = "Button_DoSearch";
            }
        }
        $Redirect = "bbk_hildes_list.php";
        if($this->Validate()) {
            if($this->PressedButton == "Button_DoSearch") {
                if(!CCGetEvent($this->Button_DoSearch->CCSEvents, "OnClick")) {
                    $Redirect = "";
                } else {
                    $Redirect = "bbk_hildes_list.php" . "?" . CCMergeQueryStrings(CCGetQueryString("Form", Array("Button_DoSearch")));
                }
            }
        } else {
            $Redirect = "";
        }
    }
//End Operation Method

//Show Method @2-F9CD124A
    function Show()
    {
        global $Tpl;
        global $FileName;
        $Error = "";

        if(!$this->Visible)
            return;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect");


        $RecordBlock = "Record " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $RecordBlock;
        $this->EditMode = $this->EditMode && $this->ReadAllowed;
        if (!$this->FormSubmitted) {
        }

        if($this->FormSubmitted || $this->CheckErrors()) {
            $Error .= $this->s_Name->Errors->ToString();
            $Error .= $this->s_Vorname->Errors->ToString();
            $Error .= $this->s_Titel->Errors->ToString();
            $Error .= $this->s_Email->Errors->ToString();
            $Error .= $this->s_Telefon->Errors->ToString();
            $Error .= $this->Errors->ToString();
            $Tpl->SetVar("Error", $Error);
            $Tpl->Parse("Error", false);
        }
        $CCSForm = $this->EditMode ? $this->ComponentName . ":" . "Edit" : $this->ComponentName;
        $this->HTMLFormAction = $FileName . "?" . CCAddParam(CCGetQueryString("QueryString", ""), "ccsForm", $CCSForm);
        $Tpl->SetVar("Action", $this->HTMLFormAction);
        $Tpl->SetVar("HTMLFormName", $this->ComponentName);
        $Tpl->SetVar("HTMLFormEnctype", $this->FormEnctype);

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow");
        if(!$this->Visible) {
            $Tpl->block_path = $ParentPath;
            return;
        }

        $this->s_Name->Show();
        $this->s_Vorname->Show();
        $this->s_Titel->Show();
        $this->s_Email->Show();
        $this->s_Telefon->Show();
        $this->Button_DoSearch->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
    }
//End Show Method

} //End bbk_hildesheimSearch Class @2-FCB6E20C

class clsGridbbk_hildesheim { //bbk_hildesheim class @10-71DDC71F

//Variables @10-41284832

    // Public variables
    var $ComponentName;
    var $Visible;
    var $Errors;
    var $ErrorBlock;
    var $ds; var $PageSize;
    var $SorterName = "";
    var $SorterDirection = "";
    var $PageNumber;

    var $CCSEvents = "";
    var $CCSEventResult;

    var $RelativePath = "";

    // Grid Controls
    var $StaticControls; var $RowControls;
    var $Sorter_vorstand_ID;
    var $Sorter_Reihenfolge;
    var $Sorter_Name;
    var $Sorter_Vorname;
    var $Sorter_Email;
    var $Sorter_Telefon;
    var $Navigator;
//End Variables

//Class_Initialize Event @10-9A25AB17
    function clsGridbbk_hildesheim($RelativePath = "")
    {
        global $FileName;
        $this->ComponentName = "bbk_hildesheim";
        $this->Visible = True;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Grid bbk_hildesheim";
        $this->ds = new clsbbk_hildesheimDataSource();
        $this->PageSize = CCGetParam($this->ComponentName . "PageSize", "");
        if(!is_numeric($this->PageSize) || !strlen($this->PageSize))
            $this->PageSize = 20;
        else
            $this->PageSize = intval($this->PageSize);
        if ($this->PageSize > 100)
            $this->PageSize = 100;
        if($this->PageSize == 0)
            $this->Errors->addError("<p>Form: Grid " . $this->ComponentName . "<br>Error: (CCS06) Invalid page size.</p>");
        $this->PageNumber = intval(CCGetParam($this->ComponentName . "Page", 1));
        $this->SorterName = CCGetParam("bbk_hildesheimOrder", "");
        $this->SorterDirection = CCGetParam("bbk_hildesheimDir", "");

        $this->vorstand_ID = new clsControl(ccsLink, "vorstand_ID", "vorstand_ID", ccsInteger, "", CCGetRequestParam("vorstand_ID", ccsGet));
        $this->Reihenfolge = new clsControl(ccsLabel, "Reihenfolge", "Reihenfolge", ccsInteger, "", CCGetRequestParam("Reihenfolge", ccsGet));
        $this->Name = new clsControl(ccsLabel, "Name", "Name", ccsText, "", CCGetRequestParam("Name", ccsGet));
        $this->Vorname = new clsControl(ccsLabel, "Vorname", "Vorname", ccsText, "", CCGetRequestParam("Vorname", ccsGet));
        $this->Email = new clsControl(ccsLabel, "Email", "Email", ccsText, "", CCGetRequestParam("Email", ccsGet));
        $this->Telefon = new clsControl(ccsLabel, "Telefon", "Telefon", ccsText, "", CCGetRequestParam("Telefon", ccsGet));
        $this->Sorter_vorstand_ID = new clsSorter($this->ComponentName, "Sorter_vorstand_ID", $FileName);
        $this->Sorter_Reihenfolge = new clsSorter($this->ComponentName, "Sorter_Reihenfolge", $FileName);
        $this->Sorter_Name = new clsSorter($this->ComponentName, "Sorter_Name", $FileName);
        $this->Sorter_Vorname = new clsSorter($this->ComponentName, "Sorter_Vorname", $FileName);
        $this->Sorter_Email = new clsSorter($this->ComponentName, "Sorter_Email", $FileName);
        $this->Sorter_Telefon = new clsSorter($this->ComponentName, "Sorter_Telefon", $FileName);
        $this->bbk_hildesheim_Insert = new clsControl(ccsLink, "bbk_hildesheim_Insert", "bbk_hildesheim_Insert", ccsText, "", CCGetRequestParam("bbk_hildesheim_Insert", ccsGet));
        $this->bbk_hildesheim_Insert->Parameters = CCGetQueryString("QueryString", Array("vorstand_ID", "ccsForm"));
        $this->bbk_hildesheim_Insert->Page = "bbk_hildes_maint.php";
        $this->Navigator = new clsNavigator($this->ComponentName, "Navigator", $FileName, 10, tpSimple);
    }
//End Class_Initialize Event

//Initialize Method @10-03626367
    function Initialize()
    {
        if(!$this->Visible) return;

        $this->ds->PageSize = $this->PageSize;
        $this->ds->AbsolutePage = $this->PageNumber;
        $this->ds->SetOrder($this->SorterName, $this->SorterDirection);
    }
//End Initialize Method

//Show Method @10-C4F0E2E0
    function Show()
    {
        global $Tpl;
        if(!$this->Visible) return;

        $ShownRecords = 0;

        $this->ds->Parameters["urls_Name"] = CCGetFromGet("s_Name", "");
        $this->ds->Parameters["urls_Vorname"] = CCGetFromGet("s_Vorname", "");
        $this->ds->Parameters["urls_Titel"] = CCGetFromGet("s_Titel", "");
        $this->ds->Parameters["urls_Email"] = CCGetFromGet("s_Email", "");
        $this->ds->Parameters["urls_Telefon"] = CCGetFromGet("s_Telefon", "");

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect");


        $this->ds->Prepare();
        $this->ds->Open();

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow");
        if(!$this->Visible) return;

        $GridBlock = "Grid " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $GridBlock;


        $is_next_record = $this->ds->next_record();
        if($is_next_record && $ShownRecords < $this->PageSize)
        {
            do {
                    $this->ds->SetValues();
                $Tpl->block_path = $ParentPath . "/" . $GridBlock . "/Row";
                $this->vorstand_ID->SetValue($this->ds->vorstand_ID->GetValue());
                $this->vorstand_ID->Parameters = CCGetQueryString("QueryString", Array("ccsForm"));
                $this->vorstand_ID->Parameters = CCAddParam($this->vorstand_ID->Parameters, "vorstand_ID", $this->ds->f("vorstand_ID"));
                $this->vorstand_ID->Page = "bbk_hildes_maint.php";
                $this->Reihenfolge->SetValue($this->ds->Reihenfolge->GetValue());
                $this->Name->SetValue($this->ds->Name->GetValue());
                $this->Vorname->SetValue($this->ds->Vorname->GetValue());
                $this->Email->SetValue($this->ds->Email->GetValue());
                $this->Telefon->SetValue($this->ds->Telefon->GetValue());
                $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShowRow");
                $this->vorstand_ID->Show();
                $this->Reihenfolge->Show();
                $this->Name->Show();
                $this->Vorname->Show();
                $this->Email->Show();
                $this->Telefon->Show();
                $Tpl->block_path = $ParentPath . "/" . $GridBlock;
                $Tpl->parse("Row", true);
                $ShownRecords++;
                $is_next_record = $this->ds->next_record();
            } while ($is_next_record && $ShownRecords < $this->PageSize);
        }
        else // Show NoRecords block if no records are found
        {
            $Tpl->parse("NoRecords", false);
        }

        $errors = $this->GetErrors();
        if(strlen($errors))
        {
            $Tpl->replaceblock("", $errors);
            $Tpl->block_path = $ParentPath;
            return;
        }
        $this->Navigator->PageNumber = $this->ds->AbsolutePage;
        $this->Navigator->TotalPages = $this->ds->PageCount();
        $this->Sorter_vorstand_ID->Show();
        $this->Sorter_Reihenfolge->Show();
        $this->Sorter_Name->Show();
        $this->Sorter_Vorname->Show();
        $this->Sorter_Email->Show();
        $this->Sorter_Telefon->Show();
        $this->bbk_hildesheim_Insert->Show();
        $this->Navigator->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
        $this->ds->close();
    }
//End Show Method

//GetErrors Method @10-D4370D37
    function GetErrors()
    {
        $errors = "";
        $errors .= $this->vorstand_ID->Errors->ToString();
        $errors .= $this->Reihenfolge->Errors->ToString();
        $errors .= $this->Name->Errors->ToString();
        $errors .= $this->Vorname->Errors->ToString();
        $errors .= $this->Email->Errors->ToString();
        $errors .= $this->Telefon->Errors->ToString();
        $errors .= $this->Errors->ToString();
        $errors .= $this->ds->Errors->ToString();
        return $errors;
    }
//End GetErrors Method

} //End bbk_hildesheim Class @10-FCB6E20C

class clsbbk_hildesheimDataSource extends clsDBConnection1 {  //bbk_hildesheimDataSource Class @10-67F5C630

//DataSource Variables @10-D3A641F0
    var $CCSEvents = "";
    var $CCSEventResult;
    var $ErrorBlock;
    var $CmdExecution;

    var $CountSQL;
    var $wp;


    // Datasource fields
    var $vorstand_ID;
    var $Reihenfolge;
    var $Name;
    var $Vorname;
    var $Email;
    var $Telefon;
//End DataSource Variables

//DataSourceClass_Initialize Event @10-0DA896FC
    function clsbbk_hildesheimDataSource()
    {
        $this->ErrorBlock = "Grid bbk_hildesheim";
        $this->Initialize();
        $this->vorstand_ID = new clsField("vorstand_ID", ccsInteger, "");
        $this->Reihenfolge = new clsField("Reihenfolge", ccsInteger, "");
        $this->Name = new clsField("Name", ccsText, "");
        $this->Vorname = new clsField("Vorname", ccsText, "");
        $this->Email = new clsField("Email", ccsText, "");
        $this->Telefon = new clsField("Telefon", ccsText, "");

    }
//End DataSourceClass_Initialize Event

//SetOrder Method @10-85F75B4B
    function SetOrder($SorterName, $SorterDirection)
    {
        $this->Order = "";
        $this->Order = CCGetOrder($this->Order, $SorterName, $SorterDirection, 
            array("Sorter_vorstand_ID" => array("vorstand_ID", ""), 
            "Sorter_Reihenfolge" => array("Reihenfolge", ""), 
            "Sorter_Name" => array("Name", ""), 
            "Sorter_Vorname" => array("Vorname", ""), 
            "Sorter_Email" => array("Email", ""), 
            "Sorter_Telefon" => array("Telefon", "")));
    }
//End SetOrder Method

//Prepare Method @10-1CD15912
    function Prepare()
    {
        $this->wp = new clsSQLParameters($this->ErrorBlock);
        $this->wp->AddParameter("1", "urls_Name", ccsText, "", "", $this->Parameters["urls_Name"], "", false);
        $this->wp->AddParameter("2", "urls_Vorname", ccsText, "", "", $this->Parameters["urls_Vorname"], "", false);
        $this->wp->AddParameter("3", "urls_Titel", ccsText, "", "", $this->Parameters["urls_Titel"], "", false);
        $this->wp->AddParameter("4", "urls_Email", ccsText, "", "", $this->Parameters["urls_Email"], "", false);
        $this->wp->AddParameter("5", "urls_Telefon", ccsText, "", "", $this->Parameters["urls_Telefon"], "", false);
        $this->wp->Criterion[1] = $this->wp->Operation(opContains, "Name", $this->wp->GetDBValue("1"), $this->ToSQL($this->wp->GetDBValue("1"), ccsText),false);
        $this->wp->Criterion[2] = $this->wp->Operation(opContains, "Vorname", $this->wp->GetDBValue("2"), $this->ToSQL($this->wp->GetDBValue("2"), ccsText),false);
        $this->wp->Criterion[3] = $this->wp->Operation(opContains, "Titel", $this->wp->GetDBValue("3"), $this->ToSQL($this->wp->GetDBValue("3"), ccsText),false);
        $this->wp->Criterion[4] = $this->wp->Operation(opContains, "Email", $this->wp->GetDBValue("4"), $this->ToSQL($this->wp->GetDBValue("4"), ccsText),false);
        $this->wp->Criterion[5] = $this->wp->Operation(opContains, "Telefon", $this->wp->GetDBValue("5"), $this->ToSQL($this->wp->GetDBValue("5"), ccsText),false);
        $this->Where = $this->wp->opAND(
             false, $this->wp->opAND(
             false, $this->wp->opAND(
             false, $this->wp->opAND(
             false, 
             $this->wp->Criterion[1], 
             $this->wp->Criterion[2]), 
             $this->wp->Criterion[3]), 
             $this->wp->Criterion[4]), 
             $this->wp->Criterion[5]);
    }
//End Prepare Method

//Open Method @10-13D925F4
    function Open()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildSelect");
        $this->CountSQL = "SELECT COUNT(*)  " .
        "FROM bbk_hildesheim";
        $this->SQL = "SELECT bbk_hildesheim.vorstand_ID, bbk_hildesheim.Reihenfolge, bbk_hildesheim.Name, bbk_hildesheim.Vorname, bbk_hildesheim.Titel, " .
        "bbk_hildesheim.Email, bbk_hildesheim.Foto, bbk_hildesheim.Telefon  " .
        "FROM bbk_hildesheim";
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteSelect");
        $this->RecordsCount = CCGetDBValue(CCBuildSQL($this->CountSQL, $this->Where, ""), $this);
        $this->query($this->OptimizeSQL(CCBuildSQL($this->SQL, $this->Where, $this->Order)));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteSelect");
    }
//End Open Method

//SetValues Method @10-B4F92D63
    function SetValues()
    {
        $this->vorstand_ID->SetDBValue(trim($this->f("vorstand_ID")));
        $this->Reihenfolge->SetDBValue(trim($this->f("Reihenfolge")));
        $this->Name->SetDBValue($this->f("Name"));
        $this->Vorname->SetDBValue($this->f("Vorname"));
        $this->Email->SetDBValue($this->f("Email"));
        $this->Telefon->SetDBValue($this->f("Telefon"));
    }
//End SetValues Method

} //End bbk_hildesheimDataSource Class @10-FCB6E20C

//Initialize Page @1-D0FB0A0D
// Variables
$FileName = "";
$Redirect = "";
$Tpl = "";
$TemplateFileName = "";
$BlockToParse = "main";
$ComponentName = "";

// Events;
$CCSEvents = "";
$CCSEventResult = "";

$FileName = "bbk_hildes_list.php";
$Redirect = "";
$TemplateFileName = "bbk_hildes_list.html";
$BlockToParse = "main";
$TemplateEncoding = "";
$FileEncoding = "";
$PathToRoot = "../";
//End Initialize Page

//Authenticate User @1-4B0BB954
CCSecurityRedirect("3", "");
//End Authenticate User

//Initialize Objects @1-4A5F1979
$DBConnection1 = new clsDBConnection1();

// Controls
$menu = new clsmenu("");
$menu->BindEvents();
$menu->Initialize();
$bbk_hildesheimSearch = new clsRecordbbk_hildesheimSearch();
$bbk_hildesheim = new clsGridbbk_hildesheim();
$bbk_hildesheim->Initialize();

$CCSEventResult = CCGetEvent($CCSEvents, "AfterInitialize");

$Charset = $Charset ? $Charset : $TemplateEncoding;
if ($Charset)
    header("Content-Type: text/html; charset=" . $Charset);
//End Initialize Objects

//Initialize HTML Template @1-E2A5B61F
$CCSEventResult = CCGetEvent($CCSEvents, "OnInitializeView");
$Tpl = new clsTemplate($FileEncoding, $TemplateEncoding);
$Tpl->LoadTemplate(PathToCurrentPage . $TemplateFileName, $BlockToParse, $TemplateEncoding);
$Tpl->block_path = "/$BlockToParse";
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeShow");
//End Initialize HTML Template

//Execute Components @1-B5BCD9CC
$menu->Operations();
$bbk_hildesheimSearch->Operation();
//End Execute Components

//Go to destination page @1-3CAD3CD4
if($Redirect)
{
    $CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload");
    $DBConnection1->close();
    header("Location: " . $Redirect);
    $menu->Class_Terminate();
    unset($menu);
    unset($bbk_hildesheimSearch);
    unset($bbk_hildesheim);
    unset($Tpl);
    exit;
}
//End Go to destination page

//Show Page @1-0E57E2F3
$menu->Show("menu");
$bbk_hildesheimSearch->Show();
$bbk_hildesheim->Show();
$Tpl->block_path = "";
$Tpl->PParse("main", false);
//End Show Page

//Unload Page @1-C030AAE1
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload");
$DBConnection1->close();
$menu->Class_Terminate();
unset($menu);
unset($bbk_hildesheimSearch);
unset($bbk_hildesheim);
unset($Tpl);
//End Unload Page


?>
