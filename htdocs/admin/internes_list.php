<?php
//Include Common Files @1-4EDE2568
define("RelativePath", "..");
define("PathToCurrentPage", "/admin/");
include(RelativePath . "/Common.php");
include(RelativePath . "/Template.php");
include(RelativePath . "/Sorter.php");
include(RelativePath . "/Navigator.php");
  
//End Include Common Files

//Include Page implementation @34-D18F3F45
include_once(RelativePath . "/admin/menu.php");
//End Include Page implementation

class clsRecordinternesSearch { //internesSearch Class @2-3E2645D7

//Variables @2-76058651

    // Public variables
    var $ComponentName;
    var $HTMLFormAction;
    var $PressedButton;
    var $Errors;
    var $ErrorBlock;
    var $FormSubmitted;
    var $FormEnctype;
    var $Visible;
    var $Recordset;

    var $CCSEvents = "";
    var $CCSEventResult;

    var $RelativePath = "";

    var $InsertAllowed = false;
    var $UpdateAllowed = false;
    var $DeleteAllowed = false;
    var $ReadAllowed   = false;
    var $EditMode      = false;
    var $ds;
    var $ValidatingControls;
    var $Controls;

    // Class variables
//End Variables

//Class_Initialize Event @2-C4E456D4
    function clsRecordinternesSearch($RelativePath = "")
    {

        global $FileName;
        $this->Visible = true;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Record internesSearch/Error";
        $this->ReadAllowed = true;
        if($this->Visible)
        {
            $this->ComponentName = "internesSearch";
            $CCSForm = split(":", CCGetFromGet("ccsForm", ""), 2);
            if(sizeof($CCSForm) == 1)
                $CCSForm[1] = "";
            list($FormName, $FormMethod) = $CCSForm;
            $this->FormEnctype = "application/x-www-form-urlencoded";
            $this->FormSubmitted = ($FormName == $this->ComponentName);
            $Method = $this->FormSubmitted ? ccsPost : ccsGet;
            $this->s_Datum = new clsControl(ccsTextBox, "s_Datum", "s_Datum", ccsText, "", CCGetRequestParam("s_Datum", $Method));
            $this->s_Titel = new clsControl(ccsTextBox, "s_Titel", "s_Titel", ccsText, "", CCGetRequestParam("s_Titel", $Method));
            $this->s_Beschreibung = new clsControl(ccsTextBox, "s_Beschreibung", "s_Beschreibung", ccsMemo, "", CCGetRequestParam("s_Beschreibung", $Method));
            $this->s_Website = new clsControl(ccsTextBox, "s_Website", "s_Website", ccsText, "", CCGetRequestParam("s_Website", $Method));
            $this->Button_DoSearch = new clsButton("Button_DoSearch");
        }
    }
//End Class_Initialize Event

//Validate Method @2-38F7BB2D
    function Validate()
    {
        $Validation = true;
        $Where = "";
        $Validation = ($this->s_Datum->Validate() && $Validation);
        $Validation = ($this->s_Titel->Validate() && $Validation);
        $Validation = ($this->s_Beschreibung->Validate() && $Validation);
        $Validation = ($this->s_Website->Validate() && $Validation);
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "OnValidate");
        $Validation =  $Validation && ($this->s_Datum->Errors->Count() == 0);
        $Validation =  $Validation && ($this->s_Titel->Errors->Count() == 0);
        $Validation =  $Validation && ($this->s_Beschreibung->Errors->Count() == 0);
        $Validation =  $Validation && ($this->s_Website->Errors->Count() == 0);
        return (($this->Errors->Count() == 0) && $Validation);
    }
//End Validate Method

//CheckErrors Method @2-9D5B9FAF
    function CheckErrors()
    {
        $errors = false;
        $errors = ($errors || $this->s_Datum->Errors->Count());
        $errors = ($errors || $this->s_Titel->Errors->Count());
        $errors = ($errors || $this->s_Beschreibung->Errors->Count());
        $errors = ($errors || $this->s_Website->Errors->Count());
        $errors = ($errors || $this->Errors->Count());
        return $errors;
    }
//End CheckErrors Method

//Operation Method @2-B4BA6724
    function Operation()
    {
        if(!$this->Visible)
            return;

        global $Redirect;
        global $FileName;

        if(!$this->FormSubmitted) {
            return;
        }

        if($this->FormSubmitted) {
            $this->PressedButton = "Button_DoSearch";
            if(strlen(CCGetParam("Button_DoSearch", ""))) {
                $this->PressedButton = "Button_DoSearch";
            }
        }
        $Redirect = "internes_list.php";
        if($this->Validate()) {
            if($this->PressedButton == "Button_DoSearch") {
                if(!CCGetEvent($this->Button_DoSearch->CCSEvents, "OnClick")) {
                    $Redirect = "";
                } else {
                    $Redirect = "internes_list.php" . "?" . CCMergeQueryStrings(CCGetQueryString("Form", Array("Button_DoSearch")));
                }
            }
        } else {
            $Redirect = "";
        }
    }
//End Operation Method

//Show Method @2-31EEE556
    function Show()
    {
        global $Tpl;
        global $FileName;
        $Error = "";

        if(!$this->Visible)
            return;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect");


        $RecordBlock = "Record " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $RecordBlock;
        $this->EditMode = $this->EditMode && $this->ReadAllowed;
        if (!$this->FormSubmitted) {
        }

        if($this->FormSubmitted || $this->CheckErrors()) {
            $Error .= $this->s_Datum->Errors->ToString();
            $Error .= $this->s_Titel->Errors->ToString();
            $Error .= $this->s_Beschreibung->Errors->ToString();
            $Error .= $this->s_Website->Errors->ToString();
            $Error .= $this->Errors->ToString();
            $Tpl->SetVar("Error", $Error);
            $Tpl->Parse("Error", false);
        }
        $CCSForm = $this->EditMode ? $this->ComponentName . ":" . "Edit" : $this->ComponentName;
        $this->HTMLFormAction = $FileName . "?" . CCAddParam(CCGetQueryString("QueryString", ""), "ccsForm", $CCSForm);
        $Tpl->SetVar("Action", $this->HTMLFormAction);
        $Tpl->SetVar("HTMLFormName", $this->ComponentName);
        $Tpl->SetVar("HTMLFormEnctype", $this->FormEnctype);

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow");
        if(!$this->Visible) {
            $Tpl->block_path = $ParentPath;
            return;
        }

        $this->s_Datum->Show();
        $this->s_Titel->Show();
        $this->s_Beschreibung->Show();
        $this->s_Website->Show();
        $this->Button_DoSearch->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
    }
//End Show Method

} //End internesSearch Class @2-FCB6E20C

class clsGridinternes { //internes class @9-D774B4BF

//Variables @9-51E13B2D

    // Public variables
    var $ComponentName;
    var $Visible;
    var $Errors;
    var $ErrorBlock;
    var $ds; var $PageSize;
    var $SorterName = "";
    var $SorterDirection = "";
    var $PageNumber;

    var $CCSEvents = "";
    var $CCSEventResult;

    var $RelativePath = "";

    // Grid Controls
    var $StaticControls; var $RowControls;
    var $Sorter_Internes_ID;
    var $Sorter_Reihenfolge;
    var $Sorter_Datum;
    var $Sorter_Titel;
    var $Sorter_Website;
    var $Navigator;
//End Variables

//Class_Initialize Event @9-B9173CBA
    function clsGridinternes($RelativePath = "")
    {
        global $FileName;
        $this->ComponentName = "internes";
        $this->Visible = True;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Grid internes";
        $this->ds = new clsinternesDataSource();
        $this->PageSize = CCGetParam($this->ComponentName . "PageSize", "");
        if(!is_numeric($this->PageSize) || !strlen($this->PageSize))
            $this->PageSize = 20;
        else
            $this->PageSize = intval($this->PageSize);
        if ($this->PageSize > 100)
            $this->PageSize = 100;
        if($this->PageSize == 0)
            $this->Errors->addError("<p>Form: Grid " . $this->ComponentName . "<br>Error: (CCS06) Invalid page size.</p>");
        $this->PageNumber = intval(CCGetParam($this->ComponentName . "Page", 1));
        $this->SorterName = CCGetParam("internesOrder", "");
        $this->SorterDirection = CCGetParam("internesDir", "");

        $this->Internes_ID = new clsControl(ccsLink, "Internes_ID", "Internes_ID", ccsInteger, "", CCGetRequestParam("Internes_ID", ccsGet));
        $this->Reihenfolge = new clsControl(ccsLabel, "Reihenfolge", "Reihenfolge", ccsInteger, "", CCGetRequestParam("Reihenfolge", ccsGet));
        $this->Datum = new clsControl(ccsLabel, "Datum", "Datum", ccsText, "", CCGetRequestParam("Datum", ccsGet));
        $this->Titel = new clsControl(ccsLabel, "Titel", "Titel", ccsText, "", CCGetRequestParam("Titel", ccsGet));
        $this->Website = new clsControl(ccsLabel, "Website", "Website", ccsText, "", CCGetRequestParam("Website", ccsGet));
        $this->Sorter_Internes_ID = new clsSorter($this->ComponentName, "Sorter_Internes_ID", $FileName);
        $this->Sorter_Reihenfolge = new clsSorter($this->ComponentName, "Sorter_Reihenfolge", $FileName);
        $this->Sorter_Datum = new clsSorter($this->ComponentName, "Sorter_Datum", $FileName);
        $this->Sorter_Titel = new clsSorter($this->ComponentName, "Sorter_Titel", $FileName);
        $this->Sorter_Website = new clsSorter($this->ComponentName, "Sorter_Website", $FileName);
        $this->internes_Insert = new clsControl(ccsLink, "internes_Insert", "internes_Insert", ccsText, "", CCGetRequestParam("internes_Insert", ccsGet));
        $this->internes_Insert->Parameters = CCGetQueryString("QueryString", Array("Internes_ID", "ccsForm"));
        $this->internes_Insert->Page = "internes_maint.php";
        $this->Navigator = new clsNavigator($this->ComponentName, "Navigator", $FileName, 10, tpSimple);
    }
//End Class_Initialize Event

//Initialize Method @9-03626367
    function Initialize()
    {
        if(!$this->Visible) return;

        $this->ds->PageSize = $this->PageSize;
        $this->ds->AbsolutePage = $this->PageNumber;
        $this->ds->SetOrder($this->SorterName, $this->SorterDirection);
    }
//End Initialize Method

//Show Method @9-20AEC911
    function Show()
    {
        global $Tpl;
        if(!$this->Visible) return;

        $ShownRecords = 0;

        $this->ds->Parameters["urls_Datum"] = CCGetFromGet("s_Datum", "");
        $this->ds->Parameters["urls_Titel"] = CCGetFromGet("s_Titel", "");
        $this->ds->Parameters["urls_Beschreibung"] = CCGetFromGet("s_Beschreibung", "");
        $this->ds->Parameters["urls_Website"] = CCGetFromGet("s_Website", "");

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect");


        $this->ds->Prepare();
        $this->ds->Open();

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow");
        if(!$this->Visible) return;

        $GridBlock = "Grid " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $GridBlock;


        $is_next_record = $this->ds->next_record();
        if($is_next_record && $ShownRecords < $this->PageSize)
        {
            do {
                    $this->ds->SetValues();
                $Tpl->block_path = $ParentPath . "/" . $GridBlock . "/Row";
                $this->Internes_ID->SetValue($this->ds->Internes_ID->GetValue());
                $this->Internes_ID->Parameters = CCGetQueryString("QueryString", Array("ccsForm"));
                $this->Internes_ID->Parameters = CCAddParam($this->Internes_ID->Parameters, "Internes_ID", $this->ds->f("Internes_ID"));
                $this->Internes_ID->Page = "internes_maint.php";
                $this->Reihenfolge->SetValue($this->ds->Reihenfolge->GetValue());
                $this->Datum->SetValue($this->ds->Datum->GetValue());
                $this->Titel->SetValue($this->ds->Titel->GetValue());
                $this->Website->SetValue($this->ds->Website->GetValue());
                $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShowRow");
                $this->Internes_ID->Show();
                $this->Reihenfolge->Show();
                $this->Datum->Show();
                $this->Titel->Show();
                $this->Website->Show();
                $Tpl->block_path = $ParentPath . "/" . $GridBlock;
                $Tpl->parse("Row", true);
                $ShownRecords++;
                $is_next_record = $this->ds->next_record();
            } while ($is_next_record && $ShownRecords < $this->PageSize);
        }
        else // Show NoRecords block if no records are found
        {
            $Tpl->parse("NoRecords", false);
        }

        $errors = $this->GetErrors();
        if(strlen($errors))
        {
            $Tpl->replaceblock("", $errors);
            $Tpl->block_path = $ParentPath;
            return;
        }
        $this->Navigator->PageNumber = $this->ds->AbsolutePage;
        $this->Navigator->TotalPages = $this->ds->PageCount();
        $this->Sorter_Internes_ID->Show();
        $this->Sorter_Reihenfolge->Show();
        $this->Sorter_Datum->Show();
        $this->Sorter_Titel->Show();
        $this->Sorter_Website->Show();
        $this->internes_Insert->Show();
        $this->Navigator->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
        $this->ds->close();
    }
//End Show Method

//GetErrors Method @9-11E109BA
    function GetErrors()
    {
        $errors = "";
        $errors .= $this->Internes_ID->Errors->ToString();
        $errors .= $this->Reihenfolge->Errors->ToString();
        $errors .= $this->Datum->Errors->ToString();
        $errors .= $this->Titel->Errors->ToString();
        $errors .= $this->Website->Errors->ToString();
        $errors .= $this->Errors->ToString();
        $errors .= $this->ds->Errors->ToString();
        return $errors;
    }
//End GetErrors Method

} //End internes Class @9-FCB6E20C

class clsinternesDataSource extends clsDBConnection1 {  //internesDataSource Class @9-BFABE371

//DataSource Variables @9-6D7B6B7F
    var $CCSEvents = "";
    var $CCSEventResult;
    var $ErrorBlock;
    var $CmdExecution;

    var $CountSQL;
    var $wp;


    // Datasource fields
    var $Internes_ID;
    var $Reihenfolge;
    var $Datum;
    var $Titel;
    var $Website;
//End DataSource Variables

//DataSourceClass_Initialize Event @9-B2A7567E
    function clsinternesDataSource()
    {
        $this->ErrorBlock = "Grid internes";
        $this->Initialize();
        $this->Internes_ID = new clsField("Internes_ID", ccsInteger, "");
        $this->Reihenfolge = new clsField("Reihenfolge", ccsInteger, "");
        $this->Datum = new clsField("Datum", ccsText, "");
        $this->Titel = new clsField("Titel", ccsText, "");
        $this->Website = new clsField("Website", ccsText, "");

    }
//End DataSourceClass_Initialize Event

//SetOrder Method @9-0A5B99C7
    function SetOrder($SorterName, $SorterDirection)
    {
        $this->Order = "";
        $this->Order = CCGetOrder($this->Order, $SorterName, $SorterDirection, 
            array("Sorter_Internes_ID" => array("Internes_ID", ""), 
            "Sorter_Reihenfolge" => array("Reihenfolge", ""), 
            "Sorter_Datum" => array("Datum", ""), 
            "Sorter_Titel" => array("Titel", ""), 
            "Sorter_Website" => array("Website", "")));
    }
//End SetOrder Method

//Prepare Method @9-12418C35
    function Prepare()
    {
        $this->wp = new clsSQLParameters($this->ErrorBlock);
        $this->wp->AddParameter("1", "urls_Datum", ccsText, "", "", $this->Parameters["urls_Datum"], "", false);
        $this->wp->AddParameter("2", "urls_Titel", ccsText, "", "", $this->Parameters["urls_Titel"], "", false);
        $this->wp->AddParameter("3", "urls_Beschreibung", ccsMemo, "", "", $this->Parameters["urls_Beschreibung"], "", false);
        $this->wp->AddParameter("4", "urls_Website", ccsText, "", "", $this->Parameters["urls_Website"], "", false);
        $this->wp->Criterion[1] = $this->wp->Operation(opContains, "Datum", $this->wp->GetDBValue("1"), $this->ToSQL($this->wp->GetDBValue("1"), ccsText),false);
        $this->wp->Criterion[2] = $this->wp->Operation(opContains, "Titel", $this->wp->GetDBValue("2"), $this->ToSQL($this->wp->GetDBValue("2"), ccsText),false);
        $this->wp->Criterion[3] = $this->wp->Operation(opContains, "Beschreibung", $this->wp->GetDBValue("3"), $this->ToSQL($this->wp->GetDBValue("3"), ccsMemo),false);
        $this->wp->Criterion[4] = $this->wp->Operation(opContains, "Website", $this->wp->GetDBValue("4"), $this->ToSQL($this->wp->GetDBValue("4"), ccsText),false);
        $this->Where = $this->wp->opAND(
             false, $this->wp->opAND(
             false, $this->wp->opAND(
             false, 
             $this->wp->Criterion[1], 
             $this->wp->Criterion[2]), 
             $this->wp->Criterion[3]), 
             $this->wp->Criterion[4]);
    }
//End Prepare Method

//Open Method @9-F1775BF8
    function Open()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildSelect");
        $this->CountSQL = "SELECT COUNT(*)  " .
        "FROM internes";
        $this->SQL = "SELECT internes.Internes_ID, internes.Reihenfolge, internes.Datum, internes.Titel, internes.Website  " .
        "FROM internes";
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteSelect");
        $this->RecordsCount = CCGetDBValue(CCBuildSQL($this->CountSQL, $this->Where, ""), $this);
        $this->query($this->OptimizeSQL(CCBuildSQL($this->SQL, $this->Where, $this->Order)));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteSelect");
    }
//End Open Method

//SetValues Method @9-08B5DD0B
    function SetValues()
    {
        $this->Internes_ID->SetDBValue(trim($this->f("Internes_ID")));
        $this->Reihenfolge->SetDBValue(trim($this->f("Reihenfolge")));
        $this->Datum->SetDBValue($this->f("Datum"));
        $this->Titel->SetDBValue($this->f("Titel"));
        $this->Website->SetDBValue($this->f("Website"));
    }
//End SetValues Method

} //End internesDataSource Class @9-FCB6E20C

//Initialize Page @1-1232EA4E
// Variables
$FileName = "";
$Redirect = "";
$Tpl = "";
$TemplateFileName = "";
$BlockToParse = "main";
$ComponentName = "";

// Events;
$CCSEvents = "";
$CCSEventResult = "";

$FileName = "internes_list.php";
$Redirect = "";
$TemplateFileName = "internes_list.html";
$BlockToParse = "main";
$TemplateEncoding = "";
$FileEncoding = "";
$PathToRoot = "../";
//End Initialize Page

//Authenticate User @1-C7A40A44
CCSecurityRedirect("3;4", "../login.php");
//End Authenticate User

//Initialize Objects @1-A09F1B2A
$DBConnection1 = new clsDBConnection1();

// Controls
$menu = new clsmenu("");
$menu->BindEvents();
$menu->Initialize();
$internesSearch = new clsRecordinternesSearch();
$internes = new clsGridinternes();
$internes->Initialize();

$CCSEventResult = CCGetEvent($CCSEvents, "AfterInitialize");

$Charset = $Charset ? $Charset : $TemplateEncoding;
if ($Charset)
    header("Content-Type: text/html; charset=" . $Charset);
//End Initialize Objects

//Initialize HTML Template @1-E2A5B61F
$CCSEventResult = CCGetEvent($CCSEvents, "OnInitializeView");
$Tpl = new clsTemplate($FileEncoding, $TemplateEncoding);
$Tpl->LoadTemplate(PathToCurrentPage . $TemplateFileName, $BlockToParse, $TemplateEncoding);
$Tpl->block_path = "/$BlockToParse";
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeShow");
//End Initialize HTML Template

//Execute Components @1-B25693E3
$menu->Operations();
$internesSearch->Operation();
//End Execute Components

//Go to destination page @1-20DD7EB8
if($Redirect)
{
    $CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload");
    $DBConnection1->close();
    header("Location: " . $Redirect);
    $menu->Class_Terminate();
    unset($menu);
    unset($internesSearch);
    unset($internes);
    unset($Tpl);
    exit;
}
//End Go to destination page

//Show Page @1-35F78B1D
$menu->Show("menu");
$internesSearch->Show();
$internes->Show();
$Tpl->block_path = "";
$Tpl->PParse("main", false);
//End Show Page

//Unload Page @1-956B3005
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload");
$DBConnection1->close();
$menu->Class_Terminate();
unset($menu);
unset($internesSearch);
unset($internes);
unset($Tpl);
//End Unload Page


?>
