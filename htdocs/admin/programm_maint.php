<?php
//Include Common Files @1-4EDE2568
define("RelativePath", "..");
define("PathToCurrentPage", "/admin/");
include(RelativePath . "/Common.php");
include(RelativePath . "/Template.php");
include(RelativePath . "/Sorter.php");
include(RelativePath . "/Navigator.php");
  
//End Include Common Files

//Include Page implementation @16-D18F3F45
include_once(RelativePath . "/admin/menu.php");
//End Include Page implementation

class clsRecordprogramm { //programm Class @2-6641D58D

//Variables @2-76058651

    // Public variables
    var $ComponentName;
    var $HTMLFormAction;
    var $PressedButton;
    var $Errors;
    var $ErrorBlock;
    var $FormSubmitted;
    var $FormEnctype;
    var $Visible;
    var $Recordset;

    var $CCSEvents = "";
    var $CCSEventResult;

    var $RelativePath = "";

    var $InsertAllowed = false;
    var $UpdateAllowed = false;
    var $DeleteAllowed = false;
    var $ReadAllowed   = false;
    var $EditMode      = false;
    var $ds;
    var $ValidatingControls;
    var $Controls;

    // Class variables
//End Variables

//Class_Initialize Event @2-1616C68E
    function clsRecordprogramm($RelativePath = "")
    {

        global $FileName;
        $this->Visible = true;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Record programm/Error";
        $this->ds = new clsprogrammDataSource();
        $this->InsertAllowed = true;
        $this->UpdateAllowed = true;
        $this->DeleteAllowed = true;
        $this->ReadAllowed = true;
        if($this->Visible)
        {
            $this->ComponentName = "programm";
            $CCSForm = split(":", CCGetFromGet("ccsForm", ""), 2);
            if(sizeof($CCSForm) == 1)
                $CCSForm[1] = "";
            list($FormName, $FormMethod) = $CCSForm;
            $this->EditMode = ($FormMethod == "Edit");
            $this->FormEnctype = "multipart/form-data";
            $this->FormSubmitted = ($FormName == $this->ComponentName);
            $Method = $this->FormSubmitted ? ccsPost : ccsGet;
            $this->Reihenfolge = new clsControl(ccsTextBox, "Reihenfolge", "Reihenfolge", ccsInteger, "", CCGetRequestParam("Reihenfolge", $Method));
            $this->Datum = new clsControl(ccsTextBox, "Datum", "Datum", ccsText, "", CCGetRequestParam("Datum", $Method));
            $this->Titel = new clsControl(ccsTextBox, "Titel", "Titel", ccsText, "", CCGetRequestParam("Titel", $Method));
            $this->Kurzbeschreibung = new clsControl(ccsTextArea, "Kurzbeschreibung", "Kurzbeschreibung", ccsMemo, "", CCGetRequestParam("Kurzbeschreibung", $Method));
            $this->Beschreibung = new clsControl(ccsTextArea, "Beschreibung", "Beschreibung", ccsMemo, "", CCGetRequestParam("Beschreibung", $Method));
            $this->Website = new clsControl(ccsTextBox, "Website", "Website", ccsText, "", CCGetRequestParam("Website", $Method));
            $this->FileUpload1 = new clsFileUpload("FileUpload1", "FileUpload1", "../TEMP/", "../fotos/", "*.jpg;*.jpeg;*.gif", "", 1000000);
            $this->FileUpload1->Required = true;
            $this->FileUpload2 = new clsFileUpload("FileUpload2", "FileUpload2", "../TEMP/", "../fotos/", "*.jpg;*.jpeg;*.gif", "", 1000000);
            $this->FileUpload2->Required = true;
            $this->FileUpload3 = new clsFileUpload("FileUpload3", "FileUpload3", "../TEMP/", "../fotos/", "*.jpg;*.jpeg;*.gif", "", 1000000);
            $this->FileUpload3->Required = true;
            $this->Button_Insert = new clsButton("Button_Insert");
            $this->Button_Update = new clsButton("Button_Update");
            $this->Button_Delete = new clsButton("Button_Delete");
        }
    }
//End Class_Initialize Event

//Initialize Method @2-1CF65D02
    function Initialize()
    {

        if(!$this->Visible)
            return;

        $this->ds->Parameters["urlProgramm_ID"] = CCGetFromGet("Programm_ID", "");
    }
//End Initialize Method

//Validate Method @2-556E4D48
    function Validate()
    {
        $Validation = true;
        $Where = "";
        $Validation = ($this->Reihenfolge->Validate() && $Validation);
        $Validation = ($this->Datum->Validate() && $Validation);
        $Validation = ($this->Titel->Validate() && $Validation);
        $Validation = ($this->Kurzbeschreibung->Validate() && $Validation);
        $Validation = ($this->Beschreibung->Validate() && $Validation);
        $Validation = ($this->Website->Validate() && $Validation);
        $Validation = ($this->FileUpload1->Validate() && $Validation);
        $Validation = ($this->FileUpload2->Validate() && $Validation);
        $Validation = ($this->FileUpload3->Validate() && $Validation);
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "OnValidate");
        $Validation =  $Validation && ($this->Reihenfolge->Errors->Count() == 0);
        $Validation =  $Validation && ($this->Datum->Errors->Count() == 0);
        $Validation =  $Validation && ($this->Titel->Errors->Count() == 0);
        $Validation =  $Validation && ($this->Kurzbeschreibung->Errors->Count() == 0);
        $Validation =  $Validation && ($this->Beschreibung->Errors->Count() == 0);
        $Validation =  $Validation && ($this->Website->Errors->Count() == 0);
        $Validation =  $Validation && ($this->FileUpload1->Errors->Count() == 0);
        $Validation =  $Validation && ($this->FileUpload2->Errors->Count() == 0);
        $Validation =  $Validation && ($this->FileUpload3->Errors->Count() == 0);
        return (($this->Errors->Count() == 0) && $Validation);
    }
//End Validate Method

//CheckErrors Method @2-995B3B77
    function CheckErrors()
    {
        $errors = false;
        $errors = ($errors || $this->Reihenfolge->Errors->Count());
        $errors = ($errors || $this->Datum->Errors->Count());
        $errors = ($errors || $this->Titel->Errors->Count());
        $errors = ($errors || $this->Kurzbeschreibung->Errors->Count());
        $errors = ($errors || $this->Beschreibung->Errors->Count());
        $errors = ($errors || $this->Website->Errors->Count());
        $errors = ($errors || $this->FileUpload1->Errors->Count());
        $errors = ($errors || $this->FileUpload2->Errors->Count());
        $errors = ($errors || $this->FileUpload3->Errors->Count());
        $errors = ($errors || $this->Errors->Count());
        $errors = ($errors || $this->ds->Errors->Count());
        return $errors;
    }
//End CheckErrors Method

//Operation Method @2-ABA82FD6
    function Operation()
    {
        if(!$this->Visible)
            return;

        global $Redirect;
        global $FileName;

        $this->ds->Prepare();
        if(!$this->FormSubmitted) {
            $this->EditMode = $this->ds->AllParametersSet;
            return;
        }

        $this->FileUpload1->Upload();
        $this->FileUpload2->Upload();
        $this->FileUpload3->Upload();

        if($this->FormSubmitted) {
            $this->PressedButton = $this->EditMode ? "Button_Update" : "Button_Insert";
            if(strlen(CCGetParam("Button_Insert", ""))) {
                $this->PressedButton = "Button_Insert";
            } else if(strlen(CCGetParam("Button_Update", ""))) {
                $this->PressedButton = "Button_Update";
            } else if(strlen(CCGetParam("Button_Delete", ""))) {
                $this->PressedButton = "Button_Delete";
            }
        }
        $Redirect = "programm_list.php" . "?" . CCGetQueryString("QueryString", Array("ccsForm"));
        if($this->PressedButton == "Button_Delete") {
            if(!CCGetEvent($this->Button_Delete->CCSEvents, "OnClick") || !$this->DeleteRow()) {
                $Redirect = "";
            }
        } else if($this->Validate()) {
            if($this->PressedButton == "Button_Insert") {
                if(!CCGetEvent($this->Button_Insert->CCSEvents, "OnClick") || !$this->InsertRow()) {
                    $Redirect = "";
                }
            } else if($this->PressedButton == "Button_Update") {
                if(!CCGetEvent($this->Button_Update->CCSEvents, "OnClick") || !$this->UpdateRow()) {
                    $Redirect = "";
                }
            }
        } else {
            $Redirect = "";
        }
    }
//End Operation Method

//InsertRow Method @2-92DBEDCF
    function InsertRow()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeInsert");
        if(!$this->InsertAllowed) return false;
        $this->ds->Reihenfolge->SetValue($this->Reihenfolge->GetValue());
        $this->ds->Datum->SetValue($this->Datum->GetValue());
        $this->ds->Titel->SetValue($this->Titel->GetValue());
        $this->ds->Kurzbeschreibung->SetValue($this->Kurzbeschreibung->GetValue());
        $this->ds->Beschreibung->SetValue($this->Beschreibung->GetValue());
        $this->ds->Website->SetValue($this->Website->GetValue());
        $this->ds->FileUpload1->SetValue($this->FileUpload1->GetValue());
        $this->ds->FileUpload2->SetValue($this->FileUpload2->GetValue());
        $this->ds->FileUpload3->SetValue($this->FileUpload3->GetValue());
        $this->ds->Insert();
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterInsert");
        if($this->ds->Errors->Count() == 0) {
            $this->FileUpload1->Move();
            $this->FileUpload2->Move();
            $this->FileUpload3->Move();
        }
        return (!$this->CheckErrors());
    }
//End InsertRow Method

//UpdateRow Method @2-F33747C6
    function UpdateRow()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeUpdate");
        if(!$this->UpdateAllowed) return false;
        $this->ds->Reihenfolge->SetValue($this->Reihenfolge->GetValue());
        $this->ds->Datum->SetValue($this->Datum->GetValue());
        $this->ds->Titel->SetValue($this->Titel->GetValue());
        $this->ds->Kurzbeschreibung->SetValue($this->Kurzbeschreibung->GetValue());
        $this->ds->Beschreibung->SetValue($this->Beschreibung->GetValue());
        $this->ds->Website->SetValue($this->Website->GetValue());
        $this->ds->FileUpload1->SetValue($this->FileUpload1->GetValue());
        $this->ds->FileUpload2->SetValue($this->FileUpload2->GetValue());
        $this->ds->FileUpload3->SetValue($this->FileUpload3->GetValue());
        $this->ds->Update();
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterUpdate");
        if($this->ds->Errors->Count() == 0) {
            $this->FileUpload1->Move();
            $this->FileUpload2->Move();
            $this->FileUpload3->Move();
        }
        return (!$this->CheckErrors());
    }
//End UpdateRow Method

//DeleteRow Method @2-FF1DDCFE
    function DeleteRow()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeDelete");
        if(!$this->DeleteAllowed) return false;
        $this->ds->Delete();
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterDelete");
        if($this->ds->Errors->Count() == 0) {
            $this->FileUpload1->Delete();
            $this->FileUpload2->Delete();
            $this->FileUpload3->Delete();
        }
        return (!$this->CheckErrors());
    }
//End DeleteRow Method

//Show Method @2-12F2233B
    function Show()
    {
        global $Tpl;
        global $FileName;
        $Error = "";

        if(!$this->Visible)
            return;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect");


        $RecordBlock = "Record " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $RecordBlock;
        $this->EditMode = $this->EditMode && $this->ReadAllowed;
        if($this->EditMode) {
            if($this->ds->Errors->Count()){
                $this->Errors->AddErrors($this->ds->Errors);
                $this->ds->Errors->clear();
            }
            $this->ds->open();
            if($this->ds->Errors->Count() == 0 && $this->ds->next_record()) {
                $this->ds->SetValues();
                if(!$this->FormSubmitted){
                    $this->Reihenfolge->SetValue($this->ds->Reihenfolge->GetValue());
                    $this->Datum->SetValue($this->ds->Datum->GetValue());
                    $this->Titel->SetValue($this->ds->Titel->GetValue());
                    $this->Kurzbeschreibung->SetValue($this->ds->Kurzbeschreibung->GetValue());
                    $this->Beschreibung->SetValue($this->ds->Beschreibung->GetValue());
                    $this->Website->SetValue($this->ds->Website->GetValue());
                    $this->FileUpload1->SetValue($this->ds->FileUpload1->GetValue());
                    $this->FileUpload2->SetValue($this->ds->FileUpload2->GetValue());
                    $this->FileUpload3->SetValue($this->ds->FileUpload3->GetValue());
                }
            } else {
                $this->EditMode = false;
            }
        }

        if($this->FormSubmitted || $this->CheckErrors()) {
            $Error .= $this->Reihenfolge->Errors->ToString();
            $Error .= $this->Datum->Errors->ToString();
            $Error .= $this->Titel->Errors->ToString();
            $Error .= $this->Kurzbeschreibung->Errors->ToString();
            $Error .= $this->Beschreibung->Errors->ToString();
            $Error .= $this->Website->Errors->ToString();
            $Error .= $this->FileUpload1->Errors->ToString();
            $Error .= $this->FileUpload2->Errors->ToString();
            $Error .= $this->FileUpload3->Errors->ToString();
            $Error .= $this->Errors->ToString();
            $Error .= $this->ds->Errors->ToString();
            $Tpl->SetVar("Error", $Error);
            $Tpl->Parse("Error", false);
        }
        $CCSForm = $this->EditMode ? $this->ComponentName . ":" . "Edit" : $this->ComponentName;
        $this->HTMLFormAction = $FileName . "?" . CCAddParam(CCGetQueryString("QueryString", ""), "ccsForm", $CCSForm);
        $Tpl->SetVar("Action", $this->HTMLFormAction);
        $Tpl->SetVar("HTMLFormName", $this->ComponentName);
        $Tpl->SetVar("HTMLFormEnctype", $this->FormEnctype);
        $this->Button_Insert->Visible = !$this->EditMode && $this->InsertAllowed;
        $this->Button_Update->Visible = $this->EditMode && $this->UpdateAllowed;
        $this->Button_Delete->Visible = $this->EditMode && $this->DeleteAllowed;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow");
        if(!$this->Visible) {
            $Tpl->block_path = $ParentPath;
            return;
        }

        $this->Reihenfolge->Show();
        $this->Datum->Show();
        $this->Titel->Show();
        $this->Kurzbeschreibung->Show();
        $this->Beschreibung->Show();
        $this->Website->Show();
        $this->FileUpload1->Show();
        $this->FileUpload2->Show();
        $this->FileUpload3->Show();
        $this->Button_Insert->Show();
        $this->Button_Update->Show();
        $this->Button_Delete->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
        $this->ds->close();
    }
//End Show Method

} //End programm Class @2-FCB6E20C

class clsprogrammDataSource extends clsDBConnection1 {  //programmDataSource Class @2-8D6C1F3D

//DataSource Variables @2-1EF02DAC
    var $CCSEvents = "";
    var $CCSEventResult;
    var $ErrorBlock;
    var $CmdExecution;

    var $InsertParameters;
    var $UpdateParameters;
    var $DeleteParameters;
    var $wp;
    var $AllParametersSet;


    // Datasource fields
    var $Reihenfolge;
    var $Datum;
    var $Titel;
    var $Kurzbeschreibung;
    var $Beschreibung;
    var $Website;
    var $FileUpload1;
    var $FileUpload2;
    var $FileUpload3;
//End DataSource Variables

//DataSourceClass_Initialize Event @2-30289506
    function clsprogrammDataSource()
    {
        $this->ErrorBlock = "Record programm/Error";
        $this->Initialize();
        $this->Reihenfolge = new clsField("Reihenfolge", ccsInteger, "");
        $this->Datum = new clsField("Datum", ccsText, "");
        $this->Titel = new clsField("Titel", ccsText, "");
        $this->Kurzbeschreibung = new clsField("Kurzbeschreibung", ccsMemo, "");
        $this->Beschreibung = new clsField("Beschreibung", ccsMemo, "");
        $this->Website = new clsField("Website", ccsText, "");
        $this->FileUpload1 = new clsField("FileUpload1", ccsText, "");
        $this->FileUpload2 = new clsField("FileUpload2", ccsText, "");
        $this->FileUpload3 = new clsField("FileUpload3", ccsText, "");

    }
//End DataSourceClass_Initialize Event

//Prepare Method @2-1BA4CE4A
    function Prepare()
    {
        $this->wp = new clsSQLParameters($this->ErrorBlock);
        $this->wp->AddParameter("1", "urlProgramm_ID", ccsInteger, "", "", $this->Parameters["urlProgramm_ID"], "", false);
        $this->AllParametersSet = $this->wp->AllParamsSet();
        $this->wp->Criterion[1] = $this->wp->Operation(opEqual, "Programm_ID", $this->wp->GetDBValue("1"), $this->ToSQL($this->wp->GetDBValue("1"), ccsInteger),false);
        $this->Where = 
             $this->wp->Criterion[1];
    }
//End Prepare Method

//Open Method @2-74EF453F
    function Open()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildSelect");
        $this->SQL = "SELECT *  " .
        "FROM programm";
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteSelect");
        $this->PageSize = 1;
        $this->query($this->OptimizeSQL(CCBuildSQL($this->SQL, $this->Where, $this->Order)));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteSelect");
    }
//End Open Method

//SetValues Method @2-22DB1B52
    function SetValues()
    {
        $this->Reihenfolge->SetDBValue(trim($this->f("Reihenfolge")));
        $this->Datum->SetDBValue($this->f("Datum"));
        $this->Titel->SetDBValue($this->f("Titel"));
        $this->Kurzbeschreibung->SetDBValue($this->f("Kurzbeschreibung"));
        $this->Beschreibung->SetDBValue($this->f("Beschreibung"));
        $this->Website->SetDBValue($this->f("Website"));
        $this->FileUpload1->SetDBValue($this->f("Foto1"));
        $this->FileUpload2->SetDBValue($this->f("Foto2"));
        $this->FileUpload3->SetDBValue($this->f("Foto3"));
    }
//End SetValues Method

//Insert Method @2-CC9D33AF
    function Insert()
    {
        $this->CmdExecution = true;
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildInsert");
        $this->SQL = "INSERT INTO programm ("
             . "Reihenfolge, "
             . "Datum, "
             . "Titel, "
             . "Kurzbeschreibung, "
             . "Beschreibung, "
             . "Website, "
             . "Foto1, "
             . "Foto2, "
             . "Foto3"
             . ") VALUES ("
             . $this->ToSQL($this->Reihenfolge->GetDBValue(), $this->Reihenfolge->DataType) . ", "
             . $this->ToSQL($this->Datum->GetDBValue(), $this->Datum->DataType) . ", "
             . $this->ToSQL($this->Titel->GetDBValue(), $this->Titel->DataType) . ", "
             . $this->ToSQL($this->Kurzbeschreibung->GetDBValue(), $this->Kurzbeschreibung->DataType) . ", "
             . $this->ToSQL($this->Beschreibung->GetDBValue(), $this->Beschreibung->DataType) . ", "
             . $this->ToSQL($this->Website->GetDBValue(), $this->Website->DataType) . ", "
             . $this->ToSQL($this->FileUpload1->GetDBValue(), $this->FileUpload1->DataType) . ", "
             . $this->ToSQL($this->FileUpload2->GetDBValue(), $this->FileUpload2->DataType) . ", "
             . $this->ToSQL($this->FileUpload3->GetDBValue(), $this->FileUpload3->DataType)
             . ")";
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteInsert");
        if($this->Errors->Count() == 0 && $this->CmdExecution) {
            $this->query($this->SQL);
            $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteInsert");
        }
        $this->close();
    }
//End Insert Method

//Update Method @2-DF0707C7
    function Update()
    {
        $this->CmdExecution = true;
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildUpdate");
        $this->SQL = "UPDATE programm SET "
             . "Reihenfolge=" . $this->ToSQL($this->Reihenfolge->GetDBValue(), $this->Reihenfolge->DataType) . ", "
             . "Datum=" . $this->ToSQL($this->Datum->GetDBValue(), $this->Datum->DataType) . ", "
             . "Titel=" . $this->ToSQL($this->Titel->GetDBValue(), $this->Titel->DataType) . ", "
             . "Kurzbeschreibung=" . $this->ToSQL($this->Kurzbeschreibung->GetDBValue(), $this->Kurzbeschreibung->DataType) . ", "
             . "Beschreibung=" . $this->ToSQL($this->Beschreibung->GetDBValue(), $this->Beschreibung->DataType) . ", "
             . "Website=" . $this->ToSQL($this->Website->GetDBValue(), $this->Website->DataType) . ", "
             . "Foto1=" . $this->ToSQL($this->FileUpload1->GetDBValue(), $this->FileUpload1->DataType) . ", "
             . "Foto2=" . $this->ToSQL($this->FileUpload2->GetDBValue(), $this->FileUpload2->DataType) . ", "
             . "Foto3=" . $this->ToSQL($this->FileUpload3->GetDBValue(), $this->FileUpload3->DataType);
        $this->SQL = CCBuildSQL($this->SQL, $this->Where, "");
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteUpdate");
        if($this->Errors->Count() == 0 && $this->CmdExecution) {
            $this->query($this->SQL);
            $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteUpdate");
        }
        $this->close();
    }
//End Update Method

//Delete Method @2-41F75CDA
    function Delete()
    {
        $this->CmdExecution = true;
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildDelete");
        $this->SQL = "DELETE FROM programm";
        $this->SQL = CCBuildSQL($this->SQL, $this->Where, "");
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteDelete");
        if($this->Errors->Count() == 0 && $this->CmdExecution) {
            $this->query($this->SQL);
            $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteDelete");
        }
        $this->close();
    }
//End Delete Method

} //End programmDataSource Class @2-FCB6E20C

//Initialize Page @1-8AF3EC6C
// Variables
$FileName = "";
$Redirect = "";
$Tpl = "";
$TemplateFileName = "";
$BlockToParse = "main";
$ComponentName = "";

// Events;
$CCSEvents = "";
$CCSEventResult = "";

$FileName = "programm_maint.php";
$Redirect = "";
$TemplateFileName = "programm_maint.html";
$BlockToParse = "main";
$TemplateEncoding = "";
$FileEncoding = "";
$PathToRoot = "../";
//End Initialize Page

//Authenticate User @1-C7A40A44
CCSecurityRedirect("3;4", "../login.php");
//End Authenticate User

//Initialize Objects @1-E677E5B4
$DBConnection1 = new clsDBConnection1();

// Controls
$menu = new clsmenu("");
$menu->BindEvents();
$menu->Initialize();
$programm = new clsRecordprogramm();
$programm->Initialize();

$CCSEventResult = CCGetEvent($CCSEvents, "AfterInitialize");

$Charset = $Charset ? $Charset : $TemplateEncoding;
if ($Charset)
    header("Content-Type: text/html; charset=" . $Charset);
//End Initialize Objects

//Initialize HTML Template @1-E2A5B61F
$CCSEventResult = CCGetEvent($CCSEvents, "OnInitializeView");
$Tpl = new clsTemplate($FileEncoding, $TemplateEncoding);
$Tpl->LoadTemplate(PathToCurrentPage . $TemplateFileName, $BlockToParse, $TemplateEncoding);
$Tpl->block_path = "/$BlockToParse";
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeShow");
//End Initialize HTML Template

//Execute Components @1-4E70CD0D
$menu->Operations();
$programm->Operation();
//End Execute Components

//Go to destination page @1-C07DB1E8
if($Redirect)
{
    $CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload");
    $DBConnection1->close();
    header("Location: " . $Redirect);
    $menu->Class_Terminate();
    unset($menu);
    unset($programm);
    unset($Tpl);
    exit;
}
//End Go to destination page

//Show Page @1-2CD73099
$menu->Show("menu");
$programm->Show();
$Tpl->block_path = "";
$Tpl->PParse("main", false);
//End Show Page

//Unload Page @1-B0945E04
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload");
$DBConnection1->close();
$menu->Class_Terminate();
unset($menu);
unset($programm);
unset($Tpl);
//End Unload Page


?>
