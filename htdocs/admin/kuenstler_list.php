<?php
//Include Common Files @1-4EDE2568
define("RelativePath", "..");
define("PathToCurrentPage", "/admin/");
include(RelativePath . "/Common.php");
include(RelativePath . "/Template.php");
include(RelativePath . "/Sorter.php");
include(RelativePath . "/Navigator.php");
  
//End Include Common Files

//Include Page implementation @76-D18F3F45
include_once(RelativePath . "/admin/menu.php");
//End Include Page implementation

class clsRecordkuenstlerSearch { //kuenstlerSearch Class @2-3F8C5B1C

//Variables @2-76058651

    // Public variables
    var $ComponentName;
    var $HTMLFormAction;
    var $PressedButton;
    var $Errors;
    var $ErrorBlock;
    var $FormSubmitted;
    var $FormEnctype;
    var $Visible;
    var $Recordset;

    var $CCSEvents = "";
    var $CCSEventResult;

    var $RelativePath = "";

    var $InsertAllowed = false;
    var $UpdateAllowed = false;
    var $DeleteAllowed = false;
    var $ReadAllowed   = false;
    var $EditMode      = false;
    var $ds;
    var $ValidatingControls;
    var $Controls;

    // Class variables
//End Variables

//Class_Initialize Event @2-2AEBCFF4
    function clsRecordkuenstlerSearch($RelativePath = "")
    {

        global $FileName;
        $this->Visible = true;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Record kuenstlerSearch/Error";
        $this->ReadAllowed = true;
        if($this->Visible)
        {
            $this->ComponentName = "kuenstlerSearch";
            $CCSForm = split(":", CCGetFromGet("ccsForm", ""), 2);
            if(sizeof($CCSForm) == 1)
                $CCSForm[1] = "";
            list($FormName, $FormMethod) = $CCSForm;
            $this->FormEnctype = "application/x-www-form-urlencoded";
            $this->FormSubmitted = ($FormName == $this->ComponentName);
            $Method = $this->FormSubmitted ? ccsPost : ccsGet;
            $this->s_Name = new clsControl(ccsTextBox, "s_Name", "s_Name", ccsText, "", CCGetRequestParam("s_Name", $Method));
            $this->s_Vorname = new clsControl(ccsTextBox, "s_Vorname", "s_Vorname", ccsText, "", CCGetRequestParam("s_Vorname", $Method));
            $this->s_Adresse = new clsControl(ccsTextBox, "s_Adresse", "s_Adresse", ccsText, "", CCGetRequestParam("s_Adresse", $Method));
            $this->s_PLZ = new clsControl(ccsTextBox, "s_PLZ", "s_PLZ", ccsText, "", CCGetRequestParam("s_PLZ", $Method));
            $this->s_Stadt = new clsControl(ccsTextBox, "s_Stadt", "s_Stadt", ccsText, "", CCGetRequestParam("s_Stadt", $Method));
            $this->s_Telefon = new clsControl(ccsTextBox, "s_Telefon", "s_Telefon", ccsText, "", CCGetRequestParam("s_Telefon", $Method));
            $this->s_Webseite = new clsControl(ccsTextBox, "s_Webseite", "s_Webseite", ccsText, "", CCGetRequestParam("s_Webseite", $Method));
            $this->s_Foto = new clsControl(ccsTextBox, "s_Foto", "s_Foto", ccsText, "", CCGetRequestParam("s_Foto", $Method));
            $this->s_Vita = new clsControl(ccsTextBox, "s_Vita", "s_Vita", ccsMemo, "", CCGetRequestParam("s_Vita", $Method));
            $this->s_Kategorie = new clsControl(ccsTextBox, "s_Kategorie", "s_Kategorie", ccsText, "", CCGetRequestParam("s_Kategorie", $Method));
            $this->s_Arbeitsbeispiele = new clsControl(ccsTextBox, "s_Arbeitsbeispiele", "s_Arbeitsbeispiele", ccsText, "", CCGetRequestParam("s_Arbeitsbeispiele", $Method));
            $this->s_Austellungstermine = new clsControl(ccsTextBox, "s_Austellungstermine", "s_Austellungstermine", ccsText, "", CCGetRequestParam("s_Austellungstermine", $Method));
            $this->Button_DoSearch = new clsButton("Button_DoSearch");
        }
    }
//End Class_Initialize Event

//Validate Method @2-C90114A0
    function Validate()
    {
        $Validation = true;
        $Where = "";
        $Validation = ($this->s_Name->Validate() && $Validation);
        $Validation = ($this->s_Vorname->Validate() && $Validation);
        $Validation = ($this->s_Adresse->Validate() && $Validation);
        $Validation = ($this->s_PLZ->Validate() && $Validation);
        $Validation = ($this->s_Stadt->Validate() && $Validation);
        $Validation = ($this->s_Telefon->Validate() && $Validation);
        $Validation = ($this->s_Webseite->Validate() && $Validation);
        $Validation = ($this->s_Foto->Validate() && $Validation);
        $Validation = ($this->s_Vita->Validate() && $Validation);
        $Validation = ($this->s_Kategorie->Validate() && $Validation);
        $Validation = ($this->s_Arbeitsbeispiele->Validate() && $Validation);
        $Validation = ($this->s_Austellungstermine->Validate() && $Validation);
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "OnValidate");
        $Validation =  $Validation && ($this->s_Name->Errors->Count() == 0);
        $Validation =  $Validation && ($this->s_Vorname->Errors->Count() == 0);
        $Validation =  $Validation && ($this->s_Adresse->Errors->Count() == 0);
        $Validation =  $Validation && ($this->s_PLZ->Errors->Count() == 0);
        $Validation =  $Validation && ($this->s_Stadt->Errors->Count() == 0);
        $Validation =  $Validation && ($this->s_Telefon->Errors->Count() == 0);
        $Validation =  $Validation && ($this->s_Webseite->Errors->Count() == 0);
        $Validation =  $Validation && ($this->s_Foto->Errors->Count() == 0);
        $Validation =  $Validation && ($this->s_Vita->Errors->Count() == 0);
        $Validation =  $Validation && ($this->s_Kategorie->Errors->Count() == 0);
        $Validation =  $Validation && ($this->s_Arbeitsbeispiele->Errors->Count() == 0);
        $Validation =  $Validation && ($this->s_Austellungstermine->Errors->Count() == 0);
        return (($this->Errors->Count() == 0) && $Validation);
    }
//End Validate Method

//CheckErrors Method @2-64945ECB
    function CheckErrors()
    {
        $errors = false;
        $errors = ($errors || $this->s_Name->Errors->Count());
        $errors = ($errors || $this->s_Vorname->Errors->Count());
        $errors = ($errors || $this->s_Adresse->Errors->Count());
        $errors = ($errors || $this->s_PLZ->Errors->Count());
        $errors = ($errors || $this->s_Stadt->Errors->Count());
        $errors = ($errors || $this->s_Telefon->Errors->Count());
        $errors = ($errors || $this->s_Webseite->Errors->Count());
        $errors = ($errors || $this->s_Foto->Errors->Count());
        $errors = ($errors || $this->s_Vita->Errors->Count());
        $errors = ($errors || $this->s_Kategorie->Errors->Count());
        $errors = ($errors || $this->s_Arbeitsbeispiele->Errors->Count());
        $errors = ($errors || $this->s_Austellungstermine->Errors->Count());
        $errors = ($errors || $this->Errors->Count());
        return $errors;
    }
//End CheckErrors Method

//Operation Method @2-04BA203B
    function Operation()
    {
        if(!$this->Visible)
            return;

        global $Redirect;
        global $FileName;

        if(!$this->FormSubmitted) {
            return;
        }

        if($this->FormSubmitted) {
            $this->PressedButton = "Button_DoSearch";
            if(strlen(CCGetParam("Button_DoSearch", ""))) {
                $this->PressedButton = "Button_DoSearch";
            }
        }
        $Redirect = "kuenstler_list.php";
        if($this->Validate()) {
            if($this->PressedButton == "Button_DoSearch") {
                if(!CCGetEvent($this->Button_DoSearch->CCSEvents, "OnClick")) {
                    $Redirect = "";
                } else {
                    $Redirect = "kuenstler_list.php" . "?" . CCMergeQueryStrings(CCGetQueryString("Form", Array("Button_DoSearch")));
                }
            }
        } else {
            $Redirect = "";
        }
    }
//End Operation Method

//Show Method @2-B0C86237
    function Show()
    {
        global $Tpl;
        global $FileName;
        $Error = "";

        if(!$this->Visible)
            return;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect");


        $RecordBlock = "Record " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $RecordBlock;
        $this->EditMode = $this->EditMode && $this->ReadAllowed;
        if (!$this->FormSubmitted) {
        }

        if($this->FormSubmitted || $this->CheckErrors()) {
            $Error .= $this->s_Name->Errors->ToString();
            $Error .= $this->s_Vorname->Errors->ToString();
            $Error .= $this->s_Adresse->Errors->ToString();
            $Error .= $this->s_PLZ->Errors->ToString();
            $Error .= $this->s_Stadt->Errors->ToString();
            $Error .= $this->s_Telefon->Errors->ToString();
            $Error .= $this->s_Webseite->Errors->ToString();
            $Error .= $this->s_Foto->Errors->ToString();
            $Error .= $this->s_Vita->Errors->ToString();
            $Error .= $this->s_Kategorie->Errors->ToString();
            $Error .= $this->s_Arbeitsbeispiele->Errors->ToString();
            $Error .= $this->s_Austellungstermine->Errors->ToString();
            $Error .= $this->Errors->ToString();
            $Tpl->SetVar("Error", $Error);
            $Tpl->Parse("Error", false);
        }
        $CCSForm = $this->EditMode ? $this->ComponentName . ":" . "Edit" : $this->ComponentName;
        $this->HTMLFormAction = $FileName . "?" . CCAddParam(CCGetQueryString("QueryString", ""), "ccsForm", $CCSForm);
        $Tpl->SetVar("Action", $this->HTMLFormAction);
        $Tpl->SetVar("HTMLFormName", $this->ComponentName);
        $Tpl->SetVar("HTMLFormEnctype", $this->FormEnctype);

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow");
        if(!$this->Visible) {
            $Tpl->block_path = $ParentPath;
            return;
        }

        $this->s_Name->Show();
        $this->s_Vorname->Show();
        $this->s_Adresse->Show();
        $this->s_PLZ->Show();
        $this->s_Stadt->Show();
        $this->s_Telefon->Show();
        $this->s_Webseite->Show();
        $this->s_Foto->Show();
        $this->s_Vita->Show();
        $this->s_Kategorie->Show();
        $this->s_Arbeitsbeispiele->Show();
        $this->s_Austellungstermine->Show();
        $this->Button_DoSearch->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
    }
//End Show Method

} //End kuenstlerSearch Class @2-FCB6E20C

class clsGridkuenstler { //kuenstler class @18-83C783C9

//Variables @18-540213C3

    // Public variables
    var $ComponentName;
    var $Visible;
    var $Errors;
    var $ErrorBlock;
    var $ds; var $PageSize;
    var $SorterName = "";
    var $SorterDirection = "";
    var $PageNumber;

    var $CCSEvents = "";
    var $CCSEventResult;

    var $RelativePath = "";

    // Grid Controls
    var $StaticControls; var $RowControls;
    var $Sorter_Kuenstler_ID;
    var $Sorter_Name;
    var $Sorter_Vorname;
    var $Sorter_Stadt;
    var $Sorter_Telefon;
    var $Sorter_Webseite;
    var $Sorter_Foto;
    var $Sorter_Austellungstermine;
    var $Navigator;
//End Variables

//Class_Initialize Event @18-4F535726
    function clsGridkuenstler($RelativePath = "")
    {
        global $FileName;
        $this->ComponentName = "kuenstler";
        $this->Visible = True;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Grid kuenstler";
        $this->ds = new clskuenstlerDataSource();
        $this->PageSize = CCGetParam($this->ComponentName . "PageSize", "");
        if(!is_numeric($this->PageSize) || !strlen($this->PageSize))
            $this->PageSize = 20;
        else
            $this->PageSize = intval($this->PageSize);
        if ($this->PageSize > 100)
            $this->PageSize = 100;
        if($this->PageSize == 0)
            $this->Errors->addError("<p>Form: Grid " . $this->ComponentName . "<br>Error: (CCS06) Invalid page size.</p>");
        $this->PageNumber = intval(CCGetParam($this->ComponentName . "Page", 1));
        $this->SorterName = CCGetParam("kuenstlerOrder", "");
        $this->SorterDirection = CCGetParam("kuenstlerDir", "");

        $this->Kuenstler_ID = new clsControl(ccsLink, "Kuenstler_ID", "Kuenstler_ID", ccsInteger, "", CCGetRequestParam("Kuenstler_ID", ccsGet));
        $this->Name = new clsControl(ccsLabel, "Name", "Name", ccsText, "", CCGetRequestParam("Name", ccsGet));
        $this->Vorname = new clsControl(ccsLabel, "Vorname", "Vorname", ccsText, "", CCGetRequestParam("Vorname", ccsGet));
        $this->Stadt = new clsControl(ccsLabel, "Stadt", "Stadt", ccsText, "", CCGetRequestParam("Stadt", ccsGet));
        $this->Telefon = new clsControl(ccsLabel, "Telefon", "Telefon", ccsText, "", CCGetRequestParam("Telefon", ccsGet));
        $this->Webseite = new clsControl(ccsLabel, "Webseite", "Webseite", ccsText, "", CCGetRequestParam("Webseite", ccsGet));
        $this->Foto = new clsControl(ccsImage, "Foto", "Foto", ccsText, "", CCGetRequestParam("Foto", ccsGet));
        $this->Austellungstermine = new clsControl(ccsLabel, "Austellungstermine", "Austellungstermine", ccsText, "", CCGetRequestParam("Austellungstermine", ccsGet));
        $this->Sorter_Kuenstler_ID = new clsSorter($this->ComponentName, "Sorter_Kuenstler_ID", $FileName);
        $this->Sorter_Name = new clsSorter($this->ComponentName, "Sorter_Name", $FileName);
        $this->Sorter_Vorname = new clsSorter($this->ComponentName, "Sorter_Vorname", $FileName);
        $this->Sorter_Stadt = new clsSorter($this->ComponentName, "Sorter_Stadt", $FileName);
        $this->Sorter_Telefon = new clsSorter($this->ComponentName, "Sorter_Telefon", $FileName);
        $this->Sorter_Webseite = new clsSorter($this->ComponentName, "Sorter_Webseite", $FileName);
        $this->Sorter_Foto = new clsSorter($this->ComponentName, "Sorter_Foto", $FileName);
        $this->Sorter_Austellungstermine = new clsSorter($this->ComponentName, "Sorter_Austellungstermine", $FileName);
        $this->kuenstler_Insert = new clsControl(ccsLink, "kuenstler_Insert", "kuenstler_Insert", ccsText, "", CCGetRequestParam("kuenstler_Insert", ccsGet));
        $this->kuenstler_Insert->Parameters = CCGetQueryString("QueryString", Array("Kuenstler_ID", "ccsForm"));
        $this->kuenstler_Insert->Page = "kuenstler_maint.php";
        $this->Navigator = new clsNavigator($this->ComponentName, "Navigator", $FileName, 10, tpSimple);
    }
//End Class_Initialize Event

//Initialize Method @18-03626367
    function Initialize()
    {
        if(!$this->Visible) return;

        $this->ds->PageSize = $this->PageSize;
        $this->ds->AbsolutePage = $this->PageNumber;
        $this->ds->SetOrder($this->SorterName, $this->SorterDirection);
    }
//End Initialize Method

//Show Method @18-EC1E700C
    function Show()
    {
        global $Tpl;
        if(!$this->Visible) return;

        $ShownRecords = 0;

        $this->ds->Parameters["urls_Name"] = CCGetFromGet("s_Name", "");
        $this->ds->Parameters["urls_Vorname"] = CCGetFromGet("s_Vorname", "");
        $this->ds->Parameters["urls_Adresse"] = CCGetFromGet("s_Adresse", "");
        $this->ds->Parameters["urls_PLZ"] = CCGetFromGet("s_PLZ", "");
        $this->ds->Parameters["urls_Stadt"] = CCGetFromGet("s_Stadt", "");
        $this->ds->Parameters["urls_Telefon"] = CCGetFromGet("s_Telefon", "");
        $this->ds->Parameters["urls_Webseite"] = CCGetFromGet("s_Webseite", "");
        $this->ds->Parameters["urls_Foto"] = CCGetFromGet("s_Foto", "");
        $this->ds->Parameters["urls_Vita"] = CCGetFromGet("s_Vita", "");
        $this->ds->Parameters["urls_Kategorie"] = CCGetFromGet("s_Kategorie", "");
        $this->ds->Parameters["urls_Arbeitsbeispiele"] = CCGetFromGet("s_Arbeitsbeispiele", "");
        $this->ds->Parameters["urls_Austellungstermine"] = CCGetFromGet("s_Austellungstermine", "");
        $this->ds->Parameters["expr92"] = 3;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect");


        $this->ds->Prepare();
        $this->ds->Open();

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow");
        if(!$this->Visible) return;

        $GridBlock = "Grid " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $GridBlock;


        $is_next_record = $this->ds->next_record();
        if($is_next_record && $ShownRecords < $this->PageSize)
        {
            do {
                    $this->ds->SetValues();
                $Tpl->block_path = $ParentPath . "/" . $GridBlock . "/Row";
                $this->Kuenstler_ID->SetValue($this->ds->Kuenstler_ID->GetValue());
                $this->Kuenstler_ID->Parameters = CCGetQueryString("QueryString", Array("ccsForm"));
                $this->Kuenstler_ID->Parameters = CCAddParam($this->Kuenstler_ID->Parameters, "Kuenstler_ID", $this->ds->f("Kuenstler_ID"));
                $this->Kuenstler_ID->Page = "kuenstler_maint.php";
                $this->Name->SetValue($this->ds->Name->GetValue());
                $this->Vorname->SetValue($this->ds->Vorname->GetValue());
                $this->Stadt->SetValue($this->ds->Stadt->GetValue());
                $this->Telefon->SetValue($this->ds->Telefon->GetValue());
                $this->Webseite->SetValue($this->ds->Webseite->GetValue());
                $this->Foto->SetValue($this->ds->Foto->GetValue());
                $this->Austellungstermine->SetValue($this->ds->Austellungstermine->GetValue());
                $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShowRow");
                $this->Kuenstler_ID->Show();
                $this->Name->Show();
                $this->Vorname->Show();
                $this->Stadt->Show();
                $this->Telefon->Show();
                $this->Webseite->Show();
                $this->Foto->Show();
                $this->Austellungstermine->Show();
                $Tpl->block_path = $ParentPath . "/" . $GridBlock;
                $Tpl->parse("Row", true);
                $ShownRecords++;
                $is_next_record = $this->ds->next_record();
            } while ($is_next_record && $ShownRecords < $this->PageSize);
        }
        else // Show NoRecords block if no records are found
        {
            $Tpl->parse("NoRecords", false);
        }

        $errors = $this->GetErrors();
        if(strlen($errors))
        {
            $Tpl->replaceblock("", $errors);
            $Tpl->block_path = $ParentPath;
            return;
        }
        $this->Navigator->PageNumber = $this->ds->AbsolutePage;
        $this->Navigator->TotalPages = $this->ds->PageCount();
        $this->Sorter_Kuenstler_ID->Show();
        $this->Sorter_Name->Show();
        $this->Sorter_Vorname->Show();
        $this->Sorter_Stadt->Show();
        $this->Sorter_Telefon->Show();
        $this->Sorter_Webseite->Show();
        $this->Sorter_Foto->Show();
        $this->Sorter_Austellungstermine->Show();
        $this->kuenstler_Insert->Show();
        $this->Navigator->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
        $this->ds->close();
    }
//End Show Method

//GetErrors Method @18-0E2D6B6A
    function GetErrors()
    {
        $errors = "";
        $errors .= $this->Kuenstler_ID->Errors->ToString();
        $errors .= $this->Name->Errors->ToString();
        $errors .= $this->Vorname->Errors->ToString();
        $errors .= $this->Stadt->Errors->ToString();
        $errors .= $this->Telefon->Errors->ToString();
        $errors .= $this->Webseite->Errors->ToString();
        $errors .= $this->Foto->Errors->ToString();
        $errors .= $this->Austellungstermine->Errors->ToString();
        $errors .= $this->Errors->ToString();
        $errors .= $this->ds->Errors->ToString();
        return $errors;
    }
//End GetErrors Method

} //End kuenstler Class @18-FCB6E20C

class clskuenstlerDataSource extends clsDBConnection1 {  //kuenstlerDataSource Class @18-03C1C86E

//DataSource Variables @18-E253F0E6
    var $CCSEvents = "";
    var $CCSEventResult;
    var $ErrorBlock;
    var $CmdExecution;

    var $CountSQL;
    var $wp;


    // Datasource fields
    var $Kuenstler_ID;
    var $Name;
    var $Vorname;
    var $Stadt;
    var $Telefon;
    var $Webseite;
    var $Foto;
    var $Austellungstermine;
//End DataSource Variables

//DataSourceClass_Initialize Event @18-DE782997
    function clskuenstlerDataSource()
    {
        $this->ErrorBlock = "Grid kuenstler";
        $this->Initialize();
        $this->Kuenstler_ID = new clsField("Kuenstler_ID", ccsInteger, "");
        $this->Name = new clsField("Name", ccsText, "");
        $this->Vorname = new clsField("Vorname", ccsText, "");
        $this->Stadt = new clsField("Stadt", ccsText, "");
        $this->Telefon = new clsField("Telefon", ccsText, "");
        $this->Webseite = new clsField("Webseite", ccsText, "");
        $this->Foto = new clsField("Foto", ccsText, "");
        $this->Austellungstermine = new clsField("Austellungstermine", ccsText, "");

    }
//End DataSourceClass_Initialize Event

//SetOrder Method @18-9AF444D7
    function SetOrder($SorterName, $SorterDirection)
    {
        $this->Order = "Name, Vorname";
        $this->Order = CCGetOrder($this->Order, $SorterName, $SorterDirection, 
            array("Sorter_Kuenstler_ID" => array("Kuenstler_ID", ""), 
            "Sorter_Name" => array("Name", ""), 
            "Sorter_Vorname" => array("Vorname", ""), 
            "Sorter_Stadt" => array("Stadt", ""), 
            "Sorter_Telefon" => array("Telefon", ""), 
            "Sorter_Webseite" => array("Webseite", ""), 
            "Sorter_Foto" => array("Foto", ""), 
            "Sorter_Austellungstermine" => array("Austellungstermine", "")));
    }
//End SetOrder Method

//Prepare Method @18-B76E6DC8
    function Prepare()
    {
        $this->wp = new clsSQLParameters($this->ErrorBlock);
        $this->wp->AddParameter("1", "urls_Name", ccsText, "", "", $this->Parameters["urls_Name"], "", false);
        $this->wp->AddParameter("2", "urls_Vorname", ccsText, "", "", $this->Parameters["urls_Vorname"], "", false);
        $this->wp->AddParameter("3", "urls_Adresse", ccsText, "", "", $this->Parameters["urls_Adresse"], "", false);
        $this->wp->AddParameter("4", "urls_PLZ", ccsText, "", "", $this->Parameters["urls_PLZ"], "", false);
        $this->wp->AddParameter("5", "urls_Stadt", ccsText, "", "", $this->Parameters["urls_Stadt"], "", false);
        $this->wp->AddParameter("6", "urls_Telefon", ccsText, "", "", $this->Parameters["urls_Telefon"], "", false);
        $this->wp->AddParameter("7", "urls_Webseite", ccsText, "", "", $this->Parameters["urls_Webseite"], "", false);
        $this->wp->AddParameter("8", "urls_Foto", ccsText, "", "", $this->Parameters["urls_Foto"], "", false);
        $this->wp->AddParameter("9", "urls_Vita", ccsMemo, "", "", $this->Parameters["urls_Vita"], "", false);
        $this->wp->AddParameter("10", "urls_Kategorie", ccsText, "", "", $this->Parameters["urls_Kategorie"], "", false);
        $this->wp->AddParameter("11", "urls_Arbeitsbeispiele", ccsText, "", "", $this->Parameters["urls_Arbeitsbeispiele"], "", false);
        $this->wp->AddParameter("12", "urls_Austellungstermine", ccsText, "", "", $this->Parameters["urls_Austellungstermine"], "", false);
        $this->wp->AddParameter("13", "expr92", ccsText, "", "", $this->Parameters["expr92"], "", false);
        $this->wp->Criterion[1] = $this->wp->Operation(opContains, "Name", $this->wp->GetDBValue("1"), $this->ToSQL($this->wp->GetDBValue("1"), ccsText),false);
        $this->wp->Criterion[2] = $this->wp->Operation(opContains, "Vorname", $this->wp->GetDBValue("2"), $this->ToSQL($this->wp->GetDBValue("2"), ccsText),false);
        $this->wp->Criterion[3] = $this->wp->Operation(opContains, "Adresse", $this->wp->GetDBValue("3"), $this->ToSQL($this->wp->GetDBValue("3"), ccsText),false);
        $this->wp->Criterion[4] = $this->wp->Operation(opContains, "PLZ", $this->wp->GetDBValue("4"), $this->ToSQL($this->wp->GetDBValue("4"), ccsText),false);
        $this->wp->Criterion[5] = $this->wp->Operation(opContains, "Stadt", $this->wp->GetDBValue("5"), $this->ToSQL($this->wp->GetDBValue("5"), ccsText),false);
        $this->wp->Criterion[6] = $this->wp->Operation(opContains, "Telefon", $this->wp->GetDBValue("6"), $this->ToSQL($this->wp->GetDBValue("6"), ccsText),false);
        $this->wp->Criterion[7] = $this->wp->Operation(opContains, "Webseite", $this->wp->GetDBValue("7"), $this->ToSQL($this->wp->GetDBValue("7"), ccsText),false);
        $this->wp->Criterion[8] = $this->wp->Operation(opContains, "Foto", $this->wp->GetDBValue("8"), $this->ToSQL($this->wp->GetDBValue("8"), ccsText),false);
        $this->wp->Criterion[9] = $this->wp->Operation(opContains, "Vita", $this->wp->GetDBValue("9"), $this->ToSQL($this->wp->GetDBValue("9"), ccsMemo),false);
        $this->wp->Criterion[10] = $this->wp->Operation(opContains, "Kategorie", $this->wp->GetDBValue("10"), $this->ToSQL($this->wp->GetDBValue("10"), ccsText),false);
        $this->wp->Criterion[11] = $this->wp->Operation(opContains, "Arbeitsbeispiele", $this->wp->GetDBValue("11"), $this->ToSQL($this->wp->GetDBValue("11"), ccsText),false);
        $this->wp->Criterion[12] = $this->wp->Operation(opContains, "Austellungstermine", $this->wp->GetDBValue("12"), $this->ToSQL($this->wp->GetDBValue("12"), ccsText),false);
        $this->wp->Criterion[13] = $this->wp->Operation(opLessThan, "`Group`", $this->wp->GetDBValue("13"), $this->ToSQL($this->wp->GetDBValue("13"), ccsText),false);
        $this->Where = $this->wp->opAND(
             false, $this->wp->opAND(
             false, $this->wp->opAND(
             false, $this->wp->opAND(
             false, $this->wp->opAND(
             false, $this->wp->opAND(
             false, $this->wp->opAND(
             false, $this->wp->opAND(
             false, $this->wp->opAND(
             false, $this->wp->opAND(
             false, $this->wp->opAND(
             false, $this->wp->opAND(
             false, 
             $this->wp->Criterion[1], 
             $this->wp->Criterion[2]), 
             $this->wp->Criterion[3]), 
             $this->wp->Criterion[4]), 
             $this->wp->Criterion[5]), 
             $this->wp->Criterion[6]), 
             $this->wp->Criterion[7]), 
             $this->wp->Criterion[8]), 
             $this->wp->Criterion[9]), 
             $this->wp->Criterion[10]), 
             $this->wp->Criterion[11]), 
             $this->wp->Criterion[12]), 
             $this->wp->Criterion[13]);
    }
//End Prepare Method

//Open Method @18-0FCEB7BD
    function Open()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildSelect");
        $this->CountSQL = "SELECT COUNT(*)  " .
        "FROM kuenstler";
        $this->SQL = "SELECT Kuenstler_ID, Name, Vorname, Adresse, PLZ, Stadt, Telefon, Webseite, Foto, Kategorie, Arbeitsbeispiele, Austellungstermine  " .
        "FROM kuenstler";
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteSelect");
        $this->RecordsCount = CCGetDBValue(CCBuildSQL($this->CountSQL, $this->Where, ""), $this);
        $this->query($this->OptimizeSQL(CCBuildSQL($this->SQL, $this->Where, $this->Order)));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteSelect");
    }
//End Open Method

//SetValues Method @18-761F6924
    function SetValues()
    {
        $this->Kuenstler_ID->SetDBValue(trim($this->f("Kuenstler_ID")));
        $this->Name->SetDBValue($this->f("Name"));
        $this->Vorname->SetDBValue($this->f("Vorname"));
        $this->Stadt->SetDBValue($this->f("Stadt"));
        $this->Telefon->SetDBValue($this->f("Telefon"));
        $this->Webseite->SetDBValue($this->f("Webseite"));
        $this->Foto->SetDBValue($this->f("Foto"));
        $this->Austellungstermine->SetDBValue($this->f("Austellungstermine"));
    }
//End SetValues Method

} //End kuenstlerDataSource Class @18-FCB6E20C

//Initialize Page @1-138AD311
// Variables
$FileName = "";
$Redirect = "";
$Tpl = "";
$TemplateFileName = "";
$BlockToParse = "main";
$ComponentName = "";

// Events;
$CCSEvents = "";
$CCSEventResult = "";

$FileName = "kuenstler_list.php";
$Redirect = "";
$TemplateFileName = "kuenstler_list.html";
$BlockToParse = "main";
$TemplateEncoding = "";
$FileEncoding = "";
$PathToRoot = "../";
//End Initialize Page

//Authenticate User @1-C7A40A44
CCSecurityRedirect("3;4", "../login.php");
//End Authenticate User

//Initialize Objects @1-EE84C20B
$DBConnection1 = new clsDBConnection1();

// Controls
$menu = new clsmenu("");
$menu->BindEvents();
$menu->Initialize();
$kuenstlerSearch = new clsRecordkuenstlerSearch();
$kuenstler = new clsGridkuenstler();
$kuenstler->Initialize();

$CCSEventResult = CCGetEvent($CCSEvents, "AfterInitialize");

$Charset = $Charset ? $Charset : $TemplateEncoding;
if ($Charset)
    header("Content-Type: text/html; charset=" . $Charset);
//End Initialize Objects

//Initialize HTML Template @1-E2A5B61F
$CCSEventResult = CCGetEvent($CCSEvents, "OnInitializeView");
$Tpl = new clsTemplate($FileEncoding, $TemplateEncoding);
$Tpl->LoadTemplate(PathToCurrentPage . $TemplateFileName, $BlockToParse, $TemplateEncoding);
$Tpl->block_path = "/$BlockToParse";
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeShow");
//End Initialize HTML Template

//Execute Components @1-6CA3C598
$menu->Operations();
$kuenstlerSearch->Operation();
//End Execute Components

//Go to destination page @1-5F99E1C9
if($Redirect)
{
    $CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload");
    $DBConnection1->close();
    header("Location: " . $Redirect);
    $menu->Class_Terminate();
    unset($menu);
    unset($kuenstlerSearch);
    unset($kuenstler);
    unset($Tpl);
    exit;
}
//End Go to destination page

//Show Page @1-1DF9D479
$menu->Show("menu");
$kuenstlerSearch->Show();
$kuenstler->Show();
$Tpl->block_path = "";
$Tpl->PParse("main", false);
//End Show Page

//Unload Page @1-9D5D803E
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload");
$DBConnection1->close();
$menu->Class_Terminate();
unset($menu);
unset($kuenstlerSearch);
unset($kuenstler);
unset($Tpl);
//End Unload Page


?>
