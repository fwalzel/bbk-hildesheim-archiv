<?php
//Include Common Files @1-4EDE2568
define("RelativePath", "..");
define("PathToCurrentPage", "/admin/");
include(RelativePath . "/Common.php");
include(RelativePath . "/Template.php");
include(RelativePath . "/Sorter.php");
include(RelativePath . "/Navigator.php");
  
//End Include Common Files

//Include Page implementation @46-D18F3F45
include_once(RelativePath . "/admin/menu.php");
//End Include Page implementation

class clsRecordprogrammSearch { //programmSearch Class @2-DC663DDC

//Variables @2-76058651

    // Public variables
    var $ComponentName;
    var $HTMLFormAction;
    var $PressedButton;
    var $Errors;
    var $ErrorBlock;
    var $FormSubmitted;
    var $FormEnctype;
    var $Visible;
    var $Recordset;

    var $CCSEvents = "";
    var $CCSEventResult;

    var $RelativePath = "";

    var $InsertAllowed = false;
    var $UpdateAllowed = false;
    var $DeleteAllowed = false;
    var $ReadAllowed   = false;
    var $EditMode      = false;
    var $ds;
    var $ValidatingControls;
    var $Controls;

    // Class variables
//End Variables

//Class_Initialize Event @2-CB43E7E6
    function clsRecordprogrammSearch($RelativePath = "")
    {

        global $FileName;
        $this->Visible = true;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Record programmSearch/Error";
        $this->ReadAllowed = true;
        if($this->Visible)
        {
            $this->ComponentName = "programmSearch";
            $CCSForm = split(":", CCGetFromGet("ccsForm", ""), 2);
            if(sizeof($CCSForm) == 1)
                $CCSForm[1] = "";
            list($FormName, $FormMethod) = $CCSForm;
            $this->FormEnctype = "application/x-www-form-urlencoded";
            $this->FormSubmitted = ($FormName == $this->ComponentName);
            $Method = $this->FormSubmitted ? ccsPost : ccsGet;
            $this->s_Datum = new clsControl(ccsTextBox, "s_Datum", "s_Datum", ccsText, "", CCGetRequestParam("s_Datum", $Method));
            $this->s_Titel = new clsControl(ccsTextBox, "s_Titel", "s_Titel", ccsText, "", CCGetRequestParam("s_Titel", $Method));
            $this->s_Kurzbeschreibung = new clsControl(ccsTextBox, "s_Kurzbeschreibung", "s_Kurzbeschreibung", ccsMemo, "", CCGetRequestParam("s_Kurzbeschreibung", $Method));
            $this->Button_DoSearch = new clsButton("Button_DoSearch");
        }
    }
//End Class_Initialize Event

//Validate Method @2-A8856C18
    function Validate()
    {
        $Validation = true;
        $Where = "";
        $Validation = ($this->s_Datum->Validate() && $Validation);
        $Validation = ($this->s_Titel->Validate() && $Validation);
        $Validation = ($this->s_Kurzbeschreibung->Validate() && $Validation);
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "OnValidate");
        $Validation =  $Validation && ($this->s_Datum->Errors->Count() == 0);
        $Validation =  $Validation && ($this->s_Titel->Errors->Count() == 0);
        $Validation =  $Validation && ($this->s_Kurzbeschreibung->Errors->Count() == 0);
        return (($this->Errors->Count() == 0) && $Validation);
    }
//End Validate Method

//CheckErrors Method @2-C93C20D4
    function CheckErrors()
    {
        $errors = false;
        $errors = ($errors || $this->s_Datum->Errors->Count());
        $errors = ($errors || $this->s_Titel->Errors->Count());
        $errors = ($errors || $this->s_Kurzbeschreibung->Errors->Count());
        $errors = ($errors || $this->Errors->Count());
        return $errors;
    }
//End CheckErrors Method

//Operation Method @2-E37375E3
    function Operation()
    {
        if(!$this->Visible)
            return;

        global $Redirect;
        global $FileName;

        if(!$this->FormSubmitted) {
            return;
        }

        if($this->FormSubmitted) {
            $this->PressedButton = "Button_DoSearch";
            if(strlen(CCGetParam("Button_DoSearch", ""))) {
                $this->PressedButton = "Button_DoSearch";
            }
        }
        $Redirect = "programm_list.php";
        if($this->Validate()) {
            if($this->PressedButton == "Button_DoSearch") {
                if(!CCGetEvent($this->Button_DoSearch->CCSEvents, "OnClick")) {
                    $Redirect = "";
                } else {
                    $Redirect = "programm_list.php" . "?" . CCMergeQueryStrings(CCGetQueryString("Form", Array("Button_DoSearch")));
                }
            }
        } else {
            $Redirect = "";
        }
    }
//End Operation Method

//Show Method @2-25D56A7C
    function Show()
    {
        global $Tpl;
        global $FileName;
        $Error = "";

        if(!$this->Visible)
            return;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect");


        $RecordBlock = "Record " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $RecordBlock;
        $this->EditMode = $this->EditMode && $this->ReadAllowed;
        if (!$this->FormSubmitted) {
        }

        if($this->FormSubmitted || $this->CheckErrors()) {
            $Error .= $this->s_Datum->Errors->ToString();
            $Error .= $this->s_Titel->Errors->ToString();
            $Error .= $this->s_Kurzbeschreibung->Errors->ToString();
            $Error .= $this->Errors->ToString();
            $Tpl->SetVar("Error", $Error);
            $Tpl->Parse("Error", false);
        }
        $CCSForm = $this->EditMode ? $this->ComponentName . ":" . "Edit" : $this->ComponentName;
        $this->HTMLFormAction = $FileName . "?" . CCAddParam(CCGetQueryString("QueryString", ""), "ccsForm", $CCSForm);
        $Tpl->SetVar("Action", $this->HTMLFormAction);
        $Tpl->SetVar("HTMLFormName", $this->ComponentName);
        $Tpl->SetVar("HTMLFormEnctype", $this->FormEnctype);

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow");
        if(!$this->Visible) {
            $Tpl->block_path = $ParentPath;
            return;
        }

        $this->s_Datum->Show();
        $this->s_Titel->Show();
        $this->s_Kurzbeschreibung->Show();
        $this->Button_DoSearch->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
    }
//End Show Method

} //End programmSearch Class @2-FCB6E20C

class clsGridprogramm { //programm class @12-562373FF

//Variables @12-B353CAC5

    // Public variables
    var $ComponentName;
    var $Visible;
    var $Errors;
    var $ErrorBlock;
    var $ds; var $PageSize;
    var $SorterName = "";
    var $SorterDirection = "";
    var $PageNumber;

    var $CCSEvents = "";
    var $CCSEventResult;

    var $RelativePath = "";

    // Grid Controls
    var $StaticControls; var $RowControls;
    var $Sorter_Programm_ID;
    var $Sorter_Reihenfolge;
    var $Sorter_Datum;
    var $Sorter_Titel;
    var $Navigator;
//End Variables

//Class_Initialize Event @12-EBCE2F95
    function clsGridprogramm($RelativePath = "")
    {
        global $FileName;
        $this->ComponentName = "programm";
        $this->Visible = True;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Grid programm";
        $this->ds = new clsprogrammDataSource();
        $this->PageSize = CCGetParam($this->ComponentName . "PageSize", "");
        if(!is_numeric($this->PageSize) || !strlen($this->PageSize))
            $this->PageSize = 20;
        else
            $this->PageSize = intval($this->PageSize);
        if ($this->PageSize > 100)
            $this->PageSize = 100;
        if($this->PageSize == 0)
            $this->Errors->addError("<p>Form: Grid " . $this->ComponentName . "<br>Error: (CCS06) Invalid page size.</p>");
        $this->PageNumber = intval(CCGetParam($this->ComponentName . "Page", 1));
        $this->SorterName = CCGetParam("programmOrder", "");
        $this->SorterDirection = CCGetParam("programmDir", "");

        $this->Programm_ID = new clsControl(ccsLink, "Programm_ID", "Programm_ID", ccsInteger, "", CCGetRequestParam("Programm_ID", ccsGet));
        $this->Reihenfolge = new clsControl(ccsLabel, "Reihenfolge", "Reihenfolge", ccsInteger, "", CCGetRequestParam("Reihenfolge", ccsGet));
        $this->Datum = new clsControl(ccsLabel, "Datum", "Datum", ccsText, "", CCGetRequestParam("Datum", ccsGet));
        $this->Titel = new clsControl(ccsLabel, "Titel", "Titel", ccsText, "", CCGetRequestParam("Titel", ccsGet));
        $this->Sorter_Programm_ID = new clsSorter($this->ComponentName, "Sorter_Programm_ID", $FileName);
        $this->Sorter_Reihenfolge = new clsSorter($this->ComponentName, "Sorter_Reihenfolge", $FileName);
        $this->Sorter_Datum = new clsSorter($this->ComponentName, "Sorter_Datum", $FileName);
        $this->Sorter_Titel = new clsSorter($this->ComponentName, "Sorter_Titel", $FileName);
        $this->programm_Insert = new clsControl(ccsLink, "programm_Insert", "programm_Insert", ccsText, "", CCGetRequestParam("programm_Insert", ccsGet));
        $this->programm_Insert->Parameters = CCGetQueryString("QueryString", Array("Programm_ID", "ccsForm"));
        $this->programm_Insert->Page = "programm_maint.php";
        $this->Navigator = new clsNavigator($this->ComponentName, "Navigator", $FileName, 10, tpSimple);
    }
//End Class_Initialize Event

//Initialize Method @12-03626367
    function Initialize()
    {
        if(!$this->Visible) return;

        $this->ds->PageSize = $this->PageSize;
        $this->ds->AbsolutePage = $this->PageNumber;
        $this->ds->SetOrder($this->SorterName, $this->SorterDirection);
    }
//End Initialize Method

//Show Method @12-094F5A43
    function Show()
    {
        global $Tpl;
        if(!$this->Visible) return;

        $ShownRecords = 0;

        $this->ds->Parameters["urls_Datum"] = CCGetFromGet("s_Datum", "");
        $this->ds->Parameters["urls_Kategorie"] = CCGetFromGet("s_Kategorie", "");
        $this->ds->Parameters["urls_Titel"] = CCGetFromGet("s_Titel", "");
        $this->ds->Parameters["urls_Name"] = CCGetFromGet("s_Name", "");
        $this->ds->Parameters["urls_Kurzbeschreibung"] = CCGetFromGet("s_Kurzbeschreibung", "");
        $this->ds->Parameters["urls_Beschreibung"] = CCGetFromGet("s_Beschreibung", "");
        $this->ds->Parameters["urls_Website"] = CCGetFromGet("s_Website", "");

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect");


        $this->ds->Prepare();
        $this->ds->Open();

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow");
        if(!$this->Visible) return;

        $GridBlock = "Grid " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $GridBlock;


        $is_next_record = $this->ds->next_record();
        if($is_next_record && $ShownRecords < $this->PageSize)
        {
            do {
                    $this->ds->SetValues();
                $Tpl->block_path = $ParentPath . "/" . $GridBlock . "/Row";
                $this->Programm_ID->SetValue($this->ds->Programm_ID->GetValue());
                $this->Programm_ID->Parameters = CCGetQueryString("QueryString", Array("ccsForm"));
                $this->Programm_ID->Parameters = CCAddParam($this->Programm_ID->Parameters, "Programm_ID", $this->ds->f("Programm_ID"));
                $this->Programm_ID->Page = "programm_maint.php";
                $this->Reihenfolge->SetValue($this->ds->Reihenfolge->GetValue());
                $this->Datum->SetValue($this->ds->Datum->GetValue());
                $this->Titel->SetValue($this->ds->Titel->GetValue());
                $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShowRow");
                $this->Programm_ID->Show();
                $this->Reihenfolge->Show();
                $this->Datum->Show();
                $this->Titel->Show();
                $Tpl->block_path = $ParentPath . "/" . $GridBlock;
                $Tpl->parse("Row", true);
                $ShownRecords++;
                $is_next_record = $this->ds->next_record();
            } while ($is_next_record && $ShownRecords < $this->PageSize);
        }
        else // Show NoRecords block if no records are found
        {
            $Tpl->parse("NoRecords", false);
        }

        $errors = $this->GetErrors();
        if(strlen($errors))
        {
            $Tpl->replaceblock("", $errors);
            $Tpl->block_path = $ParentPath;
            return;
        }
        $this->Navigator->PageNumber = $this->ds->AbsolutePage;
        $this->Navigator->TotalPages = $this->ds->PageCount();
        $this->Sorter_Programm_ID->Show();
        $this->Sorter_Reihenfolge->Show();
        $this->Sorter_Datum->Show();
        $this->Sorter_Titel->Show();
        $this->programm_Insert->Show();
        $this->Navigator->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
        $this->ds->close();
    }
//End Show Method

//GetErrors Method @12-7FFB2CC4
    function GetErrors()
    {
        $errors = "";
        $errors .= $this->Programm_ID->Errors->ToString();
        $errors .= $this->Reihenfolge->Errors->ToString();
        $errors .= $this->Datum->Errors->ToString();
        $errors .= $this->Titel->Errors->ToString();
        $errors .= $this->Errors->ToString();
        $errors .= $this->ds->Errors->ToString();
        return $errors;
    }
//End GetErrors Method

} //End programm Class @12-FCB6E20C

class clsprogrammDataSource extends clsDBConnection1 {  //programmDataSource Class @12-8D6C1F3D

//DataSource Variables @12-40FD9D86
    var $CCSEvents = "";
    var $CCSEventResult;
    var $ErrorBlock;
    var $CmdExecution;

    var $CountSQL;
    var $wp;


    // Datasource fields
    var $Programm_ID;
    var $Reihenfolge;
    var $Datum;
    var $Titel;
//End DataSource Variables

//DataSourceClass_Initialize Event @12-9227A6CF
    function clsprogrammDataSource()
    {
        $this->ErrorBlock = "Grid programm";
        $this->Initialize();
        $this->Programm_ID = new clsField("Programm_ID", ccsInteger, "");
        $this->Reihenfolge = new clsField("Reihenfolge", ccsInteger, "");
        $this->Datum = new clsField("Datum", ccsText, "");
        $this->Titel = new clsField("Titel", ccsText, "");

    }
//End DataSourceClass_Initialize Event

//SetOrder Method @12-F8CAE5A8
    function SetOrder($SorterName, $SorterDirection)
    {
        $this->Order = "";
        $this->Order = CCGetOrder($this->Order, $SorterName, $SorterDirection, 
            array("Sorter_Programm_ID" => array("Programm_ID", ""), 
            "Sorter_Reihenfolge" => array("Reihenfolge", ""), 
            "Sorter_Datum" => array("Datum", ""), 
            "Sorter_Titel" => array("Titel", "")));
    }
//End SetOrder Method

//Prepare Method @12-8CFDFEC8
    function Prepare()
    {
        $this->wp = new clsSQLParameters($this->ErrorBlock);
        $this->wp->AddParameter("1", "urls_Datum", ccsText, "", "", $this->Parameters["urls_Datum"], "", false);
        $this->wp->AddParameter("2", "urls_Kategorie", ccsText, "", "", $this->Parameters["urls_Kategorie"], "", false);
        $this->wp->AddParameter("3", "urls_Titel", ccsText, "", "", $this->Parameters["urls_Titel"], "", false);
        $this->wp->AddParameter("4", "urls_Name", ccsText, "", "", $this->Parameters["urls_Name"], "", false);
        $this->wp->AddParameter("5", "urls_Kurzbeschreibung", ccsMemo, "", "", $this->Parameters["urls_Kurzbeschreibung"], "", false);
        $this->wp->AddParameter("6", "urls_Beschreibung", ccsMemo, "", "", $this->Parameters["urls_Beschreibung"], "", false);
        $this->wp->AddParameter("7", "urls_Website", ccsText, "", "", $this->Parameters["urls_Website"], "", false);
        $this->wp->Criterion[1] = $this->wp->Operation(opContains, "Datum", $this->wp->GetDBValue("1"), $this->ToSQL($this->wp->GetDBValue("1"), ccsText),false);
        $this->wp->Criterion[2] = $this->wp->Operation(opContains, "Kategorie", $this->wp->GetDBValue("2"), $this->ToSQL($this->wp->GetDBValue("2"), ccsText),false);
        $this->wp->Criterion[3] = $this->wp->Operation(opContains, "Titel", $this->wp->GetDBValue("3"), $this->ToSQL($this->wp->GetDBValue("3"), ccsText),false);
        $this->wp->Criterion[4] = $this->wp->Operation(opContains, "Name", $this->wp->GetDBValue("4"), $this->ToSQL($this->wp->GetDBValue("4"), ccsText),false);
        $this->wp->Criterion[5] = $this->wp->Operation(opContains, "Kurzbeschreibung", $this->wp->GetDBValue("5"), $this->ToSQL($this->wp->GetDBValue("5"), ccsMemo),false);
        $this->wp->Criterion[6] = $this->wp->Operation(opContains, "Beschreibung", $this->wp->GetDBValue("6"), $this->ToSQL($this->wp->GetDBValue("6"), ccsMemo),false);
        $this->wp->Criterion[7] = $this->wp->Operation(opContains, "Website", $this->wp->GetDBValue("7"), $this->ToSQL($this->wp->GetDBValue("7"), ccsText),false);
        $this->Where = $this->wp->opAND(
             false, $this->wp->opAND(
             false, $this->wp->opAND(
             false, $this->wp->opAND(
             false, $this->wp->opAND(
             false, $this->wp->opAND(
             false, 
             $this->wp->Criterion[1], 
             $this->wp->Criterion[2]), 
             $this->wp->Criterion[3]), 
             $this->wp->Criterion[4]), 
             $this->wp->Criterion[5]), 
             $this->wp->Criterion[6]), 
             $this->wp->Criterion[7]);
    }
//End Prepare Method

//Open Method @12-C2C04D74
    function Open()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildSelect");
        $this->CountSQL = "SELECT COUNT(*)  " .
        "FROM programm";
        $this->SQL = "SELECT programm.Programm_ID, programm.Reihenfolge, programm.Datum, programm.Kategorie, programm.Titel, programm.Name, programm.Website  " .
        "FROM programm";
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteSelect");
        $this->RecordsCount = CCGetDBValue(CCBuildSQL($this->CountSQL, $this->Where, ""), $this);
        $this->query($this->OptimizeSQL(CCBuildSQL($this->SQL, $this->Where, $this->Order)));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteSelect");
    }
//End Open Method

//SetValues Method @12-8D841501
    function SetValues()
    {
        $this->Programm_ID->SetDBValue(trim($this->f("Programm_ID")));
        $this->Reihenfolge->SetDBValue(trim($this->f("Reihenfolge")));
        $this->Datum->SetDBValue($this->f("Datum"));
        $this->Titel->SetDBValue($this->f("Titel"));
    }
//End SetValues Method

} //End programmDataSource Class @12-FCB6E20C

//Initialize Page @1-B9A6AD60
// Variables
$FileName = "";
$Redirect = "";
$Tpl = "";
$TemplateFileName = "";
$BlockToParse = "main";
$ComponentName = "";

// Events;
$CCSEvents = "";
$CCSEventResult = "";

$FileName = "programm_list.php";
$Redirect = "";
$TemplateFileName = "programm_list.html";
$BlockToParse = "main";
$TemplateEncoding = "";
$FileEncoding = "";
$PathToRoot = "../";
//End Initialize Page

//Authenticate User @1-C7A40A44
CCSecurityRedirect("3;4", "../login.php");
//End Authenticate User

//Initialize Objects @1-0FFA2AC3
$DBConnection1 = new clsDBConnection1();

// Controls
$menu = new clsmenu("");
$menu->BindEvents();
$menu->Initialize();
$programmSearch = new clsRecordprogrammSearch();
$programm = new clsGridprogramm();
$programm->Initialize();

$CCSEventResult = CCGetEvent($CCSEvents, "AfterInitialize");

$Charset = $Charset ? $Charset : $TemplateEncoding;
if ($Charset)
    header("Content-Type: text/html; charset=" . $Charset);
//End Initialize Objects

//Initialize HTML Template @1-E2A5B61F
$CCSEventResult = CCGetEvent($CCSEvents, "OnInitializeView");
$Tpl = new clsTemplate($FileEncoding, $TemplateEncoding);
$Tpl->LoadTemplate(PathToCurrentPage . $TemplateFileName, $BlockToParse, $TemplateEncoding);
$Tpl->block_path = "/$BlockToParse";
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeShow");
//End Initialize HTML Template

//Execute Components @1-92861FE9
$menu->Operations();
$programmSearch->Operation();
//End Execute Components

//Go to destination page @1-D0670407
if($Redirect)
{
    $CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload");
    $DBConnection1->close();
    header("Location: " . $Redirect);
    $menu->Class_Terminate();
    unset($menu);
    unset($programmSearch);
    unset($programm);
    unset($Tpl);
    exit;
}
//End Go to destination page

//Show Page @1-F367CD2C
$menu->Show("menu");
$programmSearch->Show();
$programm->Show();
$Tpl->block_path = "";
$Tpl->PParse("main", false);
//End Show Page

//Unload Page @1-9011B79D
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload");
$DBConnection1->close();
$menu->Class_Terminate();
unset($menu);
unset($programmSearch);
unset($programm);
unset($Tpl);
//End Unload Page


?>
