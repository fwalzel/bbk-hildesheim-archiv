<?php
//Include Common Files @1-4EDE2568
define("RelativePath", "..");
define("PathToCurrentPage", "/admin/");
include(RelativePath . "/Common.php");
include(RelativePath . "/Template.php");
include(RelativePath . "/Sorter.php");
include(RelativePath . "/Navigator.php");
  
//End Include Common Files

//Include Page implementation @21-D18F3F45
include_once(RelativePath . "/admin/menu.php");
//End Include Page implementation

class clsRecordbbk_hi_textSearch { //bbk_hi_textSearch Class @2-7E2DD98B

//Variables @2-76058651

    // Public variables
    var $ComponentName;
    var $HTMLFormAction;
    var $PressedButton;
    var $Errors;
    var $ErrorBlock;
    var $FormSubmitted;
    var $FormEnctype;
    var $Visible;
    var $Recordset;

    var $CCSEvents = "";
    var $CCSEventResult;

    var $RelativePath = "";

    var $InsertAllowed = false;
    var $UpdateAllowed = false;
    var $DeleteAllowed = false;
    var $ReadAllowed   = false;
    var $EditMode      = false;
    var $ds;
    var $ValidatingControls;
    var $Controls;

    // Class variables
//End Variables

//Class_Initialize Event @2-751BE7A5
    function clsRecordbbk_hi_textSearch($RelativePath = "")
    {

        global $FileName;
        $this->Visible = true;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Record bbk_hi_textSearch/Error";
        $this->ReadAllowed = true;
        if($this->Visible)
        {
            $this->ComponentName = "bbk_hi_textSearch";
            $CCSForm = split(":", CCGetFromGet("ccsForm", ""), 2);
            if(sizeof($CCSForm) == 1)
                $CCSForm[1] = "";
            list($FormName, $FormMethod) = $CCSForm;
            $this->FormEnctype = "application/x-www-form-urlencoded";
            $this->FormSubmitted = ($FormName == $this->ComponentName);
            $Method = $this->FormSubmitted ? ccsPost : ccsGet;
            $this->s_Titel = new clsControl(ccsTextBox, "s_Titel", "s_Titel", ccsText, "", CCGetRequestParam("s_Titel", $Method));
            $this->s_Text = new clsControl(ccsTextBox, "s_Text", "s_Text", ccsMemo, "", CCGetRequestParam("s_Text", $Method));
            $this->Button_DoSearch = new clsButton("Button_DoSearch");
        }
    }
//End Class_Initialize Event

//Validate Method @2-98547664
    function Validate()
    {
        $Validation = true;
        $Where = "";
        $Validation = ($this->s_Titel->Validate() && $Validation);
        $Validation = ($this->s_Text->Validate() && $Validation);
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "OnValidate");
        $Validation =  $Validation && ($this->s_Titel->Errors->Count() == 0);
        $Validation =  $Validation && ($this->s_Text->Errors->Count() == 0);
        return (($this->Errors->Count() == 0) && $Validation);
    }
//End Validate Method

//CheckErrors Method @2-3D7EEFC2
    function CheckErrors()
    {
        $errors = false;
        $errors = ($errors || $this->s_Titel->Errors->Count());
        $errors = ($errors || $this->s_Text->Errors->Count());
        $errors = ($errors || $this->Errors->Count());
        return $errors;
    }
//End CheckErrors Method

//Operation Method @2-DC33EB71
    function Operation()
    {
        if(!$this->Visible)
            return;

        global $Redirect;
        global $FileName;

        if(!$this->FormSubmitted) {
            return;
        }

        if($this->FormSubmitted) {
            $this->PressedButton = "Button_DoSearch";
            if(strlen(CCGetParam("Button_DoSearch", ""))) {
                $this->PressedButton = "Button_DoSearch";
            }
        }
        $Redirect = "bbk_hi_tex_list.php";
        if($this->Validate()) {
            if($this->PressedButton == "Button_DoSearch") {
                if(!CCGetEvent($this->Button_DoSearch->CCSEvents, "OnClick")) {
                    $Redirect = "";
                } else {
                    $Redirect = "bbk_hi_tex_list.php" . "?" . CCMergeQueryStrings(CCGetQueryString("Form", Array("Button_DoSearch")));
                }
            }
        } else {
            $Redirect = "";
        }
    }
//End Operation Method

//Show Method @2-87F5FA0C
    function Show()
    {
        global $Tpl;
        global $FileName;
        $Error = "";

        if(!$this->Visible)
            return;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect");


        $RecordBlock = "Record " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $RecordBlock;
        $this->EditMode = $this->EditMode && $this->ReadAllowed;
        if (!$this->FormSubmitted) {
        }

        if($this->FormSubmitted || $this->CheckErrors()) {
            $Error .= $this->s_Titel->Errors->ToString();
            $Error .= $this->s_Text->Errors->ToString();
            $Error .= $this->Errors->ToString();
            $Tpl->SetVar("Error", $Error);
            $Tpl->Parse("Error", false);
        }
        $CCSForm = $this->EditMode ? $this->ComponentName . ":" . "Edit" : $this->ComponentName;
        $this->HTMLFormAction = $FileName . "?" . CCAddParam(CCGetQueryString("QueryString", ""), "ccsForm", $CCSForm);
        $Tpl->SetVar("Action", $this->HTMLFormAction);
        $Tpl->SetVar("HTMLFormName", $this->ComponentName);
        $Tpl->SetVar("HTMLFormEnctype", $this->FormEnctype);

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow");
        if(!$this->Visible) {
            $Tpl->block_path = $ParentPath;
            return;
        }

        $this->s_Titel->Show();
        $this->s_Text->Show();
        $this->Button_DoSearch->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
    }
//End Show Method

} //End bbk_hi_textSearch Class @2-FCB6E20C

class clsGridbbk_hi_text { //bbk_hi_text class @7-A90DFE25

//Variables @7-5CA369C7

    // Public variables
    var $ComponentName;
    var $Visible;
    var $Errors;
    var $ErrorBlock;
    var $ds; var $PageSize;
    var $SorterName = "";
    var $SorterDirection = "";
    var $PageNumber;

    var $CCSEvents = "";
    var $CCSEventResult;

    var $RelativePath = "";

    // Grid Controls
    var $StaticControls; var $RowControls;
    var $Sorter_BBK_Hi_Text_ID;
    var $Sorter_Titel;
    var $Navigator;
//End Variables

//Class_Initialize Event @7-A506CC01
    function clsGridbbk_hi_text($RelativePath = "")
    {
        global $FileName;
        $this->ComponentName = "bbk_hi_text";
        $this->Visible = True;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Grid bbk_hi_text";
        $this->ds = new clsbbk_hi_textDataSource();
        $this->PageSize = CCGetParam($this->ComponentName . "PageSize", "");
        if(!is_numeric($this->PageSize) || !strlen($this->PageSize))
            $this->PageSize = 20;
        else
            $this->PageSize = intval($this->PageSize);
        if ($this->PageSize > 100)
            $this->PageSize = 100;
        if($this->PageSize == 0)
            $this->Errors->addError("<p>Form: Grid " . $this->ComponentName . "<br>Error: (CCS06) Invalid page size.</p>");
        $this->PageNumber = intval(CCGetParam($this->ComponentName . "Page", 1));
        $this->SorterName = CCGetParam("bbk_hi_textOrder", "");
        $this->SorterDirection = CCGetParam("bbk_hi_textDir", "");

        $this->BBK_Hi_Text_ID = new clsControl(ccsLink, "BBK_Hi_Text_ID", "BBK_Hi_Text_ID", ccsInteger, "", CCGetRequestParam("BBK_Hi_Text_ID", ccsGet));
        $this->Titel = new clsControl(ccsLabel, "Titel", "Titel", ccsText, "", CCGetRequestParam("Titel", ccsGet));
        $this->Sorter_BBK_Hi_Text_ID = new clsSorter($this->ComponentName, "Sorter_BBK_Hi_Text_ID", $FileName);
        $this->Sorter_Titel = new clsSorter($this->ComponentName, "Sorter_Titel", $FileName);
        $this->bbk_hi_text_Insert = new clsControl(ccsLink, "bbk_hi_text_Insert", "bbk_hi_text_Insert", ccsText, "", CCGetRequestParam("bbk_hi_text_Insert", ccsGet));
        $this->bbk_hi_text_Insert->Parameters = CCGetQueryString("QueryString", Array("BBK_Hi_Text_ID", "ccsForm"));
        $this->bbk_hi_text_Insert->Page = "bbk_hi_tex_maint.php";
        $this->Navigator = new clsNavigator($this->ComponentName, "Navigator", $FileName, 10, tpSimple);
    }
//End Class_Initialize Event

//Initialize Method @7-03626367
    function Initialize()
    {
        if(!$this->Visible) return;

        $this->ds->PageSize = $this->PageSize;
        $this->ds->AbsolutePage = $this->PageNumber;
        $this->ds->SetOrder($this->SorterName, $this->SorterDirection);
    }
//End Initialize Method

//Show Method @7-68244173
    function Show()
    {
        global $Tpl;
        if(!$this->Visible) return;

        $ShownRecords = 0;

        $this->ds->Parameters["urls_Titel"] = CCGetFromGet("s_Titel", "");
        $this->ds->Parameters["urls_Text"] = CCGetFromGet("s_Text", "");

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect");


        $this->ds->Prepare();
        $this->ds->Open();

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow");
        if(!$this->Visible) return;

        $GridBlock = "Grid " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $GridBlock;


        $is_next_record = $this->ds->next_record();
        if($is_next_record && $ShownRecords < $this->PageSize)
        {
            do {
                    $this->ds->SetValues();
                $Tpl->block_path = $ParentPath . "/" . $GridBlock . "/Row";
                $this->BBK_Hi_Text_ID->SetValue($this->ds->BBK_Hi_Text_ID->GetValue());
                $this->BBK_Hi_Text_ID->Parameters = CCGetQueryString("QueryString", Array("ccsForm"));
                $this->BBK_Hi_Text_ID->Parameters = CCAddParam($this->BBK_Hi_Text_ID->Parameters, "BBK_Hi_Text_ID", $this->ds->f("BBK_Hi_Text_ID"));
                $this->BBK_Hi_Text_ID->Page = "bbk_hi_tex_maint.php";
                $this->Titel->SetValue($this->ds->Titel->GetValue());
                $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShowRow");
                $this->BBK_Hi_Text_ID->Show();
                $this->Titel->Show();
                $Tpl->block_path = $ParentPath . "/" . $GridBlock;
                $Tpl->parse("Row", true);
                $ShownRecords++;
                $is_next_record = $this->ds->next_record();
            } while ($is_next_record && $ShownRecords < $this->PageSize);
        }
        else // Show NoRecords block if no records are found
        {
            $Tpl->parse("NoRecords", false);
        }

        $errors = $this->GetErrors();
        if(strlen($errors))
        {
            $Tpl->replaceblock("", $errors);
            $Tpl->block_path = $ParentPath;
            return;
        }
        $this->Navigator->PageNumber = $this->ds->AbsolutePage;
        $this->Navigator->TotalPages = $this->ds->PageCount();
        $this->Sorter_BBK_Hi_Text_ID->Show();
        $this->Sorter_Titel->Show();
        $this->bbk_hi_text_Insert->Show();
        $this->Navigator->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
        $this->ds->close();
    }
//End Show Method

//GetErrors Method @7-31951E1B
    function GetErrors()
    {
        $errors = "";
        $errors .= $this->BBK_Hi_Text_ID->Errors->ToString();
        $errors .= $this->Titel->Errors->ToString();
        $errors .= $this->Errors->ToString();
        $errors .= $this->ds->Errors->ToString();
        return $errors;
    }
//End GetErrors Method

} //End bbk_hi_text Class @7-FCB6E20C

class clsbbk_hi_textDataSource extends clsDBConnection1 {  //bbk_hi_textDataSource Class @7-5C49B8C4

//DataSource Variables @7-6D7CBB91
    var $CCSEvents = "";
    var $CCSEventResult;
    var $ErrorBlock;
    var $CmdExecution;

    var $CountSQL;
    var $wp;


    // Datasource fields
    var $BBK_Hi_Text_ID;
    var $Titel;
//End DataSource Variables

//DataSourceClass_Initialize Event @7-97BB02DE
    function clsbbk_hi_textDataSource()
    {
        $this->ErrorBlock = "Grid bbk_hi_text";
        $this->Initialize();
        $this->BBK_Hi_Text_ID = new clsField("BBK_Hi_Text_ID", ccsInteger, "");
        $this->Titel = new clsField("Titel", ccsText, "");

    }
//End DataSourceClass_Initialize Event

//SetOrder Method @7-634BC5A5
    function SetOrder($SorterName, $SorterDirection)
    {
        $this->Order = "";
        $this->Order = CCGetOrder($this->Order, $SorterName, $SorterDirection, 
            array("Sorter_BBK_Hi_Text_ID" => array("BBK_Hi_Text_ID", ""), 
            "Sorter_Titel" => array("Titel", "")));
    }
//End SetOrder Method

//Prepare Method @7-7CD7AA9E
    function Prepare()
    {
        $this->wp = new clsSQLParameters($this->ErrorBlock);
        $this->wp->AddParameter("1", "urls_Titel", ccsText, "", "", $this->Parameters["urls_Titel"], "", false);
        $this->wp->AddParameter("2", "urls_Text", ccsMemo, "", "", $this->Parameters["urls_Text"], "", false);
        $this->wp->Criterion[1] = $this->wp->Operation(opContains, "Titel", $this->wp->GetDBValue("1"), $this->ToSQL($this->wp->GetDBValue("1"), ccsText),false);
        $this->wp->Criterion[2] = $this->wp->Operation(opContains, "Text", $this->wp->GetDBValue("2"), $this->ToSQL($this->wp->GetDBValue("2"), ccsMemo),false);
        $this->Where = $this->wp->opAND(
             false, 
             $this->wp->Criterion[1], 
             $this->wp->Criterion[2]);
    }
//End Prepare Method

//Open Method @7-E2D08EA9
    function Open()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildSelect");
        $this->CountSQL = "SELECT COUNT(*)  " .
        "FROM bbk_hi_text";
        $this->SQL = "SELECT bbk_hi_text.BBK_Hi_Text_ID, bbk_hi_text.Titel  " .
        "FROM bbk_hi_text";
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteSelect");
        $this->RecordsCount = CCGetDBValue(CCBuildSQL($this->CountSQL, $this->Where, ""), $this);
        $this->query($this->OptimizeSQL(CCBuildSQL($this->SQL, $this->Where, $this->Order)));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteSelect");
    }
//End Open Method

//SetValues Method @7-0144521C
    function SetValues()
    {
        $this->BBK_Hi_Text_ID->SetDBValue(trim($this->f("BBK_Hi_Text_ID")));
        $this->Titel->SetDBValue($this->f("Titel"));
    }
//End SetValues Method

} //End bbk_hi_textDataSource Class @7-FCB6E20C

//Initialize Page @1-5CB5897F
// Variables
$FileName = "";
$Redirect = "";
$Tpl = "";
$TemplateFileName = "";
$BlockToParse = "main";
$ComponentName = "";

// Events;
$CCSEvents = "";
$CCSEventResult = "";

$FileName = "bbk_hi_tex_list.php";
$Redirect = "";
$TemplateFileName = "bbk_hi_tex_list.html";
$BlockToParse = "main";
$TemplateEncoding = "";
$FileEncoding = "";
$PathToRoot = "../";
//End Initialize Page

//Authenticate User @1-4B0BB954
CCSecurityRedirect("3", "");
//End Authenticate User

//Initialize Objects @1-1066EF60
$DBConnection1 = new clsDBConnection1();

// Controls
$menu = new clsmenu("");
$menu->BindEvents();
$menu->Initialize();
$bbk_hi_textSearch = new clsRecordbbk_hi_textSearch();
$bbk_hi_text = new clsGridbbk_hi_text();
$bbk_hi_text->Initialize();

$CCSEventResult = CCGetEvent($CCSEvents, "AfterInitialize");

$Charset = $Charset ? $Charset : $TemplateEncoding;
if ($Charset)
    header("Content-Type: text/html; charset=" . $Charset);
//End Initialize Objects

//Initialize HTML Template @1-E2A5B61F
$CCSEventResult = CCGetEvent($CCSEvents, "OnInitializeView");
$Tpl = new clsTemplate($FileEncoding, $TemplateEncoding);
$Tpl->LoadTemplate(PathToCurrentPage . $TemplateFileName, $BlockToParse, $TemplateEncoding);
$Tpl->block_path = "/$BlockToParse";
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeShow");
//End Initialize HTML Template

//Execute Components @1-941365DD
$menu->Operations();
$bbk_hi_textSearch->Operation();
//End Execute Components

//Go to destination page @1-4145CB92
if($Redirect)
{
    $CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload");
    $DBConnection1->close();
    header("Location: " . $Redirect);
    $menu->Class_Terminate();
    unset($menu);
    unset($bbk_hi_textSearch);
    unset($bbk_hi_text);
    unset($Tpl);
    exit;
}
//End Go to destination page

//Show Page @1-D45C26F8
$menu->Show("menu");
$bbk_hi_textSearch->Show();
$bbk_hi_text->Show();
$Tpl->block_path = "";
$Tpl->PParse("main", false);
//End Show Page

//Unload Page @1-010EB421
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload");
$DBConnection1->close();
$menu->Class_Terminate();
unset($menu);
unset($bbk_hi_textSearch);
unset($bbk_hi_text);
unset($Tpl);
//End Unload Page


?>
