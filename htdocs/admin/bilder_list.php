<?php
//Include Common Files @1-4EDE2568
define("RelativePath", "..");
define("PathToCurrentPage", "/admin/");
include(RelativePath . "/Common.php");
include(RelativePath . "/Template.php");
include(RelativePath . "/Sorter.php");
include(RelativePath . "/Navigator.php");
  
//End Include Common Files

//Include Page implementation @32-D18F3F45
include_once(RelativePath . "/admin/menu.php");
//End Include Page implementation

class clsRecordbilderSearch { //bilderSearch Class @2-C61CE52D

//Variables @2-76058651

    // Public variables
    var $ComponentName;
    var $HTMLFormAction;
    var $PressedButton;
    var $Errors;
    var $ErrorBlock;
    var $FormSubmitted;
    var $FormEnctype;
    var $Visible;
    var $Recordset;

    var $CCSEvents = "";
    var $CCSEventResult;

    var $RelativePath = "";

    var $InsertAllowed = false;
    var $UpdateAllowed = false;
    var $DeleteAllowed = false;
    var $ReadAllowed   = false;
    var $EditMode      = false;
    var $ds;
    var $ValidatingControls;
    var $Controls;

    // Class variables
//End Variables

//Class_Initialize Event @2-3AF34939
    function clsRecordbilderSearch($RelativePath = "")
    {

        global $FileName;
        $this->Visible = true;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Record bilderSearch/Error";
        $this->ReadAllowed = true;
        if($this->Visible)
        {
            $this->ComponentName = "bilderSearch";
            $CCSForm = split(":", CCGetFromGet("ccsForm", ""), 2);
            if(sizeof($CCSForm) == 1)
                $CCSForm[1] = "";
            list($FormName, $FormMethod) = $CCSForm;
            $this->FormEnctype = "application/x-www-form-urlencoded";
            $this->FormSubmitted = ($FormName == $this->ComponentName);
            $Method = $this->FormSubmitted ? ccsPost : ccsGet;
            $this->s_Name = new clsControl(ccsTextBox, "s_Name", "s_Name", ccsText, "", CCGetRequestParam("s_Name", $Method));
            $this->s_Bild = new clsControl(ccsTextBox, "s_Bild", "s_Bild", ccsText, "", CCGetRequestParam("s_Bild", $Method));
            $this->s_Bild_G = new clsControl(ccsTextBox, "s_Bild_G", "s_Bild_G", ccsText, "", CCGetRequestParam("s_Bild_G", $Method));
            $this->Button_DoSearch = new clsButton("Button_DoSearch");
        }
    }
//End Class_Initialize Event

//Validate Method @2-F1745183
    function Validate()
    {
        $Validation = true;
        $Where = "";
        $Validation = ($this->s_Name->Validate() && $Validation);
        $Validation = ($this->s_Bild->Validate() && $Validation);
        $Validation = ($this->s_Bild_G->Validate() && $Validation);
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "OnValidate");
        $Validation =  $Validation && ($this->s_Name->Errors->Count() == 0);
        $Validation =  $Validation && ($this->s_Bild->Errors->Count() == 0);
        $Validation =  $Validation && ($this->s_Bild_G->Errors->Count() == 0);
        return (($this->Errors->Count() == 0) && $Validation);
    }
//End Validate Method

//CheckErrors Method @2-15045612
    function CheckErrors()
    {
        $errors = false;
        $errors = ($errors || $this->s_Name->Errors->Count());
        $errors = ($errors || $this->s_Bild->Errors->Count());
        $errors = ($errors || $this->s_Bild_G->Errors->Count());
        $errors = ($errors || $this->Errors->Count());
        return $errors;
    }
//End CheckErrors Method

//Operation Method @2-0E2159A2
    function Operation()
    {
        if(!$this->Visible)
            return;

        global $Redirect;
        global $FileName;

        if(!$this->FormSubmitted) {
            return;
        }

        if($this->FormSubmitted) {
            $this->PressedButton = "Button_DoSearch";
            if(strlen(CCGetParam("Button_DoSearch", ""))) {
                $this->PressedButton = "Button_DoSearch";
            }
        }
        $Redirect = "bilder_list.php";
        if($this->Validate()) {
            if($this->PressedButton == "Button_DoSearch") {
                if(!CCGetEvent($this->Button_DoSearch->CCSEvents, "OnClick")) {
                    $Redirect = "";
                } else {
                    $Redirect = "bilder_list.php" . "?" . CCMergeQueryStrings(CCGetQueryString("Form", Array("Button_DoSearch")));
                }
            }
        } else {
            $Redirect = "";
        }
    }
//End Operation Method

//Show Method @2-E802E5B2
    function Show()
    {
        global $Tpl;
        global $FileName;
        $Error = "";

        if(!$this->Visible)
            return;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect");


        $RecordBlock = "Record " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $RecordBlock;
        $this->EditMode = $this->EditMode && $this->ReadAllowed;
        if (!$this->FormSubmitted) {
        }

        if($this->FormSubmitted || $this->CheckErrors()) {
            $Error .= $this->s_Name->Errors->ToString();
            $Error .= $this->s_Bild->Errors->ToString();
            $Error .= $this->s_Bild_G->Errors->ToString();
            $Error .= $this->Errors->ToString();
            $Tpl->SetVar("Error", $Error);
            $Tpl->Parse("Error", false);
        }
        $CCSForm = $this->EditMode ? $this->ComponentName . ":" . "Edit" : $this->ComponentName;
        $this->HTMLFormAction = $FileName . "?" . CCAddParam(CCGetQueryString("QueryString", ""), "ccsForm", $CCSForm);
        $Tpl->SetVar("Action", $this->HTMLFormAction);
        $Tpl->SetVar("HTMLFormName", $this->ComponentName);
        $Tpl->SetVar("HTMLFormEnctype", $this->FormEnctype);

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow");
        if(!$this->Visible) {
            $Tpl->block_path = $ParentPath;
            return;
        }

        $this->s_Name->Show();
        $this->s_Bild->Show();
        $this->s_Bild_G->Show();
        $this->Button_DoSearch->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
    }
//End Show Method

} //End bilderSearch Class @2-FCB6E20C

class clsGridbilder { //bilder class @8-2D3556BA

//Variables @8-2A3771C5

    // Public variables
    var $ComponentName;
    var $Visible;
    var $Errors;
    var $ErrorBlock;
    var $ds; var $PageSize;
    var $SorterName = "";
    var $SorterDirection = "";
    var $PageNumber;

    var $CCSEvents = "";
    var $CCSEventResult;

    var $RelativePath = "";

    // Grid Controls
    var $StaticControls; var $RowControls;
    var $Sorter_Bilder_ID;
    var $Sorter_Name;
    var $Sorter_Reihenfolge;
    var $Navigator;
//End Variables

//Class_Initialize Event @8-7EF2BA50
    function clsGridbilder($RelativePath = "")
    {
        global $FileName;
        $this->ComponentName = "bilder";
        $this->Visible = True;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Grid bilder";
        $this->ds = new clsbilderDataSource();
        $this->PageSize = CCGetParam($this->ComponentName . "PageSize", "");
        if(!is_numeric($this->PageSize) || !strlen($this->PageSize))
            $this->PageSize = 20;
        else
            $this->PageSize = intval($this->PageSize);
        if ($this->PageSize > 100)
            $this->PageSize = 100;
        if($this->PageSize == 0)
            $this->Errors->addError("<p>Form: Grid " . $this->ComponentName . "<br>Error: (CCS06) Invalid page size.</p>");
        $this->PageNumber = intval(CCGetParam($this->ComponentName . "Page", 1));
        $this->SorterName = CCGetParam("bilderOrder", "");
        $this->SorterDirection = CCGetParam("bilderDir", "");

        $this->Bilder_ID = new clsControl(ccsLink, "Bilder_ID", "Bilder_ID", ccsInteger, "", CCGetRequestParam("Bilder_ID", ccsGet));
        $this->Name = new clsControl(ccsLabel, "Name", "Name", ccsText, "", CCGetRequestParam("Name", ccsGet));
        $this->Vorname = new clsControl(ccsLabel, "Vorname", "Vorname", ccsText, "", CCGetRequestParam("Vorname", ccsGet));
        $this->Reihenfolge = new clsControl(ccsLabel, "Reihenfolge", "Reihenfolge", ccsInteger, "", CCGetRequestParam("Reihenfolge", ccsGet));
        $this->Sorter_Bilder_ID = new clsSorter($this->ComponentName, "Sorter_Bilder_ID", $FileName);
        $this->Sorter_Name = new clsSorter($this->ComponentName, "Sorter_Name", $FileName);
        $this->Sorter_Reihenfolge = new clsSorter($this->ComponentName, "Sorter_Reihenfolge", $FileName);
        $this->bilder_Insert = new clsControl(ccsLink, "bilder_Insert", "bilder_Insert", ccsText, "", CCGetRequestParam("bilder_Insert", ccsGet));
        $this->bilder_Insert->Parameters = CCGetQueryString("QueryString", Array("Bilder_ID", "ccsForm"));
        $this->bilder_Insert->Page = "bilder_maint.php";
        $this->Navigator = new clsNavigator($this->ComponentName, "Navigator", $FileName, 10, tpSimple);
    }
//End Class_Initialize Event

//Initialize Method @8-03626367
    function Initialize()
    {
        if(!$this->Visible) return;

        $this->ds->PageSize = $this->PageSize;
        $this->ds->AbsolutePage = $this->PageNumber;
        $this->ds->SetOrder($this->SorterName, $this->SorterDirection);
    }
//End Initialize Method

//Show Method @8-D362A1EB
    function Show()
    {
        global $Tpl;
        if(!$this->Visible) return;

        $ShownRecords = 0;

        $this->ds->Parameters["urls_Name"] = CCGetFromGet("s_Name", "");
        $this->ds->Parameters["urls_Bild"] = CCGetFromGet("s_Bild", "");
        $this->ds->Parameters["urls_Bild_G"] = CCGetFromGet("s_Bild_G", "");

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect");


        $this->ds->Prepare();
        $this->ds->Open();

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow");
        if(!$this->Visible) return;

        $GridBlock = "Grid " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $GridBlock;


        $is_next_record = $this->ds->next_record();
        if($is_next_record && $ShownRecords < $this->PageSize)
        {
            do {
                    $this->ds->SetValues();
                $Tpl->block_path = $ParentPath . "/" . $GridBlock . "/Row";
                $this->Bilder_ID->SetValue($this->ds->Bilder_ID->GetValue());
                $this->Bilder_ID->Parameters = CCGetQueryString("QueryString", Array("ccsForm"));
                $this->Bilder_ID->Parameters = CCAddParam($this->Bilder_ID->Parameters, "Bilder_ID", $this->ds->f("Bilder_ID"));
                $this->Bilder_ID->Page = "bilder_maint.php";
                $this->Name->SetValue($this->ds->Name->GetValue());
                $this->Vorname->SetValue($this->ds->Vorname->GetValue());
                $this->Reihenfolge->SetValue($this->ds->Reihenfolge->GetValue());
                $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShowRow");
                $this->Bilder_ID->Show();
                $this->Name->Show();
                $this->Vorname->Show();
                $this->Reihenfolge->Show();
                $Tpl->block_path = $ParentPath . "/" . $GridBlock;
                $Tpl->parse("Row", true);
                $ShownRecords++;
                $is_next_record = $this->ds->next_record();
            } while ($is_next_record && $ShownRecords < $this->PageSize);
        }
        else // Show NoRecords block if no records are found
        {
            $Tpl->parse("NoRecords", false);
        }

        $errors = $this->GetErrors();
        if(strlen($errors))
        {
            $Tpl->replaceblock("", $errors);
            $Tpl->block_path = $ParentPath;
            return;
        }
        $this->Navigator->PageNumber = $this->ds->AbsolutePage;
        $this->Navigator->TotalPages = $this->ds->PageCount();
        $this->Sorter_Bilder_ID->Show();
        $this->Sorter_Name->Show();
        $this->Sorter_Reihenfolge->Show();
        $this->bilder_Insert->Show();
        $this->Navigator->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
        $this->ds->close();
    }
//End Show Method

//GetErrors Method @8-4258EDE4
    function GetErrors()
    {
        $errors = "";
        $errors .= $this->Bilder_ID->Errors->ToString();
        $errors .= $this->Name->Errors->ToString();
        $errors .= $this->Vorname->Errors->ToString();
        $errors .= $this->Reihenfolge->Errors->ToString();
        $errors .= $this->Errors->ToString();
        $errors .= $this->ds->Errors->ToString();
        return $errors;
    }
//End GetErrors Method

} //End bilder Class @8-FCB6E20C

class clsbilderDataSource extends clsDBConnection1 {  //bilderDataSource Class @8-315E6DAA

//DataSource Variables @8-208600B3
    var $CCSEvents = "";
    var $CCSEventResult;
    var $ErrorBlock;
    var $CmdExecution;

    var $CountSQL;
    var $wp;


    // Datasource fields
    var $Bilder_ID;
    var $Name;
    var $Vorname;
    var $Reihenfolge;
//End DataSource Variables

//DataSourceClass_Initialize Event @8-008AB8E7
    function clsbilderDataSource()
    {
        $this->ErrorBlock = "Grid bilder";
        $this->Initialize();
        $this->Bilder_ID = new clsField("Bilder_ID", ccsInteger, "");
        $this->Name = new clsField("Name", ccsText, "");
        $this->Vorname = new clsField("Vorname", ccsText, "");
        $this->Reihenfolge = new clsField("Reihenfolge", ccsInteger, "");

    }
//End DataSourceClass_Initialize Event

//SetOrder Method @8-5890F6DA
    function SetOrder($SorterName, $SorterDirection)
    {
        $this->Order = "Name, Reihenfolge";
        $this->Order = CCGetOrder($this->Order, $SorterName, $SorterDirection, 
            array("Sorter_Bilder_ID" => array("Bilder_ID", ""), 
            "Sorter_Name" => array("Name", ""), 
            "Sorter_Reihenfolge" => array("Reihenfolge", "")));
    }
//End SetOrder Method

//Prepare Method @8-A1233A11
    function Prepare()
    {
        $this->wp = new clsSQLParameters($this->ErrorBlock);
        $this->wp->AddParameter("1", "urls_Name", ccsText, "", "", $this->Parameters["urls_Name"], "", false);
        $this->wp->AddParameter("2", "urls_Bild", ccsText, "", "", $this->Parameters["urls_Bild"], "", false);
        $this->wp->AddParameter("3", "urls_Bild_G", ccsText, "", "", $this->Parameters["urls_Bild_G"], "", false);
        $this->wp->Criterion[1] = $this->wp->Operation(opContains, "Name", $this->wp->GetDBValue("1"), $this->ToSQL($this->wp->GetDBValue("1"), ccsText),false);
        $this->wp->Criterion[2] = $this->wp->Operation(opContains, "Bild", $this->wp->GetDBValue("2"), $this->ToSQL($this->wp->GetDBValue("2"), ccsText),false);
        $this->wp->Criterion[3] = $this->wp->Operation(opContains, "Bild_G", $this->wp->GetDBValue("3"), $this->ToSQL($this->wp->GetDBValue("3"), ccsText),false);
        $this->Where = $this->wp->opAND(
             false, $this->wp->opAND(
             false, 
             $this->wp->Criterion[1], 
             $this->wp->Criterion[2]), 
             $this->wp->Criterion[3]);
    }
//End Prepare Method

//Open Method @8-A10C5EFA
    function Open()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildSelect");
        $this->CountSQL = "SELECT COUNT(*)  " .
        "FROM bilder";
        $this->SQL = "SELECT *  " .
        "FROM bilder";
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteSelect");
        $this->RecordsCount = CCGetDBValue(CCBuildSQL($this->CountSQL, $this->Where, ""), $this);
        $this->query($this->OptimizeSQL(CCBuildSQL($this->SQL, $this->Where, $this->Order)));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteSelect");
    }
//End Open Method

//SetValues Method @8-4E869F46
    function SetValues()
    {
        $this->Bilder_ID->SetDBValue(trim($this->f("Bilder_ID")));
        $this->Name->SetDBValue($this->f("Name"));
        $this->Vorname->SetDBValue($this->f("Vorname"));
        $this->Reihenfolge->SetDBValue(trim($this->f("Reihenfolge")));
    }
//End SetValues Method

} //End bilderDataSource Class @8-FCB6E20C

//Initialize Page @1-EE79EE90
// Variables
$FileName = "";
$Redirect = "";
$Tpl = "";
$TemplateFileName = "";
$BlockToParse = "main";
$ComponentName = "";

// Events;
$CCSEvents = "";
$CCSEventResult = "";

$FileName = "bilder_list.php";
$Redirect = "";
$TemplateFileName = "bilder_list.html";
$BlockToParse = "main";
$TemplateEncoding = "";
$FileEncoding = "";
$PathToRoot = "../";
//End Initialize Page

//Authenticate User @1-B7991F58
CCSecurityRedirect("1;3;4", "../login.php");
//End Authenticate User

//Initialize Objects @1-E18027A4
$DBConnection1 = new clsDBConnection1();

// Controls
$menu = new clsmenu("");
$menu->BindEvents();
$menu->Initialize();
$bilderSearch = new clsRecordbilderSearch();
$bilder = new clsGridbilder();
$bilder->Initialize();

$CCSEventResult = CCGetEvent($CCSEvents, "AfterInitialize");

$Charset = $Charset ? $Charset : $TemplateEncoding;
if ($Charset)
    header("Content-Type: text/html; charset=" . $Charset);
//End Initialize Objects

//Initialize HTML Template @1-E2A5B61F
$CCSEventResult = CCGetEvent($CCSEvents, "OnInitializeView");
$Tpl = new clsTemplate($FileEncoding, $TemplateEncoding);
$Tpl->LoadTemplate(PathToCurrentPage . $TemplateFileName, $BlockToParse, $TemplateEncoding);
$Tpl->block_path = "/$BlockToParse";
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeShow");
//End Initialize HTML Template

//Execute Components @1-D3B2CFBF
$menu->Operations();
$bilderSearch->Operation();
//End Execute Components

//Go to destination page @1-04DF57A8
if($Redirect)
{
    $CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload");
    $DBConnection1->close();
    header("Location: " . $Redirect);
    $menu->Class_Terminate();
    unset($menu);
    unset($bilderSearch);
    unset($bilder);
    unset($Tpl);
    exit;
}
//End Go to destination page

//Show Page @1-A78FE203
$menu->Show("menu");
$bilderSearch->Show();
$bilder->Show();
$Tpl->block_path = "";
$Tpl->PParse("main", false);
//End Show Page

//Unload Page @1-45D06D2D
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload");
$DBConnection1->close();
$menu->Class_Terminate();
unset($menu);
unset($bilderSearch);
unset($bilder);
unset($Tpl);
//End Unload Page


?>
