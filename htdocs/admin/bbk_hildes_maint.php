<?php
//Include Common Files @1-4EDE2568
define("RelativePath", "..");
define("PathToCurrentPage", "/admin/");
include(RelativePath . "/Common.php");
include(RelativePath . "/Template.php");
include(RelativePath . "/Sorter.php");
include(RelativePath . "/Navigator.php");
  
//End Include Common Files

//Include Page implementation @15-D18F3F45
include_once(RelativePath . "/admin/menu.php");
//End Include Page implementation

class clsRecordbbk_hildesheim { //bbk_hildesheim Class @2-7D14DBFB

//Variables @2-76058651

    // Public variables
    var $ComponentName;
    var $HTMLFormAction;
    var $PressedButton;
    var $Errors;
    var $ErrorBlock;
    var $FormSubmitted;
    var $FormEnctype;
    var $Visible;
    var $Recordset;

    var $CCSEvents = "";
    var $CCSEventResult;

    var $RelativePath = "";

    var $InsertAllowed = false;
    var $UpdateAllowed = false;
    var $DeleteAllowed = false;
    var $ReadAllowed   = false;
    var $EditMode      = false;
    var $ds;
    var $ValidatingControls;
    var $Controls;

    // Class variables
//End Variables

//Class_Initialize Event @2-2354B475
    function clsRecordbbk_hildesheim($RelativePath = "")
    {

        global $FileName;
        $this->Visible = true;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Record bbk_hildesheim/Error";
        $this->ds = new clsbbk_hildesheimDataSource();
        $this->InsertAllowed = true;
        $this->UpdateAllowed = true;
        $this->DeleteAllowed = true;
        $this->ReadAllowed = true;
        if($this->Visible)
        {
            $this->ComponentName = "bbk_hildesheim";
            $CCSForm = split(":", CCGetFromGet("ccsForm", ""), 2);
            if(sizeof($CCSForm) == 1)
                $CCSForm[1] = "";
            list($FormName, $FormMethod) = $CCSForm;
            $this->EditMode = ($FormMethod == "Edit");
            $this->FormEnctype = "multipart/form-data";
            $this->FormSubmitted = ($FormName == $this->ComponentName);
            $Method = $this->FormSubmitted ? ccsPost : ccsGet;
            $this->Reihenfolge = new clsControl(ccsTextBox, "Reihenfolge", "Reihenfolge", ccsInteger, "", CCGetRequestParam("Reihenfolge", $Method));
            $this->Reihenfolge->Required = true;
            $this->Name = new clsControl(ccsTextBox, "Name", "Name", ccsText, "", CCGetRequestParam("Name", $Method));
            $this->Name->Required = true;
            $this->Vorname = new clsControl(ccsTextBox, "Vorname", "Vorname", ccsText, "", CCGetRequestParam("Vorname", $Method));
            $this->Titel = new clsControl(ccsTextBox, "Titel", "Titel", ccsText, "", CCGetRequestParam("Titel", $Method));
            $this->Email = new clsControl(ccsTextBox, "Email", "Email", ccsText, "", CCGetRequestParam("Email", $Method));
            $this->Foto = new clsControl(ccsImage, "Foto", "Foto", ccsText, "", CCGetRequestParam("Foto", $Method));
            $this->FileUpload1 = new clsFileUpload("FileUpload1", "FileUpload1", "../TEMP/", "../vorstand/", "*.jpg;*.jpeg;*.gif", "", 100000);
            $this->FileUpload1->Required = true;
            $this->Telefon = new clsControl(ccsTextBox, "Telefon", "Telefon", ccsText, "", CCGetRequestParam("Telefon", $Method));
            $this->Button_Insert = new clsButton("Button_Insert");
            $this->Button_Update = new clsButton("Button_Update");
            $this->Button_Delete = new clsButton("Button_Delete");
        }
    }
//End Class_Initialize Event

//Initialize Method @2-7F2EE72B
    function Initialize()
    {

        if(!$this->Visible)
            return;

        $this->ds->Parameters["urlvorstand_ID"] = CCGetFromGet("vorstand_ID", "");
    }
//End Initialize Method

//Validate Method @2-8AC9C975
    function Validate()
    {
        $Validation = true;
        $Where = "";
        $Validation = ($this->Reihenfolge->Validate() && $Validation);
        $Validation = ($this->Name->Validate() && $Validation);
        $Validation = ($this->Vorname->Validate() && $Validation);
        $Validation = ($this->Titel->Validate() && $Validation);
        $Validation = ($this->Email->Validate() && $Validation);
        $Validation = ($this->FileUpload1->Validate() && $Validation);
        $Validation = ($this->Telefon->Validate() && $Validation);
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "OnValidate");
        $Validation =  $Validation && ($this->Reihenfolge->Errors->Count() == 0);
        $Validation =  $Validation && ($this->Name->Errors->Count() == 0);
        $Validation =  $Validation && ($this->Vorname->Errors->Count() == 0);
        $Validation =  $Validation && ($this->Titel->Errors->Count() == 0);
        $Validation =  $Validation && ($this->Email->Errors->Count() == 0);
        $Validation =  $Validation && ($this->FileUpload1->Errors->Count() == 0);
        $Validation =  $Validation && ($this->Telefon->Errors->Count() == 0);
        return (($this->Errors->Count() == 0) && $Validation);
    }
//End Validate Method

//CheckErrors Method @2-DBE0E68A
    function CheckErrors()
    {
        $errors = false;
        $errors = ($errors || $this->Reihenfolge->Errors->Count());
        $errors = ($errors || $this->Name->Errors->Count());
        $errors = ($errors || $this->Vorname->Errors->Count());
        $errors = ($errors || $this->Titel->Errors->Count());
        $errors = ($errors || $this->Email->Errors->Count());
        $errors = ($errors || $this->Foto->Errors->Count());
        $errors = ($errors || $this->FileUpload1->Errors->Count());
        $errors = ($errors || $this->Telefon->Errors->Count());
        $errors = ($errors || $this->Errors->Count());
        $errors = ($errors || $this->ds->Errors->Count());
        return $errors;
    }
//End CheckErrors Method

//Operation Method @2-A667452F
    function Operation()
    {
        if(!$this->Visible)
            return;

        global $Redirect;
        global $FileName;

        $this->ds->Prepare();
        if(!$this->FormSubmitted) {
            $this->EditMode = $this->ds->AllParametersSet;
            return;
        }

        $this->FileUpload1->Upload();

        if($this->FormSubmitted) {
            $this->PressedButton = $this->EditMode ? "Button_Update" : "Button_Insert";
            if(strlen(CCGetParam("Button_Insert", ""))) {
                $this->PressedButton = "Button_Insert";
            } else if(strlen(CCGetParam("Button_Update", ""))) {
                $this->PressedButton = "Button_Update";
            } else if(strlen(CCGetParam("Button_Delete", ""))) {
                $this->PressedButton = "Button_Delete";
            }
        }
        $Redirect = "bbk_hildes_list.php" . "?" . CCGetQueryString("QueryString", Array("ccsForm"));
        if($this->PressedButton == "Button_Delete") {
            if(!CCGetEvent($this->Button_Delete->CCSEvents, "OnClick") || !$this->DeleteRow()) {
                $Redirect = "";
            }
        } else if($this->Validate()) {
            if($this->PressedButton == "Button_Insert") {
                if(!CCGetEvent($this->Button_Insert->CCSEvents, "OnClick") || !$this->InsertRow()) {
                    $Redirect = "";
                }
            } else if($this->PressedButton == "Button_Update") {
                if(!CCGetEvent($this->Button_Update->CCSEvents, "OnClick") || !$this->UpdateRow()) {
                    $Redirect = "";
                }
            }
        } else {
            $Redirect = "";
        }
    }
//End Operation Method

//InsertRow Method @2-0B5C82FE
    function InsertRow()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeInsert");
        if(!$this->InsertAllowed) return false;
        $this->ds->Reihenfolge->SetValue($this->Reihenfolge->GetValue());
        $this->ds->Name->SetValue($this->Name->GetValue());
        $this->ds->Vorname->SetValue($this->Vorname->GetValue());
        $this->ds->Titel->SetValue($this->Titel->GetValue());
        $this->ds->Email->SetValue($this->Email->GetValue());
        $this->ds->Foto->SetValue($this->Foto->GetValue());
        $this->ds->FileUpload1->SetValue($this->FileUpload1->GetValue());
        $this->ds->Telefon->SetValue($this->Telefon->GetValue());
        $this->ds->Insert();
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterInsert");
        if($this->ds->Errors->Count() == 0) {
            $this->FileUpload1->Move();
        }
        return (!$this->CheckErrors());
    }
//End InsertRow Method

//UpdateRow Method @2-BE1AE919
    function UpdateRow()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeUpdate");
        if(!$this->UpdateAllowed) return false;
        $this->ds->Reihenfolge->SetValue($this->Reihenfolge->GetValue());
        $this->ds->Name->SetValue($this->Name->GetValue());
        $this->ds->Vorname->SetValue($this->Vorname->GetValue());
        $this->ds->Titel->SetValue($this->Titel->GetValue());
        $this->ds->Email->SetValue($this->Email->GetValue());
        $this->ds->Foto->SetValue($this->Foto->GetValue());
        $this->ds->FileUpload1->SetValue($this->FileUpload1->GetValue());
        $this->ds->Telefon->SetValue($this->Telefon->GetValue());
        $this->ds->Update();
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterUpdate");
        if($this->ds->Errors->Count() == 0) {
            $this->FileUpload1->Move();
        }
        return (!$this->CheckErrors());
    }
//End UpdateRow Method

//DeleteRow Method @2-DC81EAF8
    function DeleteRow()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeDelete");
        if(!$this->DeleteAllowed) return false;
        $this->ds->Delete();
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterDelete");
        if($this->ds->Errors->Count() == 0) {
            $this->FileUpload1->Delete();
        }
        return (!$this->CheckErrors());
    }
//End DeleteRow Method

//Show Method @2-4CEBA17C
    function Show()
    {
        global $Tpl;
        global $FileName;
        $Error = "";

        if(!$this->Visible)
            return;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect");


        $RecordBlock = "Record " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $RecordBlock;
        $this->EditMode = $this->EditMode && $this->ReadAllowed;
        if($this->EditMode) {
            if($this->ds->Errors->Count()){
                $this->Errors->AddErrors($this->ds->Errors);
                $this->ds->Errors->clear();
            }
            $this->ds->open();
            if($this->ds->Errors->Count() == 0 && $this->ds->next_record()) {
                $this->ds->SetValues();
                $this->Foto->SetValue($this->ds->Foto->GetValue());
                if(!$this->FormSubmitted){
                    $this->Reihenfolge->SetValue($this->ds->Reihenfolge->GetValue());
                    $this->Name->SetValue($this->ds->Name->GetValue());
                    $this->Vorname->SetValue($this->ds->Vorname->GetValue());
                    $this->Titel->SetValue($this->ds->Titel->GetValue());
                    $this->Email->SetValue($this->ds->Email->GetValue());
                    $this->FileUpload1->SetValue($this->ds->FileUpload1->GetValue());
                    $this->Telefon->SetValue($this->ds->Telefon->GetValue());
                }
            } else {
                $this->EditMode = false;
            }
        }

        if($this->FormSubmitted || $this->CheckErrors()) {
            $Error .= $this->Reihenfolge->Errors->ToString();
            $Error .= $this->Name->Errors->ToString();
            $Error .= $this->Vorname->Errors->ToString();
            $Error .= $this->Titel->Errors->ToString();
            $Error .= $this->Email->Errors->ToString();
            $Error .= $this->Foto->Errors->ToString();
            $Error .= $this->FileUpload1->Errors->ToString();
            $Error .= $this->Telefon->Errors->ToString();
            $Error .= $this->Errors->ToString();
            $Error .= $this->ds->Errors->ToString();
            $Tpl->SetVar("Error", $Error);
            $Tpl->Parse("Error", false);
        }
        $CCSForm = $this->EditMode ? $this->ComponentName . ":" . "Edit" : $this->ComponentName;
        $this->HTMLFormAction = $FileName . "?" . CCAddParam(CCGetQueryString("QueryString", ""), "ccsForm", $CCSForm);
        $Tpl->SetVar("Action", $this->HTMLFormAction);
        $Tpl->SetVar("HTMLFormName", $this->ComponentName);
        $Tpl->SetVar("HTMLFormEnctype", $this->FormEnctype);
        $this->Button_Insert->Visible = !$this->EditMode && $this->InsertAllowed;
        $this->Button_Update->Visible = $this->EditMode && $this->UpdateAllowed;
        $this->Button_Delete->Visible = $this->EditMode && $this->DeleteAllowed;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow");
        if(!$this->Visible) {
            $Tpl->block_path = $ParentPath;
            return;
        }

        $this->Reihenfolge->Show();
        $this->Name->Show();
        $this->Vorname->Show();
        $this->Titel->Show();
        $this->Email->Show();
        $this->Foto->Show();
        $this->FileUpload1->Show();
        $this->Telefon->Show();
        $this->Button_Insert->Show();
        $this->Button_Update->Show();
        $this->Button_Delete->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
        $this->ds->close();
    }
//End Show Method

} //End bbk_hildesheim Class @2-FCB6E20C

class clsbbk_hildesheimDataSource extends clsDBConnection1 {  //bbk_hildesheimDataSource Class @2-67F5C630

//DataSource Variables @2-D0B296B4
    var $CCSEvents = "";
    var $CCSEventResult;
    var $ErrorBlock;
    var $CmdExecution;

    var $InsertParameters;
    var $UpdateParameters;
    var $DeleteParameters;
    var $wp;
    var $AllParametersSet;


    // Datasource fields
    var $Reihenfolge;
    var $Name;
    var $Vorname;
    var $Titel;
    var $Email;
    var $Foto;
    var $FileUpload1;
    var $Telefon;
//End DataSource Variables

//DataSourceClass_Initialize Event @2-934A82EC
    function clsbbk_hildesheimDataSource()
    {
        $this->ErrorBlock = "Record bbk_hildesheim/Error";
        $this->Initialize();
        $this->Reihenfolge = new clsField("Reihenfolge", ccsInteger, "");
        $this->Name = new clsField("Name", ccsText, "");
        $this->Vorname = new clsField("Vorname", ccsText, "");
        $this->Titel = new clsField("Titel", ccsText, "");
        $this->Email = new clsField("Email", ccsText, "");
        $this->Foto = new clsField("Foto", ccsText, "");
        $this->FileUpload1 = new clsField("FileUpload1", ccsText, "");
        $this->Telefon = new clsField("Telefon", ccsText, "");

    }
//End DataSourceClass_Initialize Event

//Prepare Method @2-0D49FE31
    function Prepare()
    {
        $this->wp = new clsSQLParameters($this->ErrorBlock);
        $this->wp->AddParameter("1", "urlvorstand_ID", ccsInteger, "", "", $this->Parameters["urlvorstand_ID"], "", false);
        $this->AllParametersSet = $this->wp->AllParamsSet();
        $this->wp->Criterion[1] = $this->wp->Operation(opEqual, "vorstand_ID", $this->wp->GetDBValue("1"), $this->ToSQL($this->wp->GetDBValue("1"), ccsInteger),false);
        $this->Where = 
             $this->wp->Criterion[1];
    }
//End Prepare Method

//Open Method @2-DDD5A173
    function Open()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildSelect");
        $this->SQL = "SELECT *  " .
        "FROM bbk_hildesheim";
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteSelect");
        $this->PageSize = 1;
        $this->query($this->OptimizeSQL(CCBuildSQL($this->SQL, $this->Where, $this->Order)));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteSelect");
    }
//End Open Method

//SetValues Method @2-BF883790
    function SetValues()
    {
        $this->Reihenfolge->SetDBValue(trim($this->f("Reihenfolge")));
        $this->Name->SetDBValue($this->f("Name"));
        $this->Vorname->SetDBValue($this->f("Vorname"));
        $this->Titel->SetDBValue($this->f("Titel"));
        $this->Email->SetDBValue($this->f("Email"));
        $this->Foto->SetDBValue($this->f("Foto"));
        $this->FileUpload1->SetDBValue($this->f("Foto"));
        $this->Telefon->SetDBValue($this->f("Telefon"));
    }
//End SetValues Method

//Insert Method @2-A2893B89
    function Insert()
    {
        $this->CmdExecution = true;
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildInsert");
        $this->SQL = "INSERT INTO bbk_hildesheim ("
             . "Reihenfolge, "
             . "Name, "
             . "Vorname, "
             . "Titel, "
             . "Email, "
             . "Foto, "
             . "Telefon"
             . ") VALUES ("
             . $this->ToSQL($this->Reihenfolge->GetDBValue(), $this->Reihenfolge->DataType) . ", "
             . $this->ToSQL($this->Name->GetDBValue(), $this->Name->DataType) . ", "
             . $this->ToSQL($this->Vorname->GetDBValue(), $this->Vorname->DataType) . ", "
             . $this->ToSQL($this->Titel->GetDBValue(), $this->Titel->DataType) . ", "
             . $this->ToSQL($this->Email->GetDBValue(), $this->Email->DataType) . ", "
             . $this->ToSQL($this->FileUpload1->GetDBValue(), $this->FileUpload1->DataType) . ", "
             . $this->ToSQL($this->Telefon->GetDBValue(), $this->Telefon->DataType)
             . ")";
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteInsert");
        if($this->Errors->Count() == 0 && $this->CmdExecution) {
            $this->query($this->SQL);
            $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteInsert");
        }
        $this->close();
    }
//End Insert Method

//Update Method @2-EFC78B78
    function Update()
    {
        $this->CmdExecution = true;
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildUpdate");
        $this->SQL = "UPDATE bbk_hildesheim SET "
             . "Reihenfolge=" . $this->ToSQL($this->Reihenfolge->GetDBValue(), $this->Reihenfolge->DataType) . ", "
             . "Name=" . $this->ToSQL($this->Name->GetDBValue(), $this->Name->DataType) . ", "
             . "Vorname=" . $this->ToSQL($this->Vorname->GetDBValue(), $this->Vorname->DataType) . ", "
             . "Titel=" . $this->ToSQL($this->Titel->GetDBValue(), $this->Titel->DataType) . ", "
             . "Email=" . $this->ToSQL($this->Email->GetDBValue(), $this->Email->DataType) . ", "
             . "Foto=" . $this->ToSQL($this->FileUpload1->GetDBValue(), $this->FileUpload1->DataType) . ", "
             . "Telefon=" . $this->ToSQL($this->Telefon->GetDBValue(), $this->Telefon->DataType);
        $this->SQL = CCBuildSQL($this->SQL, $this->Where, "");
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteUpdate");
        if($this->Errors->Count() == 0 && $this->CmdExecution) {
            $this->query($this->SQL);
            $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteUpdate");
        }
        $this->close();
    }
//End Update Method

//Delete Method @2-C9C2AD85
    function Delete()
    {
        $this->CmdExecution = true;
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildDelete");
        $this->SQL = "DELETE FROM bbk_hildesheim";
        $this->SQL = CCBuildSQL($this->SQL, $this->Where, "");
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteDelete");
        if($this->Errors->Count() == 0 && $this->CmdExecution) {
            $this->query($this->SQL);
            $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteDelete");
        }
        $this->close();
    }
//End Delete Method

} //End bbk_hildesheimDataSource Class @2-FCB6E20C

//Initialize Page @1-06406A8E
// Variables
$FileName = "";
$Redirect = "";
$Tpl = "";
$TemplateFileName = "";
$BlockToParse = "main";
$ComponentName = "";

// Events;
$CCSEvents = "";
$CCSEventResult = "";

$FileName = "bbk_hildes_maint.php";
$Redirect = "";
$TemplateFileName = "bbk_hildes_maint.html";
$BlockToParse = "main";
$TemplateEncoding = "";
$FileEncoding = "";
$PathToRoot = "../";
//End Initialize Page

//Authenticate User @1-4B0BB954
CCSecurityRedirect("3", "");
//End Authenticate User

//Initialize Objects @1-CE1ED2BB
$DBConnection1 = new clsDBConnection1();

// Controls
$menu = new clsmenu("");
$menu->BindEvents();
$menu->Initialize();
$bbk_hildesheim = new clsRecordbbk_hildesheim();
$bbk_hildesheim->Initialize();

$CCSEventResult = CCGetEvent($CCSEvents, "AfterInitialize");

$Charset = $Charset ? $Charset : $TemplateEncoding;
if ($Charset)
    header("Content-Type: text/html; charset=" . $Charset);
//End Initialize Objects

//Initialize HTML Template @1-E2A5B61F
$CCSEventResult = CCGetEvent($CCSEvents, "OnInitializeView");
$Tpl = new clsTemplate($FileEncoding, $TemplateEncoding);
$Tpl->LoadTemplate(PathToCurrentPage . $TemplateFileName, $BlockToParse, $TemplateEncoding);
$Tpl->block_path = "/$BlockToParse";
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeShow");
//End Initialize HTML Template

//Execute Components @1-1BF77E91
$menu->Operations();
$bbk_hildesheim->Operation();
//End Execute Components

//Go to destination page @1-0276D0C7
if($Redirect)
{
    $CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload");
    $DBConnection1->close();
    header("Location: " . $Redirect);
    $menu->Class_Terminate();
    unset($menu);
    unset($bbk_hildesheim);
    unset($Tpl);
    exit;
}
//End Go to destination page

//Show Page @1-22A7BEB3
$menu->Show("menu");
$bbk_hildesheim->Show();
$Tpl->block_path = "";
$Tpl->PParse("main", false);
//End Show Page

//Unload Page @1-9941A49F
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload");
$DBConnection1->close();
$menu->Class_Terminate();
unset($menu);
unset($bbk_hildesheim);
unset($Tpl);
//End Unload Page


?>
