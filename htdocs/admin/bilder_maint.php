<?php
//Include Common Files @1-4EDE2568
define("RelativePath", "..");
define("PathToCurrentPage", "/admin/");
include(RelativePath . "/Common.php");
include(RelativePath . "/Template.php");
include(RelativePath . "/Sorter.php");
include(RelativePath . "/Navigator.php");
  
//End Include Common Files

//Include Page implementation @14-D18F3F45
include_once(RelativePath . "/admin/menu.php");
//End Include Page implementation

class clsRecordbilder { //bilder Class @2-797C575B

//Variables @2-76058651

    // Public variables
    var $ComponentName;
    var $HTMLFormAction;
    var $PressedButton;
    var $Errors;
    var $ErrorBlock;
    var $FormSubmitted;
    var $FormEnctype;
    var $Visible;
    var $Recordset;

    var $CCSEvents = "";
    var $CCSEventResult;

    var $RelativePath = "";

    var $InsertAllowed = false;
    var $UpdateAllowed = false;
    var $DeleteAllowed = false;
    var $ReadAllowed   = false;
    var $EditMode      = false;
    var $ds;
    var $ValidatingControls;
    var $Controls;

    // Class variables
//End Variables

//Class_Initialize Event @2-6F5FE017
    function clsRecordbilder($RelativePath = "")
    {

        global $FileName;
        $this->Visible = true;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Record bilder/Error";
        $this->ds = new clsbilderDataSource();
        $this->InsertAllowed = true;
        $this->UpdateAllowed = true;
        $this->DeleteAllowed = true;
        $this->ReadAllowed = true;
        if($this->Visible)
        {
            $this->ComponentName = "bilder";
            $CCSForm = split(":", CCGetFromGet("ccsForm", ""), 2);
            if(sizeof($CCSForm) == 1)
                $CCSForm[1] = "";
            list($FormName, $FormMethod) = $CCSForm;
            $this->EditMode = ($FormMethod == "Edit");
            $this->FormEnctype = "multipart/form-data";
            $this->FormSubmitted = ($FormName == $this->ComponentName);
            $Method = $this->FormSubmitted ? ccsPost : ccsGet;
            $this->Name = new clsControl(ccsListBox, "Name", "Name", ccsText, "", CCGetRequestParam("Name", $Method));
            $this->Name->DSType = dsTable;
            list($this->Name->BoundColumn, $this->Name->TextColumn, $this->Name->DBFormat) = array("Name", "Name", "");
            $this->Name->ds = new clsDBConnection1();
            $this->Name->ds->SQL = "SELECT *  " .
"FROM kuenstler";
            $this->Name->ds->Order = "Name, Vorname";
            $this->Name->Required = true;
            $this->Reihenfolge = new clsControl(ccsTextBox, "Reihenfolge", "Reihenfolge", ccsInteger, "", CCGetRequestParam("Reihenfolge", $Method));
            $this->Bild = new clsControl(ccsImage, "Bild", "Bild", ccsText, "", CCGetRequestParam("Bild", $Method));
            $this->Beschreibung = new clsControl(ccsTextBox, "Beschreibung", "Beschreibung", ccsText, "", CCGetRequestParam("Beschreibung", $Method));
            $this->FileUpload1 = new clsFileUpload("FileUpload1", "FileUpload1", "../TEMP/", "../fotos/", "*.jpg;*.jpeg;*.gif", "", 100000);
            $this->FileUpload1->Required = true;
            $this->Bild_G = new clsControl(ccsImage, "Bild_G", "Bild G", ccsText, "", CCGetRequestParam("Bild_G", $Method));
            $this->FileUpload2 = new clsFileUpload("FileUpload2", "FileUpload2", "../TEMP/", "../fotos/", "*.jpg;*.jpeg;*.gif", "", 100000);
            $this->FileUpload2->Required = true;
            $this->Button_Insert = new clsButton("Button_Insert");
            $this->Button_Update = new clsButton("Button_Update");
            $this->Button_Delete = new clsButton("Button_Delete");
        }
    }
//End Class_Initialize Event

//Initialize Method @2-8A5BDAE8
    function Initialize()
    {

        if(!$this->Visible)
            return;

        $this->ds->Order = "Name, Reihenfolge";

        $this->ds->Parameters["urlBilder_ID"] = CCGetFromGet("Bilder_ID", "");
    }
//End Initialize Method

//Validate Method @2-791B66A0
    function Validate()
    {
        $Validation = true;
        $Where = "";
        $Validation = ($this->Name->Validate() && $Validation);
        $Validation = ($this->Reihenfolge->Validate() && $Validation);
        $Validation = ($this->Beschreibung->Validate() && $Validation);
        $Validation = ($this->FileUpload1->Validate() && $Validation);
        $Validation = ($this->FileUpload2->Validate() && $Validation);
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "OnValidate");
        $Validation =  $Validation && ($this->Name->Errors->Count() == 0);
        $Validation =  $Validation && ($this->Reihenfolge->Errors->Count() == 0);
        $Validation =  $Validation && ($this->Beschreibung->Errors->Count() == 0);
        $Validation =  $Validation && ($this->FileUpload1->Errors->Count() == 0);
        $Validation =  $Validation && ($this->FileUpload2->Errors->Count() == 0);
        return (($this->Errors->Count() == 0) && $Validation);
    }
//End Validate Method

//CheckErrors Method @2-4B6DB186
    function CheckErrors()
    {
        $errors = false;
        $errors = ($errors || $this->Name->Errors->Count());
        $errors = ($errors || $this->Reihenfolge->Errors->Count());
        $errors = ($errors || $this->Bild->Errors->Count());
        $errors = ($errors || $this->Beschreibung->Errors->Count());
        $errors = ($errors || $this->FileUpload1->Errors->Count());
        $errors = ($errors || $this->Bild_G->Errors->Count());
        $errors = ($errors || $this->FileUpload2->Errors->Count());
        $errors = ($errors || $this->Errors->Count());
        $errors = ($errors || $this->ds->Errors->Count());
        return $errors;
    }
//End CheckErrors Method

//Operation Method @2-D6B121C5
    function Operation()
    {
        if(!$this->Visible)
            return;

        global $Redirect;
        global $FileName;

        $this->ds->Prepare();
        if(!$this->FormSubmitted) {
            $this->EditMode = $this->ds->AllParametersSet;
            return;
        }

        $this->FileUpload1->Upload();
        $this->FileUpload2->Upload();

        if($this->FormSubmitted) {
            $this->PressedButton = $this->EditMode ? "Button_Update" : "Button_Insert";
            if(strlen(CCGetParam("Button_Insert", ""))) {
                $this->PressedButton = "Button_Insert";
            } else if(strlen(CCGetParam("Button_Update", ""))) {
                $this->PressedButton = "Button_Update";
            } else if(strlen(CCGetParam("Button_Delete", ""))) {
                $this->PressedButton = "Button_Delete";
            }
        }
        $Redirect = "bilder_list.php" . "?" . CCGetQueryString("QueryString", Array("ccsForm"));
        if($this->PressedButton == "Button_Delete") {
            if(!CCGetEvent($this->Button_Delete->CCSEvents, "OnClick") || !$this->DeleteRow()) {
                $Redirect = "";
            }
        } else if($this->Validate()) {
            if($this->PressedButton == "Button_Insert") {
                if(!CCGetEvent($this->Button_Insert->CCSEvents, "OnClick") || !$this->InsertRow()) {
                    $Redirect = "";
                }
            } else if($this->PressedButton == "Button_Update") {
                if(!CCGetEvent($this->Button_Update->CCSEvents, "OnClick") || !$this->UpdateRow()) {
                    $Redirect = "";
                }
            }
        } else {
            $Redirect = "";
        }
    }
//End Operation Method

//InsertRow Method @2-210BBCB8
    function InsertRow()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeInsert");
        if(!$this->InsertAllowed) return false;
        $this->ds->Name->SetValue($this->Name->GetValue());
        $this->ds->Reihenfolge->SetValue($this->Reihenfolge->GetValue());
        $this->ds->Bild->SetValue($this->Bild->GetValue());
        $this->ds->Beschreibung->SetValue($this->Beschreibung->GetValue());
        $this->ds->FileUpload1->SetValue($this->FileUpload1->GetValue());
        $this->ds->Bild_G->SetValue($this->Bild_G->GetValue());
        $this->ds->FileUpload2->SetValue($this->FileUpload2->GetValue());
        $this->ds->Insert();
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterInsert");
        if($this->ds->Errors->Count() == 0) {
            $this->FileUpload1->Move();
            $this->FileUpload2->Move();
        }
        return (!$this->CheckErrors());
    }
//End InsertRow Method

//UpdateRow Method @2-405E8D2D
    function UpdateRow()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeUpdate");
        if(!$this->UpdateAllowed) return false;
        $this->ds->Name->SetValue($this->Name->GetValue());
        $this->ds->Reihenfolge->SetValue($this->Reihenfolge->GetValue());
        $this->ds->Bild->SetValue($this->Bild->GetValue());
        $this->ds->Beschreibung->SetValue($this->Beschreibung->GetValue());
        $this->ds->FileUpload1->SetValue($this->FileUpload1->GetValue());
        $this->ds->Bild_G->SetValue($this->Bild_G->GetValue());
        $this->ds->FileUpload2->SetValue($this->FileUpload2->GetValue());
        $this->ds->Update();
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterUpdate");
        if($this->ds->Errors->Count() == 0) {
            $this->FileUpload1->Move();
            $this->FileUpload2->Move();
        }
        return (!$this->CheckErrors());
    }
//End UpdateRow Method

//DeleteRow Method @2-7D64D2A1
    function DeleteRow()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeDelete");
        if(!$this->DeleteAllowed) return false;
        $this->ds->Delete();
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterDelete");
        if($this->ds->Errors->Count() == 0) {
            $this->FileUpload1->Delete();
            $this->FileUpload2->Delete();
        }
        return (!$this->CheckErrors());
    }
//End DeleteRow Method

//Show Method @2-C49C6FCB
    function Show()
    {
        global $Tpl;
        global $FileName;
        $Error = "";

        if(!$this->Visible)
            return;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect");

        $this->Name->Prepare();

        $RecordBlock = "Record " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $RecordBlock;
        $this->EditMode = $this->EditMode && $this->ReadAllowed;
        if($this->EditMode) {
            if($this->ds->Errors->Count()){
                $this->Errors->AddErrors($this->ds->Errors);
                $this->ds->Errors->clear();
            }
            $this->ds->open();
            if($this->ds->Errors->Count() == 0 && $this->ds->next_record()) {
                $this->ds->SetValues();
                $this->Bild->SetValue($this->ds->Bild->GetValue());
                $this->Bild_G->SetValue($this->ds->Bild_G->GetValue());
                if(!$this->FormSubmitted){
                    $this->Name->SetValue($this->ds->Name->GetValue());
                    $this->Reihenfolge->SetValue($this->ds->Reihenfolge->GetValue());
                    $this->Beschreibung->SetValue($this->ds->Beschreibung->GetValue());
                    $this->FileUpload1->SetValue($this->ds->FileUpload1->GetValue());
                    $this->FileUpload2->SetValue($this->ds->FileUpload2->GetValue());
                }
            } else {
                $this->EditMode = false;
            }
        }

        if($this->FormSubmitted || $this->CheckErrors()) {
            $Error .= $this->Name->Errors->ToString();
            $Error .= $this->Reihenfolge->Errors->ToString();
            $Error .= $this->Bild->Errors->ToString();
            $Error .= $this->Beschreibung->Errors->ToString();
            $Error .= $this->FileUpload1->Errors->ToString();
            $Error .= $this->Bild_G->Errors->ToString();
            $Error .= $this->FileUpload2->Errors->ToString();
            $Error .= $this->Errors->ToString();
            $Error .= $this->ds->Errors->ToString();
            $Tpl->SetVar("Error", $Error);
            $Tpl->Parse("Error", false);
        }
        $CCSForm = $this->EditMode ? $this->ComponentName . ":" . "Edit" : $this->ComponentName;
        $this->HTMLFormAction = $FileName . "?" . CCAddParam(CCGetQueryString("QueryString", ""), "ccsForm", $CCSForm);
        $Tpl->SetVar("Action", $this->HTMLFormAction);
        $Tpl->SetVar("HTMLFormName", $this->ComponentName);
        $Tpl->SetVar("HTMLFormEnctype", $this->FormEnctype);
        $this->Button_Insert->Visible = !$this->EditMode && $this->InsertAllowed;
        $this->Button_Update->Visible = $this->EditMode && $this->UpdateAllowed;
        $this->Button_Delete->Visible = $this->EditMode && $this->DeleteAllowed;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow");
        if(!$this->Visible) {
            $Tpl->block_path = $ParentPath;
            return;
        }

        $this->Name->Show();
        $this->Reihenfolge->Show();
        $this->Bild->Show();
        $this->Beschreibung->Show();
        $this->FileUpload1->Show();
        $this->Bild_G->Show();
        $this->FileUpload2->Show();
        $this->Button_Insert->Show();
        $this->Button_Update->Show();
        $this->Button_Delete->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
        $this->ds->close();
    }
//End Show Method

} //End bilder Class @2-FCB6E20C

class clsbilderDataSource extends clsDBConnection1 {  //bilderDataSource Class @2-315E6DAA

//DataSource Variables @2-251CB8BC
    var $CCSEvents = "";
    var $CCSEventResult;
    var $ErrorBlock;
    var $CmdExecution;

    var $InsertParameters;
    var $UpdateParameters;
    var $DeleteParameters;
    var $wp;
    var $AllParametersSet;


    // Datasource fields
    var $Name;
    var $Reihenfolge;
    var $Bild;
    var $Beschreibung;
    var $FileUpload1;
    var $Bild_G;
    var $FileUpload2;
//End DataSource Variables

//DataSourceClass_Initialize Event @2-760F862A
    function clsbilderDataSource()
    {
        $this->ErrorBlock = "Record bilder/Error";
        $this->Initialize();
        $this->Name = new clsField("Name", ccsText, "");
        $this->Reihenfolge = new clsField("Reihenfolge", ccsInteger, "");
        $this->Bild = new clsField("Bild", ccsText, "");
        $this->Beschreibung = new clsField("Beschreibung", ccsText, "");
        $this->FileUpload1 = new clsField("FileUpload1", ccsText, "");
        $this->Bild_G = new clsField("Bild_G", ccsText, "");
        $this->FileUpload2 = new clsField("FileUpload2", ccsText, "");

    }
//End DataSourceClass_Initialize Event

//Prepare Method @2-2DFDE9BD
    function Prepare()
    {
        $this->wp = new clsSQLParameters($this->ErrorBlock);
        $this->wp->AddParameter("1", "urlBilder_ID", ccsInteger, "", "", $this->Parameters["urlBilder_ID"], "", false);
        $this->AllParametersSet = $this->wp->AllParamsSet();
        $this->wp->Criterion[1] = $this->wp->Operation(opEqual, "Bilder_ID", $this->wp->GetDBValue("1"), $this->ToSQL($this->wp->GetDBValue("1"), ccsInteger),false);
        $this->Where = 
             $this->wp->Criterion[1];
    }
//End Prepare Method

//Open Method @2-847ABF0C
    function Open()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildSelect");
        $this->SQL = "SELECT *  " .
        "FROM bilder";
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteSelect");
        $this->PageSize = 1;
        $this->query($this->OptimizeSQL(CCBuildSQL($this->SQL, $this->Where, $this->Order)));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteSelect");
    }
//End Open Method

//SetValues Method @2-B6430595
    function SetValues()
    {
        $this->Name->SetDBValue($this->f("Name"));
        $this->Reihenfolge->SetDBValue(trim($this->f("Reihenfolge")));
        $this->Bild->SetDBValue($this->f("Bild"));
        $this->Beschreibung->SetDBValue($this->f("Beschreibung"));
        $this->FileUpload1->SetDBValue($this->f("Bild"));
        $this->Bild_G->SetDBValue($this->f("Bild_G"));
        $this->FileUpload2->SetDBValue($this->f("Bild_G"));
    }
//End SetValues Method

//Insert Method @2-616320F1
    function Insert()
    {
        $this->CmdExecution = true;
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildInsert");
        $this->SQL = "INSERT INTO bilder ("
             . "Name, "
             . "Reihenfolge, "
             . "Beschreibung, "
             . "Bild, "
             . "Bild_G"
             . ") VALUES ("
             . $this->ToSQL($this->Name->GetDBValue(), $this->Name->DataType) . ", "
             . $this->ToSQL($this->Reihenfolge->GetDBValue(), $this->Reihenfolge->DataType) . ", "
             . $this->ToSQL($this->Beschreibung->GetDBValue(), $this->Beschreibung->DataType) . ", "
             . $this->ToSQL($this->FileUpload1->GetDBValue(), $this->FileUpload1->DataType) . ", "
             . $this->ToSQL($this->FileUpload2->GetDBValue(), $this->FileUpload2->DataType)
             . ")";
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteInsert");
        if($this->Errors->Count() == 0 && $this->CmdExecution) {
            $this->query($this->SQL);
            $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteInsert");
        }
        $this->close();
    }
//End Insert Method

//Update Method @2-78263931
    function Update()
    {
        $this->CmdExecution = true;
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildUpdate");
        $this->SQL = "UPDATE bilder SET "
             . "Name=" . $this->ToSQL($this->Name->GetDBValue(), $this->Name->DataType) . ", "
             . "Reihenfolge=" . $this->ToSQL($this->Reihenfolge->GetDBValue(), $this->Reihenfolge->DataType) . ", "
             . "Beschreibung=" . $this->ToSQL($this->Beschreibung->GetDBValue(), $this->Beschreibung->DataType) . ", "
             . "Bild=" . $this->ToSQL($this->FileUpload1->GetDBValue(), $this->FileUpload1->DataType) . ", "
             . "Bild_G=" . $this->ToSQL($this->FileUpload2->GetDBValue(), $this->FileUpload2->DataType);
        $this->SQL = CCBuildSQL($this->SQL, $this->Where, "");
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteUpdate");
        if($this->Errors->Count() == 0 && $this->CmdExecution) {
            $this->query($this->SQL);
            $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteUpdate");
        }
        $this->close();
    }
//End Update Method

//Delete Method @2-6170F6A2
    function Delete()
    {
        $this->CmdExecution = true;
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildDelete");
        $this->SQL = "DELETE FROM bilder";
        $this->SQL = CCBuildSQL($this->SQL, $this->Where, "");
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteDelete");
        if($this->Errors->Count() == 0 && $this->CmdExecution) {
            $this->query($this->SQL);
            $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteDelete");
        }
        $this->close();
    }
//End Delete Method

} //End bilderDataSource Class @2-FCB6E20C

//Initialize Page @1-86CF725C
// Variables
$FileName = "";
$Redirect = "";
$Tpl = "";
$TemplateFileName = "";
$BlockToParse = "main";
$ComponentName = "";

// Events;
$CCSEvents = "";
$CCSEventResult = "";

$FileName = "bilder_maint.php";
$Redirect = "";
$TemplateFileName = "bilder_maint.html";
$BlockToParse = "main";
$TemplateEncoding = "";
$FileEncoding = "";
$PathToRoot = "../";
//End Initialize Page

//Authenticate User @1-B7991F58
CCSecurityRedirect("1;3;4", "../login.php");
//End Authenticate User

//Initialize Objects @1-6ABF10D8
$DBConnection1 = new clsDBConnection1();

// Controls
$menu = new clsmenu("");
$menu->BindEvents();
$menu->Initialize();
$bilder = new clsRecordbilder();
$bilder->Initialize();

$CCSEventResult = CCGetEvent($CCSEvents, "AfterInitialize");

$Charset = $Charset ? $Charset : $TemplateEncoding;
if ($Charset)
    header("Content-Type: text/html; charset=" . $Charset);
//End Initialize Objects

//Initialize HTML Template @1-E2A5B61F
$CCSEventResult = CCGetEvent($CCSEvents, "OnInitializeView");
$Tpl = new clsTemplate($FileEncoding, $TemplateEncoding);
$Tpl->LoadTemplate(PathToCurrentPage . $TemplateFileName, $BlockToParse, $TemplateEncoding);
$Tpl->block_path = "/$BlockToParse";
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeShow");
//End Initialize HTML Template

//Execute Components @1-F2521A00
$menu->Operations();
$bilder->Operation();
//End Execute Components

//Go to destination page @1-4B363C9B
if($Redirect)
{
    $CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload");
    $DBConnection1->close();
    header("Location: " . $Redirect);
    $menu->Class_Terminate();
    unset($menu);
    unset($bilder);
    unset($Tpl);
    exit;
}
//End Go to destination page

//Show Page @1-3BD0E3EC
$menu->Show("menu");
$bilder->Show();
$Tpl->block_path = "";
$Tpl->PParse("main", false);
//End Show Page

//Unload Page @1-62F596A7
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload");
$DBConnection1->close();
$menu->Class_Terminate();
unset($menu);
unset($bilder);
unset($Tpl);
//End Unload Page


?>
