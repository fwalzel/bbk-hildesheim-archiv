<?php
//BindEvents Method @1-ECF9C145
function BindEvents()
{
    global $bilder;
    $bilder->CCSEvents["BeforeShow"] = "bilder_BeforeShow";
}
//End BindEvents Method

//bilder_BeforeShow @35-30E3CDE4
function bilder_BeforeShow()
{
    $bilder_BeforeShow = true;
//End bilder_BeforeShow

//Custom Code @53-7785447B
// -------------------------
    global $bilder;
	global $DBConnection1;
	$bilder->Name->SetValue(CCDLookUp("Name","kuenstler","Kuenstler_ID=". $DBConnection1->ToSQL(CCGetUserID(), ccsInteger), $DBConnection1) );
// -------------------------
//End Custom Code

//Close bilder_BeforeShow @35-B5C39B98
    return $bilder_BeforeShow;
}
//End Close bilder_BeforeShow


?>
