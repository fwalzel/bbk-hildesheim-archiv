<?php
//BindEvents Method @1-983327CF
function BindEvents()
{
    global $Request;
    $Request->Button_Request->CCSEvents["OnClick"] = "Request_Button_Request_OnClick";
    $Request->CCSEvents["OnValidate"] = "Request_OnValidate";
}
//End BindEvents Method

//Request_Button_Request_OnClick @20-E7ED4975
function Request_Button_Request_OnClick()
{
    $Request_Button_Request_OnClick = true;
//End Request_Button_Request_OnClick

//Send Email @24-9A567E38
    global $Request;
	$db = new clsDBConnection1();
    ini_set("SMTP", "SMTP.bbk-hildesheim.de");
    $to = $Request->email->GetText();
	$UserName=CCDLookUp("login", "kuenstler","email = ".$db->ToSql($to,ccsText),$db);
	$Password=CCDLookUp("passwort", "kuenstler", "email =".$db->ToSql($to,ccsText),$db);
	$subject = "Re: Passwort Anfrage";
	$message = "Sie haben Ihre Zugangsdaten vergessen:\n"."Ihre Login ist: " .$UserName ."\n"."Ihr Passwort ist: " .$Password  . "\n" ."\n";
    $from = "info@bbk-hildesheim.de";
    ini_set("SMTP", "SMTP.bbk-hildesheim.de");
    $additional_headers = "From: $from\nReply-To: $from\nContent-Type: text/plain";
    mail ($to, $subject, $message, $additional_headers);
    ini_restore("SMTP");
//End Send Email

//Custom Code @25-EE8B51DD
// -------------------------
	global  $Redirect;
 	$Redirect = "danke.php";
// -------------------------
//End Custom Code

//Close Request_Button_Request_OnClick @20-7C88C94A
    return $Request_Button_Request_OnClick;
}
//End Close Request_Button_Request_OnClick

//Request_OnValidate @16-B8B0C743
function Request_OnValidate()
{
    $Request_OnValidate = true;
//End Request_OnValidate

//Custom Code @23-EE8B51DD
// -------------------------
    global $Request;
	$db = new clsDBConnection1();
	$db->query("SELECT * FROM kuenstler WHERE email=". $db->ToSQL($Request->email->GetValue(), ccsText));	
	$db->next_record();	
	// main_users is the name of the table that holds the users' emails data (user_email)
	if($db->f("email") == ""){
	$Request->Errors->AddError("\n"."Die angegebene Mailadresse ist in der Datenbank nicht vorhanden!");
	// the given email was not found in the data base --> return Error to the user
}

// -------------------------
//End Custom Code

//Close Request_OnValidate @16-DD23907C
    return $Request_OnValidate;
}
//End Close Request_OnValidate


?>
