<?php
//Include Common Files @1-000FC694
define("RelativePath", "..");
define("PathToCurrentPage", "/daten/");
include(RelativePath . "/Common.php");
include(RelativePath . "/Template.php");
include(RelativePath . "/Sorter.php");
include(RelativePath . "/Navigator.php");
  
//End Include Common Files

class clsGridbilder { //bilder class @35-2D3556BA

//Variables @35-3D7B877A

    // Public variables
    var $ComponentName;
    var $Visible;
    var $Errors;
    var $ErrorBlock;
    var $ds; var $PageSize;
    var $SorterName = "";
    var $SorterDirection = "";
    var $PageNumber;

    var $CCSEvents = "";
    var $CCSEventResult;

    var $RelativePath = "";

    // Grid Controls
    var $StaticControls; var $RowControls;
    var $Navigator;
//End Variables

//Class_Initialize Event @35-D733044D
    function clsGridbilder($RelativePath = "")
    {
        global $FileName;
        $this->ComponentName = "bilder";
        $this->Visible = True;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Grid bilder";
        $this->ds = new clsbilderDataSource();
        $this->PageSize = CCGetParam($this->ComponentName . "PageSize", "");
        if(!is_numeric($this->PageSize) || !strlen($this->PageSize))
            $this->PageSize = 20;
        else
            $this->PageSize = intval($this->PageSize);
        if ($this->PageSize > 100)
            $this->PageSize = 100;
        if($this->PageSize == 0)
            $this->Errors->addError("<p>Form: Grid " . $this->ComponentName . "<br>Error: (CCS06) Invalid page size.</p>");
        $this->PageNumber = intval(CCGetParam($this->ComponentName . "Page", 1));

        $this->Reihenfolge = new clsControl(ccsLink, "Reihenfolge", "Reihenfolge", ccsInteger, "", CCGetRequestParam("Reihenfolge", ccsGet));
        $this->bilder_Insert = new clsControl(ccsLink, "bilder_Insert", "bilder_Insert", ccsText, "", CCGetRequestParam("bilder_Insert", ccsGet));
        $this->bilder_Insert->Parameters = CCGetQueryString("QueryString", Array("Bilder_ID", "ccsForm"));
        $this->bilder_Insert->Page = "bilder1.php";
        $this->Navigator = new clsNavigator($this->ComponentName, "Navigator", $FileName, 10, tpSimple);
    }
//End Class_Initialize Event

//Initialize Method @35-03626367
    function Initialize()
    {
        if(!$this->Visible) return;

        $this->ds->PageSize = $this->PageSize;
        $this->ds->AbsolutePage = $this->PageNumber;
        $this->ds->SetOrder($this->SorterName, $this->SorterDirection);
    }
//End Initialize Method

//Show Method @35-E0ECEC8A
    function Show()
    {
        global $Tpl;
        if(!$this->Visible) return;

        $ShownRecords = 0;

        $this->ds->Parameters["urlkuenstler_Name"] = CCGetFromGet("kuenstler_Name", "");
        $this->ds->Parameters["urlKuenstler_ID"] = CCGetFromGet("Kuenstler_ID", "");

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect");


        $this->ds->Prepare();
        $this->ds->Open();

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow");
        if(!$this->Visible) return;

        $GridBlock = "Grid " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $GridBlock;


        $is_next_record = $this->ds->next_record();
        if($is_next_record && $ShownRecords < $this->PageSize)
        {
            do {
                    $this->ds->SetValues();
                $Tpl->block_path = $ParentPath . "/" . $GridBlock . "/Row";
                $this->Reihenfolge->SetValue($this->ds->Reihenfolge->GetValue());
                $this->Reihenfolge->Parameters = CCGetQueryString("QueryString", Array("ccsForm"));
                $this->Reihenfolge->Parameters = CCAddParam($this->Reihenfolge->Parameters, "Bilder_ID", $this->ds->f("Bilder_ID"));
                $this->Reihenfolge->Page = "bilder1.php";
                $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShowRow");
                $this->Reihenfolge->Show();
                $Tpl->block_path = $ParentPath . "/" . $GridBlock;
                $Tpl->parse("Row", true);
                $ShownRecords++;
                $is_next_record = $this->ds->next_record();
            } while ($is_next_record && $ShownRecords < $this->PageSize);
        }
        else // Show NoRecords block if no records are found
        {
            $Tpl->parse("NoRecords", false);
        }

        $errors = $this->GetErrors();
        if(strlen($errors))
        {
            $Tpl->replaceblock("", $errors);
            $Tpl->block_path = $ParentPath;
            return;
        }
        $this->Navigator->PageNumber = $this->ds->AbsolutePage;
        $this->Navigator->TotalPages = $this->ds->PageCount();
        $this->bilder_Insert->Show();
        $this->Navigator->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
        $this->ds->close();
    }
//End Show Method

//GetErrors Method @35-F5C8541A
    function GetErrors()
    {
        $errors = "";
        $errors .= $this->Reihenfolge->Errors->ToString();
        $errors .= $this->Errors->ToString();
        $errors .= $this->ds->Errors->ToString();
        return $errors;
    }
//End GetErrors Method

} //End bilder Class @35-FCB6E20C

class clsbilderDataSource extends clsDBConnection1 {  //bilderDataSource Class @35-315E6DAA

//DataSource Variables @35-6C77BEE5
    var $CCSEvents = "";
    var $CCSEventResult;
    var $ErrorBlock;
    var $CmdExecution;

    var $CountSQL;
    var $wp;


    // Datasource fields
    var $Reihenfolge;
//End DataSource Variables

//DataSourceClass_Initialize Event @35-B3DB087E
    function clsbilderDataSource()
    {
        $this->ErrorBlock = "Grid bilder";
        $this->Initialize();
        $this->Reihenfolge = new clsField("Reihenfolge", ccsInteger, "");

    }
//End DataSourceClass_Initialize Event

//SetOrder Method @35-19B9B6FE
    function SetOrder($SorterName, $SorterDirection)
    {
        $this->Order = "Name, Reihenfolge";
        $this->Order = CCGetOrder($this->Order, $SorterName, $SorterDirection, 
            "");
    }
//End SetOrder Method

//Prepare Method @35-9B872D32
    function Prepare()
    {
        $this->wp = new clsSQLParameters($this->ErrorBlock);
        $this->wp->AddParameter("1", "urlkuenstler_Name", ccsText, "", "", $this->Parameters["urlkuenstler_Name"], "", false);
        $this->wp->AddParameter("2", "urlKuenstler_ID", ccsInteger, "", "", $this->Parameters["urlKuenstler_ID"], CCGetUserID(), false);
        $this->wp->Criterion[1] = $this->wp->Operation(opEqual, "bilder.Name", $this->wp->GetDBValue("1"), $this->ToSQL($this->wp->GetDBValue("1"), ccsText),false);
        $this->wp->Criterion[2] = $this->wp->Operation(opEqual, "kuenstler.Kuenstler_ID", $this->wp->GetDBValue("2"), $this->ToSQL($this->wp->GetDBValue("2"), ccsInteger),false);
        $this->Where = $this->wp->opAND(
             false, 
             $this->wp->Criterion[1], 
             $this->wp->Criterion[2]);
    }
//End Prepare Method

//Open Method @35-A2BFE9F7
    function Open()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildSelect");
        $this->CountSQL = "SELECT COUNT(*)  " .
        "FROM kuenstler INNER JOIN bilder ON " .
        "kuenstler.Name = bilder.Name";
        $this->SQL = "SELECT bilder.*, Kuenstler_ID, kuenstler.Name AS kuenstler_Name  " .
        "FROM kuenstler INNER JOIN bilder ON " .
        "kuenstler.Name = bilder.Name";
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteSelect");
        $this->RecordsCount = CCGetDBValue(CCBuildSQL($this->CountSQL, $this->Where, ""), $this);
        $this->query($this->OptimizeSQL(CCBuildSQL($this->SQL, $this->Where, $this->Order)));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteSelect");
    }
//End Open Method

//SetValues Method @35-5F56AF9F
    function SetValues()
    {
        $this->Reihenfolge->SetDBValue(trim($this->f("Reihenfolge")));
    }
//End SetValues Method

} //End bilderDataSource Class @35-FCB6E20C





//Initialize Page @1-6944F627
// Variables
$FileName = "";
$Redirect = "";
$Tpl = "";
$TemplateFileName = "";
$BlockToParse = "main";
$ComponentName = "";

// Events;
$CCSEvents = "";
$CCSEventResult = "";

$FileName = "bilder.php";
$Redirect = "";
$TemplateFileName = "bilder.html";
$BlockToParse = "main";
$TemplateEncoding = "";
$FileEncoding = "";
$PathToRoot = "../";
//End Initialize Page

//Authenticate User @1-44B6A20F
CCSecurityRedirect("1;3;4", "login_daten.php");
//End Authenticate User

//Initialize Objects @1-512A1DE7
$DBConnection1 = new clsDBConnection1();

// Controls
$bilder = new clsGridbilder();
$bilder->Initialize();

$CCSEventResult = CCGetEvent($CCSEvents, "AfterInitialize");

$Charset = $Charset ? $Charset : $TemplateEncoding;
if ($Charset)
    header("Content-Type: text/html; charset=" . $Charset);
//End Initialize Objects

//Initialize HTML Template @1-E2A5B61F
$CCSEventResult = CCGetEvent($CCSEvents, "OnInitializeView");
$Tpl = new clsTemplate($FileEncoding, $TemplateEncoding);
$Tpl->LoadTemplate(PathToCurrentPage . $TemplateFileName, $BlockToParse, $TemplateEncoding);
$Tpl->block_path = "/$BlockToParse";
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeShow");
//End Initialize HTML Template

//Go to destination page @1-49D39006
if($Redirect)
{
    $CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload");
    $DBConnection1->close();
    header("Location: " . $Redirect);
    unset($bilder);
    unset($Tpl);
    exit;
}
//End Go to destination page

//Show Page @1-151CAF61
$bilder->Show();
$Tpl->block_path = "";
$Tpl->PParse("main", false);
//End Show Page

//Unload Page @1-922F8376
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload");
$DBConnection1->close();
unset($bilder);
unset($Tpl);
//End Unload Page


?>
