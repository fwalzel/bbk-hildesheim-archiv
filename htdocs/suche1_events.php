<?php
//BindEvents Method @1-7F0D7F1A
function BindEvents()
{
    global $kuenstler;
    $kuenstler->kuenstler_TotalRecords->CCSEvents["BeforeShow"] = "kuenstler_kuenstler_TotalRecords_BeforeShow";
}
//End BindEvents Method

//kuenstler_kuenstler_TotalRecords_BeforeShow @14-3F7DE355
function kuenstler_kuenstler_TotalRecords_BeforeShow()
{
    $kuenstler_kuenstler_TotalRecords_BeforeShow = true;
//End kuenstler_kuenstler_TotalRecords_BeforeShow

//Retrieve number of records @15-C02B6726
    global $kuenstler;
    $kuenstler->kuenstler_TotalRecords->SetValue($kuenstler->ds->RecordsCount);
//End Retrieve number of records

//Close kuenstler_kuenstler_TotalRecords_BeforeShow @14-3B189406
    return $kuenstler_kuenstler_TotalRecords_BeforeShow;
}
//End Close kuenstler_kuenstler_TotalRecords_BeforeShow


?>
