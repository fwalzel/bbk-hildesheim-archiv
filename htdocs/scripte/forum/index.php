<?php
// kontent.board v1.0
// --------------------------------------------------------------------------------------
// Main File: Shows the board's topics.
// If you want to configure your board, edit the file "config.php".
// For changes in look and feel, go to ./templates and edit
// the neccessary files.
// --------------------------------------------------------------------------------------
// Startdatei: Zeigt die einzelnen Kategorien an
// Wenn Sie ihr Forum konfigurieren moechten, bearbeiten Sie bitte
// die Datei "config.php".
// Um das Aussehen des Forums zu aendern, wechseln Sie in das Verzeichnis
// ./templates und bearbeiten Sie die Dateien nach Ihren beduerfnissen.
//
// *****************************************
// INCLUDES (configuration, templates, etc.)
// *****************************************

include("config.php");                                // Include the general config file

//

include($template_dir."header.php");                // Headfile (HTML)
include($template_dir."whereareyou.php");           // Shows where you are
include($template_dir."index_viewtopics_head.php");        // Header of Topic-Table

// *********************************************
// FUNCTIONS
// *********************************************
// COUNT MESSAGES
// --------------------------------------------------------------------------------------
// This function counts the number of messages in a topic-directory.
// --------------------------------------------------------------------------------------
// Diese Funktion Zaehlt die Beitraege in einer Kategorie.

function count_msgs($topic,$data_dir) {
  // Get the number of messages for each topic
  $content_array = file("./".$data_dir.$topic.".txt");
  $mysize = count($content_array);
  return $mysize;
}

// CHECK/READ TOPICS
// --------------------------------------------------------------------------------------
// Because there is one file for each topic, the directory listing has to be read and 
// converted to a HTML list. Also create files for each topic added to the config file.
// --------------------------------------------------------------------------------------
// Da fuer jede Kategorie eine Datei angelegt wird, muss ein Verzeichnislisting gemacht
// und in eine HTML Liste umgewandelt werden. Ausserdem wird ggf. Dateien fuer neue
// Kategorien angelegt.

function ReadTopics($topics,$data_dir,$template_dir,$forum_dir) {

  while ( list($dummy,$topic) = each($topics) ) {        // "Walk" through topiclist

  // Check if the topicfile exists. If not, create it.
    if (is_file("./".$data_dir.$topic.".txt") != TRUE) {// Check if file exists
      touch("./".$data_dir.$topic.".txt", 0755);        // If not, create it
      chmod ("./".$data_dir.$topic.".txt",0777);        // Chmod to a+rwx
    }

    // Call function to count the number of messages
    $number_of_msgs_in_topic = count_msgs($topic,$data_dir);
    include($template_dir."index_viewtopics_row.php");// Includes a template for each
                                                    // row. Used variables are
                                                    // $topic, $number_of_msgs_in_topic
  }

  clearstatcache();                                // Clear "stat" cache to save memory
}

// *********************************************
// MAIN PROGRAM
// *********************************************

ReadTopics($topics,$data_dir,$template_dir,$forum_dir);

include($template_dir."index_viewtopics_foot.php");
include($template_dir."footer.php");                // Neccessary HTML Footer file
?>
