<?php

// kontent.board v1.0
// --------------------------------------------------------------------------------------
// Post File: Displays the input fields for posting a message. Also handles
// the replies if neccessary.
// For changes in look and feel, go to ./templates and edit the neccessary files.
// --------------------------------------------------------------------------------------
// Posten: Zeigt das Formular an um einen Beitrag zu schreiben, bzw.
// um eine Antwort zu schreiben.
// Um das Aussehen des Forums zu aendern, wechseln Sie in das Verzeichnis
// ./templates und bearbeiten Sie die Dateien nach Ihren beduerfnissen.

// *****************************************
// INCLUDES (configuration, templates, etc.)
// *****************************************

include("config.php");                                // Include the general config file

// *********************************************
// FUNCTIONS
// *********************************************

function newmsg($topic,$data_dir,$template_dir,$board_name,$forum_dir) {
  // If a new message is created, use this include
  $finalref = "0";
  include($template_dir."post_newmsg.php");
}

function replymsg($topic,$data_dir,$template_dir,$board_name,$forum_dir,$finalref) {
  // If a new message is created, use this include
  include($template_dir."post_newmsg.php");
}

function quotemsg($topic,$data_dir,$template_dir,$board_name,$quote,$finalref,$forum_dir) {

   // Open topicfile for read
   $index = file("./".$data_dir."/".$topic.".txt", "r");
   $i = 0;

   // Seek through array
   while ($i < count($index)) {
     // Split out each field, seperated by |
     list($nummer,$refid,$user,$subject,$date,$message_text,$ip) = split ("\\|", $index[$i], 7);
     // Find the right message to quotate
     if (isset($nummer) && $nummer == $quote) {
       // Replace <BR> with newline breaks followed by a quotation mark.
       $message_text = preg_replace("/<BR>/", "\n&gt; ", $message_text);
       include($template_dir."post_replymsg.php");        // Headfile (HTML)
       }
     $i++;
  }

}

// *********************************************
// MAIN PROGRAM
// *********************************************

include($template_dir."header.php");                // Headfile (HTML)
include($template_dir."whereareyou.php");           // Shows where you are

// If quotation was chosen, use function replymsg. Otherwise
// display newmsg.
if (isset($quote)) {
  // Set the actual reference on the topic message, not on the
  // quoted one.
  if(!isset($finalref)) { $finalref=$quote; }
  quotemsg($topic,$data_dir,$template_dir,$board_name,$quote,$finalref,$forum_dir);
} elseif (!isset($quote) && isset($finalref)) {
  replymsg($topic,$data_dir,$template_dir,$board_name,$forum_dir,$finalref);
} else {
  newmsg($topic,$data_dir,$template_dir,$board_name,$forum_dir);
}

include($template_dir."footer.php");                // Neccessary HTML Footer file
?>
