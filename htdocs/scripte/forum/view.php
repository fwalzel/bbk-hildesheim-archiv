<?php
// kontent.board v1.0
// --------------------------------------------------------------------------------------
// View: Shows the topic's threads. If a message id is given,
// it'll display the message followed by the replies.
// If you want to configure your board, edit the file "config.php".
// For changes in look and feel, go to ./templates and edit the neccessary files.
// --------------------------------------------------------------------------------------
// Anzeige: Diese Datei zeigt die Beitraege einer Kategorie an,
// wenn eine messageid angegeben wurde wird der gewuenschte Beitrag
// und alle Antworten darauf angezeigt.
// Wenn Sie ihr Forum konfigurieren moechten, bearbeiten Sie bitte
// die Datei "config.php".
// Um das Aussehen des Forums zu aendern, wechseln Sie in das Verzeichnis
// ./templates und bearbeiten Sie die Dateien nach Ihren beduerfnissen.
// FORMAT:
// NUMMER|REF|USER NAME|SUBJECT|DATUM|MESSAGE|IP

// *****************************************
// INCLUDES (configuration, templates, etc.)
// *****************************************

include("config.php");                                // Include the general config file

// *********************************************
// FUNCTIONS
// *********************************************

// CutLongWords: Breaks a word longer than a length of 40 chars.

function cut_long_words($text,$len=40){
    $neutext = $text;
        do {
            $text = $neutext;
            $neutext = preg_replace('~(^|\s)(\S{'.$len.'})(\S)~S', '\1\2 \3', $text);
        } while ($neutext != $text);
    return $neutext;
}

// HtmlFormat: Converts all html tags to harmless charssets. Additionally
//             converts links to html-links for more comfort.

function htmlformat($message_text) {

  // Replace all <BR> tags with a newline break
  $message_text = preg_replace("/<BR>/", "\n", $message_text);
  // Convert > and < tag commands to equivalent symbols &gt; and &lt;
  $message_text = preg_replace("/</", "&lt;", $message_text);
  $message_text = preg_replace("/>/", "&gt;", $message_text);
  // Strip out URLs and make them appear as a link, even though html is disabled
  $message_text = preg_replace('|([\s>]+)(\w+)://([\w#?/&=.]*)(\s+)|', '\1<a href="\2://\3">\2://\3</a>\4', $message_text);
  // Replace newline breaks with <BR> tags
  $message_text = preg_replace("/\n/", "<BR>", $message_text);

  return $message_text;
}


// List_Msgs: This function lists all the messages in a topic.

function list_msgs($topic,$data_dir,$template_dir,$board_name,$forum_dir) {

  include($template_dir."whereareyou.php");           // Shows where you are
  // Include the headerfile for ListMsgs
  include($template_dir."view_listmsgs_head.php");

  // Read the file and put it into an array
  $index = file("./".$data_dir."/".$topic.".txt", "r");
  $i = 0;

  // Seek through the array
  while ($i < count($index)) {
    $antworten = 0;

    // Split out each field, seperated by |
    list($nummer,$refid,$user,$subject,$date,$message_text,$ip) = split ("\\|", $index[$i], 7);

    // When the reference id is NULL, the message is considered as a thread message, not
    // as a reply

    if (isset($refid) && $refid == "0") {
      $y = 0;
      while ($y < count($index)) {

        // Count the number of answers to this particular message
        list($nummer_y,$refid_y,$user_y,$subject_y,$date_y,$message_text_y,$ip_y) = split ("\\|", $index[$y], 7);
        if (isset($refid_y) && isset($nummer) && $refid_y == $nummer) { $antworten++; }
        $y++;
      }
      // Include the row html file
      include($template_dir."view_listmsgs_row.php");
    }
    $i++;
  }
  include($template_dir."view_listmsgs_foot.php");
}

// Write_Msg: This function displays one message in a chosen thread

function write_msg($topic,$data_dir,$template_dir,$board_name,$msg_num, $ip_allow, $html, $forum_dir) {

  // Open the topicfile and put contents into array
  $index = file("./".$data_dir."/".$topic.".txt", "r");
  $i = 0;

  // Seek through the array
  while ($i < count($index)) {

    // Split out each field, seperated by |
    list($nummer,$refid,$user,$subject,$date,$message_text,$ip) = split ("\\|", $index[$i], 7);
    // Check if the message was found in the topicfile.
    if (isset($nummer) && $nummer == $msg_num) {
       // Check if html is disabled
       if ($html == "no") {
       $message_text = htmlformat($message_text);
       }
       $message_text = cut_long_words($message_text);
      // Include the header file for message display
      include($template_dir."whereareyou.php");           // Shows where you are
      include($template_dir."view_topmessage.php");        // Headfile (HTML)
      }
    $i++;
  }
  return $index;
}

// Write_Replies: Shows all the replies of a certain message

function write_replies($topic,$template_dir,$board_name,$msg_num,$index, $ip_allow, $html, $forum_dir) {

  $i = 0;

  // Seek through the array
  while ($i < count($index)) {
    // Split out each field, seperated by |
    list($nummer,$refid,$user,$subject,$date,$message_text,$ip) = split ("\\|", $index[$i], 7);
    // Check if the message is a reply of the original message ($msg_num)
    if (isset($refid) && $refid == $msg_num) {
       if ($html == "no") {
       $message_text = htmlformat($message_text);
       }
       $message_text = cut_long_words($message_text);
       include($template_dir."view_replies.php");        // Headfile (HTML)
      }
    $i++;
  }

}

// *********************************************
// MAIN PROGRAM
// *********************************************

//

include($template_dir."header.php");                // Headfile (HTML)

// If no particular message number is given, display all thread messages that
// aren't replies.
if (!isset($msg) || $msg == 0) {
  list_msgs($topic,$data_dir,$template_dir,$board_name,$forum_dir);
} else {
  $index = write_msg($topic,$data_dir,$template_dir,$board_name, $msg, $ip_allow, $html,$forum_dir);
  write_replies($topic,$template_dir, $board_name, $msg, $index, $ip_allow,$html,$forum_dir);
}

include($template_dir."footer.php");                // Neccessary HTML Footer file

?>
