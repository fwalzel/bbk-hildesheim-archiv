<?php
// kontent.board v1.0
// --------------------------------------------------------------------------------------
// Post_Action File: Does the actual posting and add data to the topic file.
// For changes in look and feel, go to ./templates and edit
// the neccessary files.
// --------------------------------------------------------------------------------------
// POST_ACTION: Schreibt Beitrage in die Entsprechende Kategoriedatei.
// Um das Aussehen des Forums zu aendern, wechseln Sie in das Verzeichnis
// ./templates und bearbeiten Sie die Dateien nach Ihren beduerfnissen.

// *****************************************
// INCLUDES (configuration, templates, etc.)
// *****************************************

include("config.php");                                // Include the general config file

// *****************************************
// FUNCTIONS
// *****************************************

function browsearr ($liste,$suche) {
  $zaehler = 0;
  $gefunden = 0;

  while ($zaehler < count($liste)) {
  if ($liste[$zaehler] == $suche) { $gefunden = 1; }
  $zaehler++;
  }

  if ($gefunden == 1) { return "1"; } else { return "0"; }
}

function getnewid ($data_dir,$topic) {
  $index = file("./".$data_dir."/".$topic.".txt", "r");
  $i = 0;
  $neu = 0;
  $gefunden = 1;

  // Seek through the array
  while ($i < count($index)) {
     // Split out each field, seperated by |
    list($nummer,$refid,$user,$subject,$date,$message_text,$ip) = split ("\\|", $index[$i], 7);
    $liste[$i] = $nummer;
    $i++;
    }

  while ($gefunden == 1) {
    $neu++;
    $gefunden = browsearr($liste,$neu);
  }
  return $neu;
}

function addmsg($topic,$refid,$username,$subject,$message,$refid,$board_name,$data_dir,$template_dir,$forum_dir) {
  // If message is no reply, set refid to NULL to indicate that this is an new
  // originating message.
  if (!isset($refid)) {
    $refid = "0";
  }

  // Open topicfile and count the number of messages - then create a new
  // id for this message.
  $idnum = getnewid ($data_dir,$topic);
  //echo "Neue ID: $idnum";

  // Open topicfile with "add" function, so that the file pointer will
  // be set to the end of the file.
  $fp = fopen ("./".$data_dir."/".$topic.".txt", "a");
  // Generate date
  $heute = date("d.m.y H:i");
  // Get the user's IP-Address or hostname
  $host = getenv("REMOTE_HOST");
  $ip = getenv("REMOTE_ADDR");
  if (strlen($host) > 1)
    $ip = $host;

  // Strip newline breaks and replace them with <BR> so that the
  // format of the topicfile won't be affected
  $message = preg_replace("/\r\n/", "<BR>", $message);

  // Strip "|" character so that the
  // format of the topicfile won't be affected
  $message = preg_replace("/\|/", "\/", $message);
  $subject = preg_replace("/\|/", "\/", $subject);
  $username = preg_replace("/\|/", "\/", $username);

  $username = stripslashes($username);
  $subject = stripslashes($subject);
  $message = stripslashes($message);

  // Build the formatting and print it to the file
  $line = $idnum."|".$refid."|".$username."|".$subject."|".$heute."|<BR>".$message."|".$ip."\n";
  fwrite($fp, $line);
  fclose($fp);
  header("Location: view.php?topic=$topic&msg=$refid");
}

if ( (strlen($topic) == 0) || (strlen($subject) == 0) || (strlen($username) == 0) || (strlen($message) == 0) ) {
  include($template_dir."header.php");                // Headfile (HTML)
  include($template_dir."post_action_fail.php");
} else {
  addmsg($topic,$refid,$username,$subject,$message,$refid,$board_name, $data_dir, $template_dir,$forum_dir);
}

include($template_dir."footer.php");                // Neccessary HTML Footer file
?>
