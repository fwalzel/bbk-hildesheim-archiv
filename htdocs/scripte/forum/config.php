<?php
// kontent.board v1.0
// --------------------------------------------------------------------------------------
// Main topics :
//    Here, you can add the main topics to your Board. The topics can only be
//    combined out of letters and numbers. No spaces are allowed, because
//    a file is created for each topic.
//    If the file doesn't exist, it will be created automatically.
//
//    It is important that you seperate each topic with a "," !
//    (e.g. $topic = array('Allgemeines','Hilfe','Tipps');
//    The number of topics is (nearly) unlimited.

// Hauptkategorien :
//    Hier koennen Sie die Kategorien die in Ihrem Forum angezeigt
//    werden definieren. Es sind nur Zahlen und Buchstaben erlaubt,
//    Leerstellen oder Sonderzeichen koennen unter Umstaenden zu
//    Problemen fuehren.
//
//    Jede Kategorie muss durch ein "," getrennt werden !
//    (z.B.: $topic = array('Allgemeines','Hilfe','Tipps');   

$topics = array('Allgemeines');

// Change the Name and welcome message of your board.
// Hier definieren Sie den Namen des Forums und die Begruessung

$board_name = "KONTENT Forum";
$board_sub = "Herzlich Willkommen in unserem Forum.";

// Whether to allow HTML or not in messages (yes or no)
// Geben Sie an ob HTML in Beitraegen erlaubt ist (yes oder no)

$html = "no";

// Display author's IP address (yes or no)
// IP-Adresse des Beitragsschreibers anzeigen (yes oder no)

$ip_allow = "yes";

// Directory where the forum scripts can be found
// e.g. /scripte/forum if the scripts are in www.yourdomain.de/scripte/forum
// Verzeichnis in dem sich das Forum befindet
// z.B.: /scripte/forum wenn sich die Scripte unter www.yourdomain.de/scripte/forum befinden

$forum_dir = "";

// Directory where the HTML Templates reside in
// Verzeichnis in dem sich die Vorlagedokumente befinden

$template_dir = "templates/";

// Directory where the topic directories and messages reside in
// Verzeichnis in dem sich die Foren und die Forenbeitraege befinden

$data_dir = "daten/";
?>
