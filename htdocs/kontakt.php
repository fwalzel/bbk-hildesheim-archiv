<?php
//Include Common Files @1-83C617B7
define("RelativePath", ".");
define("PathToCurrentPage", "/");
include(RelativePath . "/Common.php");
include(RelativePath . "/Template.php");
include(RelativePath . "/Sorter.php");
include(RelativePath . "/Navigator.php");
  
//End Include Common Files

class clsRecordformular { //formular Class @2-7DBDE7D3

//Variables @2-76058651

    // Public variables
    var $ComponentName;
    var $HTMLFormAction;
    var $PressedButton;
    var $Errors;
    var $ErrorBlock;
    var $FormSubmitted;
    var $FormEnctype;
    var $Visible;
    var $Recordset;

    var $CCSEvents = "";
    var $CCSEventResult;

    var $RelativePath = "";

    var $InsertAllowed = false;
    var $UpdateAllowed = false;
    var $DeleteAllowed = false;
    var $ReadAllowed   = false;
    var $EditMode      = false;
    var $ds;
    var $ValidatingControls;
    var $Controls;

    // Class variables
//End Variables

//Class_Initialize Event @2-D889EF00
    function clsRecordformular($RelativePath = "")
    {

        global $FileName;
        $this->Visible = true;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Record formular/Error";
        $this->ds = new clsformularDataSource();
        $this->InsertAllowed = true;
        if($this->Visible)
        {
            $this->ComponentName = "formular";
            $CCSForm = split(":", CCGetFromGet("ccsForm", ""), 2);
            if(sizeof($CCSForm) == 1)
                $CCSForm[1] = "";
            list($FormName, $FormMethod) = $CCSForm;
            $this->EditMode = ($FormMethod == "Edit");
            $this->FormEnctype = "application/x-www-form-urlencoded";
            $this->FormSubmitted = ($FormName == $this->ComponentName);
            $Method = $this->FormSubmitted ? ccsPost : ccsGet;
            $this->Name = new clsControl(ccsTextBox, "Name", "Name", ccsText, "", CCGetRequestParam("Name", $Method));
            $this->Name->Required = true;
            $this->Vorname = new clsControl(ccsTextBox, "Vorname", "Vorname", ccsText, "", CCGetRequestParam("Vorname", $Method));
            $this->Adresse = new clsControl(ccsTextBox, "Adresse", "Adresse", ccsText, "", CCGetRequestParam("Adresse", $Method));
            $this->PLZ = new clsControl(ccsTextBox, "PLZ", "PLZ", ccsText, "", CCGetRequestParam("PLZ", $Method));
            $this->Ort = new clsControl(ccsTextBox, "Ort", "Ort", ccsText, "", CCGetRequestParam("Ort", $Method));
            $this->Telefon = new clsControl(ccsTextBox, "Telefon", "Telefon", ccsText, "", CCGetRequestParam("Telefon", $Method));
            $this->Fax = new clsControl(ccsTextBox, "Fax", "Fax", ccsText, "", CCGetRequestParam("Fax", $Method));
            $this->Email = new clsControl(ccsTextBox, "Email", "Email", ccsText, "", CCGetRequestParam("Email", $Method));
            $this->Email->Required = true;
            $this->Kommentar = new clsControl(ccsTextArea, "Kommentar", "Kommentar", ccsText, "", CCGetRequestParam("Kommentar", $Method));
            $this->Button_Insert = new clsButton("Button_Insert");
            $this->Button_Cancel = new clsButton("Button_Cancel");
        }
    }
//End Class_Initialize Event

//Initialize Method @2-5D060BAC
    function Initialize()
    {

        if(!$this->Visible)
            return;

    }
//End Initialize Method

//Validate Method @2-23BCB0BD
    function Validate()
    {
        $Validation = true;
        $Where = "";
        if(strlen($this->Email->GetText()) && !preg_match ("/^[\w\.-]{1,}\@([\da-zA-Z-]{1,}\.){1,}[\da-zA-Z-]+$/", $this->Email->GetText())) {
            $this->Email->Errors->addError("Die Eingabepr�fung f�r das Feld Email meldet einen Fehler.");
        }
        $Validation = ($this->Name->Validate() && $Validation);
        $Validation = ($this->Vorname->Validate() && $Validation);
        $Validation = ($this->Adresse->Validate() && $Validation);
        $Validation = ($this->PLZ->Validate() && $Validation);
        $Validation = ($this->Ort->Validate() && $Validation);
        $Validation = ($this->Telefon->Validate() && $Validation);
        $Validation = ($this->Fax->Validate() && $Validation);
        $Validation = ($this->Email->Validate() && $Validation);
        $Validation = ($this->Kommentar->Validate() && $Validation);
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "OnValidate");
        $Validation =  $Validation && ($this->Name->Errors->Count() == 0);
        $Validation =  $Validation && ($this->Vorname->Errors->Count() == 0);
        $Validation =  $Validation && ($this->Adresse->Errors->Count() == 0);
        $Validation =  $Validation && ($this->PLZ->Errors->Count() == 0);
        $Validation =  $Validation && ($this->Ort->Errors->Count() == 0);
        $Validation =  $Validation && ($this->Telefon->Errors->Count() == 0);
        $Validation =  $Validation && ($this->Fax->Errors->Count() == 0);
        $Validation =  $Validation && ($this->Email->Errors->Count() == 0);
        $Validation =  $Validation && ($this->Kommentar->Errors->Count() == 0);
        return (($this->Errors->Count() == 0) && $Validation);
    }
//End Validate Method

//CheckErrors Method @2-0E13E235
    function CheckErrors()
    {
        $errors = false;
        $errors = ($errors || $this->Name->Errors->Count());
        $errors = ($errors || $this->Vorname->Errors->Count());
        $errors = ($errors || $this->Adresse->Errors->Count());
        $errors = ($errors || $this->PLZ->Errors->Count());
        $errors = ($errors || $this->Ort->Errors->Count());
        $errors = ($errors || $this->Telefon->Errors->Count());
        $errors = ($errors || $this->Fax->Errors->Count());
        $errors = ($errors || $this->Email->Errors->Count());
        $errors = ($errors || $this->Kommentar->Errors->Count());
        $errors = ($errors || $this->Errors->Count());
        $errors = ($errors || $this->ds->Errors->Count());
        return $errors;
    }
//End CheckErrors Method

//Operation Method @2-56D8EEBF
    function Operation()
    {
        if(!$this->Visible)
            return;

        global $Redirect;
        global $FileName;

        $this->ds->Prepare();
        if(!$this->FormSubmitted) {
            $this->EditMode = true;
            return;
        }

        if($this->FormSubmitted) {
            $this->PressedButton = "Button_Insert";
            if(strlen(CCGetParam("Button_Insert", ""))) {
                $this->PressedButton = "Button_Insert";
            } else if(strlen(CCGetParam("Button_Cancel", ""))) {
                $this->PressedButton = "Button_Cancel";
            }
        }
        $Redirect = "kontakt.php" . "?" . CCGetQueryString("QueryString", Array("ccsForm"));
        if($this->PressedButton == "Button_Cancel") {
            if(!CCGetEvent($this->Button_Cancel->CCSEvents, "OnClick")) {
                $Redirect = "";
            }
        } else if($this->Validate()) {
            if($this->PressedButton == "Button_Insert") {
                if(!CCGetEvent($this->Button_Insert->CCSEvents, "OnClick") || !$this->InsertRow()) {
                    $Redirect = "";
                } else {
                    $Redirect = "danke.php". "?" . CCGetQueryString("QueryString", Array("ccsForm"));
                }
            }
        } else {
            $Redirect = "";
        }
    }
//End Operation Method

//InsertRow Method @2-4BCDBF98
    function InsertRow()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeInsert");
        if(!$this->InsertAllowed) return false;
        $this->ds->Name->SetValue($this->Name->GetValue());
        $this->ds->Vorname->SetValue($this->Vorname->GetValue());
        $this->ds->Adresse->SetValue($this->Adresse->GetValue());
        $this->ds->PLZ->SetValue($this->PLZ->GetValue());
        $this->ds->Ort->SetValue($this->Ort->GetValue());
        $this->ds->Telefon->SetValue($this->Telefon->GetValue());
        $this->ds->Fax->SetValue($this->Fax->GetValue());
        $this->ds->Email->SetValue($this->Email->GetValue());
        $this->ds->Kommentar->SetValue($this->Kommentar->GetValue());
        $this->ds->Insert();
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterInsert");
        return (!$this->CheckErrors());
    }
//End InsertRow Method

//Show Method @2-C76A4BE8
    function Show()
    {
        global $Tpl;
        global $FileName;
        $Error = "";

        if(!$this->Visible)
            return;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect");


        $RecordBlock = "Record " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $RecordBlock;
        $this->EditMode = $this->EditMode && $this->ReadAllowed;
        if($this->EditMode) {
            if($this->ds->Errors->Count()){
                $this->Errors->AddErrors($this->ds->Errors);
                $this->ds->Errors->clear();
            }
            $this->ds->open();
            if($this->ds->Errors->Count() == 0 && $this->ds->next_record()) {
                $this->ds->SetValues();
                if(!$this->FormSubmitted){
                    $this->Name->SetValue($this->ds->Name->GetValue());
                    $this->Vorname->SetValue($this->ds->Vorname->GetValue());
                    $this->Adresse->SetValue($this->ds->Adresse->GetValue());
                    $this->PLZ->SetValue($this->ds->PLZ->GetValue());
                    $this->Ort->SetValue($this->ds->Ort->GetValue());
                    $this->Telefon->SetValue($this->ds->Telefon->GetValue());
                    $this->Fax->SetValue($this->ds->Fax->GetValue());
                    $this->Email->SetValue($this->ds->Email->GetValue());
                    $this->Kommentar->SetValue($this->ds->Kommentar->GetValue());
                }
            } else {
                $this->EditMode = false;
            }
        }

        if($this->FormSubmitted || $this->CheckErrors()) {
            $Error .= $this->Name->Errors->ToString();
            $Error .= $this->Vorname->Errors->ToString();
            $Error .= $this->Adresse->Errors->ToString();
            $Error .= $this->PLZ->Errors->ToString();
            $Error .= $this->Ort->Errors->ToString();
            $Error .= $this->Telefon->Errors->ToString();
            $Error .= $this->Fax->Errors->ToString();
            $Error .= $this->Email->Errors->ToString();
            $Error .= $this->Kommentar->Errors->ToString();
            $Error .= $this->Errors->ToString();
            $Error .= $this->ds->Errors->ToString();
            $Tpl->SetVar("Error", $Error);
            $Tpl->Parse("Error", false);
        }
        $CCSForm = $this->EditMode ? $this->ComponentName . ":" . "Edit" : $this->ComponentName;
        $this->HTMLFormAction = $FileName . "?" . CCAddParam(CCGetQueryString("QueryString", ""), "ccsForm", $CCSForm);
        $Tpl->SetVar("Action", $this->HTMLFormAction);
        $Tpl->SetVar("HTMLFormName", $this->ComponentName);
        $Tpl->SetVar("HTMLFormEnctype", $this->FormEnctype);
        $this->Button_Insert->Visible = !$this->EditMode && $this->InsertAllowed;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow");
        if(!$this->Visible) {
            $Tpl->block_path = $ParentPath;
            return;
        }

        $this->Name->Show();
        $this->Vorname->Show();
        $this->Adresse->Show();
        $this->PLZ->Show();
        $this->Ort->Show();
        $this->Telefon->Show();
        $this->Fax->Show();
        $this->Email->Show();
        $this->Kommentar->Show();
        $this->Button_Insert->Show();
        $this->Button_Cancel->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
        $this->ds->close();
    }
//End Show Method

} //End formular Class @2-FCB6E20C

class clsformularDataSource extends clsDBConnection1 {  //formularDataSource Class @2-F8CC79F8

//DataSource Variables @2-6D9E80D1
    var $CCSEvents = "";
    var $CCSEventResult;
    var $ErrorBlock;
    var $CmdExecution;

    var $InsertParameters;
    var $wp;
    var $AllParametersSet;


    // Datasource fields
    var $Name;
    var $Vorname;
    var $Adresse;
    var $PLZ;
    var $Ort;
    var $Telefon;
    var $Fax;
    var $Email;
    var $Kommentar;
//End DataSource Variables

//DataSourceClass_Initialize Event @2-CCEBB60B
    function clsformularDataSource()
    {
        $this->ErrorBlock = "Record formular/Error";
        $this->Initialize();
        $this->Name = new clsField("Name", ccsText, "");
        $this->Vorname = new clsField("Vorname", ccsText, "");
        $this->Adresse = new clsField("Adresse", ccsText, "");
        $this->PLZ = new clsField("PLZ", ccsText, "");
        $this->Ort = new clsField("Ort", ccsText, "");
        $this->Telefon = new clsField("Telefon", ccsText, "");
        $this->Fax = new clsField("Fax", ccsText, "");
        $this->Email = new clsField("Email", ccsText, "");
        $this->Kommentar = new clsField("Kommentar", ccsText, "");

    }
//End DataSourceClass_Initialize Event

//Prepare Method @2-DFF3DD87
    function Prepare()
    {
    }
//End Prepare Method

//Open Method @2-186DA063
    function Open()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildSelect");
        $this->SQL = "SELECT *  " .
        "FROM formular";
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteSelect");
        $this->PageSize = 1;
        $this->query($this->OptimizeSQL(CCBuildSQL($this->SQL, $this->Where, $this->Order)));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteSelect");
    }
//End Open Method

//SetValues Method @2-C7A07958
    function SetValues()
    {
        $this->Name->SetDBValue($this->f("Name"));
        $this->Vorname->SetDBValue($this->f("Vorname"));
        $this->Adresse->SetDBValue($this->f("Adresse"));
        $this->PLZ->SetDBValue($this->f("PLZ"));
        $this->Ort->SetDBValue($this->f("Ort"));
        $this->Telefon->SetDBValue($this->f("Telefon"));
        $this->Fax->SetDBValue($this->f("Fax"));
        $this->Email->SetDBValue($this->f("Email"));
        $this->Kommentar->SetDBValue($this->f("Kommentar"));
    }
//End SetValues Method

//Insert Method @2-E55D73AD
    function Insert()
    {
        $this->CmdExecution = true;
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildInsert");
        $this->SQL = "INSERT INTO formular ("
             . "Name, "
             . "Vorname, "
             . "Adresse, "
             . "PLZ, "
             . "Ort, "
             . "Telefon, "
             . "Fax, "
             . "Email, "
             . "Kommentar"
             . ") VALUES ("
             . $this->ToSQL($this->Name->GetDBValue(), $this->Name->DataType) . ", "
             . $this->ToSQL($this->Vorname->GetDBValue(), $this->Vorname->DataType) . ", "
             . $this->ToSQL($this->Adresse->GetDBValue(), $this->Adresse->DataType) . ", "
             . $this->ToSQL($this->PLZ->GetDBValue(), $this->PLZ->DataType) . ", "
             . $this->ToSQL($this->Ort->GetDBValue(), $this->Ort->DataType) . ", "
             . $this->ToSQL($this->Telefon->GetDBValue(), $this->Telefon->DataType) . ", "
             . $this->ToSQL($this->Fax->GetDBValue(), $this->Fax->DataType) . ", "
             . $this->ToSQL($this->Email->GetDBValue(), $this->Email->DataType) . ", "
             . $this->ToSQL($this->Kommentar->GetDBValue(), $this->Kommentar->DataType)
             . ")";
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteInsert");
        if($this->Errors->Count() == 0 && $this->CmdExecution) {
            $this->query($this->SQL);
            $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteInsert");
        }
        $this->close();
    }
//End Insert Method

} //End formularDataSource Class @2-FCB6E20C







//Initialize Page @1-45D35707
// Variables
$FileName = "";
$Redirect = "";
$Tpl = "";
$TemplateFileName = "";
$BlockToParse = "main";
$ComponentName = "";

// Events;
$CCSEvents = "";
$CCSEventResult = "";

$FileName = "kontakt.php";
$Redirect = "";
$TemplateFileName = "kontakt.html";
$BlockToParse = "main";
$TemplateEncoding = "";
$FileEncoding = "";
$PathToRoot = "./";
//End Initialize Page

//Initialize Objects @1-8051EB02
$DBConnection1 = new clsDBConnection1();

// Controls
$formular = new clsRecordformular();
$formular->Initialize();

// Events
include("./kontakt_events.php");
BindEvents();

$CCSEventResult = CCGetEvent($CCSEvents, "AfterInitialize");

$Charset = $Charset ? $Charset : $TemplateEncoding;
if ($Charset)
    header("Content-Type: text/html; charset=" . $Charset);
//End Initialize Objects

//Initialize HTML Template @1-E2A5B61F
$CCSEventResult = CCGetEvent($CCSEvents, "OnInitializeView");
$Tpl = new clsTemplate($FileEncoding, $TemplateEncoding);
$Tpl->LoadTemplate(PathToCurrentPage . $TemplateFileName, $BlockToParse, $TemplateEncoding);
$Tpl->block_path = "/$BlockToParse";
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeShow");
//End Initialize HTML Template

//Execute Components @1-3E68A31D
$formular->Operation();
//End Execute Components

//Go to destination page @1-90289E6F
if($Redirect)
{
    $CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload");
    $DBConnection1->close();
    header("Location: " . $Redirect);
    unset($formular);
    unset($Tpl);
    exit;
}
//End Go to destination page

//Show Page @1-B1E73164
$formular->Show();
$Tpl->block_path = "";
$Tpl->PParse("main", false);
//End Show Page

//Unload Page @1-1283A285
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload");
$DBConnection1->close();
unset($formular);
unset($Tpl);
//End Unload Page


?>
