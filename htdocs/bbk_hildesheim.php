<?php
//Include Common Files @1-83C617B7
define("RelativePath", ".");
define("PathToCurrentPage", "/");
include(RelativePath . "/Common.php");
include(RelativePath . "/Template.php");
include(RelativePath . "/Sorter.php");
include(RelativePath . "/Navigator.php");
  
//End Include Common Files

class clsGridNewGrid1 { //NewGrid1 class @2-4958862A

//Variables @2-33F76C6B

    // Public variables
    var $ComponentName;
    var $Visible;
    var $Errors;
    var $ErrorBlock;
    var $ds; var $PageSize;
    var $SorterName = "";
    var $SorterDirection = "";
    var $PageNumber;

    var $CCSEvents = "";
    var $CCSEventResult;

    var $RelativePath = "";

    // Grid Controls
    var $StaticControls; var $RowControls;
//End Variables

//Class_Initialize Event @2-9D4593C4
    function clsGridNewGrid1($RelativePath = "")
    {
        global $FileName;
        $this->ComponentName = "NewGrid1";
        $this->Visible = True;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Grid NewGrid1";
        $this->ds = new clsNewGrid1DataSource();
        $this->PageSize = 100;
        if($this->PageSize == 0)
            $this->Errors->addError("<p>Form: Grid " . $this->ComponentName . "<br>Error: (CCS06) Invalid page size.</p>");
        $this->PageNumber = intval(CCGetParam($this->ComponentName . "Page", 1));

        $this->Titel = new clsControl(ccsLabel, "Titel", "Titel", ccsText, "", CCGetRequestParam("Titel", ccsGet));
        $this->Titel->HTML = true;
        $this->Text = new clsControl(ccsLabel, "Text", "Text", ccsText, "", CCGetRequestParam("Text", ccsGet));
        $this->Text->HTML = true;
    }
//End Class_Initialize Event

//Initialize Method @2-03626367
    function Initialize()
    {
        if(!$this->Visible) return;

        $this->ds->PageSize = $this->PageSize;
        $this->ds->AbsolutePage = $this->PageNumber;
        $this->ds->SetOrder($this->SorterName, $this->SorterDirection);
    }
//End Initialize Method

//Show Method @2-47BD9BC8
    function Show()
    {
        global $Tpl;
        if(!$this->Visible) return;

        $ShownRecords = 0;


        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect");


        $this->ds->Prepare();
        $this->ds->Open();

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow");
        if(!$this->Visible) return;

        $GridBlock = "Grid " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $GridBlock;


        $is_next_record = $this->ds->next_record();
        if($is_next_record && $ShownRecords < $this->PageSize)
        {
            do {
                    $this->ds->SetValues();
                $Tpl->block_path = $ParentPath . "/" . $GridBlock . "/Row";
                $this->Titel->SetValue($this->ds->Titel->GetValue());
                $this->Text->SetValue($this->ds->Text->GetValue());
                $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShowRow");
                $this->Titel->Show();
                $this->Text->Show();
                $Tpl->block_path = $ParentPath . "/" . $GridBlock;
                $Tpl->parse("Row", true);
                $ShownRecords++;
                $is_next_record = $this->ds->next_record();
            } while ($is_next_record && $ShownRecords < $this->PageSize);
        }
        else // Show NoRecords block if no records are found
        {
            $Tpl->parse("NoRecords", false);
        }

        $errors = $this->GetErrors();
        if(strlen($errors))
        {
            $Tpl->replaceblock("", $errors);
            $Tpl->block_path = $ParentPath;
            return;
        }
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
        $this->ds->close();
    }
//End Show Method

//GetErrors Method @2-6E2D6EFC
    function GetErrors()
    {
        $errors = "";
        $errors .= $this->Titel->Errors->ToString();
        $errors .= $this->Text->Errors->ToString();
        $errors .= $this->Errors->ToString();
        $errors .= $this->ds->Errors->ToString();
        return $errors;
    }
//End GetErrors Method

} //End NewGrid1 Class @2-FCB6E20C

class clsNewGrid1DataSource extends clsDBConnection1 {  //NewGrid1DataSource Class @2-57ECF20B

//DataSource Variables @2-B5FA170B
    var $CCSEvents = "";
    var $CCSEventResult;
    var $ErrorBlock;
    var $CmdExecution;

    var $CountSQL;
    var $wp;


    // Datasource fields
    var $Titel;
    var $Text;
//End DataSource Variables

//DataSourceClass_Initialize Event @2-F1072D88
    function clsNewGrid1DataSource()
    {
        $this->ErrorBlock = "Grid NewGrid1";
        $this->Initialize();
        $this->Titel = new clsField("Titel", ccsText, "");
        $this->Text = new clsField("Text", ccsText, "");

    }
//End DataSourceClass_Initialize Event

//SetOrder Method @2-9E1383D1
    function SetOrder($SorterName, $SorterDirection)
    {
        $this->Order = "";
        $this->Order = CCGetOrder($this->Order, $SorterName, $SorterDirection, 
            "");
    }
//End SetOrder Method

//Prepare Method @2-DFF3DD87
    function Prepare()
    {
    }
//End Prepare Method

//Open Method @2-0BEB2C45
    function Open()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildSelect");
        $this->CountSQL = "SELECT COUNT(*)  " .
        "FROM bbk_hi_text";
        $this->SQL = "SELECT *  " .
        "FROM bbk_hi_text";
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteSelect");
        $this->RecordsCount = CCGetDBValue(CCBuildSQL($this->CountSQL, $this->Where, ""), $this);
        $this->query($this->OptimizeSQL(CCBuildSQL($this->SQL, $this->Where, $this->Order)));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteSelect");
    }
//End Open Method

//SetValues Method @2-6535F388
    function SetValues()
    {
        $this->Titel->SetDBValue($this->f("Titel"));
        $this->Text->SetDBValue($this->f("Text"));
    }
//End SetValues Method

} //End NewGrid1DataSource Class @2-FCB6E20C

class clsGridNewGrid2 { //NewGrid2 class @5-6275D5E9

//Variables @5-33F76C6B

    // Public variables
    var $ComponentName;
    var $Visible;
    var $Errors;
    var $ErrorBlock;
    var $ds; var $PageSize;
    var $SorterName = "";
    var $SorterDirection = "";
    var $PageNumber;

    var $CCSEvents = "";
    var $CCSEventResult;

    var $RelativePath = "";

    // Grid Controls
    var $StaticControls; var $RowControls;
//End Variables

//Class_Initialize Event @5-57FFFB46
    function clsGridNewGrid2($RelativePath = "")
    {
        global $FileName;
        $this->ComponentName = "NewGrid2";
        $this->Visible = True;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Grid NewGrid2";
        $this->ds = new clsNewGrid2DataSource();
        $this->PageSize = 100;
        if($this->PageSize == 0)
            $this->Errors->addError("<p>Form: Grid " . $this->ComponentName . "<br>Error: (CCS06) Invalid page size.</p>");
        $this->PageNumber = intval(CCGetParam($this->ComponentName . "Page", 1));

        $this->Image1 = new clsControl(ccsImage, "Image1", "Image1", ccsText, "", CCGetRequestParam("Image1", ccsGet));
        $this->Vorname = new clsControl(ccsLabel, "Vorname", "Vorname", ccsText, "", CCGetRequestParam("Vorname", ccsGet));
        $this->Name = new clsControl(ccsLabel, "Name", "Name", ccsText, "", CCGetRequestParam("Name", ccsGet));
        $this->Titel = new clsControl(ccsLabel, "Titel", "Titel", ccsText, "", CCGetRequestParam("Titel", ccsGet));
        $this->Email = new clsControl(ccsLabel, "Email", "Email", ccsText, "", CCGetRequestParam("Email", ccsGet));
    }
//End Class_Initialize Event

//Initialize Method @5-03626367
    function Initialize()
    {
        if(!$this->Visible) return;

        $this->ds->PageSize = $this->PageSize;
        $this->ds->AbsolutePage = $this->PageNumber;
        $this->ds->SetOrder($this->SorterName, $this->SorterDirection);
    }
//End Initialize Method

//Show Method @5-665F3C1D
    function Show()
    {
        global $Tpl;
        if(!$this->Visible) return;

        $ShownRecords = 0;


        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect");


        $this->ds->Prepare();
        $this->ds->Open();

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow");
        if(!$this->Visible) return;

        $GridBlock = "Grid " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $GridBlock;


        $is_next_record = $this->ds->next_record();
        if($is_next_record && $ShownRecords < $this->PageSize)
        {
            do {
                    $this->ds->SetValues();
                $Tpl->block_path = $ParentPath . "/" . $GridBlock . "/Row";
                $this->Image1->SetValue($this->ds->Image1->GetValue());
                $this->Vorname->SetValue($this->ds->Vorname->GetValue());
                $this->Name->SetValue($this->ds->Name->GetValue());
                $this->Titel->SetValue($this->ds->Titel->GetValue());
                $this->Email->SetValue($this->ds->Email->GetValue());
                $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShowRow");
                $this->Image1->Show();
                $this->Vorname->Show();
                $this->Name->Show();
                $this->Titel->Show();
                $this->Email->Show();
                $Tpl->block_path = $ParentPath . "/" . $GridBlock;
                $Tpl->parse("Row", true);
                $ShownRecords++;
                $is_next_record = $this->ds->next_record();
            } while ($is_next_record && $ShownRecords < $this->PageSize);
        }
        else // Show NoRecords block if no records are found
        {
            $Tpl->parse("NoRecords", false);
        }

        $errors = $this->GetErrors();
        if(strlen($errors))
        {
            $Tpl->replaceblock("", $errors);
            $Tpl->block_path = $ParentPath;
            return;
        }
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
        $this->ds->close();
    }
//End Show Method

//GetErrors Method @5-6F272577
    function GetErrors()
    {
        $errors = "";
        $errors .= $this->Image1->Errors->ToString();
        $errors .= $this->Vorname->Errors->ToString();
        $errors .= $this->Name->Errors->ToString();
        $errors .= $this->Titel->Errors->ToString();
        $errors .= $this->Email->Errors->ToString();
        $errors .= $this->Errors->ToString();
        $errors .= $this->ds->Errors->ToString();
        return $errors;
    }
//End GetErrors Method

} //End NewGrid2 Class @5-FCB6E20C

class clsNewGrid2DataSource extends clsDBConnection1 {  //NewGrid2DataSource Class @5-DC3FCC12

//DataSource Variables @5-64DCB407
    var $CCSEvents = "";
    var $CCSEventResult;
    var $ErrorBlock;
    var $CmdExecution;

    var $CountSQL;
    var $wp;


    // Datasource fields
    var $Image1;
    var $Vorname;
    var $Name;
    var $Titel;
    var $Email;
//End DataSource Variables

//DataSourceClass_Initialize Event @5-7CA215B2
    function clsNewGrid2DataSource()
    {
        $this->ErrorBlock = "Grid NewGrid2";
        $this->Initialize();
        $this->Image1 = new clsField("Image1", ccsText, "");
        $this->Vorname = new clsField("Vorname", ccsText, "");
        $this->Name = new clsField("Name", ccsText, "");
        $this->Titel = new clsField("Titel", ccsText, "");
        $this->Email = new clsField("Email", ccsText, "");

    }
//End DataSourceClass_Initialize Event

//SetOrder Method @5-4F4089A1
    function SetOrder($SorterName, $SorterDirection)
    {
        $this->Order = "Reihenfolge";
        $this->Order = CCGetOrder($this->Order, $SorterName, $SorterDirection, 
            "");
    }
//End SetOrder Method

//Prepare Method @5-DFF3DD87
    function Prepare()
    {
    }
//End Prepare Method

//Open Method @5-BE69F919
    function Open()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildSelect");
        $this->CountSQL = "SELECT COUNT(*)  " .
        "FROM bbk_hildesheim";
        $this->SQL = "SELECT *  " .
        "FROM bbk_hildesheim";
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteSelect");
        $this->RecordsCount = CCGetDBValue(CCBuildSQL($this->CountSQL, $this->Where, ""), $this);
        $this->query($this->OptimizeSQL(CCBuildSQL($this->SQL, $this->Where, $this->Order)));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteSelect");
    }
//End Open Method

//SetValues Method @5-401BBB28
    function SetValues()
    {
        $this->Image1->SetDBValue($this->f("Foto"));
        $this->Vorname->SetDBValue($this->f("Vorname"));
        $this->Name->SetDBValue($this->f("Name"));
        $this->Titel->SetDBValue($this->f("Titel"));
        $this->Email->SetDBValue($this->f("Email"));
    }
//End SetValues Method

} //End NewGrid2DataSource Class @5-FCB6E20C



//Initialize Page @1-F21A713E
// Variables
$FileName = "";
$Redirect = "";
$Tpl = "";
$TemplateFileName = "";
$BlockToParse = "main";
$ComponentName = "";

// Events;
$CCSEvents = "";
$CCSEventResult = "";

$FileName = "bbk_hildesheim.php";
$Redirect = "";
$TemplateFileName = "bbk_hildesheim.html";
$BlockToParse = "main";
$TemplateEncoding = "";
$FileEncoding = "";
$PathToRoot = "./";
//End Initialize Page

//Initialize Objects @1-BC043536
$DBConnection1 = new clsDBConnection1();

// Controls
$NewGrid1 = new clsGridNewGrid1();
$NewGrid2 = new clsGridNewGrid2();
$NewGrid1->Initialize();
$NewGrid2->Initialize();

$CCSEventResult = CCGetEvent($CCSEvents, "AfterInitialize");

$Charset = $Charset ? $Charset : $TemplateEncoding;
if ($Charset)
    header("Content-Type: text/html; charset=" . $Charset);
//End Initialize Objects

//Initialize HTML Template @1-E2A5B61F
$CCSEventResult = CCGetEvent($CCSEvents, "OnInitializeView");
$Tpl = new clsTemplate($FileEncoding, $TemplateEncoding);
$Tpl->LoadTemplate(PathToCurrentPage . $TemplateFileName, $BlockToParse, $TemplateEncoding);
$Tpl->block_path = "/$BlockToParse";
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeShow");
//End Initialize HTML Template

//Go to destination page @1-53F8349D
if($Redirect)
{
    $CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload");
    $DBConnection1->close();
    header("Location: " . $Redirect);
    unset($NewGrid1);
    unset($NewGrid2);
    unset($Tpl);
    exit;
}
//End Go to destination page

//Show Page @1-2A453184
$NewGrid1->Show();
$NewGrid2->Show();
$Tpl->block_path = "";
$Tpl->PParse("main", false);
//End Show Page

//Unload Page @1-682E2C1B
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload");
$DBConnection1->close();
unset($NewGrid1);
unset($NewGrid2);
unset($Tpl);
//End Unload Page


?>
