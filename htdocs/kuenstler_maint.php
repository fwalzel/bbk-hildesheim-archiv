<?php
//Include Common Files @1-83C617B7
define("RelativePath", ".");
define("PathToCurrentPage", "/");
include(RelativePath . "/Common.php");
include(RelativePath . "/Template.php");
include(RelativePath . "/Sorter.php");
include(RelativePath . "/Navigator.php");
  
//End Include Common Files

class clsRecordkuenstler { //kuenstler Class @2-3DFCF17F

//Variables @2-76058651

    // Public variables
    var $ComponentName;
    var $HTMLFormAction;
    var $PressedButton;
    var $Errors;
    var $ErrorBlock;
    var $FormSubmitted;
    var $FormEnctype;
    var $Visible;
    var $Recordset;

    var $CCSEvents = "";
    var $CCSEventResult;

    var $RelativePath = "";

    var $InsertAllowed = false;
    var $UpdateAllowed = false;
    var $DeleteAllowed = false;
    var $ReadAllowed   = false;
    var $EditMode      = false;
    var $ds;
    var $ValidatingControls;
    var $Controls;

    // Class variables
//End Variables

//Class_Initialize Event @2-9740EA34
    function clsRecordkuenstler($RelativePath = "")
    {

        global $FileName;
        $this->Visible = true;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Record kuenstler/Error";
        $this->ds = new clskuenstlerDataSource();
        $this->InsertAllowed = true;
        $this->UpdateAllowed = true;
        $this->DeleteAllowed = true;
        $this->ReadAllowed = true;
        if($this->Visible)
        {
            $this->ComponentName = "kuenstler";
            $CCSForm = split(":", CCGetFromGet("ccsForm", ""), 2);
            if(sizeof($CCSForm) == 1)
                $CCSForm[1] = "";
            list($FormName, $FormMethod) = $CCSForm;
            $this->EditMode = ($FormMethod == "Edit");
            $this->FormEnctype = "application/x-www-form-urlencoded";
            $this->FormSubmitted = ($FormName == $this->ComponentName);
            $Method = $this->FormSubmitted ? ccsPost : ccsGet;
            $this->Name = new clsControl(ccsTextBox, "Name", "Name", ccsText, "", CCGetRequestParam("Name", $Method));
            $this->Name->Required = true;
            $this->Vorname = new clsControl(ccsTextBox, "Vorname", "Vorname", ccsText, "", CCGetRequestParam("Vorname", $Method));
            $this->Adresse = new clsControl(ccsTextBox, "Adresse", "Adresse", ccsText, "", CCGetRequestParam("Adresse", $Method));
            $this->PLZ = new clsControl(ccsTextBox, "PLZ", "PLZ", ccsText, "", CCGetRequestParam("PLZ", $Method));
            $this->Stadt = new clsControl(ccsTextBox, "Stadt", "Stadt", ccsText, "", CCGetRequestParam("Stadt", $Method));
            $this->Telefon = new clsControl(ccsTextBox, "Telefon", "Telefon", ccsText, "", CCGetRequestParam("Telefon", $Method));
            $this->E_Mail = new clsControl(ccsTextBox, "E_Mail", "E Mail", ccsText, "", CCGetRequestParam("E_Mail", $Method));
            $this->Webseite = new clsControl(ccsTextBox, "Webseite", "Webseite", ccsText, "", CCGetRequestParam("Webseite", $Method));
            $this->Foto = new clsControl(ccsTextBox, "Foto", "Foto", ccsText, "", CCGetRequestParam("Foto", $Method));
            $this->Vita = new clsControl(ccsTextArea, "Vita", "Vita", ccsMemo, "", CCGetRequestParam("Vita", $Method));
            $this->Kategorie = new clsControl(ccsTextBox, "Kategorie", "Kategorie", ccsText, "", CCGetRequestParam("Kategorie", $Method));
            $this->K2 = new clsControl(ccsTextBox, "K2", "K2", ccsText, "", CCGetRequestParam("K2", $Method));
            $this->K3 = new clsControl(ccsTextBox, "K3", "K3", ccsText, "", CCGetRequestParam("K3", $Method));
            $this->K4 = new clsControl(ccsTextBox, "K4", "K4", ccsText, "", CCGetRequestParam("K4", $Method));
            $this->K5 = new clsControl(ccsTextBox, "K5", "K5", ccsText, "", CCGetRequestParam("K5", $Method));
            $this->K6 = new clsControl(ccsTextBox, "K6", "K6", ccsText, "", CCGetRequestParam("K6", $Method));
            $this->ListBox1 = new clsControl(ccsListBox, "ListBox1", "ListBox1", ccsText, "", CCGetRequestParam("ListBox1", $Method));
            $this->ListBox1->DSType = dsTable;
            list($this->ListBox1->BoundColumn, $this->ListBox1->TextColumn, $this->ListBox1->DBFormat) = array("Kategorie", "Kategorie", "");
            $this->ListBox1->ds = new clsDBConnection1();
            $this->ListBox1->ds->SQL = "SELECT *  " .
"FROM kategorie";
            $this->K8 = new clsControl(ccsListBox, "K8", "K8", ccsText, "", CCGetRequestParam("K8", $Method));
            $this->K8->DSType = dsTable;
            list($this->K8->BoundColumn, $this->K8->TextColumn, $this->K8->DBFormat) = array("Kategorie", "Kategorie", "");
            $this->K8->ds = new clsDBConnection1();
            $this->K8->ds->SQL = "SELECT *  " .
"FROM kategorie";
            $this->K9 = new clsControl(ccsListBox, "K9", "K9", ccsText, "", CCGetRequestParam("K9", $Method));
            $this->K9->DSType = dsTable;
            list($this->K9->BoundColumn, $this->K9->TextColumn, $this->K9->DBFormat) = array("Kategorie", "Kategorie", "");
            $this->K9->ds = new clsDBConnection1();
            $this->K9->ds->SQL = "SELECT *  " .
"FROM kategorie";
            $this->Arbeitsbeispiele = new clsControl(ccsTextBox, "Arbeitsbeispiele", "Arbeitsbeispiele", ccsText, "", CCGetRequestParam("Arbeitsbeispiele", $Method));
            $this->Austellungstermine = new clsControl(ccsTextBox, "Austellungstermine", "Austellungstermine", ccsText, "", CCGetRequestParam("Austellungstermine", $Method));
            $this->Button_Insert = new clsButton("Button_Insert");
            $this->Button_Update = new clsButton("Button_Update");
            $this->Button_Delete = new clsButton("Button_Delete");
        }
    }
//End Class_Initialize Event

//Initialize Method @2-FA88DE75
    function Initialize()
    {

        if(!$this->Visible)
            return;

        $this->ds->Parameters["urlKuenstler_ID"] = CCGetFromGet("Kuenstler_ID", "");
    }
//End Initialize Method

//Validate Method @2-6A16B29B
    function Validate()
    {
        $Validation = true;
        $Where = "";
        $Validation = ($this->Name->Validate() && $Validation);
        $Validation = ($this->Vorname->Validate() && $Validation);
        $Validation = ($this->Adresse->Validate() && $Validation);
        $Validation = ($this->PLZ->Validate() && $Validation);
        $Validation = ($this->Stadt->Validate() && $Validation);
        $Validation = ($this->Telefon->Validate() && $Validation);
        $Validation = ($this->E_Mail->Validate() && $Validation);
        $Validation = ($this->Webseite->Validate() && $Validation);
        $Validation = ($this->Foto->Validate() && $Validation);
        $Validation = ($this->Vita->Validate() && $Validation);
        $Validation = ($this->Kategorie->Validate() && $Validation);
        $Validation = ($this->K2->Validate() && $Validation);
        $Validation = ($this->K3->Validate() && $Validation);
        $Validation = ($this->K4->Validate() && $Validation);
        $Validation = ($this->K5->Validate() && $Validation);
        $Validation = ($this->K6->Validate() && $Validation);
        $Validation = ($this->ListBox1->Validate() && $Validation);
        $Validation = ($this->K8->Validate() && $Validation);
        $Validation = ($this->K9->Validate() && $Validation);
        $Validation = ($this->Arbeitsbeispiele->Validate() && $Validation);
        $Validation = ($this->Austellungstermine->Validate() && $Validation);
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "OnValidate");
        $Validation =  $Validation && ($this->Name->Errors->Count() == 0);
        $Validation =  $Validation && ($this->Vorname->Errors->Count() == 0);
        $Validation =  $Validation && ($this->Adresse->Errors->Count() == 0);
        $Validation =  $Validation && ($this->PLZ->Errors->Count() == 0);
        $Validation =  $Validation && ($this->Stadt->Errors->Count() == 0);
        $Validation =  $Validation && ($this->Telefon->Errors->Count() == 0);
        $Validation =  $Validation && ($this->E_Mail->Errors->Count() == 0);
        $Validation =  $Validation && ($this->Webseite->Errors->Count() == 0);
        $Validation =  $Validation && ($this->Foto->Errors->Count() == 0);
        $Validation =  $Validation && ($this->Vita->Errors->Count() == 0);
        $Validation =  $Validation && ($this->Kategorie->Errors->Count() == 0);
        $Validation =  $Validation && ($this->K2->Errors->Count() == 0);
        $Validation =  $Validation && ($this->K3->Errors->Count() == 0);
        $Validation =  $Validation && ($this->K4->Errors->Count() == 0);
        $Validation =  $Validation && ($this->K5->Errors->Count() == 0);
        $Validation =  $Validation && ($this->K6->Errors->Count() == 0);
        $Validation =  $Validation && ($this->ListBox1->Errors->Count() == 0);
        $Validation =  $Validation && ($this->K8->Errors->Count() == 0);
        $Validation =  $Validation && ($this->K9->Errors->Count() == 0);
        $Validation =  $Validation && ($this->Arbeitsbeispiele->Errors->Count() == 0);
        $Validation =  $Validation && ($this->Austellungstermine->Errors->Count() == 0);
        return (($this->Errors->Count() == 0) && $Validation);
    }
//End Validate Method

//CheckErrors Method @2-6485DA09
    function CheckErrors()
    {
        $errors = false;
        $errors = ($errors || $this->Name->Errors->Count());
        $errors = ($errors || $this->Vorname->Errors->Count());
        $errors = ($errors || $this->Adresse->Errors->Count());
        $errors = ($errors || $this->PLZ->Errors->Count());
        $errors = ($errors || $this->Stadt->Errors->Count());
        $errors = ($errors || $this->Telefon->Errors->Count());
        $errors = ($errors || $this->E_Mail->Errors->Count());
        $errors = ($errors || $this->Webseite->Errors->Count());
        $errors = ($errors || $this->Foto->Errors->Count());
        $errors = ($errors || $this->Vita->Errors->Count());
        $errors = ($errors || $this->Kategorie->Errors->Count());
        $errors = ($errors || $this->K2->Errors->Count());
        $errors = ($errors || $this->K3->Errors->Count());
        $errors = ($errors || $this->K4->Errors->Count());
        $errors = ($errors || $this->K5->Errors->Count());
        $errors = ($errors || $this->K6->Errors->Count());
        $errors = ($errors || $this->ListBox1->Errors->Count());
        $errors = ($errors || $this->K8->Errors->Count());
        $errors = ($errors || $this->K9->Errors->Count());
        $errors = ($errors || $this->Arbeitsbeispiele->Errors->Count());
        $errors = ($errors || $this->Austellungstermine->Errors->Count());
        $errors = ($errors || $this->Errors->Count());
        $errors = ($errors || $this->ds->Errors->Count());
        return $errors;
    }
//End CheckErrors Method

//Operation Method @2-5B0CDE27
    function Operation()
    {
        if(!$this->Visible)
            return;

        global $Redirect;
        global $FileName;

        $this->ds->Prepare();
        if(!$this->FormSubmitted) {
            $this->EditMode = $this->ds->AllParametersSet;
            return;
        }

        if($this->FormSubmitted) {
            $this->PressedButton = $this->EditMode ? "Button_Update" : "Button_Insert";
            if(strlen(CCGetParam("Button_Insert", ""))) {
                $this->PressedButton = "Button_Insert";
            } else if(strlen(CCGetParam("Button_Update", ""))) {
                $this->PressedButton = "Button_Update";
            } else if(strlen(CCGetParam("Button_Delete", ""))) {
                $this->PressedButton = "Button_Delete";
            }
        }
        $Redirect = "kuenstler_list.php" . "?" . CCGetQueryString("QueryString", Array("ccsForm"));
        if($this->PressedButton == "Button_Delete") {
            if(!CCGetEvent($this->Button_Delete->CCSEvents, "OnClick") || !$this->DeleteRow()) {
                $Redirect = "";
            }
        } else if($this->Validate()) {
            if($this->PressedButton == "Button_Insert") {
                if(!CCGetEvent($this->Button_Insert->CCSEvents, "OnClick") || !$this->InsertRow()) {
                    $Redirect = "";
                }
            } else if($this->PressedButton == "Button_Update") {
                if(!CCGetEvent($this->Button_Update->CCSEvents, "OnClick") || !$this->UpdateRow()) {
                    $Redirect = "";
                }
            }
        } else {
            $Redirect = "";
        }
    }
//End Operation Method

//InsertRow Method @2-62614383
    function InsertRow()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeInsert");
        if(!$this->InsertAllowed) return false;
        $this->ds->Name->SetValue($this->Name->GetValue());
        $this->ds->Vorname->SetValue($this->Vorname->GetValue());
        $this->ds->Adresse->SetValue($this->Adresse->GetValue());
        $this->ds->PLZ->SetValue($this->PLZ->GetValue());
        $this->ds->Stadt->SetValue($this->Stadt->GetValue());
        $this->ds->Telefon->SetValue($this->Telefon->GetValue());
        $this->ds->E_Mail->SetValue($this->E_Mail->GetValue());
        $this->ds->Webseite->SetValue($this->Webseite->GetValue());
        $this->ds->Foto->SetValue($this->Foto->GetValue());
        $this->ds->Vita->SetValue($this->Vita->GetValue());
        $this->ds->Kategorie->SetValue($this->Kategorie->GetValue());
        $this->ds->K2->SetValue($this->K2->GetValue());
        $this->ds->K3->SetValue($this->K3->GetValue());
        $this->ds->K4->SetValue($this->K4->GetValue());
        $this->ds->K5->SetValue($this->K5->GetValue());
        $this->ds->K6->SetValue($this->K6->GetValue());
        $this->ds->ListBox1->SetValue($this->ListBox1->GetValue());
        $this->ds->K8->SetValue($this->K8->GetValue());
        $this->ds->K9->SetValue($this->K9->GetValue());
        $this->ds->Arbeitsbeispiele->SetValue($this->Arbeitsbeispiele->GetValue());
        $this->ds->Austellungstermine->SetValue($this->Austellungstermine->GetValue());
        $this->ds->Insert();
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterInsert");
        return (!$this->CheckErrors());
    }
//End InsertRow Method

//UpdateRow Method @2-3093A197
    function UpdateRow()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeUpdate");
        if(!$this->UpdateAllowed) return false;
        $this->ds->Name->SetValue($this->Name->GetValue());
        $this->ds->Vorname->SetValue($this->Vorname->GetValue());
        $this->ds->Adresse->SetValue($this->Adresse->GetValue());
        $this->ds->PLZ->SetValue($this->PLZ->GetValue());
        $this->ds->Stadt->SetValue($this->Stadt->GetValue());
        $this->ds->Telefon->SetValue($this->Telefon->GetValue());
        $this->ds->E_Mail->SetValue($this->E_Mail->GetValue());
        $this->ds->Webseite->SetValue($this->Webseite->GetValue());
        $this->ds->Foto->SetValue($this->Foto->GetValue());
        $this->ds->Vita->SetValue($this->Vita->GetValue());
        $this->ds->Kategorie->SetValue($this->Kategorie->GetValue());
        $this->ds->K2->SetValue($this->K2->GetValue());
        $this->ds->K3->SetValue($this->K3->GetValue());
        $this->ds->K4->SetValue($this->K4->GetValue());
        $this->ds->K5->SetValue($this->K5->GetValue());
        $this->ds->K6->SetValue($this->K6->GetValue());
        $this->ds->ListBox1->SetValue($this->ListBox1->GetValue());
        $this->ds->K8->SetValue($this->K8->GetValue());
        $this->ds->K9->SetValue($this->K9->GetValue());
        $this->ds->Arbeitsbeispiele->SetValue($this->Arbeitsbeispiele->GetValue());
        $this->ds->Austellungstermine->SetValue($this->Austellungstermine->GetValue());
        $this->ds->Update();
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterUpdate");
        return (!$this->CheckErrors());
    }
//End UpdateRow Method

//DeleteRow Method @2-91867A4A
    function DeleteRow()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeDelete");
        if(!$this->DeleteAllowed) return false;
        $this->ds->Delete();
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterDelete");
        return (!$this->CheckErrors());
    }
//End DeleteRow Method

//Show Method @2-82E4983A
    function Show()
    {
        global $Tpl;
        global $FileName;
        $Error = "";

        if(!$this->Visible)
            return;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect");

        $this->ListBox1->Prepare();
        $this->K8->Prepare();
        $this->K9->Prepare();

        $RecordBlock = "Record " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $RecordBlock;
        $this->EditMode = $this->EditMode && $this->ReadAllowed;
        if($this->EditMode) {
            if($this->ds->Errors->Count()){
                $this->Errors->AddErrors($this->ds->Errors);
                $this->ds->Errors->clear();
            }
            $this->ds->open();
            if($this->ds->Errors->Count() == 0 && $this->ds->next_record()) {
                $this->ds->SetValues();
                if(!$this->FormSubmitted){
                    $this->Name->SetValue($this->ds->Name->GetValue());
                    $this->Vorname->SetValue($this->ds->Vorname->GetValue());
                    $this->Adresse->SetValue($this->ds->Adresse->GetValue());
                    $this->PLZ->SetValue($this->ds->PLZ->GetValue());
                    $this->Stadt->SetValue($this->ds->Stadt->GetValue());
                    $this->Telefon->SetValue($this->ds->Telefon->GetValue());
                    $this->E_Mail->SetValue($this->ds->E_Mail->GetValue());
                    $this->Webseite->SetValue($this->ds->Webseite->GetValue());
                    $this->Foto->SetValue($this->ds->Foto->GetValue());
                    $this->Vita->SetValue($this->ds->Vita->GetValue());
                    $this->Kategorie->SetValue($this->ds->Kategorie->GetValue());
                    $this->K2->SetValue($this->ds->K2->GetValue());
                    $this->K3->SetValue($this->ds->K3->GetValue());
                    $this->K4->SetValue($this->ds->K4->GetValue());
                    $this->K5->SetValue($this->ds->K5->GetValue());
                    $this->K6->SetValue($this->ds->K6->GetValue());
                    $this->ListBox1->SetValue($this->ds->ListBox1->GetValue());
                    $this->K8->SetValue($this->ds->K8->GetValue());
                    $this->K9->SetValue($this->ds->K9->GetValue());
                    $this->Arbeitsbeispiele->SetValue($this->ds->Arbeitsbeispiele->GetValue());
                    $this->Austellungstermine->SetValue($this->ds->Austellungstermine->GetValue());
                }
            } else {
                $this->EditMode = false;
            }
        }

        if($this->FormSubmitted || $this->CheckErrors()) {
            $Error .= $this->Name->Errors->ToString();
            $Error .= $this->Vorname->Errors->ToString();
            $Error .= $this->Adresse->Errors->ToString();
            $Error .= $this->PLZ->Errors->ToString();
            $Error .= $this->Stadt->Errors->ToString();
            $Error .= $this->Telefon->Errors->ToString();
            $Error .= $this->E_Mail->Errors->ToString();
            $Error .= $this->Webseite->Errors->ToString();
            $Error .= $this->Foto->Errors->ToString();
            $Error .= $this->Vita->Errors->ToString();
            $Error .= $this->Kategorie->Errors->ToString();
            $Error .= $this->K2->Errors->ToString();
            $Error .= $this->K3->Errors->ToString();
            $Error .= $this->K4->Errors->ToString();
            $Error .= $this->K5->Errors->ToString();
            $Error .= $this->K6->Errors->ToString();
            $Error .= $this->ListBox1->Errors->ToString();
            $Error .= $this->K8->Errors->ToString();
            $Error .= $this->K9->Errors->ToString();
            $Error .= $this->Arbeitsbeispiele->Errors->ToString();
            $Error .= $this->Austellungstermine->Errors->ToString();
            $Error .= $this->Errors->ToString();
            $Error .= $this->ds->Errors->ToString();
            $Tpl->SetVar("Error", $Error);
            $Tpl->Parse("Error", false);
        }
        $CCSForm = $this->EditMode ? $this->ComponentName . ":" . "Edit" : $this->ComponentName;
        $this->HTMLFormAction = $FileName . "?" . CCAddParam(CCGetQueryString("QueryString", ""), "ccsForm", $CCSForm);
        $Tpl->SetVar("Action", $this->HTMLFormAction);
        $Tpl->SetVar("HTMLFormName", $this->ComponentName);
        $Tpl->SetVar("HTMLFormEnctype", $this->FormEnctype);
        $this->Button_Insert->Visible = !$this->EditMode && $this->InsertAllowed;
        $this->Button_Update->Visible = $this->EditMode && $this->UpdateAllowed;
        $this->Button_Delete->Visible = $this->EditMode && $this->DeleteAllowed;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow");
        if(!$this->Visible) {
            $Tpl->block_path = $ParentPath;
            return;
        }

        $this->Name->Show();
        $this->Vorname->Show();
        $this->Adresse->Show();
        $this->PLZ->Show();
        $this->Stadt->Show();
        $this->Telefon->Show();
        $this->E_Mail->Show();
        $this->Webseite->Show();
        $this->Foto->Show();
        $this->Vita->Show();
        $this->Kategorie->Show();
        $this->K2->Show();
        $this->K3->Show();
        $this->K4->Show();
        $this->K5->Show();
        $this->K6->Show();
        $this->ListBox1->Show();
        $this->K8->Show();
        $this->K9->Show();
        $this->Arbeitsbeispiele->Show();
        $this->Austellungstermine->Show();
        $this->Button_Insert->Show();
        $this->Button_Update->Show();
        $this->Button_Delete->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
        $this->ds->close();
    }
//End Show Method

} //End kuenstler Class @2-FCB6E20C

class clskuenstlerDataSource extends clsDBConnection1 {  //kuenstlerDataSource Class @2-03C1C86E

//DataSource Variables @2-E458281A
    var $CCSEvents = "";
    var $CCSEventResult;
    var $ErrorBlock;
    var $CmdExecution;

    var $InsertParameters;
    var $UpdateParameters;
    var $DeleteParameters;
    var $wp;
    var $AllParametersSet;


    // Datasource fields
    var $Name;
    var $Vorname;
    var $Adresse;
    var $PLZ;
    var $Stadt;
    var $Telefon;
    var $E_Mail;
    var $Webseite;
    var $Foto;
    var $Vita;
    var $Kategorie;
    var $K2;
    var $K3;
    var $K4;
    var $K5;
    var $K6;
    var $ListBox1;
    var $K8;
    var $K9;
    var $Arbeitsbeispiele;
    var $Austellungstermine;
//End DataSource Variables

//DataSourceClass_Initialize Event @2-27144FCD
    function clskuenstlerDataSource()
    {
        $this->ErrorBlock = "Record kuenstler/Error";
        $this->Initialize();
        $this->Name = new clsField("Name", ccsText, "");
        $this->Vorname = new clsField("Vorname", ccsText, "");
        $this->Adresse = new clsField("Adresse", ccsText, "");
        $this->PLZ = new clsField("PLZ", ccsText, "");
        $this->Stadt = new clsField("Stadt", ccsText, "");
        $this->Telefon = new clsField("Telefon", ccsText, "");
        $this->E_Mail = new clsField("E_Mail", ccsText, "");
        $this->Webseite = new clsField("Webseite", ccsText, "");
        $this->Foto = new clsField("Foto", ccsText, "");
        $this->Vita = new clsField("Vita", ccsMemo, "");
        $this->Kategorie = new clsField("Kategorie", ccsText, "");
        $this->K2 = new clsField("K2", ccsText, "");
        $this->K3 = new clsField("K3", ccsText, "");
        $this->K4 = new clsField("K4", ccsText, "");
        $this->K5 = new clsField("K5", ccsText, "");
        $this->K6 = new clsField("K6", ccsText, "");
        $this->ListBox1 = new clsField("ListBox1", ccsText, "");
        $this->K8 = new clsField("K8", ccsText, "");
        $this->K9 = new clsField("K9", ccsText, "");
        $this->Arbeitsbeispiele = new clsField("Arbeitsbeispiele", ccsText, "");
        $this->Austellungstermine = new clsField("Austellungstermine", ccsText, "");

    }
//End DataSourceClass_Initialize Event

//Prepare Method @2-1F9C2320
    function Prepare()
    {
        $this->wp = new clsSQLParameters($this->ErrorBlock);
        $this->wp->AddParameter("1", "urlKuenstler_ID", ccsInteger, "", "", $this->Parameters["urlKuenstler_ID"], "", false);
        $this->AllParametersSet = $this->wp->AllParamsSet();
        $this->wp->Criterion[1] = $this->wp->Operation(opEqual, "Kuenstler_ID", $this->wp->GetDBValue("1"), $this->ToSQL($this->wp->GetDBValue("1"), ccsInteger),false);
        $this->Where = 
             $this->wp->Criterion[1];
    }
//End Prepare Method

//Open Method @2-EB7F5BBD
    function Open()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildSelect");
        $this->SQL = "SELECT *  " .
        "FROM kuenstler";
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteSelect");
        $this->PageSize = 1;
        $this->query($this->OptimizeSQL(CCBuildSQL($this->SQL, $this->Where, $this->Order)));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteSelect");
    }
//End Open Method

//SetValues Method @2-903774C3
    function SetValues()
    {
        $this->Name->SetDBValue($this->f("Name"));
        $this->Vorname->SetDBValue($this->f("Vorname"));
        $this->Adresse->SetDBValue($this->f("Adresse"));
        $this->PLZ->SetDBValue($this->f("PLZ"));
        $this->Stadt->SetDBValue($this->f("Stadt"));
        $this->Telefon->SetDBValue($this->f("Telefon"));
        $this->E_Mail->SetDBValue($this->f("email"));
        $this->Webseite->SetDBValue($this->f("Webseite"));
        $this->Foto->SetDBValue($this->f("Foto"));
        $this->Vita->SetDBValue($this->f("Vita"));
        $this->Kategorie->SetDBValue($this->f("Kategorie"));
        $this->K2->SetDBValue($this->f("K2"));
        $this->K3->SetDBValue($this->f("K3"));
        $this->K4->SetDBValue($this->f("K4"));
        $this->K5->SetDBValue($this->f("K5"));
        $this->K6->SetDBValue($this->f("K6"));
        $this->ListBox1->SetDBValue($this->f("K7"));
        $this->K8->SetDBValue($this->f("K8"));
        $this->K9->SetDBValue($this->f("K9"));
        $this->Arbeitsbeispiele->SetDBValue($this->f("Arbeitsbeispiele"));
        $this->Austellungstermine->SetDBValue($this->f("Austellungstermine"));
    }
//End SetValues Method

//Insert Method @2-16698ADD
    function Insert()
    {
        $this->CmdExecution = true;
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildInsert");
        $this->SQL = "INSERT INTO kuenstler ("
             . "Name, "
             . "Vorname, "
             . "Adresse, "
             . "PLZ, "
             . "Stadt, "
             . "Telefon, "
             . "email, "
             . "Webseite, "
             . "Foto, "
             . "Vita, "
             . "Kategorie, "
             . "K2, "
             . "K3, "
             . "K4, "
             . "K5, "
             . "K6, "
             . "K7, "
             . "K8, "
             . "K9, "
             . "Arbeitsbeispiele, "
             . "Austellungstermine"
             . ") VALUES ("
             . $this->ToSQL($this->Name->GetDBValue(), $this->Name->DataType) . ", "
             . $this->ToSQL($this->Vorname->GetDBValue(), $this->Vorname->DataType) . ", "
             . $this->ToSQL($this->Adresse->GetDBValue(), $this->Adresse->DataType) . ", "
             . $this->ToSQL($this->PLZ->GetDBValue(), $this->PLZ->DataType) . ", "
             . $this->ToSQL($this->Stadt->GetDBValue(), $this->Stadt->DataType) . ", "
             . $this->ToSQL($this->Telefon->GetDBValue(), $this->Telefon->DataType) . ", "
             . $this->ToSQL($this->E_Mail->GetDBValue(), $this->E_Mail->DataType) . ", "
             . $this->ToSQL($this->Webseite->GetDBValue(), $this->Webseite->DataType) . ", "
             . $this->ToSQL($this->Foto->GetDBValue(), $this->Foto->DataType) . ", "
             . $this->ToSQL($this->Vita->GetDBValue(), $this->Vita->DataType) . ", "
             . $this->ToSQL($this->Kategorie->GetDBValue(), $this->Kategorie->DataType) . ", "
             . $this->ToSQL($this->K2->GetDBValue(), $this->K2->DataType) . ", "
             . $this->ToSQL($this->K3->GetDBValue(), $this->K3->DataType) . ", "
             . $this->ToSQL($this->K4->GetDBValue(), $this->K4->DataType) . ", "
             . $this->ToSQL($this->K5->GetDBValue(), $this->K5->DataType) . ", "
             . $this->ToSQL($this->K6->GetDBValue(), $this->K6->DataType) . ", "
             . $this->ToSQL($this->ListBox1->GetDBValue(), $this->ListBox1->DataType) . ", "
             . $this->ToSQL($this->K8->GetDBValue(), $this->K8->DataType) . ", "
             . $this->ToSQL($this->K9->GetDBValue(), $this->K9->DataType) . ", "
             . $this->ToSQL($this->Arbeitsbeispiele->GetDBValue(), $this->Arbeitsbeispiele->DataType) . ", "
             . $this->ToSQL($this->Austellungstermine->GetDBValue(), $this->Austellungstermine->DataType)
             . ")";
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteInsert");
        if($this->Errors->Count() == 0 && $this->CmdExecution) {
            $this->query($this->SQL);
            $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteInsert");
        }
        $this->close();
    }
//End Insert Method

//Update Method @2-236E8F49
    function Update()
    {
        $this->CmdExecution = true;
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildUpdate");
        $this->SQL = "UPDATE kuenstler SET "
             . "Name=" . $this->ToSQL($this->Name->GetDBValue(), $this->Name->DataType) . ", "
             . "Vorname=" . $this->ToSQL($this->Vorname->GetDBValue(), $this->Vorname->DataType) . ", "
             . "Adresse=" . $this->ToSQL($this->Adresse->GetDBValue(), $this->Adresse->DataType) . ", "
             . "PLZ=" . $this->ToSQL($this->PLZ->GetDBValue(), $this->PLZ->DataType) . ", "
             . "Stadt=" . $this->ToSQL($this->Stadt->GetDBValue(), $this->Stadt->DataType) . ", "
             . "Telefon=" . $this->ToSQL($this->Telefon->GetDBValue(), $this->Telefon->DataType) . ", "
             . "email=" . $this->ToSQL($this->E_Mail->GetDBValue(), $this->E_Mail->DataType) . ", "
             . "Webseite=" . $this->ToSQL($this->Webseite->GetDBValue(), $this->Webseite->DataType) . ", "
             . "Foto=" . $this->ToSQL($this->Foto->GetDBValue(), $this->Foto->DataType) . ", "
             . "Vita=" . $this->ToSQL($this->Vita->GetDBValue(), $this->Vita->DataType) . ", "
             . "Kategorie=" . $this->ToSQL($this->Kategorie->GetDBValue(), $this->Kategorie->DataType) . ", "
             . "K2=" . $this->ToSQL($this->K2->GetDBValue(), $this->K2->DataType) . ", "
             . "K3=" . $this->ToSQL($this->K3->GetDBValue(), $this->K3->DataType) . ", "
             . "K4=" . $this->ToSQL($this->K4->GetDBValue(), $this->K4->DataType) . ", "
             . "K5=" . $this->ToSQL($this->K5->GetDBValue(), $this->K5->DataType) . ", "
             . "K6=" . $this->ToSQL($this->K6->GetDBValue(), $this->K6->DataType) . ", "
             . "K7=" . $this->ToSQL($this->ListBox1->GetDBValue(), $this->ListBox1->DataType) . ", "
             . "K8=" . $this->ToSQL($this->K8->GetDBValue(), $this->K8->DataType) . ", "
             . "K9=" . $this->ToSQL($this->K9->GetDBValue(), $this->K9->DataType) . ", "
             . "Arbeitsbeispiele=" . $this->ToSQL($this->Arbeitsbeispiele->GetDBValue(), $this->Arbeitsbeispiele->DataType) . ", "
             . "Austellungstermine=" . $this->ToSQL($this->Austellungstermine->GetDBValue(), $this->Austellungstermine->DataType);
        $this->SQL = CCBuildSQL($this->SQL, $this->Where, "");
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteUpdate");
        if($this->Errors->Count() == 0 && $this->CmdExecution) {
            $this->query($this->SQL);
            $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteUpdate");
        }
        $this->close();
    }
//End Update Method

//Delete Method @2-4E7F57D9
    function Delete()
    {
        $this->CmdExecution = true;
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildDelete");
        $this->SQL = "DELETE FROM kuenstler";
        $this->SQL = CCBuildSQL($this->SQL, $this->Where, "");
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteDelete");
        if($this->Errors->Count() == 0 && $this->CmdExecution) {
            $this->query($this->SQL);
            $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteDelete");
        }
        $this->close();
    }
//End Delete Method

} //End kuenstlerDataSource Class @2-FCB6E20C

//Initialize Page @1-D818CA9D
// Variables
$FileName = "";
$Redirect = "";
$Tpl = "";
$TemplateFileName = "";
$BlockToParse = "main";
$ComponentName = "";

// Events;
$CCSEvents = "";
$CCSEventResult = "";

$FileName = "kuenstler_maint.php";
$Redirect = "";
$TemplateFileName = "kuenstler_maint.html";
$BlockToParse = "main";
$TemplateEncoding = "";
$FileEncoding = "";
$PathToRoot = "./";
//End Initialize Page

//Initialize Objects @1-74985F30
$DBConnection1 = new clsDBConnection1();

// Controls
$kuenstler = new clsRecordkuenstler();
$kuenstler->Initialize();

$CCSEventResult = CCGetEvent($CCSEvents, "AfterInitialize");

$Charset = $Charset ? $Charset : $TemplateEncoding;
if ($Charset)
    header("Content-Type: text/html; charset=" . $Charset);
//End Initialize Objects

//Initialize HTML Template @1-E2A5B61F
$CCSEventResult = CCGetEvent($CCSEvents, "OnInitializeView");
$Tpl = new clsTemplate($FileEncoding, $TemplateEncoding);
$Tpl->LoadTemplate(PathToCurrentPage . $TemplateFileName, $BlockToParse, $TemplateEncoding);
$Tpl->block_path = "/$BlockToParse";
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeShow");
//End Initialize HTML Template

//Execute Components @1-D50641DA
$kuenstler->Operation();
//End Execute Components

//Go to destination page @1-3120B208
if($Redirect)
{
    $CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload");
    $DBConnection1->close();
    header("Location: " . $Redirect);
    unset($kuenstler);
    unset($Tpl);
    exit;
}
//End Go to destination page

//Show Page @1-5C280368
$kuenstler->Show();
$Tpl->block_path = "";
$Tpl->PParse("main", false);
//End Show Page

//Unload Page @1-8C468516
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload");
$DBConnection1->close();
unset($kuenstler);
unset($Tpl);
//End Unload Page


?>
