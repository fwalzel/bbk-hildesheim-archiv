<?php
//BindEvents Method @1-EAE72250
function BindEvents()
{
    global $kuenstler1;
    $kuenstler1->kuenstler_TotalRecords->CCSEvents["BeforeShow"] = "kuenstler1_kuenstler_TotalRecords_BeforeShow";
}
//End BindEvents Method

//kuenstler1_kuenstler_TotalRecords_BeforeShow @7-836C30AC
function kuenstler1_kuenstler_TotalRecords_BeforeShow()
{
    $kuenstler1_kuenstler_TotalRecords_BeforeShow = true;
//End kuenstler1_kuenstler_TotalRecords_BeforeShow

//Retrieve number of records @8-FA1E8F2F
    global $kuenstler1;
    $kuenstler1->kuenstler_TotalRecords->SetValue($kuenstler1->ds->RecordsCount);
//End Retrieve number of records

//Close kuenstler1_kuenstler_TotalRecords_BeforeShow @7-965E3A3E
    return $kuenstler1_kuenstler_TotalRecords_BeforeShow;
}
//End Close kuenstler1_kuenstler_TotalRecords_BeforeShow


?>
