<?php
//Include Common Files @1-83C617B7
define("RelativePath", ".");
define("PathToCurrentPage", "/");
include(RelativePath . "/Common.php");
include(RelativePath . "/Template.php");
include(RelativePath . "/Sorter.php");
include(RelativePath . "/Navigator.php");
  
//End Include Common Files

class clsGridNewGrid1 { //NewGrid1 class @2-4958862A

//Variables @2-33F76C6B

    // Public variables
    var $ComponentName;
    var $Visible;
    var $Errors;
    var $ErrorBlock;
    var $ds; var $PageSize;
    var $SorterName = "";
    var $SorterDirection = "";
    var $PageNumber;

    var $CCSEvents = "";
    var $CCSEventResult;

    var $RelativePath = "";

    // Grid Controls
    var $StaticControls; var $RowControls;
//End Variables

//Class_Initialize Event @2-5425E2BF
    function clsGridNewGrid1($RelativePath = "")
    {
        global $FileName;
        $this->ComponentName = "NewGrid1";
        $this->Visible = True;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Grid NewGrid1";
        $this->ds = new clsNewGrid1DataSource();
        $this->PageSize = 100;
        if($this->PageSize == 0)
            $this->Errors->addError("<p>Form: Grid " . $this->ComponentName . "<br>Error: (CCS06) Invalid page size.</p>");
        $this->PageNumber = intval(CCGetParam($this->ComponentName . "Page", 1));

        $this->Image1 = new clsControl(ccsImage, "Image1", "Image1", ccsText, "", CCGetRequestParam("Image1", ccsGet));
        $this->Vorname = new clsControl(ccsLabel, "Vorname", "Vorname", ccsText, "", CCGetRequestParam("Vorname", ccsGet));
        $this->Name = new clsControl(ccsLabel, "Name", "Name", ccsText, "", CCGetRequestParam("Name", ccsGet));
        $this->Kategorie = new clsControl(ccsLabel, "Kategorie", "Kategorie", ccsText, "", CCGetRequestParam("Kategorie", ccsGet));
        $this->K2 = new clsControl(ccsLabel, "K2", "K2", ccsText, "", CCGetRequestParam("K2", ccsGet));
        $this->K3 = new clsControl(ccsLabel, "K3", "K3", ccsText, "", CCGetRequestParam("K3", ccsGet));
        $this->K4 = new clsControl(ccsLabel, "K4", "K4", ccsText, "", CCGetRequestParam("K4", ccsGet));
        $this->K5 = new clsControl(ccsLabel, "K5", "K5", ccsText, "", CCGetRequestParam("K5", ccsGet));
        $this->K6 = new clsControl(ccsLabel, "K6", "K6", ccsText, "", CCGetRequestParam("K6", ccsGet));
        $this->K7 = new clsControl(ccsLabel, "K7", "K7", ccsText, "", CCGetRequestParam("K7", ccsGet));
        $this->K8 = new clsControl(ccsLabel, "K8", "K8", ccsText, "", CCGetRequestParam("K8", ccsGet));
        $this->K9 = new clsControl(ccsLabel, "K9", "K9", ccsText, "", CCGetRequestParam("K9", ccsGet));
        $this->Vita = new clsControl(ccsLabel, "Vita", "Vita", ccsText, "", CCGetRequestParam("Vita", ccsGet));
        $this->Vita->HTML = true;
        $this->Austellungstermine = new clsControl(ccsLabel, "Austellungstermine", "Austellungstermine", ccsText, "", CCGetRequestParam("Austellungstermine", ccsGet));
        $this->Austellungstermine->HTML = true;
    }
//End Class_Initialize Event

//Initialize Method @2-03626367
    function Initialize()
    {
        if(!$this->Visible) return;

        $this->ds->PageSize = $this->PageSize;
        $this->ds->AbsolutePage = $this->PageNumber;
        $this->ds->SetOrder($this->SorterName, $this->SorterDirection);
    }
//End Initialize Method

//Show Method @2-2456D362
    function Show()
    {
        global $Tpl;
        if(!$this->Visible) return;

        $ShownRecords = 0;

        $this->ds->Parameters["urlKuenstler_ID"] = CCGetFromGet("Kuenstler_ID", "");

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect");


        $this->ds->Prepare();
        $this->ds->Open();

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow");
        if(!$this->Visible) return;

        $GridBlock = "Grid " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $GridBlock;


        $is_next_record = $this->ds->next_record();
        if($is_next_record && $ShownRecords < $this->PageSize)
        {
            do {
                    $this->ds->SetValues();
                $Tpl->block_path = $ParentPath . "/" . $GridBlock . "/Row";
                $this->Image1->SetValue($this->ds->Image1->GetValue());
                $this->Vorname->SetValue($this->ds->Vorname->GetValue());
                $this->Name->SetValue($this->ds->Name->GetValue());
                $this->Kategorie->SetValue($this->ds->Kategorie->GetValue());
                $this->K2->SetValue($this->ds->K2->GetValue());
                $this->K3->SetValue($this->ds->K3->GetValue());
                $this->K4->SetValue($this->ds->K4->GetValue());
                $this->K5->SetValue($this->ds->K5->GetValue());
                $this->K6->SetValue($this->ds->K6->GetValue());
                $this->K7->SetValue($this->ds->K7->GetValue());
                $this->K8->SetValue($this->ds->K8->GetValue());
                $this->K9->SetValue($this->ds->K9->GetValue());
                $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShowRow");
                $this->Image1->Show();
                $this->Vorname->Show();
                $this->Name->Show();
                $this->Kategorie->Show();
                $this->K2->Show();
                $this->K3->Show();
                $this->K4->Show();
                $this->K5->Show();
                $this->K6->Show();
                $this->K7->Show();
                $this->K8->Show();
                $this->K9->Show();
                $Tpl->block_path = $ParentPath . "/" . $GridBlock;
                $Tpl->parse("Row", true);
                $ShownRecords++;
                $is_next_record = $this->ds->next_record();
            } while ($is_next_record && $ShownRecords < $this->PageSize);
        }
        else // Show NoRecords block if no records are found
        {
            $Tpl->parse("NoRecords", false);
        }

        $errors = $this->GetErrors();
        if(strlen($errors))
        {
            $Tpl->replaceblock("", $errors);
            $Tpl->block_path = $ParentPath;
            return;
        }
        $this->Vita->SetValue($this->ds->Vita->GetValue());
        $this->Austellungstermine->SetValue($this->ds->Austellungstermine->GetValue());
        $this->Vita->Show();
        $this->Austellungstermine->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
        $this->ds->close();
    }
//End Show Method

//GetErrors Method @2-AE0AD515
    function GetErrors()
    {
        $errors = "";
        $errors .= $this->Image1->Errors->ToString();
        $errors .= $this->Vorname->Errors->ToString();
        $errors .= $this->Name->Errors->ToString();
        $errors .= $this->Kategorie->Errors->ToString();
        $errors .= $this->K2->Errors->ToString();
        $errors .= $this->K3->Errors->ToString();
        $errors .= $this->K4->Errors->ToString();
        $errors .= $this->K5->Errors->ToString();
        $errors .= $this->K6->Errors->ToString();
        $errors .= $this->K7->Errors->ToString();
        $errors .= $this->K8->Errors->ToString();
        $errors .= $this->K9->Errors->ToString();
        $errors .= $this->Errors->ToString();
        $errors .= $this->ds->Errors->ToString();
        return $errors;
    }
//End GetErrors Method

} //End NewGrid1 Class @2-FCB6E20C

class clsNewGrid1DataSource extends clsDBConnection1 {  //NewGrid1DataSource Class @2-57ECF20B

//DataSource Variables @2-E2F2AB2E
    var $CCSEvents = "";
    var $CCSEventResult;
    var $ErrorBlock;
    var $CmdExecution;

    var $CountSQL;
    var $wp;


    // Datasource fields
    var $Image1;
    var $Vorname;
    var $Name;
    var $Kategorie;
    var $K2;
    var $K3;
    var $K4;
    var $K5;
    var $K6;
    var $K7;
    var $K8;
    var $K9;
    var $Vita;
    var $Austellungstermine;
//End DataSource Variables

//DataSourceClass_Initialize Event @2-47D9ACBA
    function clsNewGrid1DataSource()
    {
        $this->ErrorBlock = "Grid NewGrid1";
        $this->Initialize();
        $this->Image1 = new clsField("Image1", ccsText, "");
        $this->Vorname = new clsField("Vorname", ccsText, "");
        $this->Name = new clsField("Name", ccsText, "");
        $this->Kategorie = new clsField("Kategorie", ccsText, "");
        $this->K2 = new clsField("K2", ccsText, "");
        $this->K3 = new clsField("K3", ccsText, "");
        $this->K4 = new clsField("K4", ccsText, "");
        $this->K5 = new clsField("K5", ccsText, "");
        $this->K6 = new clsField("K6", ccsText, "");
        $this->K7 = new clsField("K7", ccsText, "");
        $this->K8 = new clsField("K8", ccsText, "");
        $this->K9 = new clsField("K9", ccsText, "");
        $this->Vita = new clsField("Vita", ccsText, "");
        $this->Austellungstermine = new clsField("Austellungstermine", ccsText, "");

    }
//End DataSourceClass_Initialize Event

//SetOrder Method @2-60FF38C1
    function SetOrder($SorterName, $SorterDirection)
    {
        $this->Order = "Name, Vorname";
        $this->Order = CCGetOrder($this->Order, $SorterName, $SorterDirection, 
            "");
    }
//End SetOrder Method

//Prepare Method @2-7F383C9B
    function Prepare()
    {
        $this->wp = new clsSQLParameters($this->ErrorBlock);
        $this->wp->AddParameter("1", "urlKuenstler_ID", ccsInteger, "", "", $this->Parameters["urlKuenstler_ID"], "", false);
        $this->wp->Criterion[1] = $this->wp->Operation(opEqual, "Kuenstler_ID", $this->wp->GetDBValue("1"), $this->ToSQL($this->wp->GetDBValue("1"), ccsInteger),false);
        $this->Where = 
             $this->wp->Criterion[1];
    }
//End Prepare Method

//Open Method @2-67DDF9AA
    function Open()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildSelect");
        $this->CountSQL = "SELECT COUNT(*)  " .
        "FROM kuenstler INNER JOIN kategorie ON " .
        "kuenstler.Kategorie = kategorie.Kategorie";
        $this->SQL = "SELECT *  " .
        "FROM kuenstler INNER JOIN kategorie ON " .
        "kuenstler.Kategorie = kategorie.Kategorie";
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteSelect");
        $this->RecordsCount = CCGetDBValue(CCBuildSQL($this->CountSQL, $this->Where, ""), $this);
        $this->query($this->OptimizeSQL(CCBuildSQL($this->SQL, $this->Where, $this->Order)));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteSelect");
    }
//End Open Method

//SetValues Method @2-EE27B263
    function SetValues()
    {
        $this->Image1->SetDBValue($this->f("Foto"));
        $this->Vorname->SetDBValue($this->f("Vorname"));
        $this->Name->SetDBValue($this->f("Name"));
        $this->Kategorie->SetDBValue($this->f("Kategorie"));
        $this->K2->SetDBValue($this->f("K2"));
        $this->K3->SetDBValue($this->f("K3"));
        $this->K4->SetDBValue($this->f("K4"));
        $this->K5->SetDBValue($this->f("K5"));
        $this->K6->SetDBValue($this->f("K6"));
        $this->K7->SetDBValue($this->f("K7"));
        $this->K8->SetDBValue($this->f("K8"));
        $this->K9->SetDBValue($this->f("K9"));
        $this->Vita->SetDBValue($this->f("Vita"));
        $this->Austellungstermine->SetDBValue($this->f("Austellungstermine"));
    }
//End SetValues Method

} //End NewGrid1DataSource Class @2-FCB6E20C

class clsGridNewGrid2 { //NewGrid2 class @42-6275D5E9

//Variables @42-B738F3BC

    // Public variables
    var $ComponentName;
    var $Visible;
    var $Errors;
    var $ErrorBlock;
    var $ds; var $PageSize;
    var $SorterName = "";
    var $SorterDirection = "";
    var $PageNumber;

    var $CCSEvents = "";
    var $CCSEventResult;

    var $RelativePath = "";

    // Grid Controls
    var $StaticControls; var $RowControls;
    var $Navigator_unten1;
//End Variables

//Class_Initialize Event @42-A8BF4245
    function clsGridNewGrid2($RelativePath = "")
    {
        global $FileName;
        $this->ComponentName = "NewGrid2";
        $this->Visible = True;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Grid NewGrid2";
        $this->ds = new clsNewGrid2DataSource();
        $this->PageSize = 1;
        if($this->PageSize == 0)
            $this->Errors->addError("<p>Form: Grid " . $this->ComponentName . "<br>Error: (CCS06) Invalid page size.</p>");
        $this->PageNumber = intval(CCGetParam($this->ComponentName . "Page", 1));

        $this->Link1 = new clsControl(ccsImageLink, "Link1", "Link1", ccsText, "", CCGetRequestParam("Link1", ccsGet));
        $this->Bild = new clsControl(ccsImage, "Bild", "Bild", ccsText, "", CCGetRequestParam("Bild", ccsGet));
        $this->Navigator_unten1 = new clsNavigator($this->ComponentName, "Navigator_unten1", $FileName, 5, tpCentered);
        $this->Beschreibung = new clsControl(ccsLabel, "Beschreibung", "Beschreibung", ccsText, "", CCGetRequestParam("Beschreibung", ccsGet));
        $this->Beschreibung->HTML = true;
    }
//End Class_Initialize Event

//Initialize Method @42-03626367
    function Initialize()
    {
        if(!$this->Visible) return;

        $this->ds->PageSize = $this->PageSize;
        $this->ds->AbsolutePage = $this->PageNumber;
        $this->ds->SetOrder($this->SorterName, $this->SorterDirection);
    }
//End Initialize Method

//Show Method @42-545B3A1A
    function Show()
    {
        global $Tpl;
        if(!$this->Visible) return;

        $ShownRecords = 0;

        $this->ds->Parameters["urlKuenstler_ID"] = CCGetFromGet("Kuenstler_ID", "");

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect");


        $this->ds->Prepare();
        $this->ds->Open();

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow");
        if(!$this->Visible) return;

        $GridBlock = "Grid " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $GridBlock;


        $is_next_record = $this->ds->next_record();
        if($is_next_record && $ShownRecords < $this->PageSize)
        {
            do {
                    $this->ds->SetValues();
                $Tpl->block_path = $ParentPath . "/" . $GridBlock . "/Row";
                $this->Link1->SetValue($this->ds->Link1->GetValue());
                $this->Link1->SetLink("");
                $this->Link1->Parameters = CCAddParam($this->Link1->Parameters, "Bilder_ID", $this->ds->f("Bilder_ID"));
                $this->Link1->Page = "kuenstler2.php";
                $this->Bild->SetValue($this->ds->Bild->GetValue());
                $this->Navigator_unten1->PageNumber = $this->ds->AbsolutePage;
                $this->Navigator_unten1->TotalPages = $this->ds->PageCount();
                $this->Beschreibung->SetValue($this->ds->Beschreibung->GetValue());
                $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShowRow");
                $this->Link1->Show();
                $this->Bild->Show();
                $this->Navigator_unten1->Show();
                $this->Beschreibung->Show();
                $Tpl->block_path = $ParentPath . "/" . $GridBlock;
                $Tpl->parse("Row", true);
                $ShownRecords++;
                $is_next_record = $this->ds->next_record();
            } while ($is_next_record && $ShownRecords < $this->PageSize);
        }
        else // Show NoRecords block if no records are found
        {
            $Tpl->parse("NoRecords", false);
        }

        $errors = $this->GetErrors();
        if(strlen($errors))
        {
            $Tpl->replaceblock("", $errors);
            $Tpl->block_path = $ParentPath;
            return;
        }
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
        $this->ds->close();
    }
//End Show Method

//GetErrors Method @42-3E5732CA
    function GetErrors()
    {
        $errors = "";
        $errors .= $this->Link1->Errors->ToString();
        $errors .= $this->Bild->Errors->ToString();
        $errors .= $this->Beschreibung->Errors->ToString();
        $errors .= $this->Errors->ToString();
        $errors .= $this->ds->Errors->ToString();
        return $errors;
    }
//End GetErrors Method

} //End NewGrid2 Class @42-FCB6E20C

class clsNewGrid2DataSource extends clsDBConnection1 {  //NewGrid2DataSource Class @42-DC3FCC12

//DataSource Variables @42-BF96480E
    var $CCSEvents = "";
    var $CCSEventResult;
    var $ErrorBlock;
    var $CmdExecution;

    var $CountSQL;
    var $wp;


    // Datasource fields
    var $Link1;
    var $Bild;
    var $Beschreibung;
//End DataSource Variables

//DataSourceClass_Initialize Event @42-DE7DBBA3
    function clsNewGrid2DataSource()
    {
        $this->ErrorBlock = "Grid NewGrid2";
        $this->Initialize();
        $this->Link1 = new clsField("Link1", ccsText, "");
        $this->Bild = new clsField("Bild", ccsText, "");
        $this->Beschreibung = new clsField("Beschreibung", ccsText, "");

    }
//End DataSourceClass_Initialize Event

//SetOrder Method @42-4F4089A1
    function SetOrder($SorterName, $SorterDirection)
    {
        $this->Order = "Reihenfolge";
        $this->Order = CCGetOrder($this->Order, $SorterName, $SorterDirection, 
            "");
    }
//End SetOrder Method

//Prepare Method @42-C41CC798
    function Prepare()
    {
        $this->wp = new clsSQLParameters($this->ErrorBlock);
        $this->wp->AddParameter("1", "urlKuenstler_ID", ccsInteger, "", "", $this->Parameters["urlKuenstler_ID"], "", false);
        $this->wp->Criterion[1] = $this->wp->Operation(opEqual, "kuenstler.Kuenstler_ID", $this->wp->GetDBValue("1"), $this->ToSQL($this->wp->GetDBValue("1"), ccsInteger),false);
        $this->Where = 
             $this->wp->Criterion[1];
    }
//End Prepare Method

//Open Method @42-2EDBB769
    function Open()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildSelect");
        $this->CountSQL = "SELECT COUNT(*)  " .
        "FROM kuenstler INNER JOIN bilder ON " .
        "kuenstler.Name = bilder.Name";
        $this->SQL = "SELECT *  " .
        "FROM kuenstler INNER JOIN bilder ON " .
        "kuenstler.Name = bilder.Name";
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteSelect");
        $this->RecordsCount = CCGetDBValue(CCBuildSQL($this->CountSQL, $this->Where, ""), $this);
        $this->query($this->OptimizeSQL(CCBuildSQL($this->SQL, $this->Where, $this->Order)));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteSelect");
    }
//End Open Method

//SetValues Method @42-95F45B49
    function SetValues()
    {
        $this->Link1->SetDBValue($this->f("Bild_G"));
        $this->Bild->SetDBValue($this->f("Bild"));
        $this->Beschreibung->SetDBValue($this->f("Beschreibung"));
    }
//End SetValues Method

} //End NewGrid2DataSource Class @42-FCB6E20C



class clsGridfooter_kuenstler1 { //footer_kuenstler1 class @17-458B1A2C

//Variables @17-33F76C6B

    // Public variables
    var $ComponentName;
    var $Visible;
    var $Errors;
    var $ErrorBlock;
    var $ds; var $PageSize;
    var $SorterName = "";
    var $SorterDirection = "";
    var $PageNumber;

    var $CCSEvents = "";
    var $CCSEventResult;

    var $RelativePath = "";

    // Grid Controls
    var $StaticControls; var $RowControls;
//End Variables

//Class_Initialize Event @17-884DCA1B
    function clsGridfooter_kuenstler1($RelativePath = "")
    {
        global $FileName;
        $this->ComponentName = "footer_kuenstler1";
        $this->Visible = True;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Grid footer_kuenstler1";
        $this->ds = new clsfooter_kuenstler1DataSource();
        $this->PageSize = 100;
        if($this->PageSize == 0)
            $this->Errors->addError("<p>Form: Grid " . $this->ComponentName . "<br>Error: (CCS06) Invalid page size.</p>");
        $this->PageNumber = intval(CCGetParam($this->ComponentName . "Page", 1));

        $this->Vorname = new clsControl(ccsLabel, "Vorname", "Vorname", ccsText, "", CCGetRequestParam("Vorname", ccsGet));
        $this->Name = new clsControl(ccsLabel, "Name", "Name", ccsText, "", CCGetRequestParam("Name", ccsGet));
        $this->Adresse = new clsControl(ccsLabel, "Adresse", "Adresse", ccsText, "", CCGetRequestParam("Adresse", ccsGet));
        $this->PLZ = new clsControl(ccsLabel, "PLZ", "PLZ", ccsText, "", CCGetRequestParam("PLZ", ccsGet));
        $this->Stadt = new clsControl(ccsLabel, "Stadt", "Stadt", ccsText, "", CCGetRequestParam("Stadt", ccsGet));
        $this->Telefon = new clsControl(ccsLabel, "Telefon", "Telefon", ccsText, "", CCGetRequestParam("Telefon", ccsGet));
        $this->email = new clsControl(ccsLabel, "email", "email", ccsText, "", CCGetRequestParam("email", ccsGet));
        $this->Webseite = new clsControl(ccsLabel, "Webseite", "Webseite", ccsText, "", CCGetRequestParam("Webseite", ccsGet));
    }
//End Class_Initialize Event

//Initialize Method @17-03626367
    function Initialize()
    {
        if(!$this->Visible) return;

        $this->ds->PageSize = $this->PageSize;
        $this->ds->AbsolutePage = $this->PageNumber;
        $this->ds->SetOrder($this->SorterName, $this->SorterDirection);
    }
//End Initialize Method

//Show Method @17-64B736D7
    function Show()
    {
        global $Tpl;
        if(!$this->Visible) return;

        $ShownRecords = 0;

        $this->ds->Parameters["urlKuenstler_ID"] = CCGetFromGet("Kuenstler_ID", "");

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect");


        $this->ds->Prepare();
        $this->ds->Open();

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow");
        if(!$this->Visible) return;

        $GridBlock = "Grid " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $GridBlock;


        $is_next_record = $this->ds->next_record();
        if($is_next_record && $ShownRecords < $this->PageSize)
        {
            do {
                    $this->ds->SetValues();
                $Tpl->block_path = $ParentPath . "/" . $GridBlock . "/Row";
                $this->Vorname->SetValue($this->ds->Vorname->GetValue());
                $this->Name->SetValue($this->ds->Name->GetValue());
                $this->Adresse->SetValue($this->ds->Adresse->GetValue());
                $this->PLZ->SetValue($this->ds->PLZ->GetValue());
                $this->Stadt->SetValue($this->ds->Stadt->GetValue());
                $this->Telefon->SetValue($this->ds->Telefon->GetValue());
                $this->email->SetValue($this->ds->email->GetValue());
                $this->Webseite->SetValue($this->ds->Webseite->GetValue());
                $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShowRow");
                $this->Vorname->Show();
                $this->Name->Show();
                $this->Adresse->Show();
                $this->PLZ->Show();
                $this->Stadt->Show();
                $this->Telefon->Show();
                $this->email->Show();
                $this->Webseite->Show();
                $Tpl->block_path = $ParentPath . "/" . $GridBlock;
                $Tpl->parse("Row", true);
                $ShownRecords++;
                $is_next_record = $this->ds->next_record();
            } while ($is_next_record && $ShownRecords < $this->PageSize);
        }
        else // Show NoRecords block if no records are found
        {
            $Tpl->parse("NoRecords", false);
        }

        $errors = $this->GetErrors();
        if(strlen($errors))
        {
            $Tpl->replaceblock("", $errors);
            $Tpl->block_path = $ParentPath;
            return;
        }
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
        $this->ds->close();
    }
//End Show Method

//GetErrors Method @17-94FF40D8
    function GetErrors()
    {
        $errors = "";
        $errors .= $this->Vorname->Errors->ToString();
        $errors .= $this->Name->Errors->ToString();
        $errors .= $this->Adresse->Errors->ToString();
        $errors .= $this->PLZ->Errors->ToString();
        $errors .= $this->Stadt->Errors->ToString();
        $errors .= $this->Telefon->Errors->ToString();
        $errors .= $this->email->Errors->ToString();
        $errors .= $this->Webseite->Errors->ToString();
        $errors .= $this->Errors->ToString();
        $errors .= $this->ds->Errors->ToString();
        return $errors;
    }
//End GetErrors Method

} //End footer_kuenstler1 Class @17-FCB6E20C

class clsfooter_kuenstler1DataSource extends clsDBConnection1 {  //footer_kuenstler1DataSource Class @17-FFAF71EB

//DataSource Variables @17-70E31356
    var $CCSEvents = "";
    var $CCSEventResult;
    var $ErrorBlock;
    var $CmdExecution;

    var $CountSQL;
    var $wp;


    // Datasource fields
    var $Vorname;
    var $Name;
    var $Adresse;
    var $PLZ;
    var $Stadt;
    var $Telefon;
    var $email;
    var $Webseite;
//End DataSource Variables

//DataSourceClass_Initialize Event @17-F7900566
    function clsfooter_kuenstler1DataSource()
    {
        $this->ErrorBlock = "Grid footer_kuenstler1";
        $this->Initialize();
        $this->Vorname = new clsField("Vorname", ccsText, "");
        $this->Name = new clsField("Name", ccsText, "");
        $this->Adresse = new clsField("Adresse", ccsText, "");
        $this->PLZ = new clsField("PLZ", ccsText, "");
        $this->Stadt = new clsField("Stadt", ccsText, "");
        $this->Telefon = new clsField("Telefon", ccsText, "");
        $this->email = new clsField("email", ccsText, "");
        $this->Webseite = new clsField("Webseite", ccsText, "");

    }
//End DataSourceClass_Initialize Event

//SetOrder Method @17-60FF38C1
    function SetOrder($SorterName, $SorterDirection)
    {
        $this->Order = "Name, Vorname";
        $this->Order = CCGetOrder($this->Order, $SorterName, $SorterDirection, 
            "");
    }
//End SetOrder Method

//Prepare Method @17-7F383C9B
    function Prepare()
    {
        $this->wp = new clsSQLParameters($this->ErrorBlock);
        $this->wp->AddParameter("1", "urlKuenstler_ID", ccsInteger, "", "", $this->Parameters["urlKuenstler_ID"], "", false);
        $this->wp->Criterion[1] = $this->wp->Operation(opEqual, "Kuenstler_ID", $this->wp->GetDBValue("1"), $this->ToSQL($this->wp->GetDBValue("1"), ccsInteger),false);
        $this->Where = 
             $this->wp->Criterion[1];
    }
//End Prepare Method

//Open Method @17-67DDF9AA
    function Open()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildSelect");
        $this->CountSQL = "SELECT COUNT(*)  " .
        "FROM kuenstler INNER JOIN kategorie ON " .
        "kuenstler.Kategorie = kategorie.Kategorie";
        $this->SQL = "SELECT *  " .
        "FROM kuenstler INNER JOIN kategorie ON " .
        "kuenstler.Kategorie = kategorie.Kategorie";
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteSelect");
        $this->RecordsCount = CCGetDBValue(CCBuildSQL($this->CountSQL, $this->Where, ""), $this);
        $this->query($this->OptimizeSQL(CCBuildSQL($this->SQL, $this->Where, $this->Order)));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteSelect");
    }
//End Open Method

//SetValues Method @17-1A914B2E
    function SetValues()
    {
        $this->Vorname->SetDBValue($this->f("Vorname"));
        $this->Name->SetDBValue($this->f("Name"));
        $this->Adresse->SetDBValue($this->f("Adresse"));
        $this->PLZ->SetDBValue($this->f("PLZ"));
        $this->Stadt->SetDBValue($this->f("Stadt"));
        $this->Telefon->SetDBValue($this->f("Telefon"));
        $this->email->SetDBValue($this->f("email"));
        $this->Webseite->SetDBValue($this->f("Webseite"));
    }
//End SetValues Method

} //End footer_kuenstler1DataSource Class @17-FCB6E20C







//Initialize Page @1-D958D051
// Variables
$FileName = "";
$Redirect = "";
$Tpl = "";
$TemplateFileName = "";
$BlockToParse = "main";
$ComponentName = "";

// Events;
$CCSEvents = "";
$CCSEventResult = "";

$FileName = "kuenstler1.php";
$Redirect = "";
$TemplateFileName = "kuenstler1.html";
$BlockToParse = "main";
$TemplateEncoding = "";
$FileEncoding = "";
$PathToRoot = "./";
//End Initialize Page

//Initialize Objects @1-39C3D2D9
$DBConnection1 = new clsDBConnection1();

// Controls
$NewGrid1 = new clsGridNewGrid1();
$NewGrid2 = new clsGridNewGrid2();
$footer_kuenstler1 = new clsGridfooter_kuenstler1();
$NewGrid1->Initialize();
$NewGrid2->Initialize();
$footer_kuenstler1->Initialize();

// Events
include("./kuenstler1_events.php");
BindEvents();

$CCSEventResult = CCGetEvent($CCSEvents, "AfterInitialize");

$Charset = $Charset ? $Charset : $TemplateEncoding;
if ($Charset)
    header("Content-Type: text/html; charset=" . $Charset);
//End Initialize Objects

//Initialize HTML Template @1-E2A5B61F
$CCSEventResult = CCGetEvent($CCSEvents, "OnInitializeView");
$Tpl = new clsTemplate($FileEncoding, $TemplateEncoding);
$Tpl->LoadTemplate(PathToCurrentPage . $TemplateFileName, $BlockToParse, $TemplateEncoding);
$Tpl->block_path = "/$BlockToParse";
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeShow");
//End Initialize HTML Template

//Go to destination page @1-FEDA409D
if($Redirect)
{
    $CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload");
    $DBConnection1->close();
    header("Location: " . $Redirect);
    unset($NewGrid1);
    unset($NewGrid2);
    unset($footer_kuenstler1);
    unset($Tpl);
    exit;
}
//End Go to destination page

//Show Page @1-CC79C0CA
$NewGrid1->Show();
$NewGrid2->Show();
$footer_kuenstler1->Show();
$Tpl->block_path = "";
$Tpl->PParse("main", false);
//End Show Page

//Unload Page @1-43D34D18
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload");
$DBConnection1->close();
unset($NewGrid1);
unset($NewGrid2);
unset($footer_kuenstler1);
unset($Tpl);
//End Unload Page


?>
