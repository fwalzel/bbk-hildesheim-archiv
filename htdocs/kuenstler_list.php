<?php
//Include Common Files @1-83C617B7
define("RelativePath", ".");
define("PathToCurrentPage", "/");
include(RelativePath . "/Common.php");
include(RelativePath . "/Template.php");
include(RelativePath . "/Sorter.php");
include(RelativePath . "/Navigator.php");
  
//End Include Common Files

class clsRecordkuenstlerSearch { //kuenstlerSearch Class @2-3F8C5B1C

//Variables @2-76058651

    // Public variables
    var $ComponentName;
    var $HTMLFormAction;
    var $PressedButton;
    var $Errors;
    var $ErrorBlock;
    var $FormSubmitted;
    var $FormEnctype;
    var $Visible;
    var $Recordset;

    var $CCSEvents = "";
    var $CCSEventResult;

    var $RelativePath = "";

    var $InsertAllowed = false;
    var $UpdateAllowed = false;
    var $DeleteAllowed = false;
    var $ReadAllowed   = false;
    var $EditMode      = false;
    var $ds;
    var $ValidatingControls;
    var $Controls;

    // Class variables
//End Variables

//Class_Initialize Event @2-93A97BFC
    function clsRecordkuenstlerSearch($RelativePath = "")
    {

        global $FileName;
        $this->Visible = true;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Record kuenstlerSearch/Error";
        $this->ReadAllowed = true;
        if($this->Visible)
        {
            $this->ComponentName = "kuenstlerSearch";
            $CCSForm = split(":", CCGetFromGet("ccsForm", ""), 2);
            if(sizeof($CCSForm) == 1)
                $CCSForm[1] = "";
            list($FormName, $FormMethod) = $CCSForm;
            $this->FormEnctype = "application/x-www-form-urlencoded";
            $this->FormSubmitted = ($FormName == $this->ComponentName);
            $Method = $this->FormSubmitted ? ccsPost : ccsGet;
            $this->s_Name = new clsControl(ccsTextBox, "s_Name", "s_Name", ccsText, "", CCGetRequestParam("s_Name", $Method));
            $this->s_PLZ = new clsControl(ccsTextBox, "s_PLZ", "s_PLZ", ccsText, "", CCGetRequestParam("s_PLZ", $Method));
            $this->s_Kategorie = new clsControl(ccsTextBox, "s_Kategorie", "s_Kategorie", ccsText, "", CCGetRequestParam("s_Kategorie", $Method));
            $this->s_K2 = new clsControl(ccsTextBox, "s_K2", "s_K2", ccsText, "", CCGetRequestParam("s_K2", $Method));
            $this->s_K3 = new clsControl(ccsTextBox, "s_K3", "s_K3", ccsText, "", CCGetRequestParam("s_K3", $Method));
            $this->s_K4 = new clsControl(ccsTextBox, "s_K4", "s_K4", ccsText, "", CCGetRequestParam("s_K4", $Method));
            $this->s_K5 = new clsControl(ccsTextBox, "s_K5", "s_K5", ccsText, "", CCGetRequestParam("s_K5", $Method));
            $this->s_K6 = new clsControl(ccsTextBox, "s_K6", "s_K6", ccsText, "", CCGetRequestParam("s_K6", $Method));
            $this->Button_DoSearch = new clsButton("Button_DoSearch");
        }
    }
//End Class_Initialize Event

//Validate Method @2-8B7EC9D9
    function Validate()
    {
        $Validation = true;
        $Where = "";
        $Validation = ($this->s_Name->Validate() && $Validation);
        $Validation = ($this->s_PLZ->Validate() && $Validation);
        $Validation = ($this->s_Kategorie->Validate() && $Validation);
        $Validation = ($this->s_K2->Validate() && $Validation);
        $Validation = ($this->s_K3->Validate() && $Validation);
        $Validation = ($this->s_K4->Validate() && $Validation);
        $Validation = ($this->s_K5->Validate() && $Validation);
        $Validation = ($this->s_K6->Validate() && $Validation);
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "OnValidate");
        $Validation =  $Validation && ($this->s_Name->Errors->Count() == 0);
        $Validation =  $Validation && ($this->s_PLZ->Errors->Count() == 0);
        $Validation =  $Validation && ($this->s_Kategorie->Errors->Count() == 0);
        $Validation =  $Validation && ($this->s_K2->Errors->Count() == 0);
        $Validation =  $Validation && ($this->s_K3->Errors->Count() == 0);
        $Validation =  $Validation && ($this->s_K4->Errors->Count() == 0);
        $Validation =  $Validation && ($this->s_K5->Errors->Count() == 0);
        $Validation =  $Validation && ($this->s_K6->Errors->Count() == 0);
        return (($this->Errors->Count() == 0) && $Validation);
    }
//End Validate Method

//CheckErrors Method @2-66A6DB8D
    function CheckErrors()
    {
        $errors = false;
        $errors = ($errors || $this->s_Name->Errors->Count());
        $errors = ($errors || $this->s_PLZ->Errors->Count());
        $errors = ($errors || $this->s_Kategorie->Errors->Count());
        $errors = ($errors || $this->s_K2->Errors->Count());
        $errors = ($errors || $this->s_K3->Errors->Count());
        $errors = ($errors || $this->s_K4->Errors->Count());
        $errors = ($errors || $this->s_K5->Errors->Count());
        $errors = ($errors || $this->s_K6->Errors->Count());
        $errors = ($errors || $this->Errors->Count());
        return $errors;
    }
//End CheckErrors Method

//Operation Method @2-04BA203B
    function Operation()
    {
        if(!$this->Visible)
            return;

        global $Redirect;
        global $FileName;

        if(!$this->FormSubmitted) {
            return;
        }

        if($this->FormSubmitted) {
            $this->PressedButton = "Button_DoSearch";
            if(strlen(CCGetParam("Button_DoSearch", ""))) {
                $this->PressedButton = "Button_DoSearch";
            }
        }
        $Redirect = "kuenstler_list.php";
        if($this->Validate()) {
            if($this->PressedButton == "Button_DoSearch") {
                if(!CCGetEvent($this->Button_DoSearch->CCSEvents, "OnClick")) {
                    $Redirect = "";
                } else {
                    $Redirect = "kuenstler_list.php" . "?" . CCMergeQueryStrings(CCGetQueryString("Form", Array("Button_DoSearch")));
                }
            }
        } else {
            $Redirect = "";
        }
    }
//End Operation Method

//Show Method @2-4B8077E4
    function Show()
    {
        global $Tpl;
        global $FileName;
        $Error = "";

        if(!$this->Visible)
            return;

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect");


        $RecordBlock = "Record " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $RecordBlock;
        $this->EditMode = $this->EditMode && $this->ReadAllowed;
        if (!$this->FormSubmitted) {
        }

        if($this->FormSubmitted || $this->CheckErrors()) {
            $Error .= $this->s_Name->Errors->ToString();
            $Error .= $this->s_PLZ->Errors->ToString();
            $Error .= $this->s_Kategorie->Errors->ToString();
            $Error .= $this->s_K2->Errors->ToString();
            $Error .= $this->s_K3->Errors->ToString();
            $Error .= $this->s_K4->Errors->ToString();
            $Error .= $this->s_K5->Errors->ToString();
            $Error .= $this->s_K6->Errors->ToString();
            $Error .= $this->Errors->ToString();
            $Tpl->SetVar("Error", $Error);
            $Tpl->Parse("Error", false);
        }
        $CCSForm = $this->EditMode ? $this->ComponentName . ":" . "Edit" : $this->ComponentName;
        $this->HTMLFormAction = $FileName . "?" . CCAddParam(CCGetQueryString("QueryString", ""), "ccsForm", $CCSForm);
        $Tpl->SetVar("Action", $this->HTMLFormAction);
        $Tpl->SetVar("HTMLFormName", $this->ComponentName);
        $Tpl->SetVar("HTMLFormEnctype", $this->FormEnctype);

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow");
        if(!$this->Visible) {
            $Tpl->block_path = $ParentPath;
            return;
        }

        $this->s_Name->Show();
        $this->s_PLZ->Show();
        $this->s_Kategorie->Show();
        $this->s_K2->Show();
        $this->s_K3->Show();
        $this->s_K4->Show();
        $this->s_K5->Show();
        $this->s_K6->Show();
        $this->Button_DoSearch->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
    }
//End Show Method

} //End kuenstlerSearch Class @2-FCB6E20C

class clsGridkuenstler { //kuenstler class @12-83C783C9

//Variables @12-D29FE042

    // Public variables
    var $ComponentName;
    var $Visible;
    var $Errors;
    var $ErrorBlock;
    var $ds; var $PageSize;
    var $SorterName = "";
    var $SorterDirection = "";
    var $PageNumber;

    var $CCSEvents = "";
    var $CCSEventResult;

    var $RelativePath = "";

    // Grid Controls
    var $StaticControls; var $RowControls;
    var $Sorter_Name;
    var $Sorter_Vorname;
    var $Sorter_Adresse;
    var $Sorter_PLZ;
    var $Sorter_Stadt;
    var $Sorter_Telefon;
    var $Sorter_E_Mail;
    var $Sorter_Webseite;
    var $Sorter_Foto;
    var $Sorter_Kategorie;
    var $Sorter_K2;
    var $Sorter_K3;
    var $Sorter_K4;
    var $Sorter_K5;
    var $Sorter_K6;
    var $Sorter_Arbeitsbeispiele;
    var $Sorter_Austellungstermine;
    var $Sorter_login;
    var $Sorter_passwort;
    var $Navigator;
//End Variables

//Class_Initialize Event @12-E21873E5
    function clsGridkuenstler($RelativePath = "")
    {
        global $FileName;
        $this->ComponentName = "kuenstler";
        $this->Visible = True;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Grid kuenstler";
        $this->ds = new clskuenstlerDataSource();
        $this->PageSize = CCGetParam($this->ComponentName . "PageSize", "");
        if(!is_numeric($this->PageSize) || !strlen($this->PageSize))
            $this->PageSize = 20;
        else
            $this->PageSize = intval($this->PageSize);
        if ($this->PageSize > 100)
            $this->PageSize = 100;
        if($this->PageSize == 0)
            $this->Errors->addError("<p>Form: Grid " . $this->ComponentName . "<br>Error: (CCS06) Invalid page size.</p>");
        $this->PageNumber = intval(CCGetParam($this->ComponentName . "Page", 1));
        $this->SorterName = CCGetParam("kuenstlerOrder", "");
        $this->SorterDirection = CCGetParam("kuenstlerDir", "");

        $this->Name = new clsControl(ccsLink, "Name", "Name", ccsText, "", CCGetRequestParam("Name", ccsGet));
        $this->Vorname = new clsControl(ccsLabel, "Vorname", "Vorname", ccsText, "", CCGetRequestParam("Vorname", ccsGet));
        $this->Adresse = new clsControl(ccsLabel, "Adresse", "Adresse", ccsText, "", CCGetRequestParam("Adresse", ccsGet));
        $this->PLZ = new clsControl(ccsLabel, "PLZ", "PLZ", ccsText, "", CCGetRequestParam("PLZ", ccsGet));
        $this->Stadt = new clsControl(ccsLabel, "Stadt", "Stadt", ccsText, "", CCGetRequestParam("Stadt", ccsGet));
        $this->Telefon = new clsControl(ccsLabel, "Telefon", "Telefon", ccsText, "", CCGetRequestParam("Telefon", ccsGet));
        $this->E_Mail = new clsControl(ccsLabel, "E_Mail", "E_Mail", ccsText, "", CCGetRequestParam("E_Mail", ccsGet));
        $this->Webseite = new clsControl(ccsLabel, "Webseite", "Webseite", ccsText, "", CCGetRequestParam("Webseite", ccsGet));
        $this->Foto = new clsControl(ccsLabel, "Foto", "Foto", ccsText, "", CCGetRequestParam("Foto", ccsGet));
        $this->Kategorie = new clsControl(ccsLabel, "Kategorie", "Kategorie", ccsText, "", CCGetRequestParam("Kategorie", ccsGet));
        $this->K2 = new clsControl(ccsLabel, "K2", "K2", ccsText, "", CCGetRequestParam("K2", ccsGet));
        $this->K3 = new clsControl(ccsLabel, "K3", "K3", ccsText, "", CCGetRequestParam("K3", ccsGet));
        $this->K4 = new clsControl(ccsLabel, "K4", "K4", ccsText, "", CCGetRequestParam("K4", ccsGet));
        $this->K5 = new clsControl(ccsLabel, "K5", "K5", ccsText, "", CCGetRequestParam("K5", ccsGet));
        $this->K6 = new clsControl(ccsLabel, "K6", "K6", ccsText, "", CCGetRequestParam("K6", ccsGet));
        $this->Arbeitsbeispiele = new clsControl(ccsLabel, "Arbeitsbeispiele", "Arbeitsbeispiele", ccsText, "", CCGetRequestParam("Arbeitsbeispiele", ccsGet));
        $this->Austellungstermine = new clsControl(ccsLabel, "Austellungstermine", "Austellungstermine", ccsText, "", CCGetRequestParam("Austellungstermine", ccsGet));
        $this->login = new clsControl(ccsLabel, "login", "login", ccsText, "", CCGetRequestParam("login", ccsGet));
        $this->passwort = new clsControl(ccsLabel, "passwort", "passwort", ccsText, "", CCGetRequestParam("passwort", ccsGet));
        $this->Sorter_Name = new clsSorter($this->ComponentName, "Sorter_Name", $FileName);
        $this->Sorter_Vorname = new clsSorter($this->ComponentName, "Sorter_Vorname", $FileName);
        $this->Sorter_Adresse = new clsSorter($this->ComponentName, "Sorter_Adresse", $FileName);
        $this->Sorter_PLZ = new clsSorter($this->ComponentName, "Sorter_PLZ", $FileName);
        $this->Sorter_Stadt = new clsSorter($this->ComponentName, "Sorter_Stadt", $FileName);
        $this->Sorter_Telefon = new clsSorter($this->ComponentName, "Sorter_Telefon", $FileName);
        $this->Sorter_E_Mail = new clsSorter($this->ComponentName, "Sorter_E_Mail", $FileName);
        $this->Sorter_Webseite = new clsSorter($this->ComponentName, "Sorter_Webseite", $FileName);
        $this->Sorter_Foto = new clsSorter($this->ComponentName, "Sorter_Foto", $FileName);
        $this->Sorter_Kategorie = new clsSorter($this->ComponentName, "Sorter_Kategorie", $FileName);
        $this->Sorter_K2 = new clsSorter($this->ComponentName, "Sorter_K2", $FileName);
        $this->Sorter_K3 = new clsSorter($this->ComponentName, "Sorter_K3", $FileName);
        $this->Sorter_K4 = new clsSorter($this->ComponentName, "Sorter_K4", $FileName);
        $this->Sorter_K5 = new clsSorter($this->ComponentName, "Sorter_K5", $FileName);
        $this->Sorter_K6 = new clsSorter($this->ComponentName, "Sorter_K6", $FileName);
        $this->Sorter_Arbeitsbeispiele = new clsSorter($this->ComponentName, "Sorter_Arbeitsbeispiele", $FileName);
        $this->Sorter_Austellungstermine = new clsSorter($this->ComponentName, "Sorter_Austellungstermine", $FileName);
        $this->Sorter_login = new clsSorter($this->ComponentName, "Sorter_login", $FileName);
        $this->Sorter_passwort = new clsSorter($this->ComponentName, "Sorter_passwort", $FileName);
        $this->kuenstler_Insert = new clsControl(ccsLink, "kuenstler_Insert", "kuenstler_Insert", ccsText, "", CCGetRequestParam("kuenstler_Insert", ccsGet));
        $this->kuenstler_Insert->Parameters = CCGetQueryString("QueryString", Array("Kuenstler_ID", "ccsForm"));
        $this->kuenstler_Insert->Page = "kuenstler_maint.php";
        $this->Navigator = new clsNavigator($this->ComponentName, "Navigator", $FileName, 10, tpSimple);
    }
//End Class_Initialize Event

//Initialize Method @12-03626367
    function Initialize()
    {
        if(!$this->Visible) return;

        $this->ds->PageSize = $this->PageSize;
        $this->ds->AbsolutePage = $this->PageNumber;
        $this->ds->SetOrder($this->SorterName, $this->SorterDirection);
    }
//End Initialize Method

//Show Method @12-A66076C6
    function Show()
    {
        global $Tpl;
        if(!$this->Visible) return;

        $ShownRecords = 0;

        $this->ds->Parameters["urls_Name"] = CCGetFromGet("s_Name", "");
        $this->ds->Parameters["urls_PLZ"] = CCGetFromGet("s_PLZ", "");
        $this->ds->Parameters["urls_Kategorie"] = CCGetFromGet("s_Kategorie", "");
        $this->ds->Parameters["urls_K2"] = CCGetFromGet("s_K2", "");
        $this->ds->Parameters["urls_K3"] = CCGetFromGet("s_K3", "");
        $this->ds->Parameters["urls_K4"] = CCGetFromGet("s_K4", "");
        $this->ds->Parameters["urls_K5"] = CCGetFromGet("s_K5", "");
        $this->ds->Parameters["urls_K6"] = CCGetFromGet("s_K6", "");

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect");


        $this->ds->Prepare();
        $this->ds->Open();

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow");
        if(!$this->Visible) return;

        $GridBlock = "Grid " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $GridBlock;


        $is_next_record = $this->ds->next_record();
        if($is_next_record && $ShownRecords < $this->PageSize)
        {
            do {
                    $this->ds->SetValues();
                $Tpl->block_path = $ParentPath . "/" . $GridBlock . "/Row";
                $this->Name->SetValue($this->ds->Name->GetValue());
                $this->Name->Parameters = CCGetQueryString("QueryString", Array("ccsForm"));
                $this->Name->Parameters = CCAddParam($this->Name->Parameters, "Kuenstler_ID", $this->ds->f("Kuenstler_ID"));
                $this->Name->Page = "kuenstler_maint.php";
                $this->Vorname->SetValue($this->ds->Vorname->GetValue());
                $this->Adresse->SetValue($this->ds->Adresse->GetValue());
                $this->PLZ->SetValue($this->ds->PLZ->GetValue());
                $this->Stadt->SetValue($this->ds->Stadt->GetValue());
                $this->Telefon->SetValue($this->ds->Telefon->GetValue());
                $this->E_Mail->SetValue($this->ds->E_Mail->GetValue());
                $this->Webseite->SetValue($this->ds->Webseite->GetValue());
                $this->Foto->SetValue($this->ds->Foto->GetValue());
                $this->Kategorie->SetValue($this->ds->Kategorie->GetValue());
                $this->K2->SetValue($this->ds->K2->GetValue());
                $this->K3->SetValue($this->ds->K3->GetValue());
                $this->K4->SetValue($this->ds->K4->GetValue());
                $this->K5->SetValue($this->ds->K5->GetValue());
                $this->K6->SetValue($this->ds->K6->GetValue());
                $this->Arbeitsbeispiele->SetValue($this->ds->Arbeitsbeispiele->GetValue());
                $this->Austellungstermine->SetValue($this->ds->Austellungstermine->GetValue());
                $this->login->SetValue($this->ds->login->GetValue());
                $this->passwort->SetValue($this->ds->passwort->GetValue());
                $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShowRow");
                $this->Name->Show();
                $this->Vorname->Show();
                $this->Adresse->Show();
                $this->PLZ->Show();
                $this->Stadt->Show();
                $this->Telefon->Show();
                $this->E_Mail->Show();
                $this->Webseite->Show();
                $this->Foto->Show();
                $this->Kategorie->Show();
                $this->K2->Show();
                $this->K3->Show();
                $this->K4->Show();
                $this->K5->Show();
                $this->K6->Show();
                $this->Arbeitsbeispiele->Show();
                $this->Austellungstermine->Show();
                $this->login->Show();
                $this->passwort->Show();
                $Tpl->block_path = $ParentPath . "/" . $GridBlock;
                $Tpl->parse("Row", true);
                $ShownRecords++;
                $is_next_record = $this->ds->next_record();
            } while ($is_next_record && $ShownRecords < $this->PageSize);
        }
        else // Show NoRecords block if no records are found
        {
            $Tpl->parse("NoRecords", false);
        }

        $errors = $this->GetErrors();
        if(strlen($errors))
        {
            $Tpl->replaceblock("", $errors);
            $Tpl->block_path = $ParentPath;
            return;
        }
        $this->Navigator->PageNumber = $this->ds->AbsolutePage;
        $this->Navigator->TotalPages = $this->ds->PageCount();
        $this->Sorter_Name->Show();
        $this->Sorter_Vorname->Show();
        $this->Sorter_Adresse->Show();
        $this->Sorter_PLZ->Show();
        $this->Sorter_Stadt->Show();
        $this->Sorter_Telefon->Show();
        $this->Sorter_E_Mail->Show();
        $this->Sorter_Webseite->Show();
        $this->Sorter_Foto->Show();
        $this->Sorter_Kategorie->Show();
        $this->Sorter_K2->Show();
        $this->Sorter_K3->Show();
        $this->Sorter_K4->Show();
        $this->Sorter_K5->Show();
        $this->Sorter_K6->Show();
        $this->Sorter_Arbeitsbeispiele->Show();
        $this->Sorter_Austellungstermine->Show();
        $this->Sorter_login->Show();
        $this->Sorter_passwort->Show();
        $this->kuenstler_Insert->Show();
        $this->Navigator->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
        $this->ds->close();
    }
//End Show Method

//GetErrors Method @12-021C90C9
    function GetErrors()
    {
        $errors = "";
        $errors .= $this->Name->Errors->ToString();
        $errors .= $this->Vorname->Errors->ToString();
        $errors .= $this->Adresse->Errors->ToString();
        $errors .= $this->PLZ->Errors->ToString();
        $errors .= $this->Stadt->Errors->ToString();
        $errors .= $this->Telefon->Errors->ToString();
        $errors .= $this->E_Mail->Errors->ToString();
        $errors .= $this->Webseite->Errors->ToString();
        $errors .= $this->Foto->Errors->ToString();
        $errors .= $this->Kategorie->Errors->ToString();
        $errors .= $this->K2->Errors->ToString();
        $errors .= $this->K3->Errors->ToString();
        $errors .= $this->K4->Errors->ToString();
        $errors .= $this->K5->Errors->ToString();
        $errors .= $this->K6->Errors->ToString();
        $errors .= $this->Arbeitsbeispiele->Errors->ToString();
        $errors .= $this->Austellungstermine->Errors->ToString();
        $errors .= $this->login->Errors->ToString();
        $errors .= $this->passwort->Errors->ToString();
        $errors .= $this->Errors->ToString();
        $errors .= $this->ds->Errors->ToString();
        return $errors;
    }
//End GetErrors Method

} //End kuenstler Class @12-FCB6E20C

class clskuenstlerDataSource extends clsDBConnection1 {  //kuenstlerDataSource Class @12-03C1C86E

//DataSource Variables @12-0618B4C7
    var $CCSEvents = "";
    var $CCSEventResult;
    var $ErrorBlock;
    var $CmdExecution;

    var $CountSQL;
    var $wp;


    // Datasource fields
    var $Name;
    var $Vorname;
    var $Adresse;
    var $PLZ;
    var $Stadt;
    var $Telefon;
    var $E_Mail;
    var $Webseite;
    var $Foto;
    var $Kategorie;
    var $K2;
    var $K3;
    var $K4;
    var $K5;
    var $K6;
    var $Arbeitsbeispiele;
    var $Austellungstermine;
    var $login;
    var $passwort;
//End DataSource Variables

//DataSourceClass_Initialize Event @12-0E0EB2F9
    function clskuenstlerDataSource()
    {
        $this->ErrorBlock = "Grid kuenstler";
        $this->Initialize();
        $this->Name = new clsField("Name", ccsText, "");
        $this->Vorname = new clsField("Vorname", ccsText, "");
        $this->Adresse = new clsField("Adresse", ccsText, "");
        $this->PLZ = new clsField("PLZ", ccsText, "");
        $this->Stadt = new clsField("Stadt", ccsText, "");
        $this->Telefon = new clsField("Telefon", ccsText, "");
        $this->E_Mail = new clsField("E_Mail", ccsText, "");
        $this->Webseite = new clsField("Webseite", ccsText, "");
        $this->Foto = new clsField("Foto", ccsText, "");
        $this->Kategorie = new clsField("Kategorie", ccsText, "");
        $this->K2 = new clsField("K2", ccsText, "");
        $this->K3 = new clsField("K3", ccsText, "");
        $this->K4 = new clsField("K4", ccsText, "");
        $this->K5 = new clsField("K5", ccsText, "");
        $this->K6 = new clsField("K6", ccsText, "");
        $this->Arbeitsbeispiele = new clsField("Arbeitsbeispiele", ccsText, "");
        $this->Austellungstermine = new clsField("Austellungstermine", ccsText, "");
        $this->login = new clsField("login", ccsText, "");
        $this->passwort = new clsField("passwort", ccsText, "");

    }
//End DataSourceClass_Initialize Event

//SetOrder Method @12-CFC5545E
    function SetOrder($SorterName, $SorterDirection)
    {
        $this->Order = "Name";
        $this->Order = CCGetOrder($this->Order, $SorterName, $SorterDirection, 
            array("Sorter_Name" => array("Name", ""), 
            "Sorter_Vorname" => array("Vorname", ""), 
            "Sorter_Adresse" => array("Adresse", ""), 
            "Sorter_PLZ" => array("PLZ", ""), 
            "Sorter_Stadt" => array("Stadt", ""), 
            "Sorter_Telefon" => array("Telefon", ""), 
            "Sorter_E_Mail" => array("E-Mail", ""), 
            "Sorter_Webseite" => array("Webseite", ""), 
            "Sorter_Foto" => array("Foto", ""), 
            "Sorter_Kategorie" => array("Kategorie", ""), 
            "Sorter_K2" => array("K2", ""), 
            "Sorter_K3" => array("K3", ""), 
            "Sorter_K4" => array("K4", ""), 
            "Sorter_K5" => array("K5", ""), 
            "Sorter_K6" => array("K6", ""), 
            "Sorter_Arbeitsbeispiele" => array("Arbeitsbeispiele", ""), 
            "Sorter_Austellungstermine" => array("Austellungstermine", ""), 
            "Sorter_login" => array("login", ""), 
            "Sorter_passwort" => array("passwort", "")));
    }
//End SetOrder Method

//Prepare Method @12-95B44CE3
    function Prepare()
    {
        $this->wp = new clsSQLParameters($this->ErrorBlock);
        $this->wp->AddParameter("1", "urls_Name", ccsText, "", "", $this->Parameters["urls_Name"], "", false);
        $this->wp->AddParameter("2", "urls_PLZ", ccsText, "", "", $this->Parameters["urls_PLZ"], "", false);
        $this->wp->AddParameter("3", "urls_Kategorie", ccsText, "", "", $this->Parameters["urls_Kategorie"], "", false);
        $this->wp->AddParameter("4", "urls_K2", ccsText, "", "", $this->Parameters["urls_K2"], "", false);
        $this->wp->AddParameter("5", "urls_K3", ccsText, "", "", $this->Parameters["urls_K3"], "", false);
        $this->wp->AddParameter("6", "urls_K4", ccsText, "", "", $this->Parameters["urls_K4"], "", false);
        $this->wp->AddParameter("7", "urls_K5", ccsText, "", "", $this->Parameters["urls_K5"], "", false);
        $this->wp->AddParameter("8", "urls_K6", ccsText, "", "", $this->Parameters["urls_K6"], "", false);
        $this->wp->Criterion[1] = $this->wp->Operation(opContains, "Name", $this->wp->GetDBValue("1"), $this->ToSQL($this->wp->GetDBValue("1"), ccsText),false);
        $this->wp->Criterion[2] = $this->wp->Operation(opContains, "PLZ", $this->wp->GetDBValue("2"), $this->ToSQL($this->wp->GetDBValue("2"), ccsText),false);
        $this->wp->Criterion[3] = $this->wp->Operation(opContains, "Kategorie", $this->wp->GetDBValue("3"), $this->ToSQL($this->wp->GetDBValue("3"), ccsText),false);
        $this->wp->Criterion[4] = $this->wp->Operation(opContains, "K2", $this->wp->GetDBValue("4"), $this->ToSQL($this->wp->GetDBValue("4"), ccsText),false);
        $this->wp->Criterion[5] = $this->wp->Operation(opContains, "K3", $this->wp->GetDBValue("5"), $this->ToSQL($this->wp->GetDBValue("5"), ccsText),false);
        $this->wp->Criterion[6] = $this->wp->Operation(opContains, "K4", $this->wp->GetDBValue("6"), $this->ToSQL($this->wp->GetDBValue("6"), ccsText),false);
        $this->wp->Criterion[7] = $this->wp->Operation(opContains, "K5", $this->wp->GetDBValue("7"), $this->ToSQL($this->wp->GetDBValue("7"), ccsText),false);
        $this->wp->Criterion[8] = $this->wp->Operation(opContains, "K6", $this->wp->GetDBValue("8"), $this->ToSQL($this->wp->GetDBValue("8"), ccsText),false);
        $this->Where = $this->wp->opAND(
             false, $this->wp->opAND(
             false, $this->wp->opAND(
             false, $this->wp->opAND(
             false, $this->wp->opAND(
             false, $this->wp->opAND(
             false, $this->wp->opAND(
             false, 
             $this->wp->Criterion[1], 
             $this->wp->Criterion[2]), 
             $this->wp->Criterion[3]), 
             $this->wp->Criterion[4]), 
             $this->wp->Criterion[5]), 
             $this->wp->Criterion[6]), 
             $this->wp->Criterion[7]), 
             $this->wp->Criterion[8]);
    }
//End Prepare Method

//Open Method @12-AB809A08
    function Open()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildSelect");
        $this->CountSQL = "SELECT COUNT(*)  " .
        "FROM kuenstler";
        $this->SQL = "SELECT Kuenstler_ID, kuenstler.Name, kuenstler.Vorname, kuenstler.Adresse, kuenstler.PLZ, kuenstler.Stadt, kuenstler.Telefon, kuenstler.`E-Mail`, " .
        "kuenstler.Webseite, kuenstler.Foto, kuenstler.Kategorie, kuenstler.K2, kuenstler.K3, kuenstler.K4, kuenstler.K5, kuenstler.K6, " .
        "kuenstler.Arbeitsbeispiele, kuenstler.Austellungstermine, kuenstler.login, kuenstler.passwort  " .
        "FROM kuenstler";
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteSelect");
        $this->RecordsCount = CCGetDBValue(CCBuildSQL($this->CountSQL, $this->Where, ""), $this);
        $this->query($this->OptimizeSQL(CCBuildSQL($this->SQL, $this->Where, $this->Order)));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteSelect");
    }
//End Open Method

//SetValues Method @12-1F8D635C
    function SetValues()
    {
        $this->Name->SetDBValue($this->f("Name"));
        $this->Vorname->SetDBValue($this->f("Vorname"));
        $this->Adresse->SetDBValue($this->f("Adresse"));
        $this->PLZ->SetDBValue($this->f("PLZ"));
        $this->Stadt->SetDBValue($this->f("Stadt"));
        $this->Telefon->SetDBValue($this->f("Telefon"));
        $this->E_Mail->SetDBValue($this->f("E-Mail"));
        $this->Webseite->SetDBValue($this->f("Webseite"));
        $this->Foto->SetDBValue($this->f("Foto"));
        $this->Kategorie->SetDBValue($this->f("Kategorie"));
        $this->K2->SetDBValue($this->f("K2"));
        $this->K3->SetDBValue($this->f("K3"));
        $this->K4->SetDBValue($this->f("K4"));
        $this->K5->SetDBValue($this->f("K5"));
        $this->K6->SetDBValue($this->f("K6"));
        $this->Arbeitsbeispiele->SetDBValue($this->f("Arbeitsbeispiele"));
        $this->Austellungstermine->SetDBValue($this->f("Austellungstermine"));
        $this->login->SetDBValue($this->f("login"));
        $this->passwort->SetDBValue($this->f("passwort"));
    }
//End SetValues Method

} //End kuenstlerDataSource Class @12-FCB6E20C

class clsGridNewGrid1 { //NewGrid1 class @83-4958862A

//Variables @83-290B0D86

    // Public variables
    var $ComponentName;
    var $Visible;
    var $Errors;
    var $ErrorBlock;
    var $ds; var $PageSize;
    var $SorterName = "";
    var $SorterDirection = "";
    var $PageNumber;

    var $CCSEvents = "";
    var $CCSEventResult;

    var $RelativePath = "";

    // Grid Controls
    var $StaticControls; var $RowControls;
    var $Navigator1;
//End Variables

//Class_Initialize Event @83-58D43E17
    function clsGridNewGrid1($RelativePath = "")
    {
        global $FileName;
        $this->ComponentName = "NewGrid1";
        $this->Visible = True;
        $this->RelativePath = $RelativePath;
        $this->Errors = new clsErrors();
        $this->ErrorBlock = "Grid NewGrid1";
        $this->ds = new clsNewGrid1DataSource();
        $this->PageSize = 1;
        if($this->PageSize == 0)
            $this->Errors->addError("<p>Form: Grid " . $this->ComponentName . "<br>Error: (CCS06) Invalid page size.</p>");
        $this->PageNumber = intval(CCGetParam($this->ComponentName . "Page", 1));

        $this->Vorname = new clsControl(ccsLink, "Vorname", "Vorname", ccsText, "", CCGetRequestParam("Vorname", ccsGet));
        $this->Name = new clsControl(ccsLink, "Name", "Name", ccsText, "", CCGetRequestParam("Name", ccsGet));
        $this->Navigator1 = new clsNavigator($this->ComponentName, "Navigator1", $FileName, 10, tpSimple);
    }
//End Class_Initialize Event

//Initialize Method @83-03626367
    function Initialize()
    {
        if(!$this->Visible) return;

        $this->ds->PageSize = $this->PageSize;
        $this->ds->AbsolutePage = $this->PageNumber;
        $this->ds->SetOrder($this->SorterName, $this->SorterDirection);
    }
//End Initialize Method

//Show Method @83-787D763B
    function Show()
    {
        global $Tpl;
        if(!$this->Visible) return;

        $ShownRecords = 0;


        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeSelect");


        $this->ds->Prepare();
        $this->ds->Open();

        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShow");
        if(!$this->Visible) return;

        $GridBlock = "Grid " . $this->ComponentName;
        $ParentPath = $Tpl->block_path;
        $Tpl->block_path = $ParentPath . "/" . $GridBlock;


        $is_next_record = $this->ds->next_record();
        if($is_next_record && $ShownRecords < $this->PageSize)
        {
            do {
                    $this->ds->SetValues();
                $Tpl->block_path = $ParentPath . "/" . $GridBlock . "/Row";
                $this->Vorname->SetValue($this->ds->Vorname->GetValue());
                $this->Vorname->Parameters = CCGetQueryString("QueryString", Array("ccsForm"));
                $this->Vorname->Parameters = CCAddParam($this->Vorname->Parameters, "Kuenstler_ID", $this->ds->f("Kuenstler_ID"));
                $this->Vorname->Page = "kuenstler1.php";
                $this->Name->SetValue($this->ds->Name->GetValue());
                $this->Name->Parameters = CCGetQueryString("QueryString", Array("ccsForm"));
                $this->Name->Parameters = CCAddParam($this->Name->Parameters, "Kuenstler_ID", $this->ds->f("Kuenstler_ID"));
                $this->Name->Page = "kuenstler1.php";
                $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeShowRow");
                $this->Vorname->Show();
                $this->Name->Show();
                $Tpl->block_path = $ParentPath . "/" . $GridBlock;
                $Tpl->parse("Row", true);
                $ShownRecords++;
                $is_next_record = $this->ds->next_record();
            } while ($is_next_record && $ShownRecords < $this->PageSize);
        }
        else // Show NoRecords block if no records are found
        {
            $Tpl->parse("NoRecords", false);
        }

        $errors = $this->GetErrors();
        if(strlen($errors))
        {
            $Tpl->replaceblock("", $errors);
            $Tpl->block_path = $ParentPath;
            return;
        }
        $this->Navigator1->PageNumber = $this->ds->AbsolutePage;
        $this->Navigator1->TotalPages = $this->ds->PageCount();
        $this->Navigator1->Show();
        $Tpl->parse();
        $Tpl->block_path = $ParentPath;
        $this->ds->close();
    }
//End Show Method

//GetErrors Method @83-99A4F248
    function GetErrors()
    {
        $errors = "";
        $errors .= $this->Vorname->Errors->ToString();
        $errors .= $this->Name->Errors->ToString();
        $errors .= $this->Errors->ToString();
        $errors .= $this->ds->Errors->ToString();
        return $errors;
    }
//End GetErrors Method

} //End NewGrid1 Class @83-FCB6E20C

class clsNewGrid1DataSource extends clsDBConnection1 {  //NewGrid1DataSource Class @83-57ECF20B

//DataSource Variables @83-1A374049
    var $CCSEvents = "";
    var $CCSEventResult;
    var $ErrorBlock;
    var $CmdExecution;

    var $CountSQL;
    var $wp;


    // Datasource fields
    var $Vorname;
    var $Name;
//End DataSource Variables

//DataSourceClass_Initialize Event @83-F685FAF9
    function clsNewGrid1DataSource()
    {
        $this->ErrorBlock = "Grid NewGrid1";
        $this->Initialize();
        $this->Vorname = new clsField("Vorname", ccsText, "");
        $this->Name = new clsField("Name", ccsText, "");

    }
//End DataSourceClass_Initialize Event

//SetOrder Method @83-60FF38C1
    function SetOrder($SorterName, $SorterDirection)
    {
        $this->Order = "Name, Vorname";
        $this->Order = CCGetOrder($this->Order, $SorterName, $SorterDirection, 
            "");
    }
//End SetOrder Method

//Prepare Method @83-DFF3DD87
    function Prepare()
    {
    }
//End Prepare Method

//Open Method @83-98ED8B0D
    function Open()
    {
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeBuildSelect");
        $this->CountSQL = "SELECT COUNT(*)  " .
        "FROM kuenstler";
        $this->SQL = "SELECT *  " .
        "FROM kuenstler";
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "BeforeExecuteSelect");
        $this->RecordsCount = CCGetDBValue(CCBuildSQL($this->CountSQL, $this->Where, ""), $this);
        $this->query($this->OptimizeSQL(CCBuildSQL($this->SQL, $this->Where, $this->Order)));
        $this->CCSEventResult = CCGetEvent($this->CCSEvents, "AfterExecuteSelect");
    }
//End Open Method

//SetValues Method @83-1562584A
    function SetValues()
    {
        $this->Vorname->SetDBValue($this->f("Vorname"));
        $this->Name->SetDBValue($this->f("Name"));
    }
//End SetValues Method

} //End NewGrid1DataSource Class @83-FCB6E20C

//Initialize Page @1-39268BBF
// Variables
$FileName = "";
$Redirect = "";
$Tpl = "";
$TemplateFileName = "";
$BlockToParse = "main";
$ComponentName = "";

// Events;
$CCSEvents = "";
$CCSEventResult = "";

$FileName = "kuenstler_list.php";
$Redirect = "";
$TemplateFileName = "kuenstler_list.html";
$BlockToParse = "main";
$TemplateEncoding = "";
$FileEncoding = "";
$PathToRoot = "./";
//End Initialize Page

//Initialize Objects @1-B58DCF6D
$DBConnection1 = new clsDBConnection1();

// Controls
$kuenstlerSearch = new clsRecordkuenstlerSearch();
$kuenstler = new clsGridkuenstler();
$NewGrid1 = new clsGridNewGrid1();
$kuenstler->Initialize();
$NewGrid1->Initialize();

$CCSEventResult = CCGetEvent($CCSEvents, "AfterInitialize");

$Charset = $Charset ? $Charset : $TemplateEncoding;
if ($Charset)
    header("Content-Type: text/html; charset=" . $Charset);
//End Initialize Objects

//Initialize HTML Template @1-E2A5B61F
$CCSEventResult = CCGetEvent($CCSEvents, "OnInitializeView");
$Tpl = new clsTemplate($FileEncoding, $TemplateEncoding);
$Tpl->LoadTemplate(PathToCurrentPage . $TemplateFileName, $BlockToParse, $TemplateEncoding);
$Tpl->block_path = "/$BlockToParse";
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeShow");
//End Initialize HTML Template

//Execute Components @1-56B9C6BC
$kuenstlerSearch->Operation();
//End Execute Components

//Go to destination page @1-450F9825
if($Redirect)
{
    $CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload");
    $DBConnection1->close();
    header("Location: " . $Redirect);
    unset($kuenstlerSearch);
    unset($kuenstler);
    unset($NewGrid1);
    unset($Tpl);
    exit;
}
//End Go to destination page

//Show Page @1-FDCEA182
$kuenstlerSearch->Show();
$kuenstler->Show();
$NewGrid1->Show();
$Tpl->block_path = "";
$Tpl->PParse("main", false);
//End Show Page

//Unload Page @1-8401CA0C
$CCSEventResult = CCGetEvent($CCSEvents, "BeforeUnload");
$DBConnection1->close();
unset($kuenstlerSearch);
unset($kuenstler);
unset($NewGrid1);
unset($Tpl);
//End Unload Page


?>
